package com.hancy.house.web.controller;

import com.hancy.house.bean.BasePrivilege;
import com.hancy.house.service.IBasePrivilegeService;
import com.hancy.house.vm.PrivilegeTree;
import com.hancy.house.jacky.Message;
import com.hancy.house.jacky.MessageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 权限相关接口
 * @author TomORrow
 */
@Api(description = "权限相关接口")
@Validated
@RestController
@RequestMapping({"/privilege"})
public class PrivilegeController {
    private IBasePrivilegeService basePrivilegeService;
    @Autowired
    public void setBasePrivilegeService(IBasePrivilegeService basePrivilegeService) {
        this.basePrivilegeService = basePrivilegeService;
    }

    @ApiOperation("查询所有")
    @GetMapping({"findAll"})
    public Message findAll() {
        List<BasePrivilege> list = this.basePrivilegeService.findAll();
        return MessageUtil.success(list);
    }

    @ApiOperation("根据ID删除")
    @DeleteMapping({"deleteById"})
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "编号", paramType = "query", required = true)})
    public Message deleteById(long id) {
        this.basePrivilegeService.deleteById(id);
        return MessageUtil.success("删除成功");
    }


    @ApiOperation("通过parentId查询")
    @GetMapping({"findByParentId"})
    public Message findByParentId(Long id) {
        List<BasePrivilege> list = this.basePrivilegeService.findByParentId(id);
        return MessageUtil.success(list);
    }

    @ApiOperation("保存或更新")
    @PostMapping({"saveOrUpdate"})
    public Message saveOrUpdate(BasePrivilege privilege) {
        this.basePrivilegeService.saveOrUpdate(privilege);
        return MessageUtil.success("更新成功");
    }

    @ApiOperation("查询树")
    @GetMapping({"findPrivilegeTree"})
    public Message findPrivilegeTree() {
        List<PrivilegeTree> list = this.basePrivilegeService.findPrivilegeTree();
        return MessageUtil.success(list);
    }
}
