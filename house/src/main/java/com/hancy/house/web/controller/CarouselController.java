package com.hancy.house.web.controller;

import com.hancy.house.bean.Carousel;
import com.hancy.house.service.ICarouselService;
import com.hancy.house.jacky.Message;
import com.hancy.house.jacky.MessageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 轮播图相关接口
 * @author TomORrow
 */
@Api(description = "轮播图相关接口")
@Validated
@RestController
@RequestMapping({"/carousel"})
public class CarouselController {
    private ICarouselService carouselService;
    @Autowired
    public void setCarouselService(ICarouselService carouselService) {
        this.carouselService = carouselService;
    }

    @ApiOperation("查询所有轮播图")
    @GetMapping({"findAll"})
    public Message findAll() {
        List<Carousel> list = this.carouselService.findAll();
        return MessageUtil.success(list);
    }

    @ApiOperation("查询所有开启轮播图")
    @GetMapping({"query"})
    public Message query(String status) {
        List<Carousel> list = this.carouselService.query(status);
        return MessageUtil.success(list);
    }

    @ApiOperation("通过id删除")
    @GetMapping({"deleteById"})
    public Message deleteById(long id) {
        this.carouselService.deleteById(id);
        return MessageUtil.success("删除成功");
    }

    @ApiOperation("保存或更新")
    @PostMapping({"saveOrUpdate"})
    public Message saveOrUpdate(Carousel carousel) {
        this.carouselService.saveOrUpdate(carousel);
        return MessageUtil.success("更新成功");
    }
}
