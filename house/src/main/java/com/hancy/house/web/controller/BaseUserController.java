package com.hancy.house.web.controller;

import com.hancy.house.bean.BasePrivilege;
import com.hancy.house.bean.BaseUser;
import com.hancy.house.bean.extend.BaseUserExtend;
import com.hancy.house.service.IBasePrivilegeService;
import com.hancy.house.service.IBaseUserService;
import com.hancy.house.vm.UserRoleVM;
import com.hancy.house.jacky.Message;
import com.hancy.house.jacky.MessageUtil;
import com.hancy.house.jacky.PageVM;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 用户相关接口
 * @author TomORrow
 */
@Api(description = "用户相关接口")
@Validated
@RestController
@RequestMapping({"/baseUser"})
public class BaseUserController {
    private IBaseUserService baseUserService;
    @Autowired
    public void setBaseUserService(IBaseUserService baseUserService) {
        this.baseUserService = baseUserService;
    }

    private IBasePrivilegeService basePrivilegeService;
    @Autowired
    public void setBasePrivilegeService(IBasePrivilegeService basePrivilegeService) {
        this.basePrivilegeService = basePrivilegeService;
    }

    @ApiOperation("分页查询用户信息")
    @GetMapping({"pageQuery"})
    @ApiImplicitParams({@ApiImplicitParam(name = "page", value = "当前页", required = true, paramType = "query"),
            @ApiImplicitParam(name = "pageSize", value = "每页大小", required = true, paramType = "query"),
            @ApiImplicitParam(name = "username", value = "用户名", paramType = "query"),
            @ApiImplicitParam(name = "roleId", value = "角色ID", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", paramType = "query")})
    public Message pageQuery(int page, int pageSize, String username, Long roleId, String status) {
        PageVM<BaseUser> pageVM = this.baseUserService.pageQuery(page, pageSize, username, roleId, status);
        return MessageUtil.success(pageVM);
    }


    @ApiOperation("通过ID查询权限信息")
    @GetMapping({"findMenuByUserId"})
    public Message findMenuByUserId(long id) {
        List<BasePrivilege> list = this.basePrivilegeService.findMenuByUserId(id);
        return MessageUtil.success(list);
    }

    @ApiOperation("通过ID查询用户详情")
    @GetMapping({"findUserDetailsById"})
    public Message findUserDetailsById(long id) {
        BaseUserExtend user = this.baseUserService.findById(id);
        return MessageUtil.success(user);
    }

    @ApiOperation(value = "查询所有", notes = "级联用户角色")
    @GetMapping({"cascadeRoleFindAll"})
    public Message cascadeRoleFindAll() {
        List<BaseUserExtend> list = this.baseUserService.cascadeRoleFindAll();
        return MessageUtil.success(list);
    }

    @ApiOperation("保存或更新")
    @PostMapping({"saveOrUpdate"})
    public Message saveOrUpdate(BaseUser baseUser) {
        this.baseUserService.saveOrUpdate(baseUser);
        return MessageUtil.success("更新成功");
    }


    @ApiOperation("保存用户并指定角色")
    @PostMapping({"addUserWidthRole"})
    @ApiImplicitParams({@ApiImplicitParam(name = "username", value = "用户名", required = true, paramType = "query"),
            @ApiImplicitParam(name = "password", value = "密码", required = true, paramType = "query"),
            @ApiImplicitParam(name = "telephone", value = "手机号", required = false, paramType = "query"),
            @ApiImplicitParam(name = "realname", value = "姓名", required = false, paramType = "query"),
            @ApiImplicitParam(name = "gender", value = "性别", required = false, paramType = "query")})
    public Message addUserWidthRole(BaseUser baseUser, long roleId) {
        this.baseUserService.addUserWidthRole(baseUser, roleId);
        return MessageUtil.success("保存成功");
    }

    @ApiOperation("修改用户头像")
    @PostMapping({"alterUserface"})
    public Message alterUserface(long id, String userface) {
        this.baseUserService.alterUserface(id, userface);
        return MessageUtil.success("保存成功");
    }

    @ApiOperation("通过id删除")
    @GetMapping({"deleteById"})
    public Message deleteById(long id) {
        this.baseUserService.deleteById(id);
        return MessageUtil.success("删除成功");
    }

    @ApiOperation("设置权限")
    @PostMapping({"setRoles"})
    public Message setRoles(UserRoleVM userRoleVM) {
        System.out.println(userRoleVM);
        this.baseUserService.setRoles(userRoleVM.getId().longValue(), userRoleVM.getRoles());
        return MessageUtil.success("设置成功");
    }
}
