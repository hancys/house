package com.hancy.house.web.controller;

import com.hancy.house.bean.BaseLog;
import com.hancy.house.service.IBaseLogService;
import com.hancy.house.jacky.Message;
import com.hancy.house.jacky.MessageUtil;
import com.hancy.house.jacky.PageVM;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 日志相关接口
 * @author TomORrow
 */
@Api(description = "日志相关接口")
@Validated
@RestController
@RequestMapping({"/baseLog"})
public class BaseLogController {
    private IBaseLogService baseLogService;
    @Autowired
    public void setBaseLogService(IBaseLogService baseLogService) {
        this.baseLogService = baseLogService;
    }

    @ApiOperation("根据名称、类型分页查询物品类型信息")
    @GetMapping({"pageQuery"})
    @ApiImplicitParams({@ApiImplicitParam(name = "page", value = "当前页", required = true, paramType = "query"), @ApiImplicitParam(name = "pageSize", value = "每页大小", required = true, paramType = "query")})
    public Message pageQuery(int page, int pageSize) {
        PageVM<BaseLog> pageVM = this.baseLogService.pageQuery(page, pageSize);
        return MessageUtil.success(pageVM);
    }


    @ApiOperation(value = "保存日志信息", notes = "模型中具有id属性为更新，否则为保存")
    @PostMapping({"save"})
    public Message save(BaseLog log) {
        this.baseLogService.save(log);
        return MessageUtil.success("更新成功！");
    }
}
