package com.hancy.house.service;

import com.hancy.house.bean.AccountApply;
import com.hancy.house.jacky.PageVM;

/**
 * 账户申请相关接口
 * @author TomORrow
 */
public interface IAccountApplyService {
  /**
   * 撤销账户变动申请
   * @param id id
   */
  void revoke(long id);
  /**
   * 提交账户变动申请
   * @param paramAccountApply 账户申请实体
   */
  void submit(AccountApply paramAccountApply);
  /**
   * 分页查询账户变动申请
   * @param page 当前页
   * @param pageSize 页面大小
   * @param status 状态
   * @param applyType 申请类型
   * @param userId 用户ID
   * @param beginTime 开始时间
   * @param endTime 结束时间
   * @return 分页数据
   */
  PageVM<AccountApply> pageQuery(int page, int pageSize, String status, String applyType, Long userId, Long beginTime, Long endTime);

  /**
   * 检查申请
   * @param id id
   * @param status 状态
   * @param reason 申请变更原因
   */
  void check(long id, String status, String reason);
}
