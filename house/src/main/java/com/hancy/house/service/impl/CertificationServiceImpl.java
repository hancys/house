package com.hancy.house.service.impl;

import com.hancy.house.bean.BaseUser;
import com.hancy.house.bean.CertificationApply;
import com.hancy.house.dao.BaseUserMapper;
import com.hancy.house.dao.CertificationApplyMapper;
import com.hancy.house.dao.extend.CertificationApplyExtendMapper;
import com.hancy.house.service.ICertificationService;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 实名认证
 * @author TomORrow
 */
@Service
public class CertificationServiceImpl implements ICertificationService {
    @Autowired
    private CertificationApplyMapper certificationApplyMapper;
    @Autowired
    private CertificationApplyExtendMapper certificationApplyExtendMapper;
    @Autowired
    private BaseUserMapper baseUserMapper;


    @Override
    public void revoke(long id) {
        CertificationApply apply = this.certificationApplyMapper.selectByPrimaryKey(Long.valueOf(id));
        if (apply.getStatus().equals("未审核")) {
            this.certificationApplyMapper.deleteByPrimaryKey(Long.valueOf(id));
        } else {
            throw new CustomerException("实名认证申请状态不合法，不允许删除");
        }
    }

    @Override
    public void submit(CertificationApply certificationApply) {
        certificationApply.setApplyTime(Long.valueOf((new Date()).getTime()));
        certificationApply.setStatus("未审核");
        this.certificationApplyMapper.insert(certificationApply);
    }

    @Override
    public PageVM<CertificationApply> pageQuery(int page, int pageSize, String status, Long userId, Long beginTime, Long endTime) {
        List<CertificationApply> list = this.certificationApplyExtendMapper.pageQuery(page, pageSize, status, userId, beginTime, endTime);
        long total = this.certificationApplyExtendMapper.count(status, userId, beginTime, endTime);

        return new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void check(long id, String status, String reason) {
        CertificationApply apply = this.certificationApplyMapper.selectByPrimaryKey(Long.valueOf(id));
        apply.setStatus(status);
        apply.setReason(reason);
        this.certificationApplyMapper.updateByPrimaryKey(apply);

        if (status.equals("通过")) {
            BaseUser user = this.baseUserMapper.selectByPrimaryKey(apply.getUserId());
            user.setBankCard(apply.getBankCard());
            user.setBankCardPhoto(apply.getBankCardPhoto());
            user.setIdCard(apply.getIdCard());
            user.setIdcardPhotoNegative(apply.getIdcardPhotoNegative());
            user.setIdcardPhotoPositive(apply.getIdcardPhotoPositive());
            user.setRealname(apply.getRealname());
            user.setCertificationStatus("已实名认证");
            user.setCertificationTime(Long.valueOf((new Date()).getTime()));
            this.baseUserMapper.updateByPrimaryKey(user);
        }
    }
}
