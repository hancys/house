package com.hancy.house.service.impl;

import com.hancy.house.bean.Carousel;
import com.hancy.house.bean.CarouselExample;
import com.hancy.house.dao.CarouselMapper;
import com.hancy.house.service.ICarouselService;
import com.hancy.house.jacky.CustomerException;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * 轮播图
 * @author TomORrow
 */
@Service
public class CarouselServiceImpl implements ICarouselService {
    @Autowired
    private CarouselMapper carouselMapper;

    @Override
    public List<Carousel> findAll() {
        return this.carouselMapper.selectByExample(new CarouselExample());
    }

    @Override
    public void saveOrUpdate(Carousel carousel) throws CustomerException {
        if (carousel.getId() != null) {
            this.carouselMapper.updateByPrimaryKey(carousel);
        } else {

            CarouselExample example = new CarouselExample();
            example.createCriteria().andNameEqualTo(carousel.getName());
            List<Carousel> list = this.carouselMapper.selectByExample(example);
            if (list.size() > 0) {
                throw new CustomerException("该轮播图已经存在");
            }
            carousel.setStatus("正常");
            this.carouselMapper.insert(carousel);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteById(long id) throws CustomerException {
        if (id < 4L) {
            throw new CustomerException("内置轮播图无法删除");
        }
        Carousel carousel = this.carouselMapper.selectByPrimaryKey(Long.valueOf(id));
        if (carousel == null) {
            throw new CustomerException("要删除的轮播图不存在");
        }
        this.carouselMapper.deleteByPrimaryKey(Long.valueOf(id));
    }

    @Override
    public List<Carousel> query(String status) {
        CarouselExample example = new CarouselExample();
        if (status != null) {
            example.createCriteria().andStatusEqualTo(status);
        }
        List<Carousel> list = this.carouselMapper.selectByExample(example);
        return list;
    }
}
