package com.hancy.house.service.impl;

import com.hancy.house.bean.BaseRole;
import com.hancy.house.bean.BaseRoleExample;
import com.hancy.house.bean.BaseRolePrivilege;
import com.hancy.house.bean.BaseRolePrivilegeExample;
import com.hancy.house.bean.extend.BaseRoleExtend;
import com.hancy.house.dao.BaseRoleMapper;
import com.hancy.house.dao.BaseRolePrivilegeMapper;
import com.hancy.house.dao.extend.BaseRoleExtendMapper;
import com.hancy.house.service.IBaseRoleService;
import com.hancy.house.jacky.CustomerException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;


/**
 * 角色
 * @author TomORrow
 */
@Service
public class BaseRoleServiceImpl implements IBaseRoleService {
    @Resource
    private BaseRoleMapper baseRoleMapper;
    @Resource
    private BaseRoleExtendMapper baseRoleExtendMapper;
    @Resource
    private BaseRolePrivilegeMapper baseRolePrivilegeMapper;

    @Override
    public void authorization(long roleId, List<Long> privilegeIds) {
        BaseRolePrivilegeExample example = new BaseRolePrivilegeExample();
        example.createCriteria().andRoleIdEqualTo(Long.valueOf(roleId));
        List<BaseRolePrivilege> list = this.baseRolePrivilegeMapper.selectByExample(example);

        List<Long> old_privilegeIds = new ArrayList<>();
        for (BaseRolePrivilege rp : list) {
            old_privilegeIds.add(rp.getPrivilegeId());
        }
        Iterator<Long> iterator;
        for (iterator = privilegeIds.iterator(); iterator.hasNext(); ) {
            long privilegeId = ((Long) iterator.next()).longValue();
            if (!old_privilegeIds.contains(Long.valueOf(privilegeId))) {
                BaseRolePrivilege rp = new BaseRolePrivilege();
                rp.setRoleId(Long.valueOf(roleId));
                rp.setPrivilegeId(Long.valueOf(privilegeId));
                this.baseRolePrivilegeMapper.insert(rp);
            }
        }


        if (old_privilegeIds != null) {
            for (Long privilegeId : old_privilegeIds) {
                if (privilegeId != null && !privilegeIds.contains(privilegeId)) {

                    example.clear();
                    example.createCriteria()
                            .andRoleIdEqualTo(Long.valueOf(roleId))
                            .andPrivilegeIdEqualTo(privilegeId);
                    this.baseRolePrivilegeMapper.deleteByExample(example);
                }
            }
        }
    }

    @Override
    public List<BaseRole> findAll() {
        return this.baseRoleMapper.selectByExample(new BaseRoleExample());
    }

    @Override
    public List<BaseRoleExtend> cascadePrivilegeFindAll() {
        return this.baseRoleExtendMapper.selectAll();
    }

    @Override
    public void saveOrUpdate(BaseRole baseRole) throws CustomerException {
        if (baseRole.getId() != null) {
            this.baseRoleMapper.updateByPrimaryKey(baseRole);
        } else {
            this.baseRoleMapper.insert(baseRole);
        }
    }

    @Override
    public void deleteById(long id) throws CustomerException {
        BaseRole role = this.baseRoleMapper.selectByPrimaryKey(Long.valueOf(id));
        if (role == null) {
            throw new CustomerException("要删除的角色不存在");
        }
        this.baseRoleMapper.deleteByPrimaryKey(Long.valueOf(id));
    }
}
