package com.hancy.house.service.impl;

import com.hancy.house.bean.Product;
import com.hancy.house.bean.extend.ProductExtend;
import com.hancy.house.dao.ProductMapper;
import com.hancy.house.dao.extend.ProductExtendMapper;
import com.hancy.house.service.IProductService;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 产品
 * @author TomORrow
 */
@Service
public class ProductServiceImpl implements IProductService {
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductExtendMapper productExtendMapper;

    @Override
    public PageVM<ProductExtend> pageQuery(int page, int pageSize, String name, String status, Long productCategoryId) {
        List<ProductExtend> list = this.productExtendMapper.pageQuery(page, pageSize, name, status, productCategoryId);
        long total = this.productExtendMapper.count(name, status, productCategoryId);
        return new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
    }

    @Override
    public void saveOrUpdate(Product product) throws CustomerException {
        if (product.getId() != null) {
            this.productMapper.updateByPrimaryKey(product);
        } else {
            product.setStatus("正常");
            this.productMapper.insert(product);
        }
    }

    @Override
    public void deleteById(long id) throws CustomerException {
        Product product = this.productMapper.selectByPrimaryKey(Long.valueOf(id));
        if (product == null) {
            throw new CustomerException("要删除的产品不存在");
        }
        this.productMapper.deleteByPrimaryKey(Long.valueOf(id));
    }

    @Override
    public void changeStatus(long id, int flag) throws CustomerException {
        Product product = this.productMapper.selectByPrimaryKey(Long.valueOf(id));
        if (product == null) {
            throw new CustomerException("要操作的产品不存在");
        }
        switch (flag) {
            case 1:
                product.setStatus("正常");
                break;

            case 0:
                product.setStatus("下架");
                break;
        }
        this.productMapper.updateByPrimaryKey(product);
    }
}
