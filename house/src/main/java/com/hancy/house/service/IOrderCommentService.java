package com.hancy.house.service;

import com.hancy.house.bean.OrderComment;
import com.hancy.house.jacky.PageVM;

/**
 * 订单评论
 * @author TomORrow
 */
public interface IOrderCommentService {
  /**
   * 分页查询
   * @param page 当前页
   * @param pageSize 页面大小
   * @param content 内容
   * @param status 状态
   * @param orderId 订单ID
   * @return list分页数据
   */
  PageVM<OrderComment> pageQuery(int page, int pageSize, String content, String status, Long orderId);

  /**
   * 提交评论
   * @param comment 评论
   */
  void submit(OrderComment comment);

  /**
   * 根据ID删除
   * @param id 评论ID
   */
  void deleteById(long id);

  /**
   * 检查状态
   * @param id id
   * @param status 状态
   */
  void check(long id, String status);
}


