package com.hancy.house.service;

import com.hancy.house.bean.Address;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;

/**
 * 地址相关
 * @author TomORrow
 */
public interface IAddressService {
  /**
   * 分页查询
   * @param page 当前页
   * @param pageSize 页面大小
   * @param userId 用户ID
   * @return PageVM<Address>
   */
  PageVM<Address> pageQuery(int page, int pageSize, Long userId);

  /**
   * 跟新地址
   * @param address 地址
   * @throws CustomerException 全局异常
   */
  void saveOrUpdate(Address address) throws CustomerException;

  /**
   * 根据ID删除
   * @param id 用户ID
   * @throws CustomerException 全局异常
   */
  void deleteById(long id) throws CustomerException;
}
