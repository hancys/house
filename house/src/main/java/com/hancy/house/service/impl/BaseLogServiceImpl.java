package com.hancy.house.service.impl;

import com.hancy.house.bean.BaseLog;
import com.hancy.house.dao.BaseLogMapper;
import com.hancy.house.dao.extend.BaseLogExtendMapper;
import com.hancy.house.service.IBaseLogService;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;

import java.util.List;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;


/**
 * 日志
 * @author TomORrow
 */
@Service
public class BaseLogServiceImpl implements IBaseLogService {
    @Resource
    private BaseLogMapper baseLogMapper;
    @Resource
    private BaseLogExtendMapper baseLogExtendMapper;

    @Override
    public void save(BaseLog log) throws CustomerException {
        this.baseLogMapper.insert(log);
    }

    @Override
    public PageVM<BaseLog> pageQuery(int page, int pageSize) {
        List<BaseLog> list = this.baseLogExtendMapper.pageQuery(page, pageSize);
        long total = this.baseLogExtendMapper.count();
        PageVM<BaseLog> pagevm = new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
        return pagevm;
    }
}
