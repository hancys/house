package com.hancy.house.service;

import com.hancy.house.bean.ProductCategory;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;

/**
 * 产品分类
 * @author TomORrow
 */
public interface IProductCategoryService {
  /**
   * 分页查询
   * @param page 当前页
   * @param pageSize 页面大小
   * @param name 名字
   * @return PageVM<ProductCategory>
   */
  PageVM<ProductCategory> pageQuery(int page, int pageSize, String name);

  /**
   * 保存更新
   * @param productCategory 对象
   * @throws CustomerException 全局异常
   */
  void saveOrUpdate(ProductCategory productCategory) throws CustomerException;

  /**
   * 根据ID删除
   * @param id ID
   * @throws CustomerException 全局异常
   */
  void deleteById(long id) throws CustomerException;
}


