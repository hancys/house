package com.hancy.house.service.impl;

import com.hancy.house.bean.BasePrivilege;
import com.hancy.house.bean.BasePrivilegeExample;
import com.hancy.house.dao.BasePrivilegeMapper;
import com.hancy.house.dao.extend.BasePrivilegeExtendMapper;
import com.hancy.house.service.IBasePrivilegeService;
import com.hancy.house.vm.PrivilegeTree;
import com.hancy.house.jacky.CustomerException;

import java.util.List;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;


/**
 * 权限
 * @author TomORrow
 */
@Service
public class BasePrivilegeServiceImpl implements IBasePrivilegeService {
    @Resource
    private BasePrivilegeMapper basePrivilegeMapper;
    @Resource
    private BasePrivilegeExtendMapper basePrivilegeExtendMapper;

    @Override
    public List<BasePrivilege> findByUserId(long id) {
        return this.basePrivilegeExtendMapper.selectByUserId(id);
    }

    @Override
    public List<BasePrivilege> findAll() {
        return this.basePrivilegeMapper.selectByExample(new BasePrivilegeExample());
    }

    @Override
    public void saveOrUpdate(BasePrivilege privilege) throws CustomerException {
        if (privilege.getId() != null) {
            this.basePrivilegeMapper.updateByPrimaryKey(privilege);
        } else {
            this.basePrivilegeMapper.insert(privilege);
        }
    }

    @Override
    public List<BasePrivilege> findByParentId(Long parentId) {
        BasePrivilegeExample example = new BasePrivilegeExample();
        if (parentId == null) {
            example.createCriteria().andParentIdIsNull();
        } else {
            example.createCriteria().andParentIdEqualTo(parentId);
        }
        return this.basePrivilegeMapper.selectByExample(example);
    }

    @Override
    public List<PrivilegeTree> findPrivilegeTree() {
        return this.basePrivilegeExtendMapper.selectAll();
    }

    @Override
    public List<BasePrivilege> findMenuByUserId(long id) {
        return this.basePrivilegeExtendMapper.selectMenuByUserId(id);
    }

    @Override
    public void deleteById(long id) throws CustomerException {
        BasePrivilege privilege = this.basePrivilegeMapper.selectByPrimaryKey(Long.valueOf(id));
        if (privilege == null) {
            throw new CustomerException("要删除的权限信息不存在");
        }
        this.basePrivilegeMapper.deleteByPrimaryKey(Long.valueOf(id));
    }
}
