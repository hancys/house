package com.hancy.house.service.impl;

import com.hancy.house.bean.BaseFile;
import com.hancy.house.bean.BaseFileExample;
import com.hancy.house.dao.BaseFileMapper;
import com.hancy.house.dao.extend.BaseFileExtendMapper;
import com.hancy.house.service.IBaseFileService;
import com.hancy.house.vm.Pair;
import com.hancy.house.jacky.PageVM;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 文件
 * @author TomORrow
 */
@Service
public class BaseFileServiceImpl implements IBaseFileService {
    @Autowired
    private BaseFileMapper baseFileMapper;
    @Autowired
    private BaseFileExtendMapper baseFileExtendMapper;

    @Override
    public void save(BaseFile baseFile) throws Exception {
        this.baseFileMapper.insert(baseFile);
    }

    @Override
    public void deleteById(String id) throws Exception {
        this.baseFileMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<BaseFile> findAll() {
        return this.baseFileMapper.selectByExample(new BaseFileExample());
    }

    @Override
    public PageVM<BaseFile> pageQuery(int page, int pageSize, String fileName, Long datasetId, String uploadDate) {
        List<BaseFile> list = this.baseFileExtendMapper.query(page, pageSize, fileName, datasetId, uploadDate);
        long total = this.baseFileExtendMapper.count(fileName, datasetId, uploadDate);
        PageVM<BaseFile> pageVM = new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
        return pageVM;
    }

    @Override
    public List<Pair> perMonthImport() {
        return this.baseFileExtendMapper.perMonthImport();
    }
}
