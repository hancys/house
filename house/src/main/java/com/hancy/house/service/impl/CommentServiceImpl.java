package com.hancy.house.service.impl;

import com.hancy.house.bean.Comment;
import com.hancy.house.bean.extend.CommentExtend;
import com.hancy.house.dao.CommentMapper;
import com.hancy.house.dao.extend.CommentExtendMapper;
import com.hancy.house.service.ICommentService;
import com.hancy.house.jacky.PageVM;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 评论
 * @author TomORrow
 */
@Service
public class CommentServiceImpl implements ICommentService {
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private CommentExtendMapper CommentExtendMapper;

    @Override
    public PageVM<CommentExtend> pageQuery(int page, int pageSize, String keywords) {
        if (keywords != null) {
            keywords = "%" + keywords + "%";
        }
        List<CommentExtend> list = this.CommentExtendMapper.pageQuery(page, pageSize, keywords);
        long total = this.CommentExtendMapper.count(keywords);

        PageVM<CommentExtend> pageVM = new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
        return pageVM;
    }

    @Override
    public void saveOrUpdate(Comment comment) throws Exception {
        if (comment.getId() != null) {
            this.commentMapper.updateByPrimaryKey(comment);
        } else {

            comment.setCommentTime(Long.valueOf((new Date()).getTime()));
            comment.setStatus("未审核");
            this.commentMapper.insert(comment);
        }
    }

    @Override
    public void deleteById(long id) throws Exception {
        this.commentMapper.deleteByPrimaryKey(Long.valueOf(id));
    }

    @Override
    public void batchDelete(long[] ids) throws Exception {
        if (ids != null && ids.length > 0) {
            for (long id : ids) {
                this.commentMapper.deleteByPrimaryKey(Long.valueOf(id));
            }
        } else {
            throw new Exception("参数异常");
        }
    }

    @Override
    public void checkComment(long id, String status) throws Exception {
        Comment comment = this.commentMapper.selectByPrimaryKey(Long.valueOf(id));
        if (comment != null) {
            if (comment.getStatus().equals("未审核")) {
                comment.setStatus(status);
                this.commentMapper.updateByPrimaryKey(comment);
            } else {
                throw new Exception("不能重复审核");
            }
        } else {
            throw new Exception("评论不存在");
        }
    }
}
