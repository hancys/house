package com.hancy.house.service.impl;

import com.hancy.house.bean.OrderComment;
import com.hancy.house.dao.OrderCommentMapper;
import com.hancy.house.dao.extend.OrderCommentExtendMapper;
import com.hancy.house.service.IOrderCommentService;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 订单评论
 * @author TomORrow
 */
@Service
public class OrderCommentServiceImpl
        implements IOrderCommentService {
    @Autowired
    private OrderCommentMapper orderCommentMapper;
    @Autowired
    private OrderCommentExtendMapper orderCommentExtendMapper;

    @Override
    public PageVM<OrderComment> pageQuery(int page, int pageSize, String content, String status, Long orderId) {
        List<OrderComment> list = this.orderCommentExtendMapper.pageQuery(page, pageSize, content, status, orderId);
        long total = this.orderCommentExtendMapper.count(content, status, orderId);
        return new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
    }


    @Override
    public void submit(OrderComment comment) {
        comment.setCommentTime(Long.valueOf((new Date()).getTime()));
        comment.setStatus("未审核");
        this.orderCommentMapper.insert(comment);
    }


    @Override
    public void deleteById(long id) {
        this.orderCommentMapper.deleteByPrimaryKey(Long.valueOf(id));
    }


    @Override
    public void check(long id, String status) {
        OrderComment comment = this.orderCommentMapper.selectByPrimaryKey(Long.valueOf(id));
        if (comment == null) {
            throw new CustomerException("评论信息不存在");
        }
        comment.setStatus(status);
        this.orderCommentMapper.updateByPrimaryKey(comment);
    }
}
