package com.hancy.house.service.impl;

import com.hancy.house.service.AlipayService;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;


/**
 * 付款
 * @author TomORrow
 */
@Service
public class AlipayServiceImpl implements AlipayService {
    @Override
    public String create(String orderId, String returnUrl) {
        return null;
    }

    @Override
    public void notify(HttpServletRequest request) {
    }

    @Override
    public void refund(String orderId) {
    }
}
