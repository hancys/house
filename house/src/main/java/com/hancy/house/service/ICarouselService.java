package com.hancy.house.service;

import com.hancy.house.bean.Carousel;
import com.hancy.house.jacky.CustomerException;
import java.util.List;

/**
 * 轮播图
 * @author TomORrow
 */
public interface ICarouselService {
  /**
   * 查询所有
   * @return List<Carousel>
   */
  List<Carousel> findAll();

  /**
   * 保存更新
   * @param carousel 轮播图对象
   * @throws CustomerException 全局异常
   */
  void saveOrUpdate(Carousel carousel) throws CustomerException;

  /**
   * 批量删除
   * @param id 轮播图ID
   * @throws CustomerException 全局异常
   */
  void deleteById(long id) throws CustomerException;

  /**
   * 根据状态查询
   * @param status 状态
   * @return List<Carousel>
   */
  List<Carousel> query(String status);
}


