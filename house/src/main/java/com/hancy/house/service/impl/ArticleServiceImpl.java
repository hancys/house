package com.hancy.house.service.impl;

import com.hancy.house.bean.Article;
import com.hancy.house.bean.ArticleExample;
import com.hancy.house.bean.extend.ArticleExtend;
import com.hancy.house.dao.ArticleMapper;
import com.hancy.house.dao.extend.ArticleExtendMapper;
import com.hancy.house.service.IArticleService;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;

import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Service;


/**
 * 资讯
 * @author TomORrow
 */
@Service
public class ArticleServiceImpl implements IArticleService {
    @Resource
    private ArticleMapper articleMapper;
    @Resource
    private ArticleExtendMapper articleExtendMapper;

    @Override
    public List<Article> findAll() {
        return this.articleMapper.selectByExample(new ArticleExample());
    }


    @Override
    public List<ArticleExtend> cascadeFindAll() {
        return this.articleExtendMapper.selectAll();
    }


    @Override
    public ArticleExtend readArticle(long id) {
        Article a = this.articleMapper.selectByPrimaryKey(Long.valueOf(id));
        if (a == null) {
            throw new CustomerException("资讯信息不存在");
        }
        if (a.getReadTimes() == null) {
            a.setReadTimes(Long.valueOf(1L));
        } else {
            a.setReadTimes(Long.valueOf(1L + a.getReadTimes().longValue()));
        }
        this.articleMapper.updateByPrimaryKey(a);
        ArticleExtend article = this.articleExtendMapper.selectById(id);
        return article;
    }


    @Override
    public ArticleExtend findById(long id) {
        return this.articleExtendMapper.selectById(id);
    }


    @Override
    public void saveOrUpdate(Article article) throws CustomerException {
        if (article.getId() != null) {
            Article dbArticle = this.articleMapper.selectByPrimaryKey(article.getId());
            if (dbArticle == null) {
                throw new CustomerException("要修改的数据不存在");
            }
            dbArticle.setTitle(article.getTitle());
            dbArticle.setContent(article.getContent());
            dbArticle.setCategoryId(article.getCategoryId());
            dbArticle.setPublishTime(Long.valueOf((new Date()).getTime()));
            this.articleMapper.updateByPrimaryKey(dbArticle);
        } else {
            ArticleExample example = new ArticleExample();
            //example.createCriteria().andTitleEqualTo(article.getTitle());
            List<Article> articles = this.articleMapper.selectByExample(example);
            if (articles.size() > 0) {
                throw new CustomerException("标题不能重复");
            }
            article.setPublishTime(Long.valueOf((new Date()).getTime()));
            article.setStatus("未审核");
            article.setThumpUp(Long.valueOf(0L));
            article.setReadTimes(Long.valueOf(0L));
            this.articleMapper.insert(article);
        }
    }


    @Override
    public PageVM<ArticleExtend> pageQuery(int page, int pageSize, String title, String status, Long authorId, Long categoryId) {
        List<ArticleExtend> list = this.articleExtendMapper.query(page, pageSize, title, status, authorId, categoryId);
        long total = this.articleExtendMapper.count(title, status, authorId, categoryId);
        return new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
    }


    @Override
    public void deleteById(long id) throws CustomerException {
        Article article = this.articleMapper.selectByPrimaryKey(Long.valueOf(id));
        if (article == null) {
            throw new CustomerException("要删除的数据不存在");
        }
        this.articleMapper.deleteByPrimaryKey(Long.valueOf(id));
    }

    @Override
    public PageVM<ArticleExtend> findAllRecommend(int page, int pageSize) {
        List<ArticleExtend> list = this.articleExtendMapper.findAllRecommend(page, pageSize);
        long total = this.articleExtendMapper.countRecommend();
        return new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
    }

    @Override
    public void changeStatus(long id, String status) throws CustomerException {
        ArticleExtend artile = findById(id);
        if (artile == null) {
            throw new CustomerException("该风采不存在");
        }
        artile.setStatus(status);
        this.articleMapper.updateByPrimaryKey((Article) artile);
    }

    @Override
    public void thumbUp(@NotNull long id) {
        ArticleExtend article = findById(id);
        if (article == null) {
            throw new CustomerException("该资讯不存在");
        }
        article.setThumpUp(Long.valueOf(article.getThumpUp().longValue() + 1L));
        this.articleMapper.updateByPrimaryKey((Article) article);
    }
}
