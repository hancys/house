package com.hancy.house.service;

import com.hancy.house.bean.Category;
import com.hancy.house.jacky.CustomerException;
import java.util.List;

/**
 * 资讯分类
 * @author TomORrow
 */
public interface ICategoryService {
  /**
   * 查询所有
   * @return List<Category>
   */
  List<Category> findAll();

  /**
   * 保存更新
   * @param category 资讯
   * @throws CustomerException 全局异常
   */
  void saveOrUpdate(Category category) throws CustomerException;

  /**
   * 删除
   * @param id 资讯ID
   * @throws CustomerException 全局异常
   */
  void deleteById(long id) throws CustomerException;

  /**
   * 批量删除
   * @param ids 资讯ID
   * @throws CustomerException 全局异常
   */
  void batchDelete(long[] ids) throws CustomerException;
}


