package com.hancy.house.service.impl;

import com.hancy.house.bean.AccountApply;
import com.hancy.house.dao.AccountApplyMapper;
import com.hancy.house.dao.extend.AccountApplyExtendMapper;
import com.hancy.house.service.IAccountApplyService;
import com.hancy.house.service.IAccountService;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 账户变更申请
 * @author TomORrow
 */
@Service
public class AccountApplyServiceImpl implements IAccountApplyService {
    @Autowired
    private AccountApplyMapper accountApplyMapper;
    @Autowired
    private AccountApplyExtendMapper accountApplyExtendMapper;
    @Autowired
    private IAccountService accountService;

    @Override
    public void revoke(long id) {
        AccountApply apply = this.accountApplyMapper.selectByPrimaryKey(Long.valueOf(id));
        if (apply.getStatus().equals("未审核")) {
            this.accountApplyMapper.deleteByPrimaryKey(Long.valueOf(id));
        } else {
            throw new CustomerException("实名认证申请状态不合法，不允许删除");
        }
    }


    @Override
    public void submit(AccountApply accountApply) {
        accountApply.setApplyTime(Long.valueOf((new Date()).getTime()));
        accountApply.setStatus("未审核");
        this.accountApplyMapper.insert(accountApply);
    }


    @Override
    public PageVM<AccountApply> pageQuery(int page, int pageSize, String status, String applyType, Long userId, Long beginTime, Long endTime) {
        List<AccountApply> list = this.accountApplyExtendMapper.pageQuery(page, pageSize, status, applyType, userId, beginTime, endTime);
        long total = this.accountApplyExtendMapper.count(status, applyType, userId, beginTime, endTime);
        return new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void check(long id, String status, String reason) {
        AccountApply apply = this.accountApplyMapper.selectByPrimaryKey(Long.valueOf(id));
        apply.setStatus(status);
        apply.setReason(reason);
        this.accountApplyMapper.updateByPrimaryKey(apply);
        if("通过".equals(status)){
            this.accountService.withdraw(apply.getUserId().longValue(), apply.getMoney().doubleValue());
        }
    }
}
