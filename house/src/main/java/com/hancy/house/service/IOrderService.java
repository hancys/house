package com.hancy.house.service;

import com.hancy.house.bean.extend.OrderExtend;
import com.hancy.house.vm.OrderAndOrderLineVM;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;

/**
 * @author TomORrow
 */
public interface IOrderService {
  /**
   * 分页查询
   * @param page 当前页
   * @param pageSize 页面大小
   * @param beginTime 开始时间
   * @param endTime 结束时间
   * @param status 状态
   * @param customerId 客户ID
   * @param employeeId 职工ID
   * @return list分页数据
   */
  PageVM<OrderExtend> pageQuery(int page, int pageSize, Long beginTime, Long endTime, String status, Long customerId, Long employeeId);

  /**
   * 根据ID删除
   * @param id id
   * @throws CustomerException 全局异常
   */
  void deleteById(long id) throws CustomerException;

  /**
   * 提交订单
   * @param orderAndOrderLineVM 订单对象
   * @throws CustomerException 全局异常
   */
  void submitOrder(OrderAndOrderLineVM orderAndOrderLineVM) throws CustomerException;

  /**
   * 发送订单
   * @param employeeId 职工ID
   * @param orderId 订单ID
   * @throws CustomerException 全局异常
   */
  void sendOrder(long employeeId, long orderId) throws CustomerException;

  /**
   * 取消订单
   * @param orderId 订单ID
   * @throws CustomerException 全局异常
   */
  void cancelSendOrder(long orderId) throws CustomerException;

  /**
   * 新建订单
   * @param orderId 订单ID
   * @throws CustomerException 全局异常
   */
  void takeOrder(long orderId) throws CustomerException;

  /**
   * 拒绝订单
   * @param orderId 订单ID
   * @throws CustomerException 全局异常
   */
  void rejectOrder(long orderId) throws CustomerException;

  /**
   * 订单完成
   * @param orderId 订单ID
   * @throws CustomerException 全局异常
   */
  void serviceCompleted(long orderId) throws CustomerException;

  /**
   * 证实订单
   * @param orderId 订单ID
   * @throws CustomerException 全局异常
   */
  void confirmOrder(long orderId) throws CustomerException;

  /**
   * 根据ID查询
   * @param orderId 订单ID
   * @return OrderExtend
   */
  OrderExtend findById(long orderId);
}


