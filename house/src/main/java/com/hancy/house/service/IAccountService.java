package com.hancy.house.service;

import com.hancy.house.bean.AccountCustomer;
import com.hancy.house.bean.AccountEmployee;
import com.hancy.house.bean.AccountSystem;
import com.hancy.house.jacky.PageVM;

/**
 * 账单相关
 * @author TomORrow
 */
public interface IAccountService {
  /**
   * 改变账单
   * @param customerId 用户ID
   * @param money 金额
   */
  void recharge(long customerId, double money);

  /**
   * 消费
   * @param customerId 用户ID
   * @param money 金额
   */
  void consume(long customerId, double money);

  /**
   * 订单检查
   * @param orderId 订单ID
   */
  void checkout(long orderId);

  /**
   * 增加
   * @param employeeId 职工ID
   * @param money 金额
   */
  void withdraw(long employeeId, double money);

  /**
   * 分页查询顾客账单
   * @param page 当前页
   * @param pageSize 页面数量
   * @param status 状态
   * @param type 类型
   * @param beginTime 开始时间
   * @param endTime 结束时间
   * @param userId 用户ID
   * @return 分页数据
   */
  PageVM<AccountCustomer> pageQueryCustomerAccount(int page, int pageSize, String status, String type, Long beginTime, Long endTime, Long userId);

  /**
   * 分页查询员工账单
   * @param page 当前页
   * @param pageSize 页面数量
   * @param status 状态
   * @param type 类型
   * @param beginTime 开始时间
   * @param endTime 结束时间
   * @param userId 用户ID
   * @return PageVM<AccountEmployee>
   */
  PageVM<AccountEmployee> pageQueryEmployeeAccount(int page, int pageSize, String status, String type, Long beginTime, Long endTime, Long userId);

  /**
   * 分页查询系统账单
   * @param page 当前页
   * @param pageSize 页面数量
   * @param status 状态
   * @param type 类型
   * @param beginTime 开始时间
   * @param endTime 结束时间
   * @return 分页数据
   */
  PageVM<AccountSystem> pageQuerySystemAccount(int page, int pageSize, String status, String type, Long beginTime, Long endTime);
}
