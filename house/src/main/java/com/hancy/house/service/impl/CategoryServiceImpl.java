package com.hancy.house.service.impl;

import com.hancy.house.bean.Category;
import com.hancy.house.bean.CategoryExample;
import com.hancy.house.dao.CategoryMapper;
import com.hancy.house.service.ICategoryService;
import com.hancy.house.jacky.CustomerException;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * 资讯分类
 * @author TomORrow
 */
@Service
public class CategoryServiceImpl implements ICategoryService {
    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public List<Category> findAll() {
        return this.categoryMapper.selectByExample(new CategoryExample());
    }

    @Override
    public void saveOrUpdate(Category category) throws CustomerException {
        if (category.getId() != null) {
            this.categoryMapper.updateByPrimaryKey(category);
        } else {

            CategoryExample example = new CategoryExample();
            example.createCriteria().andNameEqualTo(category.getName());
            List<Category> list = this.categoryMapper.selectByExample(example);
            if (list.size() > 0) {
                throw new CustomerException("该栏目已经存在");
            }
            this.categoryMapper.insert(category);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteById(long id) throws CustomerException {
        if (id < 8L) {
            throw new CustomerException("内置栏目无法删除");
        }
        Category category = this.categoryMapper.selectByPrimaryKey(Long.valueOf(id));
        if (category == null) {
            throw new CustomerException("要删除的栏目不存在");
        }

        this.categoryMapper.deleteByPrimaryKey(Long.valueOf(id));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchDelete(long[] ids) throws CustomerException {
        for (long id : ids) {
            deleteById(id);
        }
    }
}
