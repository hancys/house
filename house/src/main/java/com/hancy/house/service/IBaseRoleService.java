package com.hancy.house.service;

import com.hancy.house.bean.BaseRole;
import com.hancy.house.bean.extend.BaseRoleExtend;
import com.hancy.house.jacky.CustomerException;
import java.util.List;

/**
 * 角色
 * @author TomORrow
 */
public interface IBaseRoleService {
  /**
   * 授权
   * @param roleId 角色id
   * @param privilegeIds 权限ID
   */
  void authorization(long roleId, List<Long> privilegeIds);

  /**
   * 查询所有
   * @return List<BaseRole>
   */
  List<BaseRole> findAll();

  /**
   * 查询所有权限
   * @return List<BaseRoleExtend>
   */
  List<BaseRoleExtend> cascadePrivilegeFindAll();

  /**
   * 保存更新
   * @param baseRole 角色
   * @throws CustomerException 全局异常
   */
  void saveOrUpdate(BaseRole baseRole) throws CustomerException;

  /**
   * 删除
   * @param id 权限ID
   * @throws CustomerException  全局异常
   */
  void deleteById(long id) throws CustomerException;
}


