package com.hancy.house.service.impl;

import com.hancy.house.bean.Address;
import com.hancy.house.dao.AddressMapper;
import com.hancy.house.dao.extend.AddressExtendMapper;
import com.hancy.house.service.IAddressService;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 地址
 * @author TomORrow
 */
@Service
public class AddressServiceImpl implements IAddressService {
    @Autowired
    private AddressMapper addressMapper;
    @Autowired
    private AddressExtendMapper addressExtendMapper;

    @Override
    public PageVM<Address> pageQuery(int page, int pageSize, Long userId) {
        List<Address> list = this.addressExtendMapper.pageQuery(page, pageSize, userId);
        long total = this.addressExtendMapper.count(userId);
        return new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
    }


    @Override
    public void saveOrUpdate(Address address) throws CustomerException {
        if (address.getId() != null) {
            this.addressMapper.updateByPrimaryKey(address);
        } else {

            if (address.getIsDefault() == null) {
                address.setIsDefault(Integer.valueOf(0));
            }
            this.addressMapper.insert(address);
        }
    }


    @Override
    public void deleteById(long id) throws CustomerException {
        Address address = this.addressMapper.selectByPrimaryKey(Long.valueOf(id));
        if (address == null) {
            throw new CustomerException("要删除的产品不存在");
        }
        this.addressMapper.deleteByPrimaryKey(Long.valueOf(id));
    }
}
