package com.hancy.house.service;

import com.hancy.house.bean.Article;
import com.hancy.house.bean.extend.ArticleExtend;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;
import java.util.List;
import javax.validation.constraints.NotNull;

/**
 * 咨询相关
 * @author TomORrow
 */
public interface IArticleService {
  /**
   * 查询所有
   * @return List<Article>
   */
  List<Article> findAll();

  /**
   * 连级查询
   * @return List<ArticleExtend>
   */
  List<ArticleExtend> cascadeFindAll();

  /**
   * 根据ID查询
   * @param id id
   * @return ArticleExtend
   */
  ArticleExtend findById(long id);

  /**
   * 保存更新
   * @param article 对象实体
   * @throws CustomerException 全局异常
   */
  void saveOrUpdate(Article article) throws CustomerException;

  /**
   * 分页查询
   * @param page 当前页
   * @param pageSize 页面大小
   * @param title 标题
   * @param status 状态
   * @param authorId 作者ID
   * @param categoryId 资讯ID
   * @return 分页数据
   */
  PageVM<ArticleExtend> pageQuery(int page, int pageSize, String title, String status, Long authorId, Long categoryId);

  /**
   * 根据ID删除
   * @param id id
   * @throws CustomerException 全局异常
   */
  void deleteById(long id) throws CustomerException;

  /**
   * 查询评论
   * @param page 当前页
   * @param pageSize 页面大小
   * @return PageVM<ArticleExtend>
   */
  PageVM<ArticleExtend> findAllRecommend(int page, int pageSize);

  /**
   * 改变状态
   * @param id id
   * @param status 状态
   * @throws CustomerException 全局异常
   */
  void changeStatus(long id, String status) throws CustomerException;

  /**
   * id查询
   * @param id id
   * @return ArticleExtend
   * @throws CustomerException 全局异常
   */
  ArticleExtend readArticle(long id) throws CustomerException;

  /**
   * 检查资讯
   * @param id id
   * @throws CustomerException 全局异常
   */
  void thumbUp(@NotNull long id) throws CustomerException;
}
