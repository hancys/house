package com.hancy.house.service.impl;

import com.hancy.house.bean.ProductCategory;
import com.hancy.house.dao.ProductCategoryMapper;
import com.hancy.house.dao.extend.ProductCategoryExtendMapper;
import com.hancy.house.service.IProductCategoryService;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 产品分类
 * @author TomORrow
 */
@Service
public class ProductCategoryServiceImpl implements IProductCategoryService {
    @Autowired
    private ProductCategoryMapper productCategoryMapper;
    @Autowired
    private ProductCategoryExtendMapper productCategoryExtendMapper;

    @Override
    public PageVM<ProductCategory> pageQuery(int page, int pageSize, String name) {
        List<ProductCategory> list = this.productCategoryExtendMapper.pageQuery(page, pageSize, name);
        long total = this.productCategoryExtendMapper.count(name);
        return new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
    }

    @Override
    public void saveOrUpdate(ProductCategory category) throws CustomerException {
        if (category.getId() != null) {
            this.productCategoryMapper.updateByPrimaryKey(category);
        } else {
            this.productCategoryMapper.insert(category);
        }
    }

    @Override
    public void deleteById(long id) throws CustomerException {
        ProductCategory category = this.productCategoryMapper.selectByPrimaryKey(Long.valueOf(id));
        if (category == null) {
            throw new CustomerException("要删除的产品不存在");
        }
        this.productCategoryMapper.deleteByPrimaryKey(Long.valueOf(id));
    }
}
