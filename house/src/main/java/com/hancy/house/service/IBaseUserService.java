package com.hancy.house.service;

import com.hancy.house.bean.BaseUser;
import com.hancy.house.bean.extend.BaseUserExtend;
import com.hancy.house.vm.UserVM;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;
import java.util.List;

/**
 * 用户相关
 * @author TomORrow
 */
public interface IBaseUserService {
  /**
   * 给用户添加角色
   * @param baseUser 用户
   * @param roleId 角色ID
   * @throws CustomerException 全局异常
   */
  void addUserWidthRole(BaseUser baseUser, long roleId) throws CustomerException;

  /**
   * 分页查询用户
   * @param page 当前页
   * @param pageSize 页面大小
   * @param username 用户名
   * @param roleId 角色ID
   * @param status 状态
   * @return list分页数据
   */
  PageVM<BaseUser> pageQuery(int page, int pageSize, String username, Long roleId, String status);

  /**
   * 批量插入
   * @param list 用户列表
   * @throws CustomerException 全局异常
   */
  void batchInsert(List<BaseUser> list) throws CustomerException;

  /**
   * 根据ID查询
   * @param id id
   * @return  List<BaseUser>
   */
  List<BaseUser> findByClazzId(long id);

  /**
   * 登录
   * @param userVM 用户对象 登录使用
   * @return 用户
   * @throws CustomerException 全局异常
   */
  BaseUser login(UserVM userVM) throws CustomerException;

  /**
   * 根据ID 查询
   * @param id id
   * @return BaseUserExtend
   */
  BaseUserExtend findById(long id);

  /**
   * 查询所有
   * @return List<BaseUser>
   */
  List<BaseUser> findAll();

  /**
   * 根据角色查询所有
   * @return  List<BaseUserExtend>
   */
  List<BaseUserExtend> cascadeRoleFindAll();

  /**
   * 保存更新
   * @param baseUser 用户
   * @throws CustomerException 全局异常
   */
  void saveOrUpdate(BaseUser baseUser) throws CustomerException;

  /**
   * 改变状态
   * @param id id
   * @param status 状态
   * @throws CustomerException 全局异常
   */
  void changeStatus(long id, String status) throws CustomerException;

  /**
   * 删除
   * @param id id
   * @throws CustomerException 全局异常
   */
  void deleteById(long id) throws CustomerException;

  /**
   * 设置角色
   * @param id id
   * @param list 角色id
   */
  void setRoles(long id, List<Long> list);

  /**
   * 更新用户
   * @param id id
   * @param userFace 用户头像
   * @throws CustomerException 全局异常
   */
  void alterUserface(long id, String userFace) throws CustomerException;

  /**
   * 注册
   * @param baseUser 用户
   * @param roleId 角色
   */
  void register(BaseUser baseUser, long roleId);
}


