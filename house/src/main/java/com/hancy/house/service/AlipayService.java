package com.hancy.house.service;

import javax.servlet.http.HttpServletRequest;

/**
 * 支付相关
 * @author TomORrow
 */
public interface AlipayService {
  /**
   * 创建订单
   * @param orderId 订单ID
   * @param returnUrl 返回URL
   * @return String
   */
  String create(String orderId, String returnUrl);

  /**
   * 通知
   * @param request 请求
   */
  void notify(HttpServletRequest request);

  /**
   * 拒绝
   * @param orderId 订单ID
   */
  void refund(String orderId);
}
