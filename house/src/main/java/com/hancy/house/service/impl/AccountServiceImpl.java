package com.hancy.house.service.impl;

import com.hancy.house.bean.AccountCustomer;
import com.hancy.house.bean.AccountEmployee;
import com.hancy.house.bean.AccountSystem;
import com.hancy.house.bean.BaseUser;
import com.hancy.house.bean.Order;
import com.hancy.house.bean.Platform;
import com.hancy.house.dao.AccountCustomerMapper;
import com.hancy.house.dao.AccountEmployeeMapper;
import com.hancy.house.dao.AccountSystemMapper;
import com.hancy.house.dao.BaseUserMapper;
import com.hancy.house.dao.OrderMapper;
import com.hancy.house.dao.PlatformMapper;
import com.hancy.house.dao.extend.AccountCustomerExtendMapper;
import com.hancy.house.dao.extend.AccountEmployeeExtendMapper;
import com.hancy.house.dao.extend.AccountSystemExtendMapper;
import com.hancy.house.service.IAccountService;
import com.hancy.house.utils.BigDecimalUtil;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * 账户
 * @author TomORrow
 */
@Service
public class AccountServiceImpl implements IAccountService {
    @Autowired
    private AccountCustomerMapper accountCustomerMapper;
    @Autowired
    private AccountEmployeeMapper accountEmployeeMapper;
    @Autowired
    private AccountSystemMapper accountSystemMapper;
    @Autowired
    private AccountCustomerExtendMapper accountCustomerExtendMapper;
    @Autowired
    private AccountEmployeeExtendMapper accountEmployeeExtendMapper;
    @Autowired
    private AccountSystemExtendMapper accountSystemExtendMapper;
    @Autowired
    private BaseUserMapper baseUserMapper;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private PlatformMapper platformMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void recharge(long customerId, double money) {
        if (money > 1000.0D) {
            throw new CustomerException("一次最多只能充值1000元！");
        }
        AccountCustomer accountCustomer = new AccountCustomer();
        accountCustomer.setType("充值");
        accountCustomer.setStatus("正常");
        accountCustomer.setTransferTime(Long.valueOf((new Date()).getTime()));
        accountCustomer.setUserId(Long.valueOf(customerId));
        accountCustomer.setTransferMoney(Double.valueOf(money));
        this.accountCustomerMapper.insert(accountCustomer);

        BaseUser user = this.baseUserMapper.selectByPrimaryKey(Long.valueOf(customerId));
        user.setBalance(Double.valueOf(BigDecimalUtil.add(user.getBalance().doubleValue(), money).doubleValue()));
        this.baseUserMapper.updateByPrimaryKey(user);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void consume(long customerId, double money) {
        BaseUser user = this.baseUserMapper.selectByPrimaryKey(Long.valueOf(customerId));
        if (user.getBalance().doubleValue() < money) {
            throw new CustomerException("余额不足!");
        }
        user.setBalance(Double.valueOf(BigDecimalUtil.sub(user.getBalance().doubleValue(), money).doubleValue()));
        this.baseUserMapper.updateByPrimaryKey(user);

        AccountCustomer accountCustomer = new AccountCustomer();
        accountCustomer.setStatus("正常");
        accountCustomer.setType("消费");
        accountCustomer.setTransferTime(Long.valueOf((new Date()).getTime()));
        accountCustomer.setUserId(Long.valueOf(customerId));
        accountCustomer.setTransferMoney(Double.valueOf(money));
        this.accountCustomerMapper.insert(accountCustomer);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void checkout(long orderId) {
        Order order = this.orderMapper.selectByPrimaryKey(Long.valueOf(orderId));
        Long employeeId = order.getEmployeeId();
        if (employeeId == null) {
            throw new CustomerException("订单异常，请联系管理员！订单编号" + orderId);
        }
        BaseUser employee = this.baseUserMapper.selectByPrimaryKey(employeeId);

        AccountEmployee accountEmployee = new AccountEmployee();
        accountEmployee.setStatus("正常");
        accountEmployee.setType("收益");
        accountEmployee.setTransferTime(Long.valueOf((new Date()).getTime()));
        accountEmployee.setUserId(employeeId);
        accountEmployee.setTransferMoney(Double.valueOf(BigDecimalUtil.mul(order.getTotal().doubleValue(), 0.8D).doubleValue()));
        this.accountEmployeeMapper.insert(accountEmployee);

        employee.setBalance(Double.valueOf(BigDecimalUtil.add(employee.getBalance().doubleValue(), accountEmployee.getTransferMoney().doubleValue()).doubleValue()));
        this.baseUserMapper.updateByPrimaryKey(employee);

        AccountSystem accountSystem = new AccountSystem();
        accountSystem.setStatus("正常");
        accountSystem.setType("收益");
        accountSystem.setTransferTime(Long.valueOf((new Date()).getTime()));
        accountSystem.setTransferMoney(Double.valueOf(BigDecimalUtil.sub(order.getTotal().doubleValue(), accountEmployee.getTransferMoney().doubleValue()).doubleValue()));
        this.accountSystemMapper.insert(accountSystem);

        Platform platform = this.platformMapper.selectByPrimaryKey(Long.valueOf(1L));
        platform.setBalance(Double.valueOf(BigDecimalUtil.sub(platform.getBalance().doubleValue(), accountSystem.getTransferMoney().doubleValue()).doubleValue()));
        this.platformMapper.updateByPrimaryKey(platform);
    }


    @Override
    public void withdraw(long employeeId, double money) {
        BaseUser employee = this.baseUserMapper.selectByPrimaryKey(Long.valueOf(employeeId));
        if (employee.getBalance().doubleValue() < money) {
            throw new CustomerException("余额不足!");
        }

        AccountEmployee accountEmployee = new AccountEmployee();
        accountEmployee.setStatus("正常");
        accountEmployee.setType("提现");
        accountEmployee.setTransferTime(Long.valueOf((new Date()).getTime()));
        accountEmployee.setUserId(Long.valueOf(employeeId));
        accountEmployee.setTransferMoney(Double.valueOf(money));
        this.accountEmployeeMapper.insert(accountEmployee);

        employee.setBalance(Double.valueOf(BigDecimalUtil.sub(employee.getBalance().doubleValue(), accountEmployee.getTransferMoney().doubleValue()).doubleValue()));
        this.baseUserMapper.updateByPrimaryKey(employee);
    }


    @Override
    public PageVM<AccountCustomer> pageQueryCustomerAccount(int page, int pageSize, String status, String type, Long beginTime, Long endTime, Long userId) {
        List<AccountCustomer> list = this.accountCustomerExtendMapper.pageQuery(page, pageSize, status, type, beginTime, endTime, userId);
        long total = this.accountCustomerExtendMapper.count(status, type, beginTime, endTime, userId);
        return new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
    }


    @Override
    public PageVM<AccountEmployee> pageQueryEmployeeAccount(int page, int pageSize, String status, String type, Long beginTime, Long endTime, Long userId) {
        List<AccountEmployee> list = this.accountEmployeeExtendMapper.pageQuery(page, pageSize, status, type, beginTime, endTime, userId);
        long total = this.accountEmployeeExtendMapper.count(status, type, beginTime, endTime, userId);
        return new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
    }


    @Override
    public PageVM<AccountSystem> pageQuerySystemAccount(int page, int pageSize, String status, String type, Long beginTime, Long endTime) {
        List<AccountSystem> list = this.accountSystemExtendMapper.pageQuery(page, pageSize, status, type, beginTime, endTime);
        long total = this.accountSystemExtendMapper.count(status, type, beginTime, endTime);
        return new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
    }
}
