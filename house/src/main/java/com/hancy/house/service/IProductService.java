package com.hancy.house.service;

import com.hancy.house.bean.Product;
import com.hancy.house.bean.extend.ProductExtend;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;

/**
 * 产品
 * @author TomORrow
 */
public interface IProductService {
  /**
   * 分页查询
   * @param page 当前页
   * @param pageSize 页面大小
   * @param name 名字
   * @param status 状态
   * @param productCategoryId 产品分类ID
   * @return 分页数据
   */
  PageVM<ProductExtend> pageQuery(int page, int pageSize, String name, String status, Long productCategoryId);

  /**
   * 保存跟新
   * @param product 产品
   * @throws CustomerException 全局异常
   */
  void saveOrUpdate(Product product) throws CustomerException;

  /**
   * 根据ID删除
   * @param id id
   * @throws CustomerException 全局异常
   */
  void deleteById(long id) throws CustomerException;

  /**
   * 改变状态
   * @param id id
   * @param flag 状态标记
   * @throws CustomerException 全局异常
   */
  void changeStatus(long id, int flag) throws CustomerException;
}


