package com.hancy.house.service;

import com.hancy.house.bean.BaseConfig;
import java.util.List;

/**
 * 基础配置
 * @author TomORrow
 */
public interface IBaseConfigService {
  /**
   * 保存更新
   * @param baseConfig 基础配置
   */
  void saveOrUpdate(BaseConfig baseConfig);

  /**
   * 删除
   * @param id id
   */
  void deleteById(long id);

  /**
   * 查询所有
   * @return List<BaseConfig>
   */
  List<BaseConfig> findAll();

  /**
   * 根据ID查询
   * @param id id
   * @return BaseConfig
   */
  BaseConfig findByKey(String id);
}


