package com.hancy.house.service.impl;

import com.hancy.house.bean.BaseConfig;
import com.hancy.house.bean.BaseConfigExample;
import com.hancy.house.dao.BaseConfigMapper;
import com.hancy.house.service.IBaseConfigService;
import com.hancy.house.jacky.CustomerException;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 基本配置
 * @author TomORrow
 */
@Service
public class BaseConfigServiceImpl implements IBaseConfigService {
    @Autowired
    private BaseConfigMapper baseConfigMapper;

    @Override
    public void saveOrUpdate(BaseConfig config) {
        if (config.getId() == null) {
            BaseConfig db_config = findByKey(config.getName());
            if (db_config != null) {
                throw new CustomerException("key值重复");
            }
            this.baseConfigMapper.insert(config);
        } else {
            this.baseConfigMapper.updateByPrimaryKey(config);
        }
    }

    @Override
    public void deleteById(long id) {
        this.baseConfigMapper.deleteByPrimaryKey(Long.valueOf(id));
    }

    @Override
    public List<BaseConfig> findAll() {
        List<BaseConfig> list = this.baseConfigMapper.selectByExample(new BaseConfigExample());
        return list;
    }

    @Override
    public BaseConfig findByKey(String name) {
        BaseConfigExample example = new BaseConfigExample();
        //example.createCriteria().andNameEqualTo(name);
        List<BaseConfig> list = this.baseConfigMapper.selectByExample(example);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }
}
