package com.hancy.house.service.impl;

import com.hancy.house.bean.BaseUser;
import com.hancy.house.bean.Order;
import com.hancy.house.bean.OrderLine;
import com.hancy.house.bean.extend.OrderExtend;
import com.hancy.house.dao.BaseUserMapper;
import com.hancy.house.dao.OrderLineMapper;
import com.hancy.house.dao.OrderMapper;
import com.hancy.house.dao.extend.OrderExtendMapper;
import com.hancy.house.service.IAccountService;
import com.hancy.house.service.IOrderService;
import com.hancy.house.vm.OrderAndOrderLineVM;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * 订单
 * @author TomORrow
 */
@Service
public class OrderServiceImpl implements IOrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderExtendMapper orderExtendMapper;
    @Autowired
    private OrderLineMapper orderLineMapper;
    @Autowired
    private BaseUserMapper baseUserMapper;
    @Autowired
    private IAccountService accountService;

    @Override
    public PageVM<OrderExtend> pageQuery(int page, int pageSize, Long beginTime, Long endTime, String status, Long customerId, Long employeeId) {
        List<OrderExtend> list = this.orderExtendMapper.pageQuery(page, pageSize, beginTime, endTime, status, customerId, employeeId);

        long total = this.orderExtendMapper.count(beginTime, endTime, status, customerId, employeeId);
        return new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
    }

    @Override
    public void deleteById(long id) throws CustomerException {
        Order order = this.orderMapper.selectByPrimaryKey(Long.valueOf(id));
        if (order == null) {
            throw new CustomerException("要删除的产品不存在");
        }
        this.orderMapper.deleteByPrimaryKey(Long.valueOf(id));
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void submitOrder(OrderAndOrderLineVM order) throws CustomerException {
        Order o = new Order();

        o.setStatus("待派单");
        o.setOrderTime(Long.valueOf((new Date()).getTime()));
        o.setCustomerId(order.getCustomerId());
        o.setAddressId(order.getAddressId());
        o.setServiceTime(order.getServiceTime());

        List<OrderLine> list = order.getOrderLines();
        double total = 0.0D;
        for (OrderLine ol : list) {
            total += ol.getPrice().doubleValue() * ol.getNumber().intValue();
        }
        o.setTotal(Double.valueOf(total));

        this.accountService.consume(order.getCustomerId().longValue(), o.getTotal().doubleValue());
        this.orderMapper.insert(o);
        for (OrderLine ol : list) {

            ol.setOrderId(o.getId());
            this.orderLineMapper.insert(ol);
        }
    }

    @Override
    public void sendOrder(long employeeId, long orderId) throws CustomerException {
        Order order = this.orderMapper.selectByPrimaryKey(Long.valueOf(orderId));
        BaseUser user = this.baseUserMapper.selectByPrimaryKey(Long.valueOf(employeeId));
        if (order == null) {
            throw new CustomerException("该订单不存在");
        }
        if (user == null) {
            throw new CustomerException("该员工不存在");
        }

        order.setStatus("待接单");
        order.setEmployeeId(Long.valueOf(employeeId));
        this.orderMapper.updateByPrimaryKey(order);
    }

    @Override
    public void cancelSendOrder(long orderId) throws CustomerException {
        Order order = this.orderMapper.selectByPrimaryKey(Long.valueOf(orderId));
        if (order == null) {
            throw new CustomerException("该订单不存在");
        }
        order.setStatus("待派单");

        order.setEmployeeId(null);
        this.orderMapper.updateByPrimaryKey(order);
    }

    @Override
    public void takeOrder(long orderId) throws CustomerException {
        changeOrderStatus(orderId, "待服务");
    }

    @Override
    public void rejectOrder(long orderId) throws CustomerException {
        changeOrderStatus(orderId, "待派单");
    }

    @Override
    public void serviceCompleted(long orderId) throws CustomerException {
        changeOrderStatus(orderId, "待确认");
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void confirmOrder(long orderId) throws CustomerException {
        this.accountService.checkout(orderId);

        changeOrderStatus(orderId, "已完成");
    }

    private void changeOrderStatus(long orderId, String status) throws CustomerException {
        Order order = this.orderMapper.selectByPrimaryKey(Long.valueOf(orderId));
        if (order == null) {
            throw new CustomerException("该订单不存在");
        }
        order.setStatus(status);
        if (status.equals("待派单")) {
            order.setEmployeeId(null);
        }
        this.orderMapper.updateByPrimaryKey(order);
    }

    @Override
    public OrderExtend findById(long id) {
        return this.orderExtendMapper.selectById(id);
    }
}

