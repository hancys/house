package com.hancy.house.service;

import com.hancy.house.bean.CertificationApply;
import com.hancy.house.jacky.PageVM;

/**
 * 实名认证
 * @author TomORrow
 */
public interface ICertificationService {
  /**
   * 撤回实名认证
   * @param id 实名ID
   */
  void revoke(long id);

  /**
   * 提交实名
   * @param certificationApply 实名对象
   */
  void submit(CertificationApply certificationApply);

  /**
   * 分页查询
   * @param page 当前页
   * @param pageSize 页面数量
   * @param status 状态
   * @param userId 用户ID
   * @param beginTime 开始时间
   * @param endTime 结束时间
   * @return list数据
   */
  PageVM<CertificationApply> pageQuery(int page, int pageSize, String status, Long userId, Long beginTime, Long endTime);

  /**
   * 检查实名状态
   * @param id 实名ID
   * @param status 状态
   * @param reason 原因
   */
  void check(long id, String status, String reason);
}


