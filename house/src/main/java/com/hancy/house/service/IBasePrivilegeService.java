package com.hancy.house.service;

import com.hancy.house.bean.BasePrivilege;
import com.hancy.house.vm.PrivilegeTree;
import com.hancy.house.jacky.CustomerException;
import java.util.List;

/**
 * 权限
 * @author TomORrow
 */
public interface IBasePrivilegeService {
  /**
   * 删除
   * @param id id
   * @throws CustomerException 全局异常
   */
  void deleteById(long id) throws CustomerException;

  /**
   * 查询所有
   * @return List<BasePrivilege>
   */
  List<BasePrivilege> findAll();

  /**
   * 根据父ID查询
   * @param id 父ID
   * @return List<BasePrivilege>
   */
  List<BasePrivilege> findByParentId(Long id);

  /**
   * 保存更新
   * @param basePrivilege 权限
   * @throws CustomerException 全局异常
   */
  void saveOrUpdate(BasePrivilege basePrivilege) throws CustomerException;

  /**
   * 查询权限
   * @return List<PrivilegeTree>
   */
  List<PrivilegeTree> findPrivilegeTree();

  /**
   * 查询用户
   * @param id 用户ID
   * @return List<BasePrivilege>
   */
  List<BasePrivilege> findByUserId(long id);

  /**
   * 查询菜单
   * @param id 用户ID
   * @return List<BasePrivilege>
   */
  List<BasePrivilege> findMenuByUserId(long id);
}


