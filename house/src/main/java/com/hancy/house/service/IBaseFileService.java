package com.hancy.house.service;

import com.hancy.house.bean.BaseFile;
import com.hancy.house.vm.Pair;
import com.hancy.house.jacky.PageVM;
import java.util.List;

/**
 * 文件
 * @author TomORrow
 */
public interface IBaseFileService {
  /**
   * 查询每月文件
   * @return List<Pair>
   */
  List<Pair> perMonthImport();

  /**
   * 分页查询附件信息
   * @param page 当前页
   * @param pageSize 页面大小
   * @param fileName 文件名
   * @param datasetId 数据ID
   * @param uploadDate 上传数据
   * @return 分页数据
   */
  PageVM<BaseFile> pageQuery(int page, int pageSize, String fileName, Long datasetId, String uploadDate);

  /**
   * 查询所有
   * @return List<BaseFile>
   */
  List<BaseFile> findAll();

  /**
   * 保存
   * @param baseFile 文件
   * @throws Exception 抛出异常
   */
  void save(BaseFile baseFile) throws Exception;

  /**
   * 删除
   * @param id 文件ID
   * @throws Exception 异常处理
   */
  void deleteById(String id) throws Exception;
}


