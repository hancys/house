package com.hancy.house.service;

import com.hancy.house.bean.Comment;
import com.hancy.house.bean.extend.CommentExtend;
import com.hancy.house.jacky.PageVM;

/**
 * 评论相关
 * @author TomORrow
 */
public interface ICommentService {
  /**
   * 修改状态
   * @param id 评论ID
   * @param status 状态
   * @throws Exception 抛出异常
   */
  void checkComment(long id, String status) throws Exception;

  /**
   * 分页查询
   * @param page 当前页
   * @param pageSize 页面大小
   * @param keywords 关键词
   * @return PageVM<CommentExtend>
   */
  PageVM<CommentExtend> pageQuery(int page, int pageSize, String keywords);

  /**
   * 保存更新
   * @param comment 评论
   * @throws Exception 抛出异常
   */
  void saveOrUpdate(Comment comment) throws Exception;

  /**
   * 删除
   * @param id 评论ID
   * @throws Exception 抛出异常
   */
  void deleteById(long id) throws Exception;

  /**
   * 批量删除
   * @param ids 评论ID
   * @throws Exception 抛出异常
   */
  void batchDelete(long[] ids) throws Exception;
}


