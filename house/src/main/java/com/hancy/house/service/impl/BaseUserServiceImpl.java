package com.hancy.house.service.impl;

import com.hancy.house.bean.BaseRole;
import com.hancy.house.bean.BaseUser;
import com.hancy.house.bean.BaseUserExample;
import com.hancy.house.bean.BaseUserRole;
import com.hancy.house.bean.BaseUserRoleExample;
import com.hancy.house.bean.extend.BaseUserExtend;
import com.hancy.house.dao.BaseRoleMapper;
import com.hancy.house.dao.BaseUserMapper;
import com.hancy.house.dao.BaseUserRoleMapper;
import com.hancy.house.dao.extend.BaseUserExtendMapper;
import com.hancy.house.service.IBaseUserService;
import com.hancy.house.vm.UserVM;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;


/**
 * 用户
 * @author TomORrow
 */
@Service
public class BaseUserServiceImpl implements IBaseUserService {
    @Resource
    private BaseUserExtendMapper baseUserExtendMapper;
    @Resource
    private BaseUserMapper baseUserMapper;
    @Resource
    private BaseUserRoleMapper baseUserRoleMapper;
    @Resource
    private BaseRoleMapper baseRoleMapper;

    @Override
    public PageVM<BaseUser> pageQuery(int page, int pageSize, String username, Long roleId, String status) {
        List<BaseUser> list = this.baseUserExtendMapper.query(page, pageSize, username, roleId, status);
        long total = this.baseUserExtendMapper.count(username, roleId, status);
        PageVM<BaseUser> pageVM = new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
        return pageVM;
    }

    @Override
    public BaseUser login(UserVM userVM) throws CustomerException {
        BaseUserExample example = new BaseUserExample();
        example.createCriteria().andUsernameEqualTo(userVM.getUsername());
        List<BaseUser> list = this.baseUserMapper.selectByExample(example);
        if (list.size() <= 0) {
            throw new CustomerException("该用户不存在");
        }
        BaseUser user = list.get(0);
        if (!user.getPassword().equals(userVM.getPassword())) {
            throw new CustomerException("密码不匹配");
        }
        return user;
    }

    @Override
    public BaseUserExtend findById(long id) {
        return this.baseUserExtendMapper.selectById(id);
    }

    @Override
    public List<BaseUser> findAll() {
        return this.baseUserMapper.selectByExample(new BaseUserExample());
    }

    @Override
    public List<BaseUserExtend> cascadeRoleFindAll() {
        return this.baseUserExtendMapper.selectAll();
    }

    private BaseUser findByTelephone(String telephone) {
        BaseUserExample example = new BaseUserExample();
        example.createCriteria().andTelephoneEqualTo(telephone);
        List<BaseUser> list = this.baseUserMapper.selectByExample(example);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    private BaseUser findByUsername(String username) {
        BaseUserExample example = new BaseUserExample();
        example.createCriteria().andUsernameEqualTo(username);
        List<BaseUser> list = this.baseUserMapper.selectByExample(example);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public void saveOrUpdate(BaseUser baseUser) throws CustomerException {
        if (baseUser.getId() != null) {
            this.baseUserMapper.updateByPrimaryKey(baseUser);
        } else {
            if (baseUser.getTelephone() != null && findByTelephone(baseUser.getTelephone()) != null) {
                throw new CustomerException("手机号重复！");
            }
            if (baseUser.getUsername() != null && findByUsername(baseUser.getUsername()) != null) {
                throw new CustomerException("用户名重复！");
            }

            baseUser.setNation("汉");
            baseUser.setCertificationStatus("未认证");
            baseUser.setBalance(Double.valueOf(0.0D));
            baseUser.setUserFace("http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-Td6-Aawn0AACq962TS9c719.jpg");
            baseUser.setRegisterTime(Long.valueOf((new Date()).getTime()));
            baseUser.setStatus("正常");
            this.baseUserMapper.insert(baseUser);
        }
    }

    @Override
    public void changeStatus(long id, String status) throws CustomerException {
        BaseUserExtend baseUserExtend = findById(id);
        if (baseUserExtend == null) {
            throw new CustomerException("该用户不存在");
        }
        baseUserExtend.setStatus(status);
        this.baseUserMapper.updateByPrimaryKey((BaseUser) baseUserExtend);
    }

    @Override
    public void deleteById(long id) throws CustomerException {
        BaseUserExtend baseUserExtend = findById(id);
        if (baseUserExtend == null) {
            throw new CustomerException("该用户不存在");
        }
        this.baseUserMapper.deleteByPrimaryKey(Long.valueOf(id));
    }

    @Override
    public void setRoles(long id, List<Long> roles) {
        BaseUserRoleExample example = new BaseUserRoleExample();
        example.createCriteria().andUserIdEqualTo(Long.valueOf(id));

        List<BaseUserRole> list = this.baseUserRoleMapper.selectByExample(example);
        List<Long> oldRoles = new ArrayList<>();
        Iterator<BaseUserRole> iterator = list.iterator();
        while (iterator.hasNext()) {
            oldRoles.add(((BaseUserRole) iterator.next()).getRoleId());
        }


        for (Long roleId : roles) {
            if (!oldRoles.contains(roleId)) {
                BaseUserRole userRole = new BaseUserRole();
                userRole.setRoleId(roleId);
                userRole.setUserId(Long.valueOf(id));
                this.baseUserRoleMapper.insert(userRole);
            }
        }


        for (BaseUserRole userRole : list) {
            if (!roles.contains(userRole.getRoleId())) {
                this.baseUserRoleMapper.deleteByPrimaryKey(userRole.getId());
            }
        }
    }

    @Override
    public List<BaseUser> findByClazzId(long clazzId) {
        BaseUserExample example = new BaseUserExample();
        return this.baseUserMapper.selectByExample(example);
    }

    @Override
    public void batchInsert(List<BaseUser> list) throws CustomerException {
        for (BaseUser u : list) {

            saveOrUpdate(u);


            BaseUserRole ur = new BaseUserRole();
            ur.setUserId(u.getId());
            ur.setRoleId(Long.valueOf(6L));
            this.baseUserRoleMapper.insert(ur);
        }
    }

    @Override
    public void addUserWidthRole(BaseUser user, long roleId) throws CustomerException {
        BaseRole role = this.baseRoleMapper.selectByPrimaryKey(Long.valueOf(roleId));
        if (role == null) {
            throw new CustomerException("角色不存在");
        }

        saveOrUpdate(user);

        BaseUserRole ur = new BaseUserRole();
        ur.setUserId(user.getId());
        ur.setRoleId(Long.valueOf(roleId));
        this.baseUserRoleMapper.insert(ur);
    }

    @Override
    public void alterUserface(long id, String userface) throws CustomerException {
        BaseUser user = this.baseUserMapper.selectByPrimaryKey(Long.valueOf(id));
        if (user == null) {
            throw new CustomerException("用户信息不存在");
        }
        user.setUserFace(userface);
        this.baseUserMapper.updateByPrimaryKey(user);
    }

    @Override
    public void register(BaseUser u, long roleId) {
        saveOrUpdate(u);

        BaseUserRole ur = new BaseUserRole();
        ur.setUserId(u.getId());
        ur.setRoleId(Long.valueOf(roleId));
        this.baseUserRoleMapper.insert(ur);
    }
}
