package com.hancy.house.service;

import com.hancy.house.bean.BaseLog;
import com.hancy.house.jacky.CustomerException;
import com.hancy.house.jacky.PageVM;

/**
 * 日志
 * @author TomORrow
 */
public interface IBaseLogService {
  /**
   * 分页查询
   * @param page 当前页
   * @param pageSize 页面大小
   * @return PageVM<BaseLog>
   */
  PageVM<BaseLog> pageQuery(int page, int pageSize);

  /**
   * 保存
   * @param baseLog 日志
   * @throws CustomerException 全局异常
   */
  void save(BaseLog baseLog) throws CustomerException;
}


