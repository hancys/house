package com.hancy.house.dao.extend;

import com.hancy.house.bean.BaseConfig;

/**
 * 基础配置
 * @author TomORrow
 */
public interface BaseConfigExtendMapper {
    /**
     * 通过名字查询
     * @param name 名字
     * @return BaseConfig
     */
    BaseConfig findByKey(String name);
}

