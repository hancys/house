package com.hancy.house.dao;

import com.hancy.house.bean.BaseUserRole;
import com.hancy.house.bean.BaseUserRoleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BaseUserRoleMapper {
  long countByExample(BaseUserRoleExample paramBaseUserRoleExample);
  
  int deleteByExample(BaseUserRoleExample paramBaseUserRoleExample);
  
  int deleteByPrimaryKey(Long paramLong);
  
  int insert(BaseUserRole paramBaseUserRole);
  
  int insertSelective(BaseUserRole paramBaseUserRole);
  
  List<BaseUserRole> selectByExample(BaseUserRoleExample paramBaseUserRoleExample);
  
  BaseUserRole selectByPrimaryKey(Long paramLong);
  
  int updateByExampleSelective(@Param("record") BaseUserRole paramBaseUserRole, @Param("example") BaseUserRoleExample paramBaseUserRoleExample);
  
  int updateByExample(@Param("record") BaseUserRole paramBaseUserRole, @Param("example") BaseUserRoleExample paramBaseUserRoleExample);
  
  int updateByPrimaryKeySelective(BaseUserRole paramBaseUserRole);
  
  int updateByPrimaryKey(BaseUserRole paramBaseUserRole);
}


