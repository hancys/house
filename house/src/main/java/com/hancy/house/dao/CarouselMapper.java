package com.hancy.house.dao;

import com.hancy.house.bean.Carousel;
import com.hancy.house.bean.CarouselExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CarouselMapper {
  long countByExample(CarouselExample paramCarouselExample);
  
  int deleteByExample(CarouselExample paramCarouselExample);
  
  int deleteByPrimaryKey(Long paramLong);
  
  int insert(Carousel paramCarousel);
  
  int insertSelective(Carousel paramCarousel);
  
  List<Carousel> selectByExample(CarouselExample paramCarouselExample);
  
  Carousel selectByPrimaryKey(Long paramLong);
  
  int updateByExampleSelective(@Param("record") Carousel paramCarousel, @Param("example") CarouselExample paramCarouselExample);
  
  int updateByExample(@Param("record") Carousel paramCarousel, @Param("example") CarouselExample paramCarouselExample);
  
  int updateByPrimaryKeySelective(Carousel paramCarousel);
  
  int updateByPrimaryKey(Carousel paramCarousel);
}


