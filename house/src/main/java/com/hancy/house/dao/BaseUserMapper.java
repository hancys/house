package com.hancy.house.dao;

import com.hancy.house.bean.BaseUser;
import com.hancy.house.bean.BaseUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BaseUserMapper {
  long countByExample(BaseUserExample paramBaseUserExample);
  
  int deleteByExample(BaseUserExample paramBaseUserExample);
  
  int deleteByPrimaryKey(Long paramLong);
  
  int insert(BaseUser paramBaseUser);
  
  int insertSelective(BaseUser paramBaseUser);
  
  List<BaseUser> selectByExample(BaseUserExample paramBaseUserExample);
  
  BaseUser selectByPrimaryKey(Long paramLong);
  
  int updateByExampleSelective(@Param("record") BaseUser paramBaseUser, @Param("example") BaseUserExample paramBaseUserExample);
  
  int updateByExample(@Param("record") BaseUser paramBaseUser, @Param("example") BaseUserExample paramBaseUserExample);
  
  int updateByPrimaryKeySelective(BaseUser paramBaseUser);
  
  int updateByPrimaryKey(BaseUser paramBaseUser);
}


