package com.hancy.house.dao.extend;

import com.hancy.house.bean.OrderComment;
import java.util.List;

/**
 * 订单评论
 * @author TomORrow
 */
public interface OrderCommentExtendMapper {
  /**
   * 分页查询
   * @param page 当前页
   * @param pageSize 页面大小
   * @param content 内容
   * @param status 状态
   * @param orderId 订单ID
   * @return list分页数据
   */
  List<OrderComment> pageQuery(int page, int pageSize, String content, String status, Long orderId);

  /**
   * 计算数量
   * @param content 内容
   * @param status 状态
   * @param orderId 订单ID
   * @return 数量
   */
  long count(String content, String status, Long orderId);

  /**
   * 根据ID查询
   * @param id
   * @return list数据
   */
  List<OrderComment> selectByOrderId(long id);
}
