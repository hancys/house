package com.hancy.house.dao.extend;

import com.hancy.house.bean.AccountSystem;
import java.util.List;

/**
 * 系统账单
 * @author TomORrow
 */
public interface AccountSystemExtendMapper {
  /**
   * 分页查询系统账单
   * @param page 当前页
   * @param pageSize 页面数量
   * @param status 状态
   * @param type 类型
   * @param beginTime 开始时间
   * @param endTime 结束时间
   * @return 分页数据
   */
  List<AccountSystem> pageQuery(int page, int pageSize, String status, String type, Long beginTime, Long endTime);

  /**
   * 数据数量计算
   * @param status 状态
   * @param type 类型
   * @param beginTime 开始时间
   * @param endTime 结束时间
   * @return 数据数量
   */
  long count(String status, String type, Long beginTime, Long endTime);
}


