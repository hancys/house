package com.hancy.house.dao.extend;

import com.hancy.house.bean.extend.ArticleExtend;
import java.util.List;

/**
 * 学生风采相关接口
 * @author TomORrow
 */
public interface ArticleExtendMapper {
  /**
   * 查询所有
   * @return list数据
   */
  List<ArticleExtend> selectAll();

  /**
   * 通过id查询
   * @param id ID
   * @return 对象
   */
  ArticleExtend selectById(long id);

  /**
   * 分页查询
   * @param page 当前页
   * @param pageSize 页面大小
   * @param title 标题
   * @param status 状态
   * @param authorId 作者ID
   * @param categoryId 资讯ID
   * @return 分页数据
   */
  List<ArticleExtend> query(int page, int pageSize, String title, String status, Long authorId, Long categoryId);

  /**
   * 计算数据数量
   * @param title 标题
   * @param status 状态
   * @param authorId 作者ID
   * @param categoryId 资讯ID
   * @return 数据数量
   */
  long count(String title, String status, Long authorId, Long categoryId);

  /**
   * 分页评论
   * @param page 当前页
   * @param pageSize 页面大小
   * @return 分页数据
   */
  List<ArticleExtend> findAllRecommend(int page, int pageSize);

  /**
   * 数据计算
   * @return 数量
   */
  long countRecommend();
}


