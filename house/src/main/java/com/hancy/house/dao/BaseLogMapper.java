package com.hancy.house.dao;

import com.hancy.house.bean.BaseLog;
import com.hancy.house.bean.BaseLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BaseLogMapper {
  long countByExample(BaseLogExample paramBaseLogExample);
  
  int deleteByExample(BaseLogExample paramBaseLogExample);
  
  int deleteByPrimaryKey(Long paramLong);
  
  int insert(BaseLog paramBaseLog);
  
  int insertSelective(BaseLog paramBaseLog);
  
  List<BaseLog> selectByExample(BaseLogExample paramBaseLogExample);
  
  BaseLog selectByPrimaryKey(Long paramLong);
  
  int updateByExampleSelective(@Param("record") BaseLog paramBaseLog, @Param("example") BaseLogExample paramBaseLogExample);
  
  int updateByExample(@Param("record") BaseLog paramBaseLog, @Param("example") BaseLogExample paramBaseLogExample);
  
  int updateByPrimaryKeySelective(BaseLog paramBaseLog);
  
  int updateByPrimaryKey(BaseLog paramBaseLog);
}


