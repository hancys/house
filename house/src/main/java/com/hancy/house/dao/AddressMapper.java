package com.hancy.house.dao;

import com.hancy.house.bean.Address;
import com.hancy.house.bean.AddressExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AddressMapper {
  long countByExample(AddressExample paramAddressExample);
  
  int deleteByExample(AddressExample paramAddressExample);
  
  int deleteByPrimaryKey(Long paramLong);
  
  int insert(Address paramAddress);
  
  int insertSelective(Address paramAddress);
  
  List<Address> selectByExample(AddressExample paramAddressExample);
  
  Address selectByPrimaryKey(Long paramLong);
  
  int updateByExampleSelective(@Param("record") Address paramAddress, @Param("example") AddressExample paramAddressExample);
  
  int updateByExample(@Param("record") Address paramAddress, @Param("example") AddressExample paramAddressExample);
  
  int updateByPrimaryKeySelective(Address paramAddress);
  
  int updateByPrimaryKey(Address paramAddress);
}


