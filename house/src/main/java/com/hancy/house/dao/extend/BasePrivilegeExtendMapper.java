package com.hancy.house.dao.extend;

import com.hancy.house.bean.BasePrivilege;
import com.hancy.house.vm.PrivilegeTree;
import java.util.List;

/**
 * 权限相关
 * @author TomORrow
 */
public interface BasePrivilegeExtendMapper {
  /**
   * 查询所有
   * @return 所有数据
   */
  List<PrivilegeTree> selectAll();

  /**
   * 根据父ID查询
   * @param id 父ID
   * @return 数据
   */
  List<BasePrivilege> selectByParentId(long id);

  /**
   * 根据角色ID查询
   * @param id 角色ID
   * @return 数据
   */
  List<BasePrivilege> selectByRoleId(long id);

  /**
   * 根据用户ID查询
   * @param id 用户ID
   * @return 数据
   */
  List<BasePrivilege> selectByUserId(long id);

  /**
   * 根据用户ID查询菜单
   * @param id 用户ID
   * @return 数据
   */
  List<BasePrivilege> selectMenuByUserId(long id);
}


