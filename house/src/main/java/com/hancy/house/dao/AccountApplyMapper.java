package com.hancy.house.dao;

import com.hancy.house.bean.AccountApply;
import com.hancy.house.bean.AccountApplyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 账户申请
 * @author TomORrow
 */
public interface AccountApplyMapper {
  /**
   * 通过Example 计算数量
   * @param accountApplyExample 账户实例条件
   * @return long
   */
  long countByExample(AccountApplyExample accountApplyExample);

  /**
   * 通过Example删除
   * @param accountApplyExample 账户实例条件
   * @return int
   */
  int deleteByExample(AccountApplyExample accountApplyExample);
  /**
   * 撤销账户变动申请
   * @param id id
   * @return int
   */
  int deleteByPrimaryKey(Long id);
  /**
   * 提交账户变动申请
   * @param accountApply 账户申请实体
   * @return int
   */
  int insert(AccountApply accountApply);

  /**
   * 插入
   * @param accountApply 账户申请对象
   * @return int
   */
  int insertSelective(AccountApply accountApply);

  /**
   * 通过Example查询
   * @param accountApplyExample 账户条件对象
   * @return List<AccountApply>
   */
  List<AccountApply> selectByExample(AccountApplyExample accountApplyExample);

  /**
   * 主键查询
   * @param id ID
   * @return 账户
   */
  AccountApply selectByPrimaryKey(Long id);

  /**
   * 更新账户部分
   * @param accountApply 账户对象
   * @param accountApplyExample 账户条件对象
   * @return int
   */
  int updateByExampleSelective(@Param("record") AccountApply accountApply, @Param("example") AccountApplyExample accountApplyExample);

  /**
   * 更新账户
   * @param accountApply 账户对象
   * @param accountApplyExample 账户条件对象
   * @return int
   */
  int updateByExample(@Param("record") AccountApply accountApply, @Param("example") AccountApplyExample accountApplyExample);

  /**
   * 根据主键更新部分
   * @param accountApply 账户对象
   * @return int
   */
  int updateByPrimaryKeySelective(AccountApply accountApply);

  /**
   * 根据主键更新
   * @param accountApply 账户对象
   * @return int
   */
  int updateByPrimaryKey(AccountApply accountApply);
}


