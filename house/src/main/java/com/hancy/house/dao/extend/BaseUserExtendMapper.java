package com.hancy.house.dao.extend;

import com.hancy.house.bean.BaseUser;
import com.hancy.house.bean.extend.BaseUserExtend;
import java.util.List;

/**
 * 用户相关
 * @author TomORrow
 */
public interface BaseUserExtendMapper {
  /**
   * 根据ID查询
   * @param id 用户ID
   * @return 用户对象
   */
  BaseUserExtend selectById(long id);

  /**
   * 查询所有
   * @return list数据
   */
  List<BaseUserExtend> selectAll();

  /**
   * 分页查询用户
   * @param page 当前页
   * @param pageSize 页面大小
   * @param username 用户名
   * @param roleId 角色ID
   * @param status 状态
   * @return list分页数据
   */
  List<BaseUser> query(int page, int pageSize, String username, Long roleId, String status);

  /**
   * 计算数量
   * @param username 用户名
   * @param roleId 角色ID
   * @param status 状态
   * @return long数量
   */
  long count(String username, Long roleId, String status);

  /**
   * 根据IDcard查询
   * @param idCard idCard
   * @return 对象
   */
  BaseUser findIDCard(String idCard);

  /**
   * 新增用户
   * @param baseUser 用户对象
   */
  void insertPeopleAndStu(BaseUser baseUser);
}


