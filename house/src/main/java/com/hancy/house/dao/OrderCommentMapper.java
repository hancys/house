package com.hancy.house.dao;

import com.hancy.house.bean.OrderComment;
import com.hancy.house.bean.OrderCommentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderCommentMapper {
  long countByExample(OrderCommentExample paramOrderCommentExample);
  
  int deleteByExample(OrderCommentExample paramOrderCommentExample);
  
  int deleteByPrimaryKey(Long paramLong);
  
  int insert(OrderComment paramOrderComment);
  
  int insertSelective(OrderComment paramOrderComment);
  
  List<OrderComment> selectByExample(OrderCommentExample paramOrderCommentExample);
  
  OrderComment selectByPrimaryKey(Long paramLong);
  
  int updateByExampleSelective(@Param("record") OrderComment paramOrderComment, @Param("example") OrderCommentExample paramOrderCommentExample);
  
  int updateByExample(@Param("record") OrderComment paramOrderComment, @Param("example") OrderCommentExample paramOrderCommentExample);
  
  int updateByPrimaryKeySelective(OrderComment paramOrderComment);
  
  int updateByPrimaryKey(OrderComment paramOrderComment);
}


