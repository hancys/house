package com.hancy.house.dao.extend;

import com.hancy.house.bean.BaseFile;
import com.hancy.house.vm.Pair;
import java.util.List;

/**
 * 文件
 * @author TomORrow
 */
public interface BaseFileExtendMapper {
  /**
   * 分页查询附件信息
   * @param page 当前页
   * @param pageSize 页面大小
   * @param fileName 文件名
   * @param datasetId 数据ID
   * @param uploadDate 上传数据
   * @return 分页数据
   */
  List<BaseFile> query(int page, int pageSize, String fileName, Long datasetId, String uploadDate);

  /**
   * 计算数量
   * @param fileName 文件名
   * @param datasetId 数据ID
   * @param uploadDate 上传数据
   * @return 数据数量
   */
  long count(String fileName, Long datasetId, String uploadDate);

  /**
   * 查询每月数据
   * @return 数据
   */
  List<Pair> perMonthImport();
}


