package com.hancy.house.dao;

import com.hancy.house.bean.AccountEmployee;
import com.hancy.house.bean.AccountEmployeeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 职工账单
 * @author TomORrow
 */
public interface AccountEmployeeMapper {
  /**
   * 数量查询
   * @param accountEmployeeExample 账户条件对象
   * @return long
   */
  long countByExample(AccountEmployeeExample accountEmployeeExample);

  /**
   * 根据条件删除
   * @param accountEmployeeExample 账户条件对象
   * @return int
   */
  int deleteByExample(AccountEmployeeExample accountEmployeeExample);

  /**
   * 根据id删除
   * @param id 账户id
   * @return int
   */
  int deleteByPrimaryKey(Long id);

  /**
   * 插入
   * @param accountEmployee 账户对象
   * @return int
   */
  int insert(AccountEmployee accountEmployee);

  /**
   * 插入部分数据
   * @param accountEmployee 账户对象
   * @return int
   */
  int insertSelective(AccountEmployee accountEmployee);

  /**
   * 根据条件查询
   * @param accountEmployeeExample 账户条件对象
   * @return List<AccountEmployee>
   */
  List<AccountEmployee> selectByExample(AccountEmployeeExample accountEmployeeExample);

  /**
   * 根据主键查询
   * @param id
   * @return AccountEmployee
   */
  AccountEmployee selectByPrimaryKey(Long id);

  /**
   * 根据条件更新部分数据
   * @param accountEmployee 账户对象
   * @param accountEmployeeExample 账户条件对象
   * @return int
   */
  int updateByExampleSelective(@Param("record") AccountEmployee accountEmployee, @Param("example") AccountEmployeeExample accountEmployeeExample);

  /**
   * 根据条件更新
   * @param accountEmployee 账户对象
   * @param accountEmployeeExample 账户条件对象
   * @return int
   */
  int updateByExample(@Param("record") AccountEmployee accountEmployee, @Param("example") AccountEmployeeExample accountEmployeeExample);

  /**
   * 根据主键更新部分数据
   * @param accountEmployee 账户对象
   * @return int
   */
  int updateByPrimaryKeySelective(AccountEmployee accountEmployee);

  /**
   * 根据主键更新
   * @param accountEmployee 账户对象
   * @return int
   */
  int updateByPrimaryKey(AccountEmployee accountEmployee);
}


