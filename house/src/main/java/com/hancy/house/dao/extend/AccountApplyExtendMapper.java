package com.hancy.house.dao.extend;

import com.hancy.house.bean.AccountApply;

import java.util.List;

/**
 * 账户申请
 * @author TomORrow
 */
public interface AccountApplyExtendMapper {
    /**
     * 分页查询账户变动申请
     * @param page 当前页
     * @param pageSize 页面大小
     * @param status 状态
     * @param applyType 申请类型
     * @param userId 用户ID
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @return 分页数据
     */
    List<AccountApply> pageQuery(int page, int pageSize, String status, String applyType, Long userId, Long beginTime, Long endTime);

    /**
     * 计算数据数量
     * @param status 状态
     * @param applyType 申请类型
     * @param userId 用户ID
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @return 数据数量
     */
    long count(String status, String applyType, Long userId, Long beginTime, Long endTime);
}
