package com.hancy.house.dao;

import com.hancy.house.bean.AccountCustomer;
import com.hancy.house.bean.AccountCustomerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 客户账户
 * @author TomORrow
 */
public interface AccountCustomerMapper {
  /**
   * 计算数量
   * @param accountCustomerExample 用户账单条件对象
   * @return long
   */
  long countByExample(AccountCustomerExample accountCustomerExample);

  /**
   * 根据条件删除
   * @param accountCustomerExample 用户账单条件对象
   * @return int
   */
  int deleteByExample(AccountCustomerExample accountCustomerExample);

  /**
   * 根据主键删除
   * @param id ID
   * @return int
   */
  int deleteByPrimaryKey(Long id);

  /**
   * 插入
   * @param accountCustomer 用户账单对象
   * @return int
   */
  int insert(AccountCustomer accountCustomer);

  /**
   * 插入部分数据
   * @param accountCustomer 用户账单对象
   * @return int
   */
  int insertSelective(AccountCustomer accountCustomer);

  /**
   * 根据条件查询
   * @param accountCustomerExample 用户账单条件对象
   * @return List<AccountCustomer>
   */
  List<AccountCustomer> selectByExample(AccountCustomerExample accountCustomerExample);

  /**
   * 根据主键查询
   * @param id ID
   * @return AccountCustomer
   */
  AccountCustomer selectByPrimaryKey(Long id);

  /**
   * 根据条件更新部分
   * @param accountCustomer 用户账单对象
   * @param accountCustomerExample 用户账单条件对象
   * @return int
   */
  int updateByExampleSelective(@Param("record") AccountCustomer accountCustomer, @Param("example") AccountCustomerExample accountCustomerExample);

  /**
   * 根据条件更新
   * @param accountCustomer 用户账单对象
   * @param accountCustomerExample 用户账单条件对象
   * @return int
   */
  int updateByExample(@Param("record") AccountCustomer accountCustomer, @Param("example") AccountCustomerExample accountCustomerExample);

  /**
   * 根据主键更新部分
   * @param accountCustomer 用户账单对象
   * @return int
   */
  int updateByPrimaryKeySelective(AccountCustomer accountCustomer);

  /**
   * 根据主键更新
   * @param accountCustomer 用户账单对象
   * @return int
   */
  int updateByPrimaryKey(AccountCustomer accountCustomer);
}


