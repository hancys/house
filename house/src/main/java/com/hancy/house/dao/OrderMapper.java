package com.hancy.house.dao;

import com.hancy.house.bean.Order;
import com.hancy.house.bean.OrderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderMapper {
  long countByExample(OrderExample paramOrderExample);
  
  int deleteByExample(OrderExample paramOrderExample);
  
  int deleteByPrimaryKey(Long paramLong);
  
  int insert(Order paramOrder);
  
  int insertSelective(Order paramOrder);
  
  List<Order> selectByExample(OrderExample paramOrderExample);
  
  Order selectByPrimaryKey(Long paramLong);
  
  int updateByExampleSelective(@Param("record") Order paramOrder, @Param("example") OrderExample paramOrderExample);
  
  int updateByExample(@Param("record") Order paramOrder, @Param("example") OrderExample paramOrderExample);
  
  int updateByPrimaryKeySelective(Order paramOrder);
  
  int updateByPrimaryKey(Order paramOrder);
}


