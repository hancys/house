package com.hancy.house.dao.extend;

import com.hancy.house.bean.ProductCategory;
import java.util.List;

/**
 * 产品分类
 * @author TomORrow
 */
public interface ProductCategoryExtendMapper {
  /**
   * 分页相关
   * @param page 当前页
   * @param pageSize 页面大小
   * @param name 名字
   * @return 数据
   */
  List<ProductCategory> pageQuery(int page, int pageSize, String name);

  /**
   * 计算数量
   * @param name 名字
   * @return 数量
   */
  long count(String name);
}


