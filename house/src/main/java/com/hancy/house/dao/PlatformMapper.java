package com.hancy.house.dao;

import com.hancy.house.bean.Platform;
import com.hancy.house.bean.PlatformExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PlatformMapper {
  long countByExample(PlatformExample paramPlatformExample);
  
  int deleteByExample(PlatformExample paramPlatformExample);
  
  int deleteByPrimaryKey(Long paramLong);
  
  int insert(Platform paramPlatform);
  
  int insertSelective(Platform paramPlatform);
  
  List<Platform> selectByExample(PlatformExample paramPlatformExample);
  
  Platform selectByPrimaryKey(Long paramLong);
  
  int updateByExampleSelective(@Param("record") Platform paramPlatform, @Param("example") PlatformExample paramPlatformExample);
  
  int updateByExample(@Param("record") Platform paramPlatform, @Param("example") PlatformExample paramPlatformExample);
  
  int updateByPrimaryKeySelective(Platform paramPlatform);
  
  int updateByPrimaryKey(Platform paramPlatform);
}


