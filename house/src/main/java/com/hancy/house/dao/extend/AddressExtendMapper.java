package com.hancy.house.dao.extend;

import com.hancy.house.bean.Address;
import java.util.List;

/**
 * 地址相关
 * @author TomORrow
 */
public interface AddressExtendMapper {
  /**
   * 分页查询地址
   * @param page 当前页
   * @param pageSize 页面大小
   * @param userId 用户ID
   * @return
   */
  List<Address> pageQuery(int page, int pageSize, Long userId);

  /**
   * 计算数据数量
   * @param userId 用户ID
   * @return 数据数量
   */
  long count(Long userId);
}


