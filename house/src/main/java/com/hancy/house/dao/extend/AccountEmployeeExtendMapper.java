package com.hancy.house.dao.extend;

import com.hancy.house.bean.AccountEmployee;
import java.util.List;

/**
 * 员工账单查询
 * @author TomORrow
 */
public interface AccountEmployeeExtendMapper {
  /**
   * 分页查询员工账单
   * @param page 当前页
   * @param pageSize 页面数量
   * @param status 状态
   * @param type 类型
   * @param beginTime 开始时间
   * @param endTime 结束时间
   * @param userId 用户ID
   * @return
   */
  List<AccountEmployee> pageQuery(int page, int pageSize, String status, String type, Long beginTime, Long endTime, Long userId);

  /**
   * 数据数量
   * @param status 状态
   * @param type 类型
   * @param beginTime 开始时间
   * @param endTime 结束时间
   * @param userId 用户ID
   * @return 数量
   */
  long count(String status, String type, Long beginTime, Long endTime, Long userId);
}


