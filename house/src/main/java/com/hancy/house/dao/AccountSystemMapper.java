package com.hancy.house.dao;

import com.hancy.house.bean.AccountSystem;
import com.hancy.house.bean.AccountSystemExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AccountSystemMapper {
  long countByExample(AccountSystemExample paramAccountSystemExample);
  
  int deleteByExample(AccountSystemExample paramAccountSystemExample);
  
  int deleteByPrimaryKey(Long paramLong);
  
  int insert(AccountSystem paramAccountSystem);
  
  int insertSelective(AccountSystem paramAccountSystem);
  
  List<AccountSystem> selectByExample(AccountSystemExample paramAccountSystemExample);
  
  AccountSystem selectByPrimaryKey(Long paramLong);
  
  int updateByExampleSelective(@Param("record") AccountSystem paramAccountSystem, @Param("example") AccountSystemExample paramAccountSystemExample);
  
  int updateByExample(@Param("record") AccountSystem paramAccountSystem, @Param("example") AccountSystemExample paramAccountSystemExample);
  
  int updateByPrimaryKeySelective(AccountSystem paramAccountSystem);
  
  int updateByPrimaryKey(AccountSystem paramAccountSystem);
}


