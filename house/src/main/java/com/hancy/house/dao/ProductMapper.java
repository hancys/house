package com.hancy.house.dao;

import com.hancy.house.bean.Product;
import com.hancy.house.bean.ProductExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ProductMapper {
  long countByExample(ProductExample paramProductExample);
  
  int deleteByExample(ProductExample paramProductExample);
  
  int deleteByPrimaryKey(Long paramLong);
  
  int insert(Product paramProduct);
  
  int insertSelective(Product paramProduct);
  
  List<Product> selectByExample(ProductExample paramProductExample);
  
  Product selectByPrimaryKey(Long paramLong);
  
  int updateByExampleSelective(@Param("record") Product paramProduct, @Param("example") ProductExample paramProductExample);
  
  int updateByExample(@Param("record") Product paramProduct, @Param("example") ProductExample paramProductExample);
  
  int updateByPrimaryKeySelective(Product paramProduct);
  
  int updateByPrimaryKey(Product paramProduct);
}


