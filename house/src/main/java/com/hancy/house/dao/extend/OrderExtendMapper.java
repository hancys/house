package com.hancy.house.dao.extend;

import com.hancy.house.bean.extend.OrderExtend;
import java.util.List;

/**
 * 订单
 * @author TomORrow
 */
public interface OrderExtendMapper {
  /**
   * 分页查询
   * @param page 当前页
   * @param pageSize 页面大小
   * @param beginTime 开始时间
   * @param endTime 结束时间
   * @param status 状态
   * @param customerId 客户ID
   * @param employeeId 职工ID
   * @return list分页数据
   */
  List<OrderExtend> pageQuery(int page, int pageSize, Long beginTime, Long endTime, String status, Long customerId, Long employeeId);

  /**
   * 计算数量
   * @param beginTime 开始时间
   * @param endTime 结束时间
   * @param status 状态
   * @param customerId 客户ID
   * @param employeeId 职工ID
   * @return 数量
   */
  long count(Long beginTime, Long endTime, String status, Long customerId, Long employeeId);

  /**
   * 根据ID查询
   * @param id ID
   * @return 订单对象
   */
  OrderExtend selectById(long id);
}


