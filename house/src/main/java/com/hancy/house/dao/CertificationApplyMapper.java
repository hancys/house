package com.hancy.house.dao;

import com.hancy.house.bean.CertificationApply;
import com.hancy.house.bean.CertificationApplyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CertificationApplyMapper {
  long countByExample(CertificationApplyExample paramCertificationApplyExample);
  
  int deleteByExample(CertificationApplyExample paramCertificationApplyExample);
  
  int deleteByPrimaryKey(Long paramLong);
  
  int insert(CertificationApply paramCertificationApply);
  
  int insertSelective(CertificationApply paramCertificationApply);
  
  List<CertificationApply> selectByExample(CertificationApplyExample paramCertificationApplyExample);
  
  CertificationApply selectByPrimaryKey(Long paramLong);
  
  int updateByExampleSelective(@Param("record") CertificationApply paramCertificationApply, @Param("example") CertificationApplyExample paramCertificationApplyExample);
  
  int updateByExample(@Param("record") CertificationApply paramCertificationApply, @Param("example") CertificationApplyExample paramCertificationApplyExample);
  
  int updateByPrimaryKeySelective(CertificationApply paramCertificationApply);
  
  int updateByPrimaryKey(CertificationApply paramCertificationApply);
}


