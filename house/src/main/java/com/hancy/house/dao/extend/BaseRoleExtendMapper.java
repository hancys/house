package com.hancy.house.dao.extend;

import com.hancy.house.bean.BaseRole;
import com.hancy.house.bean.extend.BaseRoleExtend;
import java.util.List;

/**
 * 角色相关
 * @author TomORrow
 */
public interface BaseRoleExtendMapper {
  /**
   * 根据用户ID查询
   * @param id 用户ID
   * @return list数据
   */
  List<BaseRole> selectByUserId(long id);

  /**
   * 查询所欲
   * @return list数据
   */
  List<BaseRoleExtend> selectAll();
}


