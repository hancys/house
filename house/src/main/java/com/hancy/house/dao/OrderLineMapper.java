package com.hancy.house.dao;

import com.hancy.house.bean.OrderLine;
import com.hancy.house.bean.OrderLineExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderLineMapper {
  long countByExample(OrderLineExample paramOrderLineExample);
  
  int deleteByExample(OrderLineExample paramOrderLineExample);
  
  int deleteByPrimaryKey(Long paramLong);
  
  int insert(OrderLine paramOrderLine);
  
  int insertSelective(OrderLine paramOrderLine);
  
  List<OrderLine> selectByExample(OrderLineExample paramOrderLineExample);
  
  OrderLine selectByPrimaryKey(Long paramLong);
  
  int updateByExampleSelective(@Param("record") OrderLine paramOrderLine, @Param("example") OrderLineExample paramOrderLineExample);
  
  int updateByExample(@Param("record") OrderLine paramOrderLine, @Param("example") OrderLineExample paramOrderLineExample);
  
  int updateByPrimaryKeySelective(OrderLine paramOrderLine);
  
  int updateByPrimaryKey(OrderLine paramOrderLine);
}


