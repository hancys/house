package com.hancy.house.dao;

import com.hancy.house.bean.ProductCategory;
import com.hancy.house.bean.ProductCategoryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ProductCategoryMapper {
  long countByExample(ProductCategoryExample paramProductCategoryExample);
  
  int deleteByExample(ProductCategoryExample paramProductCategoryExample);
  
  int deleteByPrimaryKey(Long paramLong);
  
  int insert(ProductCategory paramProductCategory);
  
  int insertSelective(ProductCategory paramProductCategory);
  
  List<ProductCategory> selectByExample(ProductCategoryExample paramProductCategoryExample);
  
  ProductCategory selectByPrimaryKey(Long paramLong);
  
  int updateByExampleSelective(@Param("record") ProductCategory paramProductCategory, @Param("example") ProductCategoryExample paramProductCategoryExample);
  
  int updateByExample(@Param("record") ProductCategory paramProductCategory, @Param("example") ProductCategoryExample paramProductCategoryExample);
  
  int updateByPrimaryKeySelective(ProductCategory paramProductCategory);
  
  int updateByPrimaryKey(ProductCategory paramProductCategory);
}


