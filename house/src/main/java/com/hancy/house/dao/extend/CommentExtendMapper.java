package com.hancy.house.dao.extend;

import com.hancy.house.bean.Comment;
import com.hancy.house.bean.extend.CommentExtend;
import java.util.List;

/**
 * 评论相关
 * @author TomORrow
 */
public interface CommentExtendMapper {
  /**
   * 根据ID查询
   * @param article_id 资讯ID
   * @return list数据
   */
  List<Comment> selectByArticleId(long article_id);

  /**
   * 分页查询
   * @param page 当前页
   * @param pageSize 页面大小
   * @param keywords 关键字
   * @return list数据
   */
  List<CommentExtend> pageQuery(int page, int pageSize, String keywords);

  /**
   * 计算数量
   * @param keywords 关键字
   * @return long数据
   */
  long count(String keywords);
}


