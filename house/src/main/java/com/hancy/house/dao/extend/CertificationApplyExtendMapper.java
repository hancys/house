package com.hancy.house.dao.extend;

import com.hancy.house.bean.CertificationApply;
import java.util.List;

/**
 * 实名认证
 * @author TomORrow
 */
public interface CertificationApplyExtendMapper {
  /**
   * 分页查询
   * @param page 当前页
   * @param pageSize 页面数量
   * @param status 状态
   * @param userId 用户ID
   * @param beginTime 开始时间
   * @param endTime 结束时间
   * @return list数据
   */
  List<CertificationApply> pageQuery(int page, int pageSize, String status, Long userId, Long beginTime, Long endTime);

  /**
   * 计算数量
   * @param status 状态
   * @param userId 用户ID
   * @param beginTime 开始时间
   * @param endTime 结束时间
   * @return long数量
   */
  long count(String status, Long userId, Long beginTime, Long endTime);
}


