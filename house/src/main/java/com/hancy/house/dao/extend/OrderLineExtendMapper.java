package com.hancy.house.dao.extend;

import com.hancy.house.bean.OrderLine;
import java.util.List;

/**
 * 订单
 * @author TomORrow
 */
public interface OrderLineExtendMapper {
  /**
   * 订单查询
   * @param orderId 订单ID
   * @return
   */
  List<OrderLine> selectByOrderId(Long orderId);
}


