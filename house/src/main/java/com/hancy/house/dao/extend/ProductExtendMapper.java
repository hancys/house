package com.hancy.house.dao.extend;

import com.hancy.house.bean.extend.ProductExtend;
import java.util.List;

/**
 * 产品相关
 * @author TomORrow
 */
public interface ProductExtendMapper {
  /**
   * 分页查询
   * @param page 当前页
   * @param pageSize 页面大小
   * @param name 名字
   * @param status 状态
   * @param productCategoryId 产品分类ID
   * @return 分页数据
   */
  List<ProductExtend> pageQuery(int page, int pageSize, String name, String status, Long productCategoryId);

  /**
   * 计算数量
   * @param name 名字
   * @param status 状态
   * @param productCategoryId 产品分类ID
   * @return 数量
   */
  long count(String name, String status, Long productCategoryId);
}
