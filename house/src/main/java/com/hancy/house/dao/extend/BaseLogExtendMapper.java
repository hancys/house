package com.hancy.house.dao.extend;

import com.hancy.house.bean.BaseLog;

import java.util.List;

/**
 * 日志相关
 * @author TomORrow
 */
public interface BaseLogExtendMapper {
    /**
     * 分页查询
     * @param page 当前页
     * @param pageSize 页面大小
     * @return 分页数据
     */
    List<BaseLog> pageQuery(int page, int pageSize);

    /**
     * 计算数量
     * @return 数量
     */
    long count();
}

