package com.hancy.house.bean.extend;

import com.hancy.house.bean.BasePrivilege;
import com.hancy.house.bean.BaseRole;

import java.util.List;

public class BaseRoleExtend extends BaseRole {
    private static final long serialVersionUID = 1L;
    private List<BasePrivilege> privileges;

    public List<BasePrivilege> getPrivileges() {
        return this.privileges;
    }

    public void setPrivileges(List<BasePrivilege> privileges) {
        this.privileges = privileges;
    }
}
