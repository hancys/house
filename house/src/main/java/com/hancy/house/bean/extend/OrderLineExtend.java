package com.hancy.house.bean.extend;

import com.hancy.house.bean.OrderLine;
import com.hancy.house.bean.Product;


public class OrderLineExtend extends OrderLine {
    private static final long serialVersionUID = 1L;
    private Product product;

    public Product getProduct() {
        return this.product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
