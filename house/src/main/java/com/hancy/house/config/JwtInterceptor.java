package com.hancy.house.config;

import com.hancy.house.bean.BasePrivilege;
import com.hancy.house.service.IBasePrivilegeService;
import com.hancy.house.jacky.JwtTokenUtil;
import com.hancy.house.jacky.PermissionException;
import com.hancy.house.jacky.UnAuthorizedException;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;


/**
 * JWT拦截器
 * @author TomORrow
 */
public class JwtInterceptor implements HandlerInterceptor {
    @Autowired
    private IBasePrivilegeService basePrivilegeService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (request.getMethod().equals("OPTIONS")) {
            response.setStatus(200);
            return true;
        }
        String token = request.getHeader("Authorization");
        if (StringUtils.isEmpty(token)) {
            throw new UnAuthorizedException("用户还未登录");
        }
        JwtTokenUtil.parseJWT(token, "MDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjY=");
        return true;
    }

    private boolean auth(long userId, String path) {
        List<BasePrivilege> privileges = this.basePrivilegeService.findByUserId(userId);
        for (BasePrivilege p : privileges) {
            if (p.getRoute().matches(path)) {
                return true;
            }
        }
        throw new PermissionException("权限不足");
    }
}
