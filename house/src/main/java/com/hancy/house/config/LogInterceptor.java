package com.hancy.house.config;

import com.hancy.house.bean.BaseLog;
import com.hancy.house.bean.extend.BaseUserExtend;
import com.hancy.house.service.IBaseLogService;
import com.hancy.house.service.IBaseUserService;
import com.hancy.house.jacky.JwtTokenUtil;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


/**
 * 日志拦截器
 * @author TomORrow
 */
public class LogInterceptor implements HandlerInterceptor {
    @Autowired
    private IBaseUserService baseUserService;
    @Autowired
    private IBaseLogService logService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("Authorization");
        if (token != null) {
            long id = Long.parseLong(JwtTokenUtil.getUserId(token, "MDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjY="));
            BaseUserExtend user = this.baseUserService.findById(id);

            String uri = request.getRequestURI();
            String method = request.getMethod();
            if (!uri.contains("Query") && !uri.contains("find")) {
                String realname = user.getRealname();
                String content = uri;
                BaseLog log = new BaseLog();
                log.setRealname(realname);
                log.setMethod(method);
                log.setContent(content);
                log.setLogTime(Long.valueOf((new Date()).getTime()));
                this.logService.save(log);
            }
        }
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }
}


