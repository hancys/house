package com.hancy.house.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * mybatis 配置类
 * @author TomORrow
 */
@Configuration
@MapperScan({"com.hancy.house.dao"})
public class MybatisConfig {
}


