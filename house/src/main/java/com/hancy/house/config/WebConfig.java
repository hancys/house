package com.hancy.house.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * mvc控制器
 *
 * @author TomORrow
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Bean
    public JwtInterceptor jwtInterceptor() {
        return new JwtInterceptor();
    }

    @Bean
    public LogInterceptor logInterceptor() {
        return new LogInterceptor();
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor((HandlerInterceptor) logInterceptor())
                .addPathPatterns(new String[]{"/**"
                }).excludePathPatterns(new String[]{"/swagger-resources/**", "/v2/**", "/swagger-ui.html", "/webjars/**"});

        registry.addInterceptor((HandlerInterceptor) jwtInterceptor())
                .addPathPatterns(new String[]{"/**"
                }).excludePathPatterns(new String[]{
                "/file/**", "/user/register", "/index/**", "/swagger-resources/**", "/v2/**", "/swagger-ui.html", "/webjars/**", "/user/login", "/user/logout", "/article/download",
                "/privilege/findPrivilegeTree"});
    }


    /**
     * 跨域问题
     * @param registry 跨域
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOriginPatterns(new String[]{"*"
                }).allowedMethods(new String[]{"GET", "POST", "PUT", "OPTIONS", "DELETE", "PATCH"
        }).allowedHeaders(new String[]{"*"
        }).allowCredentials(true)
                .maxAge(3600L);
    }
}


