/*
 Navicat MySQL Data Transfer

 Source Server         : 47.102.208.199_3306
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : 47.102.208.199:3306
 Source Schema         : briup-ej

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 15/01/2022 21:24:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_config
-- ----------------------------
DROP TABLE IF EXISTS `base_config`;
CREATE TABLE `base_config`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `val` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `introduce` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name_un`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_config
-- ----------------------------
INSERT INTO `base_config` VALUES (1, 'sys_name', '家政服务平台', '系统名称');
INSERT INTO `base_config` VALUES (2, '系统描述', '123', NULL);
INSERT INTO `base_config` VALUES (5, 'logo', 'http://121.199.29.84:8888/group1/M00/00/22/rBD-SWCvIv2Aad66AAEqUXeJ28435.jpeg', '项目logo');

-- ----------------------------
-- Table structure for base_file
-- ----------------------------
DROP TABLE IF EXISTS `base_file`;
CREATE TABLE `base_file`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `file_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `group_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `upload_time` bigint(255) NULL DEFAULT NULL,
  `ext_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `file_size` bigint(20) NULL DEFAULT NULL,
  `file_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for base_log
-- ----------------------------
DROP TABLE IF EXISTS `base_log`;
CREATE TABLE `base_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `realname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `log_time` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4012 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_log
-- ----------------------------
INSERT INTO `base_log` VALUES (416, '张晓明', 'GET', '/user/info', 1603376727905);
INSERT INTO `base_log` VALUES (417, '张晓明', 'GET', '/error', 1603376771253);
INSERT INTO `base_log` VALUES (418, '张晓明', 'GET', '/error', 1603376771901);
INSERT INTO `base_log` VALUES (419, '张晓明', 'DELETE', '/privilege/deleteById', 1603376779677);
INSERT INTO `base_log` VALUES (420, '张晓明', 'DELETE', '/privilege/deleteById', 1603376786873);
INSERT INTO `base_log` VALUES (421, '张晓明', 'DELETE', '/privilege/deleteById', 1603376797574);
INSERT INTO `base_log` VALUES (422, '张晓明', 'GET', '/user/info', 1603376816834);
INSERT INTO `base_log` VALUES (423, '张晓明', 'GET', '/user/info', 1603376879483);
INSERT INTO `base_log` VALUES (424, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603376973419);
INSERT INTO `base_log` VALUES (425, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603377014576);
INSERT INTO `base_log` VALUES (426, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603377032570);
INSERT INTO `base_log` VALUES (427, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603377040060);
INSERT INTO `base_log` VALUES (428, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603377050387);
INSERT INTO `base_log` VALUES (429, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603377145223);
INSERT INTO `base_log` VALUES (430, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603377162453);
INSERT INTO `base_log` VALUES (431, '张晓明', 'GET', '/user/info', 1603377185620);
INSERT INTO `base_log` VALUES (432, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603377256062);
INSERT INTO `base_log` VALUES (433, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603377270706);
INSERT INTO `base_log` VALUES (434, '张晓明', 'GET', '/user/info', 1603377303739);
INSERT INTO `base_log` VALUES (435, '张晓明', 'GET', '/user/info', 1603377321183);
INSERT INTO `base_log` VALUES (436, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603377360838);
INSERT INTO `base_log` VALUES (437, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603377369047);
INSERT INTO `base_log` VALUES (438, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603377392379);
INSERT INTO `base_log` VALUES (439, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603377400733);
INSERT INTO `base_log` VALUES (440, '张晓明', 'POST', '/role/authorization', 1603377418252);
INSERT INTO `base_log` VALUES (441, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603377418294);
INSERT INTO `base_log` VALUES (442, '张晓明', 'GET', '/user/info', 1603377421452);
INSERT INTO `base_log` VALUES (443, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603377421851);
INSERT INTO `base_log` VALUES (444, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603377554480);
INSERT INTO `base_log` VALUES (445, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603377588213);
INSERT INTO `base_log` VALUES (446, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603377593094);
INSERT INTO `base_log` VALUES (447, '张晓明', 'POST', '/role/authorization', 1603377599536);
INSERT INTO `base_log` VALUES (448, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603377599565);
INSERT INTO `base_log` VALUES (449, '张晓明', 'GET', '/user/info', 1603377971839);
INSERT INTO `base_log` VALUES (450, '张晓明', 'GET', '/user/info', 1603378005119);
INSERT INTO `base_log` VALUES (451, '张晓明', 'GET', '/user/info', 1603378034927);
INSERT INTO `base_log` VALUES (452, '张晓明', 'GET', '/user/info', 1603378143748);
INSERT INTO `base_log` VALUES (453, '张晓明', 'GET', '/user/info', 1603378162841);
INSERT INTO `base_log` VALUES (454, '张晓明', 'GET', '/error', 1603378791850);
INSERT INTO `base_log` VALUES (455, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603413993375);
INSERT INTO `base_log` VALUES (456, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603414138417);
INSERT INTO `base_log` VALUES (457, '张晓明', 'GET', '/article/deleteById', 1603418618195);
INSERT INTO `base_log` VALUES (458, '张晓明', 'GET', '/user/info', 1603418803604);
INSERT INTO `base_log` VALUES (459, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603418929254);
INSERT INTO `base_log` VALUES (460, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603418937200);
INSERT INTO `base_log` VALUES (461, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603418950490);
INSERT INTO `base_log` VALUES (462, '张晓明', 'POST', '/role/authorization', 1603418955947);
INSERT INTO `base_log` VALUES (463, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603418955981);
INSERT INTO `base_log` VALUES (464, '张晓明', 'GET', '/user/info', 1603419049668);
INSERT INTO `base_log` VALUES (465, '张晓明', 'GET', '/user/info', 1603419180628);
INSERT INTO `base_log` VALUES (466, '张晓明', 'GET', '/user/info', 1603419237296);
INSERT INTO `base_log` VALUES (467, '张晓明', 'POST', '/comment/saveOrUpdate', 1603423381618);
INSERT INTO `base_log` VALUES (468, '张晓明', 'GET', '/user/info', 1603423480196);
INSERT INTO `base_log` VALUES (469, '张晓明', 'GET', '/article/cascadeFindAll', 1603424031234);
INSERT INTO `base_log` VALUES (470, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603440611592);
INSERT INTO `base_log` VALUES (471, '张晓明', 'POST', '/comment/saveOrUpdate', 1603440640650);
INSERT INTO `base_log` VALUES (472, '张晓明', 'GET', '/user/info', 1603442778823);
INSERT INTO `base_log` VALUES (473, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603442893556);
INSERT INTO `base_log` VALUES (474, '张晓明', 'POST', '/role/authorization', 1603442899997);
INSERT INTO `base_log` VALUES (475, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603442900067);
INSERT INTO `base_log` VALUES (476, '张晓明', 'GET', '/user/info', 1603442903228);
INSERT INTO `base_log` VALUES (477, '张晓明', 'GET', '/user/info', 1603442925304);
INSERT INTO `base_log` VALUES (478, '张晓明', 'POST', '/baseUser/saveOrUpdate', 1603443074506);
INSERT INTO `base_log` VALUES (479, '张晓明', 'POST', '/baseUser/setRoles', 1603443082910);
INSERT INTO `base_log` VALUES (480, '张晓明', 'POST', '/baseUser/saveOrUpdate', 1603443150084);
INSERT INTO `base_log` VALUES (481, '张晓明', 'POST', '/baseUser/setRoles', 1603443157503);
INSERT INTO `base_log` VALUES (482, '张晓明', 'GET', '/user/info', 1603445712176);
INSERT INTO `base_log` VALUES (483, '张晓明', 'GET', '/user/info', 1603446367273);
INSERT INTO `base_log` VALUES (484, '张晓明', 'GET', '/user/info', 1603465160233);
INSERT INTO `base_log` VALUES (485, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603465220459);
INSERT INTO `base_log` VALUES (486, '张晓明', 'GET', '/user/info', 1603466321483);
INSERT INTO `base_log` VALUES (487, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603466394617);
INSERT INTO `base_log` VALUES (488, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603466402346);
INSERT INTO `base_log` VALUES (489, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603466409738);
INSERT INTO `base_log` VALUES (490, '张晓明', 'GET', '/role/deleteById', 1603466412566);
INSERT INTO `base_log` VALUES (491, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603466412611);
INSERT INTO `base_log` VALUES (492, '张晓明', 'GET', '/role/deleteById', 1603466414828);
INSERT INTO `base_log` VALUES (493, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603466414850);
INSERT INTO `base_log` VALUES (494, '张晓明', 'GET', '/user/info', 1603466486805);
INSERT INTO `base_log` VALUES (495, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603466487235);
INSERT INTO `base_log` VALUES (496, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603466617935);
INSERT INTO `base_log` VALUES (497, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603466632130);
INSERT INTO `base_log` VALUES (498, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603466636738);
INSERT INTO `base_log` VALUES (499, '张晓明', 'POST', '/role/authorization', 1603466644795);
INSERT INTO `base_log` VALUES (500, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603466644830);
INSERT INTO `base_log` VALUES (501, '张晓明', 'GET', '/user/info', 1603466696859);
INSERT INTO `base_log` VALUES (502, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603466697282);
INSERT INTO `base_log` VALUES (503, '张晓明', 'GET', '/user/info', 1603466708388);
INSERT INTO `base_log` VALUES (504, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603466708754);
INSERT INTO `base_log` VALUES (505, '张晓明', 'GET', '/user/info', 1603466720594);
INSERT INTO `base_log` VALUES (506, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603466720957);
INSERT INTO `base_log` VALUES (507, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603466788155);
INSERT INTO `base_log` VALUES (508, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603466798402);
INSERT INTO `base_log` VALUES (509, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603466836305);
INSERT INTO `base_log` VALUES (510, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603466849877);
INSERT INTO `base_log` VALUES (511, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603466855094);
INSERT INTO `base_log` VALUES (512, '张晓明', 'POST', '/role/authorization', 1603466861718);
INSERT INTO `base_log` VALUES (513, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603466861748);
INSERT INTO `base_log` VALUES (514, '张晓明', 'POST', '/baseUser/saveOrUpdate', 1603466882449);
INSERT INTO `base_log` VALUES (515, '张晓明', 'POST', '/baseUser/saveOrUpdate', 1603466902311);
INSERT INTO `base_log` VALUES (516, '张晓明', 'POST', '/baseUser/setRoles', 1603466908558);
INSERT INTO `base_log` VALUES (517, '张晓明', 'POST', '/baseUser/saveOrUpdate', 1603466928030);
INSERT INTO `base_log` VALUES (518, '张晓明', 'POST', '/baseUser/setRoles', 1603466933866);
INSERT INTO `base_log` VALUES (519, '张晓明', 'POST', '/user/logout', 1603466936147);
INSERT INTO `base_log` VALUES (520, '张晓明同学', 'GET', '/user/info', 1603466941355);
INSERT INTO `base_log` VALUES (521, '张晓明同学', 'GET', '/project/delete', 1603467150017);
INSERT INTO `base_log` VALUES (522, '张晓明同学', 'GET', '/user/info', 1603467298786);
INSERT INTO `base_log` VALUES (523, '张晓明同学', 'GET', '/user/info', 1603467380292);
INSERT INTO `base_log` VALUES (524, '张晓明同学', 'GET', '/user/info', 1603467380445);
INSERT INTO `base_log` VALUES (525, '张晓明同学', 'POST', '/project/apply', 1603468455078);
INSERT INTO `base_log` VALUES (526, '张晓明同学', 'POST', '/project/apply', 1603468502107);
INSERT INTO `base_log` VALUES (527, '张晓明', 'GET', '/project/selectApply', 1603468567692);
INSERT INTO `base_log` VALUES (528, '张晓明', 'GET', '/project/selectApply', 1603468594946);
INSERT INTO `base_log` VALUES (529, '张晓明', 'GET', '/project/selectApply', 1603468608665);
INSERT INTO `base_log` VALUES (530, '张晓明', 'GET', '/project/selectApply', 1603468608830);
INSERT INTO `base_log` VALUES (531, '张晓明', 'GET', '/project/selectApply', 1603468609011);
INSERT INTO `base_log` VALUES (532, '张晓明', 'GET', '/project/selectApply', 1603468609193);
INSERT INTO `base_log` VALUES (533, '张晓明', 'GET', '/project/selectApply', 1603468649427);
INSERT INTO `base_log` VALUES (534, '张晓明', 'GET', '/project/selectApply', 1603468690184);
INSERT INTO `base_log` VALUES (535, '张晓明同学', 'POST', '/user/logout', 1603468707999);
INSERT INTO `base_log` VALUES (536, '张晓明', 'GET', '/user/info', 1603468712199);
INSERT INTO `base_log` VALUES (537, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603468757610);
INSERT INTO `base_log` VALUES (538, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603468787426);
INSERT INTO `base_log` VALUES (539, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603468802704);
INSERT INTO `base_log` VALUES (540, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603468805937);
INSERT INTO `base_log` VALUES (541, '张晓明', 'POST', '/role/authorization', 1603468812438);
INSERT INTO `base_log` VALUES (542, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603468812486);
INSERT INTO `base_log` VALUES (543, '张晓明', 'GET', '/user/info', 1603468828971);
INSERT INTO `base_log` VALUES (544, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603468829330);
INSERT INTO `base_log` VALUES (545, '张晓明', 'POST', '/user/logout', 1603468847635);
INSERT INTO `base_log` VALUES (546, '张晓明同学', 'GET', '/user/info', 1603468851951);
INSERT INTO `base_log` VALUES (547, '张晓明同学', 'GET', '/project/selectApply', 1603469063241);
INSERT INTO `base_log` VALUES (548, '张晓明同学', 'GET', '/project/selectApply', 1603469076243);
INSERT INTO `base_log` VALUES (549, '张晓明同学', 'GET', '/project/selectApply', 1603469114647);
INSERT INTO `base_log` VALUES (550, '张晓明同学', 'GET', '/project/selectApply', 1603469147867);
INSERT INTO `base_log` VALUES (551, '张晓明同学', 'GET', '/project/selectApply', 1603469419720);
INSERT INTO `base_log` VALUES (552, '张晓明同学', 'GET', '/project/selectApply', 1603469450942);
INSERT INTO `base_log` VALUES (553, '张晓明同学', 'GET', '/project/selectApply', 1603469467076);
INSERT INTO `base_log` VALUES (554, '张晓明同学', 'POST', '/user/logout', 1603469507168);
INSERT INTO `base_log` VALUES (555, '张晓明', 'GET', '/user/info', 1603469510737);
INSERT INTO `base_log` VALUES (556, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603469523586);
INSERT INTO `base_log` VALUES (557, '张晓明', 'POST', '/user/logout', 1603469529374);
INSERT INTO `base_log` VALUES (558, '张晓明同学', 'GET', '/user/info', 1603469534355);
INSERT INTO `base_log` VALUES (559, '张晓明同学', 'GET', '/project/selectApply', 1603469541537);
INSERT INTO `base_log` VALUES (560, '张晓明同学', 'GET', '/user/info', 1603469577155);
INSERT INTO `base_log` VALUES (561, '张晓明同学', 'GET', '/user/info', 1603469577543);
INSERT INTO `base_log` VALUES (562, '张晓明同学', 'GET', '/project/selectApply', 1603469577883);
INSERT INTO `base_log` VALUES (563, '张晓明同学', 'GET', '/user/info', 1603469578758);
INSERT INTO `base_log` VALUES (564, '张晓明同学', 'GET', '/project/selectApply', 1603469583274);
INSERT INTO `base_log` VALUES (565, '张晓明同学', 'GET', '/project/selectApply', 1603469587119);
INSERT INTO `base_log` VALUES (566, '张晓明同学', 'GET', '/project/selectApply', 1603493606861);
INSERT INTO `base_log` VALUES (567, '张晓明同学', 'GET', '/project/selectApply', 1603493610165);
INSERT INTO `base_log` VALUES (568, '张晓明同学', 'GET', '/project/selectApply', 1603493685420);
INSERT INTO `base_log` VALUES (569, '张晓明同学', 'GET', '/project/selectApply', 1603493688412);
INSERT INTO `base_log` VALUES (570, '张晓明同学', 'GET', '/project/selectApply', 1603493697715);
INSERT INTO `base_log` VALUES (571, '张晓明同学', 'GET', '/project/selectApply', 1603493707918);
INSERT INTO `base_log` VALUES (572, '张晓明同学', 'GET', '/project/selectApply', 1603493711670);
INSERT INTO `base_log` VALUES (573, '张晓明同学', 'GET', '/project/selectApply', 1603493831501);
INSERT INTO `base_log` VALUES (574, '张晓明同学', 'GET', '/user/info', 1603493987012);
INSERT INTO `base_log` VALUES (575, '张晓明同学', 'GET', '/user/info', 1603493987055);
INSERT INTO `base_log` VALUES (576, '张晓明同学', 'POST', '/user/logout', 1603494009181);
INSERT INTO `base_log` VALUES (577, '张晓明', 'GET', '/user/info', 1603494018780);
INSERT INTO `base_log` VALUES (578, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603494064819);
INSERT INTO `base_log` VALUES (579, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603494073472);
INSERT INTO `base_log` VALUES (580, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603494075401);
INSERT INTO `base_log` VALUES (581, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603494123312);
INSERT INTO `base_log` VALUES (582, '张晓明', 'DELETE', '/privilege/deleteById', 1603494130655);
INSERT INTO `base_log` VALUES (583, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603494140870);
INSERT INTO `base_log` VALUES (584, '张晓明', 'POST', '/user/logout', 1603494169389);
INSERT INTO `base_log` VALUES (585, '张晓明同学', 'GET', '/user/info', 1603494174634);
INSERT INTO `base_log` VALUES (586, '张晓明同学', 'GET', '/user/info', 1603494182689);
INSERT INTO `base_log` VALUES (587, '张晓明', 'GET', '/user/info', 1603494218508);
INSERT INTO `base_log` VALUES (588, '张晓明', 'GET', '/user/info', 1603494222522);
INSERT INTO `base_log` VALUES (589, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603494229376);
INSERT INTO `base_log` VALUES (590, '张晓明', 'POST', '/role/authorization', 1603494239312);
INSERT INTO `base_log` VALUES (591, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603494239363);
INSERT INTO `base_log` VALUES (592, '张晓明同学', 'GET', '/user/info', 1603494245058);
INSERT INTO `base_log` VALUES (593, '张晓明同学', 'GET', '/project/selectApply', 1603494250571);
INSERT INTO `base_log` VALUES (594, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603494306716);
INSERT INTO `base_log` VALUES (595, '张晓明同学', 'GET', '/user/info', 1603494314786);
INSERT INTO `base_log` VALUES (596, '张晓明同学', 'GET', '/user/info', 1603494323864);
INSERT INTO `base_log` VALUES (597, '张晓明同学', 'GET', '/project/selectApply', 1603494579070);
INSERT INTO `base_log` VALUES (598, '张晓明同学', 'GET', '/project/selectApply', 1603494582595);
INSERT INTO `base_log` VALUES (599, '张晓明同学', 'GET', '/project/selectApply', 1603494586880);
INSERT INTO `base_log` VALUES (600, '张晓明同学', 'GET', '/user/info', 1603494638578);
INSERT INTO `base_log` VALUES (601, '张晓明同学', 'GET', '/user/info', 1603494641665);
INSERT INTO `base_log` VALUES (602, '张晓明同学', 'GET', '/project/selectApply', 1603494815122);
INSERT INTO `base_log` VALUES (603, '张晓明同学', 'GET', '/project/selectApply', 1603499524603);
INSERT INTO `base_log` VALUES (604, '张晓明同学', 'GET', '/user/info', 1603499541815);
INSERT INTO `base_log` VALUES (605, '张晓明同学', 'GET', '/project/selectApply', 1603499797317);
INSERT INTO `base_log` VALUES (606, '张晓明同学', 'GET', '/project/selectApply', 1603499803130);
INSERT INTO `base_log` VALUES (607, '张晓明同学', 'POST', '/article/saveOrUpdate', 1603500015864);
INSERT INTO `base_log` VALUES (608, '张晓明同学', 'GET', '/project/selectApply', 1603500300336);
INSERT INTO `base_log` VALUES (609, '张晓明同学', 'GET', '/project/selectApply', 1603500333690);
INSERT INTO `base_log` VALUES (610, '张晓明同学', 'GET', '/project/selectApply', 1603500408864);
INSERT INTO `base_log` VALUES (611, '张晓明同学', 'POST', '/article/saveOrUpdate', 1603500430416);
INSERT INTO `base_log` VALUES (612, '张晓明同学', 'GET', '/project/selectApply', 1603500516814);
INSERT INTO `base_log` VALUES (613, '张晓明同学', 'POST', '/article/saveOrUpdate', 1603500548614);
INSERT INTO `base_log` VALUES (614, '张晓明同学', 'GET', '/project/selectApply', 1603501610811);
INSERT INTO `base_log` VALUES (615, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603501678545);
INSERT INTO `base_log` VALUES (616, '张晓明同学', 'GET', '/user/info', 1603501723098);
INSERT INTO `base_log` VALUES (617, '张晓明', 'GET', '/user/info', 1603501723833);
INSERT INTO `base_log` VALUES (618, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603501734558);
INSERT INTO `base_log` VALUES (619, '张晓明', 'POST', '/role/authorization', 1603501742653);
INSERT INTO `base_log` VALUES (620, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603501742691);
INSERT INTO `base_log` VALUES (621, '张晓明同学', 'GET', '/user/info', 1603501749234);
INSERT INTO `base_log` VALUES (622, '张晓明同学', 'GET', '/project/selectApply', 1603501790165);
INSERT INTO `base_log` VALUES (623, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603501824366);
INSERT INTO `base_log` VALUES (624, '张晓明同学', 'GET', '/user/info', 1603501830363);
INSERT INTO `base_log` VALUES (625, '张晓明同学', 'GET', '/project/selectApply', 1603501891072);
INSERT INTO `base_log` VALUES (626, '张晓明同学', 'GET', '/project/selectApply', 1603501896607);
INSERT INTO `base_log` VALUES (627, '张晓明同学', 'GET', '/project/selectApply', 1603502023830);
INSERT INTO `base_log` VALUES (628, '张晓明同学', 'POST', '/project/apply', 1603502026951);
INSERT INTO `base_log` VALUES (629, '张晓明同学', 'GET', '/project/selectApply', 1603502029965);
INSERT INTO `base_log` VALUES (630, '张晓明同学', 'GET', '/project/selectApply', 1603502085354);
INSERT INTO `base_log` VALUES (631, '张晓明同学', 'GET', '/project/selectApply', 1603502132890);
INSERT INTO `base_log` VALUES (632, '张晓明同学', 'GET', '/project/selectApply', 1603502181993);
INSERT INTO `base_log` VALUES (633, '张晓明同学', 'GET', '/project/selectApply', 1603502281050);
INSERT INTO `base_log` VALUES (634, '张晓明同学', 'GET', '/project/selectApply', 1603505691285);
INSERT INTO `base_log` VALUES (635, '张晓明同学', 'GET', '/project/selectApply', 1603505719484);
INSERT INTO `base_log` VALUES (636, '张晓明同学', 'GET', '/project/selectApply', 1603505731541);
INSERT INTO `base_log` VALUES (637, '张晓明同学', 'GET', '/project/selectApply', 1603505736696);
INSERT INTO `base_log` VALUES (638, '张晓明同学', 'GET', '/project/selectApply', 1603505746817);
INSERT INTO `base_log` VALUES (639, '张晓明同学', 'GET', '/project/selectApply', 1603505754671);
INSERT INTO `base_log` VALUES (640, '张晓明同学', 'GET', '/project/selectApply', 1603505772031);
INSERT INTO `base_log` VALUES (641, '张晓明同学', 'GET', '/project/selectApply', 1603505774099);
INSERT INTO `base_log` VALUES (642, '张晓明同学', 'GET', '/project/selectApply', 1603505785946);
INSERT INTO `base_log` VALUES (643, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603505804508);
INSERT INTO `base_log` VALUES (644, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603505825160);
INSERT INTO `base_log` VALUES (645, '张晓明同学', 'GET', '/user/info', 1603505856552);
INSERT INTO `base_log` VALUES (646, '张晓明同学', 'GET', '/user/info', 1603505890770);
INSERT INTO `base_log` VALUES (647, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603505952567);
INSERT INTO `base_log` VALUES (648, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603505980736);
INSERT INTO `base_log` VALUES (649, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603505990624);
INSERT INTO `base_log` VALUES (650, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603506027619);
INSERT INTO `base_log` VALUES (651, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603506042890);
INSERT INTO `base_log` VALUES (652, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603506087743);
INSERT INTO `base_log` VALUES (653, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603506097679);
INSERT INTO `base_log` VALUES (654, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603506100249);
INSERT INTO `base_log` VALUES (655, '张晓明', 'POST', '/role/authorization', 1603506120456);
INSERT INTO `base_log` VALUES (656, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603506120488);
INSERT INTO `base_log` VALUES (657, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603506141203);
INSERT INTO `base_log` VALUES (658, '张晓明', 'POST', '/user/logout', 1603506154310);
INSERT INTO `base_log` VALUES (659, '刘敏老师', 'GET', '/user/info', 1603506163330);
INSERT INTO `base_log` VALUES (660, '张晓明同学', 'POST', '/user/logout', 1603506185203);
INSERT INTO `base_log` VALUES (661, '刘敏老师', 'GET', '/user/info', 1603506189351);
INSERT INTO `base_log` VALUES (662, '刘敏老师', 'POST', '/user/logout', 1603506194758);
INSERT INTO `base_log` VALUES (663, '张晓明', 'GET', '/user/info', 1603506200217);
INSERT INTO `base_log` VALUES (664, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603506203798);
INSERT INTO `base_log` VALUES (665, '张晓明', 'POST', '/role/authorization', 1603506217366);
INSERT INTO `base_log` VALUES (666, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603506217407);
INSERT INTO `base_log` VALUES (667, '刘敏老师', 'GET', '/user/info', 1603506264221);
INSERT INTO `base_log` VALUES (668, '张晓明', 'GET', '/user/info', 1603506270288);
INSERT INTO `base_log` VALUES (669, '刘敏老师', 'GET', '/user/info', 1603536835726);
INSERT INTO `base_log` VALUES (670, '刘敏老师', 'GET', '/user/info', 1603538009491);
INSERT INTO `base_log` VALUES (671, '刘敏老师', 'POST', '/user/logout', 1603538026860);
INSERT INTO `base_log` VALUES (672, '张晓明', 'GET', '/user/info', 1603538029206);
INSERT INTO `base_log` VALUES (673, '张晓明', 'GET', '/project/selectApply', 1603538155307);
INSERT INTO `base_log` VALUES (674, '张晓明', 'GET', '/project/selectApply', 1603538170493);
INSERT INTO `base_log` VALUES (675, '张晓明', 'GET', '/project/selectApply', 1603538251021);
INSERT INTO `base_log` VALUES (676, '张晓明', 'GET', '/project/selectApply', 1603538292642);
INSERT INTO `base_log` VALUES (677, '张晓明', 'GET', '/project/selectApply', 1603538311293);
INSERT INTO `base_log` VALUES (678, '张晓明', 'GET', '/project/selectApply', 1603538327164);
INSERT INTO `base_log` VALUES (679, '张晓明', 'GET', '/project/selectApply', 1603538447462);
INSERT INTO `base_log` VALUES (680, '张晓明', 'GET', '/project/selectApply', 1603538472930);
INSERT INTO `base_log` VALUES (681, '张晓明', 'GET', '/project/selectApply', 1603538477283);
INSERT INTO `base_log` VALUES (682, '张晓明', 'GET', '/project/selectApply', 1603538563378);
INSERT INTO `base_log` VALUES (683, '张晓明', 'GET', '/project/selectApply', 1603538632002);
INSERT INTO `base_log` VALUES (684, '张晓明', 'GET', '/project/selectApply', 1603538682467);
INSERT INTO `base_log` VALUES (685, '张晓明', 'GET', '/project/selectApply', 1603538753814);
INSERT INTO `base_log` VALUES (686, '张晓明', 'GET', '/project/selectApply', 1603538774384);
INSERT INTO `base_log` VALUES (687, '张晓明', 'GET', '/project/selectApply', 1603538857960);
INSERT INTO `base_log` VALUES (688, '张晓明', 'GET', '/project/selectApply', 1603538969775);
INSERT INTO `base_log` VALUES (689, '张晓明', 'GET', '/user/info', 1603539046835);
INSERT INTO `base_log` VALUES (690, '张晓明', 'GET', '/project/selectApply', 1603539047367);
INSERT INTO `base_log` VALUES (691, '张晓明', 'GET', '/project/selectApply', 1603539094833);
INSERT INTO `base_log` VALUES (692, '张晓明', 'GET', '/project/selectApply', 1603539209361);
INSERT INTO `base_log` VALUES (693, '张晓明', 'GET', '/user/info', 1603539212311);
INSERT INTO `base_log` VALUES (694, '张晓明', 'GET', '/user/info', 1603539250473);
INSERT INTO `base_log` VALUES (695, '张晓明', 'GET', '/project/selectApply', 1603539251791);
INSERT INTO `base_log` VALUES (696, '张晓明', 'GET', '/user/info', 1603539252860);
INSERT INTO `base_log` VALUES (697, '张晓明', 'GET', '/project/selectApply', 1603539256476);
INSERT INTO `base_log` VALUES (698, '张晓明', 'GET', '/project/selectApply', 1603539265172);
INSERT INTO `base_log` VALUES (699, '张晓明', 'GET', '/project/selectApply', 1603539268915);
INSERT INTO `base_log` VALUES (700, '张晓明', 'POST', '/user/logout', 1603540491996);
INSERT INTO `base_log` VALUES (701, '张晓明', 'GET', '/user/info', 1603540505322);
INSERT INTO `base_log` VALUES (702, '张晓明', 'GET', '/project/selectApply', 1603540507508);
INSERT INTO `base_log` VALUES (703, '张晓明', 'POST', '/user/logout', 1603540633979);
INSERT INTO `base_log` VALUES (704, '刘敏老师', 'GET', '/user/info', 1603540639366);
INSERT INTO `base_log` VALUES (705, '刘敏老师', 'POST', '/user/logout', 1603540667542);
INSERT INTO `base_log` VALUES (706, '张晓明', 'GET', '/user/info', 1603540669928);
INSERT INTO `base_log` VALUES (707, '张晓明', 'GET', '/project/selectApply', 1603540674557);
INSERT INTO `base_log` VALUES (708, '张晓明', 'GET', '/project/selectApply', 1603541069619);
INSERT INTO `base_log` VALUES (709, '张晓明', 'GET', '/project/selectApply', 1603541154415);
INSERT INTO `base_log` VALUES (710, '张晓明', 'GET', '/project/selectApply', 1603541161656);
INSERT INTO `base_log` VALUES (711, '张晓明', 'GET', '/project/selectApply', 1603541215144);
INSERT INTO `base_log` VALUES (712, '张晓明', 'GET', '/project/selectApply', 1603541217268);
INSERT INTO `base_log` VALUES (713, '张晓明', 'GET', '/project/selectApply', 1603541249497);
INSERT INTO `base_log` VALUES (714, '张晓明', 'GET', '/project/selectApply', 1603541359025);
INSERT INTO `base_log` VALUES (715, '张晓明', 'GET', '/project/selectApply', 1603541664734);
INSERT INTO `base_log` VALUES (716, '张晓明', 'GET', '/project/selectApply', 1603541667586);
INSERT INTO `base_log` VALUES (717, '张晓明', 'GET', '/project/selectApply', 1603541683385);
INSERT INTO `base_log` VALUES (718, '张晓明', 'POST', '/user/logout', 1603541694180);
INSERT INTO `base_log` VALUES (719, '刘敏老师', 'GET', '/user/info', 1603541700260);
INSERT INTO `base_log` VALUES (720, '刘敏老师', 'GET', '/user/info', 1603541774965);
INSERT INTO `base_log` VALUES (721, '刘敏老师', 'POST', '/project/saveOrUpdate', 1603542571828);
INSERT INTO `base_log` VALUES (722, '刘敏老师', 'POST', '/project/saveOrUpdate', 1603542778445);
INSERT INTO `base_log` VALUES (723, '刘敏老师', 'POST', '/user/logout', 1603584170361);
INSERT INTO `base_log` VALUES (724, '张晓明', 'GET', '/user/info', 1603584175305);
INSERT INTO `base_log` VALUES (725, '张晓明', 'GET', '/project/selectApply', 1603584562072);
INSERT INTO `base_log` VALUES (726, '张晓明', 'GET', '/user/info', 1603586417345);
INSERT INTO `base_log` VALUES (727, '张晓明', 'GET', '/user/info', 1603586417959);
INSERT INTO `base_log` VALUES (728, '张晓明', 'GET', '/user/info', 1603586465551);
INSERT INTO `base_log` VALUES (729, '张晓明', 'GET', '/user/info', 1603586486606);
INSERT INTO `base_log` VALUES (730, '张晓明', 'GET', '/user/info', 1603586487430);
INSERT INTO `base_log` VALUES (731, '张晓明', 'POST', '/baseUser/saveOrUpdate', 1603586729715);
INSERT INTO `base_log` VALUES (732, '张晓明', 'GET', '/user/info', 1603586729826);
INSERT INTO `base_log` VALUES (733, '张晓明', 'POST', '/baseUser/saveOrUpdate', 1603586775532);
INSERT INTO `base_log` VALUES (734, '张晓明', 'GET', '/user/info', 1603586775624);
INSERT INTO `base_log` VALUES (735, '张晓明', 'POST', '/baseUser/saveOrUpdate', 1603586863095);
INSERT INTO `base_log` VALUES (736, '张晓明', 'POST', '/baseUser/saveOrUpdate', 1603586879963);
INSERT INTO `base_log` VALUES (737, '张晓明', 'POST', '/baseUser/saveOrUpdate', 1603586909400);
INSERT INTO `base_log` VALUES (738, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603586991194);
INSERT INTO `base_log` VALUES (739, '张晓明', 'POST', '/role/saveOrUpdate', 1603587000823);
INSERT INTO `base_log` VALUES (740, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603587000851);
INSERT INTO `base_log` VALUES (741, '张晓明', 'POST', '/baseUser/addUserWidthRole', 1603587819877);
INSERT INTO `base_log` VALUES (742, '张晓明', 'POST', '/baseUser/addUserWidthRole', 1603588232674);
INSERT INTO `base_log` VALUES (743, '张晓明', 'POST', '/baseUser/alterUserface', 1603589009162);
INSERT INTO `base_log` VALUES (744, '张晓明', 'POST', '/baseUser/alterUserface', 1603589030512);
INSERT INTO `base_log` VALUES (745, '张晓明', 'POST', '/baseUser/alterUserface', 1603589055901);
INSERT INTO `base_log` VALUES (746, '张晓明', 'POST', '/user/logout', 1603589071144);
INSERT INTO `base_log` VALUES (747, '王萌导师', 'GET', '/user/info', 1603589075554);
INSERT INTO `base_log` VALUES (748, '王萌导师', 'POST', '/user/logout', 1603589078592);
INSERT INTO `base_log` VALUES (749, '张晓明', 'GET', '/user/info', 1603589081174);
INSERT INTO `base_log` VALUES (750, '张晓明', 'GET', '/project/selectApply', 1603589083848);
INSERT INTO `base_log` VALUES (751, '张晓明', 'GET', '/user/info', 1603589334724);
INSERT INTO `base_log` VALUES (752, '张晓明', 'GET', '/user/info', 1603589336357);
INSERT INTO `base_log` VALUES (753, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603589364098);
INSERT INTO `base_log` VALUES (754, '张晓明', 'POST', '/privilege/saveOrUpdate', 1603589376954);
INSERT INTO `base_log` VALUES (755, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603589419307);
INSERT INTO `base_log` VALUES (756, '张晓明', 'POST', '/role/authorization', 1603589427475);
INSERT INTO `base_log` VALUES (757, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603589427522);
INSERT INTO `base_log` VALUES (758, '张晓明', 'POST', '/role/authorization', 1603589438579);
INSERT INTO `base_log` VALUES (759, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603589438608);
INSERT INTO `base_log` VALUES (760, '张晓明', 'POST', '/role/authorization', 1603589448753);
INSERT INTO `base_log` VALUES (761, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603589448782);
INSERT INTO `base_log` VALUES (762, '张晓明', 'GET', '/user/info', 1603589450741);
INSERT INTO `base_log` VALUES (763, '张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603589451144);
INSERT INTO `base_log` VALUES (764, '张晓明', 'POST', '/baseUser/saveOrUpdate', 1603589542431);
INSERT INTO `base_log` VALUES (765, '张晓明1', 'GET', '/user/info', 1603589542547);
INSERT INTO `base_log` VALUES (766, '张晓明1', 'POST', '/baseUser/saveOrUpdate', 1603589745072);
INSERT INTO `base_log` VALUES (767, '张晓明1', 'POST', '/baseUser/saveOrUpdate', 1603589759645);
INSERT INTO `base_log` VALUES (768, '张晓明', 'GET', '/user/info', 1603589759687);
INSERT INTO `base_log` VALUES (769, '张晓明', 'POST', '/baseUser/saveOrUpdate', 1603589767113);
INSERT INTO `base_log` VALUES (770, '1张晓明11', 'GET', '/user/info', 1603589767147);
INSERT INTO `base_log` VALUES (771, '1张晓明11', 'GET', '/role/cascadePrivilegeFindAll', 1603589785357);
INSERT INTO `base_log` VALUES (772, '1张晓明11', 'GET', '/role/cascadePrivilegeFindAll', 1603589833571);
INSERT INTO `base_log` VALUES (773, '1张晓明11', 'GET', '/user/info', 1603589839608);
INSERT INTO `base_log` VALUES (774, '1张晓明11', 'POST', '/baseUser/saveOrUpdate', 1603589845195);
INSERT INTO `base_log` VALUES (775, '1张晓明', 'GET', '/user/info', 1603589845240);
INSERT INTO `base_log` VALUES (776, '1张晓明', 'GET', '/user/info', 1603589849979);
INSERT INTO `base_log` VALUES (777, '1张晓明', 'POST', '/baseUser/saveOrUpdate', 1603589955189);
INSERT INTO `base_log` VALUES (778, '1张晓明', 'POST', '/baseUser/alterUserface', 1603590009792);
INSERT INTO `base_log` VALUES (779, '1张晓明', 'GET', '/user/info', 1603590009853);
INSERT INTO `base_log` VALUES (780, '1张晓明', 'POST', '/user/logout', 1603590017580);
INSERT INTO `base_log` VALUES (781, '王萌导师1', 'GET', '/user/info', 1603590023215);
INSERT INTO `base_log` VALUES (782, '王萌导师1', 'POST', '/baseUser/saveOrUpdate', 1603590031342);
INSERT INTO `base_log` VALUES (783, '王萌导师', 'GET', '/user/info', 1603590031401);
INSERT INTO `base_log` VALUES (784, '王萌导师', 'POST', '/project/saveOrUpdate', 1603613123079);
INSERT INTO `base_log` VALUES (785, '王萌导师', 'POST', '/project/saveOrUpdate', 1603613311596);
INSERT INTO `base_log` VALUES (786, '王萌导师', 'GET', '/user/info', 1603613581129);
INSERT INTO `base_log` VALUES (787, '王萌导师', 'POST', '/article/check', 1603614025450);
INSERT INTO `base_log` VALUES (788, '王萌导师', 'POST', '/article/check', 1603614063172);
INSERT INTO `base_log` VALUES (789, '1张晓明', 'GET', '/user/info', 1603614118013);
INSERT INTO `base_log` VALUES (790, '王萌导师', 'GET', '/user/info', 1603614119580);
INSERT INTO `base_log` VALUES (791, '王萌导师', 'POST', '/user/logout', 1603616767785);
INSERT INTO `base_log` VALUES (792, '1张晓明', 'GET', '/user/info', 1603616770431);
INSERT INTO `base_log` VALUES (793, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1603616821832);
INSERT INTO `base_log` VALUES (794, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603616824187);
INSERT INTO `base_log` VALUES (795, '1张晓明', 'POST', '/role/authorization', 1603616829891);
INSERT INTO `base_log` VALUES (796, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603616829924);
INSERT INTO `base_log` VALUES (797, '1张晓明', 'POST', '/role/authorization', 1603616836297);
INSERT INTO `base_log` VALUES (798, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603616836345);
INSERT INTO `base_log` VALUES (799, '1张晓明', 'POST', '/role/authorization', 1603616842606);
INSERT INTO `base_log` VALUES (800, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603616842635);
INSERT INTO `base_log` VALUES (801, '1张晓明', 'POST', '/user/logout', 1603616847378);
INSERT INTO `base_log` VALUES (802, '1张晓明', 'GET', '/user/info', 1603616849488);
INSERT INTO `base_log` VALUES (803, '1张晓明', 'POST', '/user/logout', 1603616959057);
INSERT INTO `base_log` VALUES (804, '刘敏老师', 'GET', '/user/info', 1603616967013);
INSERT INTO `base_log` VALUES (805, '刘敏老师', 'POST', '/user/logout', 1603617101959);
INSERT INTO `base_log` VALUES (806, '王萌导师', 'GET', '/user/info', 1603617105545);
INSERT INTO `base_log` VALUES (807, '王萌导师', 'POST', '/user/logout', 1603617113254);
INSERT INTO `base_log` VALUES (808, '1张晓明', 'GET', '/user/info', 1603617115318);
INSERT INTO `base_log` VALUES (809, '1张晓明', 'POST', '/user/logout', 1603617131030);
INSERT INTO `base_log` VALUES (810, '王萌导师', 'GET', '/user/info', 1603617134774);
INSERT INTO `base_log` VALUES (811, '王萌导师', 'POST', '/user/logout', 1603617900800);
INSERT INTO `base_log` VALUES (812, '1张晓明', 'GET', '/user/info', 1603617902845);
INSERT INTO `base_log` VALUES (813, '1张晓明', 'POST', '/user/logout', 1603618475205);
INSERT INTO `base_log` VALUES (814, '刘敏老师', 'GET', '/user/info', 1603618482023);
INSERT INTO `base_log` VALUES (815, '刘敏老师', 'POST', '/user/logout', 1603618502574);
INSERT INTO `base_log` VALUES (816, '1张晓明', 'GET', '/user/info', 1603618505939);
INSERT INTO `base_log` VALUES (817, '1张晓明', 'GET', '/project/selectApply', 1603618517491);
INSERT INTO `base_log` VALUES (818, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603618536645);
INSERT INTO `base_log` VALUES (819, '1张晓明', 'GET', '/project/selectApply', 1603619288120);
INSERT INTO `base_log` VALUES (820, '1张晓明', 'GET', '/user/info', 1603619323094);
INSERT INTO `base_log` VALUES (821, '1张晓明', 'POST', '/project/checkProject', 1603619327568);
INSERT INTO `base_log` VALUES (822, '1张晓明', 'GET', '/project/selectApply', 1603619331550);
INSERT INTO `base_log` VALUES (823, '1张晓明', 'POST', '/project/checkProject', 1603619336566);
INSERT INTO `base_log` VALUES (824, '1张晓明', 'POST', '/project/checkProject', 1603619365490);
INSERT INTO `base_log` VALUES (825, '1张晓明', 'POST', '/project/checkProject', 1603619371532);
INSERT INTO `base_log` VALUES (826, '1张晓明', 'GET', '/project/selectApply', 1603619493641);
INSERT INTO `base_log` VALUES (827, '1张晓明', 'GET', '/project/delete', 1603619542419);
INSERT INTO `base_log` VALUES (828, '1张晓明', 'GET', '/project/selectApply', 1603619559834);
INSERT INTO `base_log` VALUES (829, '1张晓明', 'GET', '/project/selectApply', 1603619565689);
INSERT INTO `base_log` VALUES (830, '1张晓明', 'POST', '/user/logout', 1603619744638);
INSERT INTO `base_log` VALUES (831, '张晓明同学', 'GET', '/user/info', 1603619771392);
INSERT INTO `base_log` VALUES (832, '张晓明同学', 'GET', '/project/selectApply', 1603619778434);
INSERT INTO `base_log` VALUES (833, '张晓明同学', 'GET', '/project/selectApply', 1603619782991);
INSERT INTO `base_log` VALUES (834, '张晓明同学', 'POST', '/project/apply', 1603619788686);
INSERT INTO `base_log` VALUES (835, '张晓明同学', 'GET', '/project/selectApply', 1603619790950);
INSERT INTO `base_log` VALUES (836, '张晓明同学', 'GET', '/project/selectApply', 1603619956385);
INSERT INTO `base_log` VALUES (837, '张晓明同学', 'GET', '/project/selectApply', 1603620011301);
INSERT INTO `base_log` VALUES (838, '张晓明同学', 'GET', '/project/deleteApply', 1603620029844);
INSERT INTO `base_log` VALUES (839, '张晓明同学', 'GET', '/project/selectApply', 1603620029911);
INSERT INTO `base_log` VALUES (840, '张晓明同学', 'GET', '/project/selectApply', 1603620051733);
INSERT INTO `base_log` VALUES (841, '张晓明同学', 'GET', '/project/selectApply', 1603620055247);
INSERT INTO `base_log` VALUES (842, '张晓明同学', 'POST', '/project/apply', 1603620081644);
INSERT INTO `base_log` VALUES (843, '张晓明同学', 'GET', '/project/selectApply', 1603620084034);
INSERT INTO `base_log` VALUES (844, '张晓明同学', 'POST', '/user/logout', 1603620087385);
INSERT INTO `base_log` VALUES (845, '1张晓明', 'GET', '/user/info', 1603620089391);
INSERT INTO `base_log` VALUES (846, '1张晓明', 'GET', '/project/delete', 1603620116461);
INSERT INTO `base_log` VALUES (847, '1张晓明', 'GET', '/project/selectApply', 1603620121785);
INSERT INTO `base_log` VALUES (848, '1张晓明', 'POST', '/user/logout', 1603620288584);
INSERT INTO `base_log` VALUES (849, '张晓明同学', 'GET', '/user/info', 1603620293766);
INSERT INTO `base_log` VALUES (850, '张晓明同学', 'POST', '/project/apply', 1603620307690);
INSERT INTO `base_log` VALUES (851, '张晓明同学', 'GET', '/project/selectApply', 1603620309676);
INSERT INTO `base_log` VALUES (852, '张晓明同学', 'POST', '/user/logout', 1603620317239);
INSERT INTO `base_log` VALUES (853, '1张晓明', 'GET', '/user/info', 1603620319180);
INSERT INTO `base_log` VALUES (854, '1张晓明', 'GET', '/project/selectApply', 1603620325484);
INSERT INTO `base_log` VALUES (855, '1张晓明', 'GET', '/project/delete', 1603620338325);
INSERT INTO `base_log` VALUES (856, '1张晓明', 'GET', '/project/selectApply', 1603620347643);
INSERT INTO `base_log` VALUES (857, '1张晓明', 'GET', '/project/selectApply', 1603620402490);
INSERT INTO `base_log` VALUES (858, '1张晓明', 'GET', '/project/selectApply', 1603620615319);
INSERT INTO `base_log` VALUES (859, '1张晓明', 'GET', '/project/selectApply', 1603620622733);
INSERT INTO `base_log` VALUES (860, '1张晓明', 'GET', '/project/selectApply', 1603620650805);
INSERT INTO `base_log` VALUES (861, '1张晓明', 'GET', '/project/selectApply', 1603620660292);
INSERT INTO `base_log` VALUES (862, '1张晓明', 'GET', '/project/selectApply', 1603620705595);
INSERT INTO `base_log` VALUES (863, '1张晓明', 'POST', '/comment/saveOrUpdate', 1603620743627);
INSERT INTO `base_log` VALUES (864, '1张晓明', 'POST', '/comment/saveOrUpdate', 1603620856666);
INSERT INTO `base_log` VALUES (865, '1张晓明', 'POST', '/comment/saveOrUpdate', 1603620870247);
INSERT INTO `base_log` VALUES (866, '1张晓明', 'POST', '/user/logout', 1603620917736);
INSERT INTO `base_log` VALUES (867, '学生4号', 'GET', '/user/info', 1603621347047);
INSERT INTO `base_log` VALUES (868, '学生4号', 'GET', '/project/selectApply', 1603621354023);
INSERT INTO `base_log` VALUES (869, '学生4号', 'POST', '/baseUser/saveOrUpdate', 1603621375306);
INSERT INTO `base_log` VALUES (870, '学生4号', 'GET', '/user/info', 1603621375367);
INSERT INTO `base_log` VALUES (871, '学生4号', 'POST', '/baseUser/alterUserface', 1603621384231);
INSERT INTO `base_log` VALUES (872, '学生4号', 'GET', '/user/info', 1603621384284);
INSERT INTO `base_log` VALUES (873, '学生4号', 'POST', '/article/saveOrUpdate', 1603621410743);
INSERT INTO `base_log` VALUES (874, '学生4号', 'GET', '/user/info', 1603621498199);
INSERT INTO `base_log` VALUES (875, '学生4号', 'GET', '/user/info', 1603621589672);
INSERT INTO `base_log` VALUES (876, '1张晓明', 'GET', '/user/info', 1603621589956);
INSERT INTO `base_log` VALUES (877, '学生4号', 'GET', '/project/selectApply', 1603621659595);
INSERT INTO `base_log` VALUES (878, '学生4号', 'POST', '/article/saveOrUpdate', 1603621669191);
INSERT INTO `base_log` VALUES (879, '学生4号', 'GET', '/project/selectApply', 1603621679081);
INSERT INTO `base_log` VALUES (880, '学生4号', 'POST', '/project/apply', 1603621685214);
INSERT INTO `base_log` VALUES (881, '学生4号', 'GET', '/project/selectApply', 1603621686787);
INSERT INTO `base_log` VALUES (882, '学生4号', 'POST', '/user/logout', 1603621690386);
INSERT INTO `base_log` VALUES (883, '刘敏老师', 'GET', '/user/info', 1603621695296);
INSERT INTO `base_log` VALUES (884, '刘敏老师', 'POST', '/article/check', 1603621730765);
INSERT INTO `base_log` VALUES (885, '刘敏老师', 'POST', '/user/logout', 1603621733885);
INSERT INTO `base_log` VALUES (886, '1张晓明', 'GET', '/user/info', 1603621735938);
INSERT INTO `base_log` VALUES (887, '1张晓明', 'GET', '/project/selectApply', 1603621740177);
INSERT INTO `base_log` VALUES (888, '1张晓明', 'POST', '/project/checkApply', 1603621743095);
INSERT INTO `base_log` VALUES (889, '1张晓明', 'GET', '/project/selectApply', 1603621743125);
INSERT INTO `base_log` VALUES (890, '1张晓明', 'POST', '/project/checkApply', 1603621746373);
INSERT INTO `base_log` VALUES (891, '1张晓明', 'GET', '/project/selectApply', 1603621746400);
INSERT INTO `base_log` VALUES (892, '1张晓明', 'POST', '/user/logout', 1603621928862);
INSERT INTO `base_log` VALUES (893, '1张晓明', 'GET', '/user/info', 1603622896069);
INSERT INTO `base_log` VALUES (894, '1张晓明', 'POST', '/user/logout', 1603622901573);
INSERT INTO `base_log` VALUES (895, '1张晓明', 'GET', '/user/info', 1603623203365);
INSERT INTO `base_log` VALUES (896, '1张晓明', 'POST', '/user/logout', 1603623905771);
INSERT INTO `base_log` VALUES (897, '刘敏老师', 'GET', '/user/info', 1603624063093);
INSERT INTO `base_log` VALUES (898, '刘敏老师', 'POST', '/user/logout', 1603624096264);
INSERT INTO `base_log` VALUES (899, '张涛老师', 'GET', '/user/info', 1603624101399);
INSERT INTO `base_log` VALUES (900, '张涛老师', 'POST', '/user/logout', 1603624109877);
INSERT INTO `base_log` VALUES (901, '王萌导师', 'GET', '/user/info', 1603624114416);
INSERT INTO `base_log` VALUES (902, '王萌导师', 'POST', '/project/saveOrUpdate', 1603624228549);
INSERT INTO `base_log` VALUES (903, '王萌导师', 'POST', '/user/logout', 1603624236004);
INSERT INTO `base_log` VALUES (904, '1张晓明', 'GET', '/user/info', 1603624242762);
INSERT INTO `base_log` VALUES (905, '1张晓明', 'POST', '/project/checkProject', 1603624261483);
INSERT INTO `base_log` VALUES (906, '1张晓明', 'POST', '/user/logout', 1603624272642);
INSERT INTO `base_log` VALUES (907, '张小龙学生', 'GET', '/user/info', 1603624293853);
INSERT INTO `base_log` VALUES (908, '张小龙学生', 'POST', '/project/apply', 1603624302564);
INSERT INTO `base_log` VALUES (909, '张小龙学生', 'GET', '/project/selectApply', 1603624304487);
INSERT INTO `base_log` VALUES (910, '张小龙学生', 'POST', '/article/saveOrUpdate', 1603624332270);
INSERT INTO `base_log` VALUES (911, '张小龙学生', 'POST', '/article/saveOrUpdate', 1603624338229);
INSERT INTO `base_log` VALUES (912, '张小龙学生', 'POST', '/baseUser/alterUserface', 1603624361490);
INSERT INTO `base_log` VALUES (913, '张小龙学生', 'GET', '/user/info', 1603624361531);
INSERT INTO `base_log` VALUES (914, '张小龙学生', 'POST', '/baseUser/saveOrUpdate', 1603624375981);
INSERT INTO `base_log` VALUES (915, '张小龙学生', 'GET', '/user/info', 1603624376029);
INSERT INTO `base_log` VALUES (916, '张小龙学生', 'POST', '/user/logout', 1603624381380);
INSERT INTO `base_log` VALUES (917, '1张晓明', 'GET', '/user/info', 1603624388430);
INSERT INTO `base_log` VALUES (918, '1张晓明', 'POST', '/user/logout', 1603624399155);
INSERT INTO `base_log` VALUES (919, '王萌导师', 'GET', '/user/info', 1603624410146);
INSERT INTO `base_log` VALUES (920, '王萌导师', 'POST', '/article/check', 1603624418048);
INSERT INTO `base_log` VALUES (921, '王萌导师', 'POST', '/comment/saveOrUpdate', 1603624452723);
INSERT INTO `base_log` VALUES (922, '王萌导师', 'POST', '/user/logout', 1603624463555);
INSERT INTO `base_log` VALUES (923, '1张晓明', 'GET', '/user/info', 1603624468061);
INSERT INTO `base_log` VALUES (924, '1张晓明', 'POST', '/comment/saveOrUpdate', 1603624483919);
INSERT INTO `base_log` VALUES (925, '1张晓明', 'GET', '/project/selectApply', 1603624497249);
INSERT INTO `base_log` VALUES (926, '1张晓明', 'POST', '/baseUser/addUserWidthRole', 1603624573303);
INSERT INTO `base_log` VALUES (927, '1张晓明', 'POST', '/user/logout', 1603624728368);
INSERT INTO `base_log` VALUES (928, '1张晓明', 'GET', '/user/info', 1603624913651);
INSERT INTO `base_log` VALUES (929, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1603624931916);
INSERT INTO `base_log` VALUES (930, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1603624938826);
INSERT INTO `base_log` VALUES (931, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1603624942359);
INSERT INTO `base_log` VALUES (932, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1603624946185);
INSERT INTO `base_log` VALUES (933, '1张晓明', 'GET', '/user/info', 1603624948591);
INSERT INTO `base_log` VALUES (934, '1张晓明', 'GET', '/user/info', 1603624965604);
INSERT INTO `base_log` VALUES (935, '1张晓明', 'GET', '/user/info', 1603625004910);
INSERT INTO `base_log` VALUES (936, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603625006513);
INSERT INTO `base_log` VALUES (937, '1张晓明', 'GET', '/user/info', 1603625064667);
INSERT INTO `base_log` VALUES (938, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603625064896);
INSERT INTO `base_log` VALUES (939, '1张晓明', 'POST', '/role/authorization', 1603625083119);
INSERT INTO `base_log` VALUES (940, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1603625083164);
INSERT INTO `base_log` VALUES (941, '1张晓明', 'GET', '/user/info', 1603625085106);
INSERT INTO `base_log` VALUES (942, '1张晓明', 'GET', '/user/info', 1603625088874);
INSERT INTO `base_log` VALUES (943, '张晓明同学', 'GET', '/user/info', 1604474774734);
INSERT INTO `base_log` VALUES (944, '张晓明同学', 'GET', '/project/selectApply', 1604475013917);
INSERT INTO `base_log` VALUES (945, '张铭', 'GET', '/user/info', 1614230513006);
INSERT INTO `base_log` VALUES (946, '张铭', 'GET', '/user/info', 1614230513014);
INSERT INTO `base_log` VALUES (947, '张铭', 'GET', '/project/selectApply', 1614230522241);
INSERT INTO `base_log` VALUES (948, '张铭', 'POST', '/user/logout', 1614230529081);
INSERT INTO `base_log` VALUES (949, '1张晓明', 'GET', '/user/info', 1614230537372);
INSERT INTO `base_log` VALUES (950, '1张晓明', 'GET', '/user/info', 1614230657861);
INSERT INTO `base_log` VALUES (951, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1614230667015);
INSERT INTO `base_log` VALUES (952, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1614230677732);
INSERT INTO `base_log` VALUES (953, '1张晓明', 'GET', '/user/info', 1614234047769);
INSERT INTO `base_log` VALUES (954, '1张晓明', 'GET', '/user/info', 1614234291179);
INSERT INTO `base_log` VALUES (955, '1张晓明', 'GET', '/project/selectApply', 1614234300667);
INSERT INTO `base_log` VALUES (956, '1张晓明', 'GET', '/project/selectApply', 1614234303642);
INSERT INTO `base_log` VALUES (957, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1614234316895);
INSERT INTO `base_log` VALUES (958, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1614234326626);
INSERT INTO `base_log` VALUES (959, '1张晓明', 'POST', '/role/authorization', 1614234342227);
INSERT INTO `base_log` VALUES (960, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1614234342267);
INSERT INTO `base_log` VALUES (961, '1张晓明', 'GET', '/user/info', 1614234344601);
INSERT INTO `base_log` VALUES (962, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1614234344932);
INSERT INTO `base_log` VALUES (963, '1张晓明', 'GET', '/user/info', 1614238149464);
INSERT INTO `base_log` VALUES (964, '1张晓明', 'GET', '/error', 1614238155704);
INSERT INTO `base_log` VALUES (965, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1614238165275);
INSERT INTO `base_log` VALUES (966, '1张晓明', 'POST', '/user/logout', 1614238188445);
INSERT INTO `base_log` VALUES (967, '1张晓明', 'GET', '/user/info', 1614238254646);
INSERT INTO `base_log` VALUES (968, '1张晓明', 'GET', '/error', 1614238254840);
INSERT INTO `base_log` VALUES (969, '1张晓明', 'GET', '/user/info', 1614238299089);
INSERT INTO `base_log` VALUES (970, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1614238351157);
INSERT INTO `base_log` VALUES (971, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1614238362415);
INSERT INTO `base_log` VALUES (972, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1614238368363);
INSERT INTO `base_log` VALUES (973, '1张晓明', 'POST', '/role/authorization', 1614238375175);
INSERT INTO `base_log` VALUES (974, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1614238375206);
INSERT INTO `base_log` VALUES (975, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1614238388592);
INSERT INTO `base_log` VALUES (976, '1张晓明', 'GET', '/user/info', 1614238392022);
INSERT INTO `base_log` VALUES (977, '1张晓明', 'GET', '/error', 1614238398419);
INSERT INTO `base_log` VALUES (978, '1张晓明', 'DELETE', '/privilege/deleteById', 1614238409223);
INSERT INTO `base_log` VALUES (979, '1张晓明', 'DELETE', '/privilege/deleteById', 1614238412115);
INSERT INTO `base_log` VALUES (980, '1张晓明', 'DELETE', '/privilege/deleteById', 1614238415403);
INSERT INTO `base_log` VALUES (981, '1张晓明', 'DELETE', '/privilege/deleteById', 1614238418222);
INSERT INTO `base_log` VALUES (982, '1张晓明', 'GET', '/user/info', 1614238484054);
INSERT INTO `base_log` VALUES (983, '1张晓明', 'GET', '/user/info', 1614238504851);
INSERT INTO `base_log` VALUES (984, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1614238564515);
INSERT INTO `base_log` VALUES (985, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1614238586897);
INSERT INTO `base_log` VALUES (986, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1614238594928);
INSERT INTO `base_log` VALUES (987, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1614238640107);
INSERT INTO `base_log` VALUES (988, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1614238647675);
INSERT INTO `base_log` VALUES (989, '1张晓明', 'DELETE', '/privilege/deleteById', 1614238654984);
INSERT INTO `base_log` VALUES (990, '1张晓明', 'DELETE', '/privilege/deleteById', 1614238657855);
INSERT INTO `base_log` VALUES (991, '1张晓明', 'DELETE', '/privilege/deleteById', 1614238663550);
INSERT INTO `base_log` VALUES (992, '1张晓明', 'DELETE', '/privilege/deleteById', 1614238669372);
INSERT INTO `base_log` VALUES (993, '1张晓明', 'GET', '/user/info', 1614238675763);
INSERT INTO `base_log` VALUES (994, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1614238797088);
INSERT INTO `base_log` VALUES (995, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1614238803080);
INSERT INTO `base_log` VALUES (996, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1614238808453);
INSERT INTO `base_log` VALUES (997, '1张晓明', 'GET', '/user/info', 1614238810922);
INSERT INTO `base_log` VALUES (998, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1614238814904);
INSERT INTO `base_log` VALUES (999, '1张晓明', 'POST', '/role/authorization', 1614238823153);
INSERT INTO `base_log` VALUES (1000, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1614238823196);
INSERT INTO `base_log` VALUES (1001, '1张晓明', 'DELETE', '/privilege/deleteById', 1614238832659);
INSERT INTO `base_log` VALUES (1002, '1张晓明', 'GET', '/user/info', 1614238849867);
INSERT INTO `base_log` VALUES (1003, '1张晓明', 'GET', '/user/info', 1614238895767);
INSERT INTO `base_log` VALUES (1004, '1张晓明', 'GET', '/user/info', 1614242935963);
INSERT INTO `base_log` VALUES (1005, '1张晓明', 'GET', '/category/deleteById', 1614242965203);
INSERT INTO `base_log` VALUES (1006, '1张晓明', 'GET', '/article/deleteById', 1614242986811);
INSERT INTO `base_log` VALUES (1007, '1张晓明', 'POST', '/category/saveOrUpdate', 1614243151185);
INSERT INTO `base_log` VALUES (1008, '1张晓明', 'POST', '/category/saveOrUpdate', 1614243168469);
INSERT INTO `base_log` VALUES (1009, '1张晓明', 'POST', '/category/saveOrUpdate', 1614243178440);
INSERT INTO `base_log` VALUES (1010, '1张晓明', 'POST', '/category/saveOrUpdate', 1614243189945);
INSERT INTO `base_log` VALUES (1011, '1张晓明', 'POST', '/category/saveOrUpdate', 1614243210091);
INSERT INTO `base_log` VALUES (1012, '1张晓明', 'GET', '/article/deleteById', 1614243238907);
INSERT INTO `base_log` VALUES (1013, '1张晓明', 'GET', '/article/deleteById', 1614243240624);
INSERT INTO `base_log` VALUES (1014, '1张晓明', 'GET', '/article/deleteById', 1614243246238);
INSERT INTO `base_log` VALUES (1015, '1张晓明', 'GET', '/article/deleteById', 1614243248214);
INSERT INTO `base_log` VALUES (1016, '1张晓明', 'GET', '/article/deleteById', 1614243250192);
INSERT INTO `base_log` VALUES (1017, '1张晓明', 'GET', '/article/deleteById', 1614243255993);
INSERT INTO `base_log` VALUES (1018, '1张晓明', 'GET', '/article/deleteById', 1614243257851);
INSERT INTO `base_log` VALUES (1019, '1张晓明', 'GET', '/article/deleteById', 1614243260011);
INSERT INTO `base_log` VALUES (1020, '1张晓明', 'GET', '/article/deleteById', 1614243262352);
INSERT INTO `base_log` VALUES (1021, '1张晓明', 'GET', '/article/deleteById', 1614243264296);
INSERT INTO `base_log` VALUES (1022, '1张晓明', 'GET', '/article/deleteById', 1614243274436);
INSERT INTO `base_log` VALUES (1023, '1张晓明', 'GET', '/article/deleteById', 1614243276246);
INSERT INTO `base_log` VALUES (1024, '1张晓明', 'GET', '/article/deleteById', 1614243278047);
INSERT INTO `base_log` VALUES (1025, '1张晓明', 'GET', '/article/deleteById', 1614243280004);
INSERT INTO `base_log` VALUES (1026, '1张晓明', 'GET', '/article/deleteById', 1614243282590);
INSERT INTO `base_log` VALUES (1027, '1张晓明', 'GET', '/article/deleteById', 1614243285170);
INSERT INTO `base_log` VALUES (1028, '1张晓明', 'GET', '/article/deleteById', 1614243287439);
INSERT INTO `base_log` VALUES (1029, '1张晓明', 'GET', '/article/deleteById', 1614243289407);
INSERT INTO `base_log` VALUES (1030, '1张晓明', 'GET', '/article/deleteById', 1614243291552);
INSERT INTO `base_log` VALUES (1031, '1张晓明', 'GET', '/article/deleteById', 1614243294701);
INSERT INTO `base_log` VALUES (1032, '1张晓明', 'GET', '/article/deleteById', 1614243296571);
INSERT INTO `base_log` VALUES (1033, '1张晓明', 'GET', '/user/info', 1614243335550);
INSERT INTO `base_log` VALUES (1034, '1张晓明', 'GET', '/article/deleteById', 1614243339772);
INSERT INTO `base_log` VALUES (1035, '1张晓明', 'GET', '/article/deleteById', 1614243341880);
INSERT INTO `base_log` VALUES (1036, '1张晓明', 'GET', '/article/deleteById', 1614243344234);
INSERT INTO `base_log` VALUES (1037, '1张晓明', 'GET', '/article/deleteById', 1614243346915);
INSERT INTO `base_log` VALUES (1038, '1张晓明', 'POST', '/article/saveOrUpdate', 1614244776997);
INSERT INTO `base_log` VALUES (1039, '1张晓明', 'GET', '/user/info', 1614245136714);
INSERT INTO `base_log` VALUES (1040, '1张晓明', 'POST', '/article/saveOrUpdate', 1614245473346);
INSERT INTO `base_log` VALUES (1041, '1张晓明', 'GET', '/user/info', 1614301807418);
INSERT INTO `base_log` VALUES (1042, '1张晓明', 'GET', '/user/info', 1614302022200);
INSERT INTO `base_log` VALUES (1043, '1张晓明', 'GET', '/user/info', 1614302083058);
INSERT INTO `base_log` VALUES (1044, '1张晓明', 'POST', '/article/saveOrUpdate', 1614302165519);
INSERT INTO `base_log` VALUES (1045, '1张晓明', 'POST', '/article/saveOrUpdate', 1614302220642);
INSERT INTO `base_log` VALUES (1046, '1张晓明', 'POST', '/article/saveOrUpdate', 1614302256854);
INSERT INTO `base_log` VALUES (1047, '1张晓明', 'GET', '/article/deleteById', 1614302260317);
INSERT INTO `base_log` VALUES (1048, '1张晓明', 'POST', '/article/saveOrUpdate', 1614302278083);
INSERT INTO `base_log` VALUES (1049, '1张晓明', 'POST', '/article/saveOrUpdate', 1614302303312);
INSERT INTO `base_log` VALUES (1050, '1张晓明', 'POST', '/article/saveOrUpdate', 1614302340550);
INSERT INTO `base_log` VALUES (1051, '1张晓明', 'POST', '/article/saveOrUpdate', 1614302395825);
INSERT INTO `base_log` VALUES (1052, '1张晓明', 'POST', '/article/saveOrUpdate', 1614302450824);
INSERT INTO `base_log` VALUES (1053, '1张晓明', 'GET', '/user/info', 1614302503702);
INSERT INTO `base_log` VALUES (1054, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1614302528675);
INSERT INTO `base_log` VALUES (1055, '1张晓明', 'POST', '/article/saveOrUpdate', 1614302564445);
INSERT INTO `base_log` VALUES (1056, '1张晓明', 'POST', '/article/saveOrUpdate', 1614302564493);
INSERT INTO `base_log` VALUES (1057, '1张晓明', 'GET', '/user/info', 1614302590860);
INSERT INTO `base_log` VALUES (1058, '1张晓明', 'GET', '/user/info', 1614302593699);
INSERT INTO `base_log` VALUES (1059, '1张晓明', 'POST', '/article/saveOrUpdate', 1614302623425);
INSERT INTO `base_log` VALUES (1060, '1张晓明', 'GET', '/user/info', 1614302769705);
INSERT INTO `base_log` VALUES (1061, '1张晓明', 'POST', '/carousel/saveOrUpdate', 1614303536550);
INSERT INTO `base_log` VALUES (1062, '1张晓明', 'GET', '/user/info', 1620630068345);
INSERT INTO `base_log` VALUES (1063, '1张晓明', 'GET', '/user/info', 1620630105513);
INSERT INTO `base_log` VALUES (1064, '1张晓明', 'GET', '/user/info', 1620630105525);
INSERT INTO `base_log` VALUES (1065, '1张晓明', 'POST', '/user/logout', 1620630111406);
INSERT INTO `base_log` VALUES (1066, '1张晓明', 'GET', '/user/info', 1620630144719);
INSERT INTO `base_log` VALUES (1067, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620630241821);
INSERT INTO `base_log` VALUES (1068, '1张晓明', 'GET', '/user/info', 1620630540747);
INSERT INTO `base_log` VALUES (1069, '1张晓明', 'GET', '/user/info', 1620630540827);
INSERT INTO `base_log` VALUES (1070, '1张晓明', 'GET', '/user/info', 1620630780880);
INSERT INTO `base_log` VALUES (1071, '1张晓明', 'POST', '/role/saveOrUpdate', 1620630818925);
INSERT INTO `base_log` VALUES (1072, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620630819169);
INSERT INTO `base_log` VALUES (1073, '1张晓明', 'POST', '/role/saveOrUpdate', 1620630825287);
INSERT INTO `base_log` VALUES (1074, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620630825507);
INSERT INTO `base_log` VALUES (1075, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620630830592);
INSERT INTO `base_log` VALUES (1076, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620630989756);
INSERT INTO `base_log` VALUES (1077, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620631019250);
INSERT INTO `base_log` VALUES (1078, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620631043453);
INSERT INTO `base_log` VALUES (1079, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620631069783);
INSERT INTO `base_log` VALUES (1080, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620631114200);
INSERT INTO `base_log` VALUES (1081, '1张晓明', 'POST', '/role/authorization', 1620631131548);
INSERT INTO `base_log` VALUES (1082, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620631132215);
INSERT INTO `base_log` VALUES (1083, '1张晓明', 'POST', '/baseUser/saveOrUpdate', 1620631151042);
INSERT INTO `base_log` VALUES (1084, '1张晓明', 'POST', '/user/logout', 1620631156306);
INSERT INTO `base_log` VALUES (1085, '刘亮', 'GET', '/user/info', 1620631160721);
INSERT INTO `base_log` VALUES (1086, '刘亮', 'GET', '/user/info', 1620631238039);
INSERT INTO `base_log` VALUES (1087, '刘亮', 'POST', '/user/logout', 1620631247693);
INSERT INTO `base_log` VALUES (1088, '1张晓明', 'GET', '/user/info', 1620631251115);
INSERT INTO `base_log` VALUES (1089, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620631254384);
INSERT INTO `base_log` VALUES (1090, '1张晓明', 'GET', '/user/info', 1620631498543);
INSERT INTO `base_log` VALUES (1091, '1张晓明', 'GET', '/user/info', 1620631498670);
INSERT INTO `base_log` VALUES (1092, '1张晓明', 'GET', '/user/info', 1620632043714);
INSERT INTO `base_log` VALUES (1093, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620632113776);
INSERT INTO `base_log` VALUES (1094, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620632249634);
INSERT INTO `base_log` VALUES (1095, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620632278776);
INSERT INTO `base_log` VALUES (1096, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620632286624);
INSERT INTO `base_log` VALUES (1097, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620632309738);
INSERT INTO `base_log` VALUES (1098, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620632314894);
INSERT INTO `base_log` VALUES (1099, '1张晓明', 'POST', '/role/authorization', 1620632363606);
INSERT INTO `base_log` VALUES (1100, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620632364109);
INSERT INTO `base_log` VALUES (1101, '1张晓明', 'GET', '/user/info', 1620632367950);
INSERT INTO `base_log` VALUES (1102, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620632368396);
INSERT INTO `base_log` VALUES (1103, '1张晓明', 'GET', '/user/info', 1620632416206);
INSERT INTO `base_log` VALUES (1104, '1张晓明', 'GET', '/user/info', 1620632436999);
INSERT INTO `base_log` VALUES (1105, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620632446767);
INSERT INTO `base_log` VALUES (1106, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620632449821);
INSERT INTO `base_log` VALUES (1107, '1张晓明', 'GET', '/user/info', 1620632468766);
INSERT INTO `base_log` VALUES (1108, '1张晓明', 'GET', '/user/info', 1620632478323);
INSERT INTO `base_log` VALUES (1109, '1张晓明', 'GET', '/user/info', 1620632522659);
INSERT INTO `base_log` VALUES (1110, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620632547298);
INSERT INTO `base_log` VALUES (1111, '1张晓明', 'POST', '/role/authorization', 1620632559888);
INSERT INTO `base_log` VALUES (1112, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620632560027);
INSERT INTO `base_log` VALUES (1113, '1张晓明', 'GET', '/user/info', 1620632562938);
INSERT INTO `base_log` VALUES (1114, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620632563663);
INSERT INTO `base_log` VALUES (1115, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620633097643);
INSERT INTO `base_log` VALUES (1116, '1张晓明', 'GET', '/user/info', 1620633196656);
INSERT INTO `base_log` VALUES (1117, '1张晓明', 'GET', '/user/info', 1620633205705);
INSERT INTO `base_log` VALUES (1118, '1张晓明', 'GET', '/user/info', 1620633215372);
INSERT INTO `base_log` VALUES (1119, '1张晓明', 'GET', '/user/info', 1620633236404);
INSERT INTO `base_log` VALUES (1120, '1张晓明', 'GET', '/user/info', 1620633244832);
INSERT INTO `base_log` VALUES (1121, '1张晓明', 'GET', '/user/info', 1620633257061);
INSERT INTO `base_log` VALUES (1122, '1张晓明', 'GET', '/user/info', 1620633273951);
INSERT INTO `base_log` VALUES (1123, '1张晓明', 'GET', '/user/info', 1620633284889);
INSERT INTO `base_log` VALUES (1124, '1张晓明', 'GET', '/user/info', 1620633306925);
INSERT INTO `base_log` VALUES (1125, '1张晓明', 'GET', '/user/info', 1620633464064);
INSERT INTO `base_log` VALUES (1126, '1张晓明', 'GET', '/user/info', 1620633533097);
INSERT INTO `base_log` VALUES (1127, '1张晓明', 'GET', '/user/info', 1620633766114);
INSERT INTO `base_log` VALUES (1128, '1张晓明', 'GET', '/user/info', 1620633795871);
INSERT INTO `base_log` VALUES (1129, '1张晓明', 'GET', '/user/info', 1620633850185);
INSERT INTO `base_log` VALUES (1130, '1张晓明', 'GET', '/user/info', 1620633860567);
INSERT INTO `base_log` VALUES (1131, '1张晓明', 'GET', '/user/info', 1620633877911);
INSERT INTO `base_log` VALUES (1132, '1张晓明', 'GET', '/user/info', 1620633882597);
INSERT INTO `base_log` VALUES (1133, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620633899151);
INSERT INTO `base_log` VALUES (1134, '1张晓明', 'GET', '/user/info', 1620634678336);
INSERT INTO `base_log` VALUES (1135, '1张晓明', 'GET', '/user/info', 1620634720116);
INSERT INTO `base_log` VALUES (1136, '1张晓明', 'GET', '/user/info', 1620634797634);
INSERT INTO `base_log` VALUES (1137, '1张晓明', 'GET', '/user/info', 1620634829362);
INSERT INTO `base_log` VALUES (1138, '1张晓明', 'GET', '/user/info', 1620634861436);
INSERT INTO `base_log` VALUES (1139, '1张晓明', 'GET', '/user/info', 1620634933887);
INSERT INTO `base_log` VALUES (1140, '1张晓明', 'GET', '/user/info', 1620634949716);
INSERT INTO `base_log` VALUES (1141, '1张晓明', 'GET', '/user/info', 1620634956143);
INSERT INTO `base_log` VALUES (1142, '1张晓明', 'GET', '/user/info', 1620634964313);
INSERT INTO `base_log` VALUES (1143, '1张晓明', 'GET', '/user/info', 1620634977522);
INSERT INTO `base_log` VALUES (1144, '1张晓明', 'GET', '/user/info', 1620635002516);
INSERT INTO `base_log` VALUES (1145, '1张晓明', 'GET', '/user/info', 1620635008763);
INSERT INTO `base_log` VALUES (1146, '1张晓明', 'GET', '/user/info', 1620635024921);
INSERT INTO `base_log` VALUES (1147, '1张晓明', 'GET', '/user/info', 1620635057915);
INSERT INTO `base_log` VALUES (1148, '1张晓明', 'GET', '/user/info', 1620635107160);
INSERT INTO `base_log` VALUES (1149, '1张晓明', 'GET', '/user/info', 1620635117148);
INSERT INTO `base_log` VALUES (1150, '1张晓明', 'GET', '/user/info', 1620635147021);
INSERT INTO `base_log` VALUES (1151, '1张晓明', 'GET', '/user/info', 1620635154149);
INSERT INTO `base_log` VALUES (1152, '1张晓明', 'GET', '/user/info', 1620635160636);
INSERT INTO `base_log` VALUES (1153, '1张晓明', 'GET', '/user/info', 1620635735282);
INSERT INTO `base_log` VALUES (1154, '1张晓明', 'GET', '/user/info', 1620635779819);
INSERT INTO `base_log` VALUES (1155, '1张晓明', 'GET', '/user/info', 1620635802081);
INSERT INTO `base_log` VALUES (1156, '1张晓明', 'GET', '/user/info', 1620635836942);
INSERT INTO `base_log` VALUES (1157, '1张晓明', 'GET', '/user/info', 1620635986150);
INSERT INTO `base_log` VALUES (1158, '1张晓明', 'POST', '/baseUser/saveOrUpdate', 1620636050238);
INSERT INTO `base_log` VALUES (1159, '1张晓明', 'POST', '/baseUser/saveOrUpdate', 1620636079487);
INSERT INTO `base_log` VALUES (1160, '1张晓明', 'POST', '/baseUser/saveOrUpdate', 1620636096012);
INSERT INTO `base_log` VALUES (1161, '1张晓明', 'GET', '/user/info', 1620636112318);
INSERT INTO `base_log` VALUES (1162, '1张晓明', 'GET', '/user/info', 1620636134885);
INSERT INTO `base_log` VALUES (1163, '1张晓明', 'POST', '/baseUser/saveOrUpdate', 1620636156046);
INSERT INTO `base_log` VALUES (1164, '1张晓明', 'GET', '/user/info', 1620636160650);
INSERT INTO `base_log` VALUES (1165, '1张晓明', 'GET', '/user/info', 1620636166676);
INSERT INTO `base_log` VALUES (1166, '1张晓明', 'GET', '/user/info', 1620636318474);
INSERT INTO `base_log` VALUES (1167, '1张晓明', 'GET', '/user/info', 1620636333568);
INSERT INTO `base_log` VALUES (1168, '1张晓明', 'GET', '/user/info', 1620636446106);
INSERT INTO `base_log` VALUES (1169, '1张晓明', 'GET', '/user/info', 1620636609400);
INSERT INTO `base_log` VALUES (1170, '1张晓明', 'GET', '/user/info', 1620636672410);
INSERT INTO `base_log` VALUES (1171, '1张晓明', 'GET', '/user/info', 1620636789774);
INSERT INTO `base_log` VALUES (1172, '1张晓明', 'GET', '/user/info', 1620636923385);
INSERT INTO `base_log` VALUES (1173, '1张晓明', 'GET', '/user/info', 1620637011242);
INSERT INTO `base_log` VALUES (1174, '1张晓明', 'POST', '/productCategory/saveOrUpdate', 1620637028093);
INSERT INTO `base_log` VALUES (1175, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637116930);
INSERT INTO `base_log` VALUES (1176, '1张晓明', 'GET', '/user/info', 1620637194894);
INSERT INTO `base_log` VALUES (1177, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637197187);
INSERT INTO `base_log` VALUES (1178, '1张晓明', 'GET', '/user/info', 1620637216101);
INSERT INTO `base_log` VALUES (1179, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637219784);
INSERT INTO `base_log` VALUES (1180, '1张晓明', 'GET', '/user/info', 1620637224498);
INSERT INTO `base_log` VALUES (1181, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637225972);
INSERT INTO `base_log` VALUES (1182, '1张晓明', 'GET', '/user/info', 1620637237527);
INSERT INTO `base_log` VALUES (1183, '1张晓明', 'POST', '/product/saveOrUpdate', 1620637241394);
INSERT INTO `base_log` VALUES (1184, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637241777);
INSERT INTO `base_log` VALUES (1185, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637326079);
INSERT INTO `base_log` VALUES (1186, '1张晓明', 'GET', '/user/info', 1620637560070);
INSERT INTO `base_log` VALUES (1187, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637569269);
INSERT INTO `base_log` VALUES (1188, '1张晓明', 'GET', '/user/info', 1620637574976);
INSERT INTO `base_log` VALUES (1189, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637578107);
INSERT INTO `base_log` VALUES (1190, '1张晓明', 'GET', '/user/info', 1620637586453);
INSERT INTO `base_log` VALUES (1191, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637588609);
INSERT INTO `base_log` VALUES (1192, '1张晓明', 'GET', '/user/info', 1620637616281);
INSERT INTO `base_log` VALUES (1193, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637624757);
INSERT INTO `base_log` VALUES (1194, '1张晓明', 'GET', '/user/info', 1620637628717);
INSERT INTO `base_log` VALUES (1195, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637631816);
INSERT INTO `base_log` VALUES (1196, '1张晓明', 'GET', '/user/info', 1620637635792);
INSERT INTO `base_log` VALUES (1197, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637638624);
INSERT INTO `base_log` VALUES (1198, '1张晓明', 'GET', '/user/info', 1620637651537);
INSERT INTO `base_log` VALUES (1199, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637653058);
INSERT INTO `base_log` VALUES (1200, '1张晓明', 'GET', '/user/info', 1620637700903);
INSERT INTO `base_log` VALUES (1201, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637704148);
INSERT INTO `base_log` VALUES (1202, '1张晓明', 'GET', '/user/info', 1620637709957);
INSERT INTO `base_log` VALUES (1203, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637716919);
INSERT INTO `base_log` VALUES (1204, '1张晓明', 'GET', '/user/info', 1620637725380);
INSERT INTO `base_log` VALUES (1205, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637726938);
INSERT INTO `base_log` VALUES (1206, '1张晓明', 'GET', '/user/info', 1620637764916);
INSERT INTO `base_log` VALUES (1207, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637767489);
INSERT INTO `base_log` VALUES (1208, '1张晓明', 'GET', '/user/info', 1620637775249);
INSERT INTO `base_log` VALUES (1209, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637779383);
INSERT INTO `base_log` VALUES (1210, '1张晓明', 'GET', '/user/info', 1620637788656);
INSERT INTO `base_log` VALUES (1211, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620637790539);
INSERT INTO `base_log` VALUES (1212, '1张晓明', 'GET', '/user/info', 1620637915750);
INSERT INTO `base_log` VALUES (1213, '1张晓明', 'POST', '/baseUser/addUserWidthRole', 1620637953599);
INSERT INTO `base_log` VALUES (1214, '1张晓明', 'GET', '/user/info', 1620638555463);
INSERT INTO `base_log` VALUES (1215, '1张晓明', 'POST', '/baseUser/addUserWidthRole', 1620638600425);
INSERT INTO `base_log` VALUES (1216, '1张晓明', 'GET', '/user/info', 1620638792805);
INSERT INTO `base_log` VALUES (1217, '1张晓明', 'GET', '/user/info', 1620638873692);
INSERT INTO `base_log` VALUES (1218, '1张晓明', 'GET', '/user/info', 1620638906755);
INSERT INTO `base_log` VALUES (1219, '1张晓明', 'GET', '/user/info', 1620639104457);
INSERT INTO `base_log` VALUES (1220, '1张晓明', 'POST', '/user/login', 1620639178175);
INSERT INTO `base_log` VALUES (1221, '1张晓明', 'GET', '/user/info', 1620639212408);
INSERT INTO `base_log` VALUES (1222, '1张晓明', 'GET', '/user/info', 1620639220492);
INSERT INTO `base_log` VALUES (1223, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620639349915);
INSERT INTO `base_log` VALUES (1224, '1张晓明', 'GET', '/user/info', 1620639359468);
INSERT INTO `base_log` VALUES (1225, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620639380631);
INSERT INTO `base_log` VALUES (1226, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620639381389);
INSERT INTO `base_log` VALUES (1227, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620639386867);
INSERT INTO `base_log` VALUES (1228, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620639393635);
INSERT INTO `base_log` VALUES (1229, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620639399879);
INSERT INTO `base_log` VALUES (1230, '1张晓明', 'GET', '/user/info', 1620639404611);
INSERT INTO `base_log` VALUES (1231, '1张晓明', 'GET', '/user/info', 1620639566853);
INSERT INTO `base_log` VALUES (1232, '1张晓明', 'GET', '/user/info', 1620639600359);
INSERT INTO `base_log` VALUES (1233, '1张晓明', 'GET', '/user/info', 1620640054369);
INSERT INTO `base_log` VALUES (1234, '1张晓明', 'POST', '/user/login', 1620640391519);
INSERT INTO `base_log` VALUES (1235, '1张晓明', 'POST', '/user/login', 1620640428174);
INSERT INTO `base_log` VALUES (1236, '1张晓明', 'POST', '/user/login', 1620640441865);
INSERT INTO `base_log` VALUES (1237, '1张晓明', 'POST', '/user/login', 1620640947952);
INSERT INTO `base_log` VALUES (1238, '1张晓明', 'POST', '/user/login', 1620640953931);
INSERT INTO `base_log` VALUES (1239, '1张晓明', 'POST', '/user/login', 1620640961304);
INSERT INTO `base_log` VALUES (1240, '1张晓明', 'POST', '/user/login', 1620640980503);
INSERT INTO `base_log` VALUES (1241, '1张晓明', 'POST', '/user/login', 1620640985383);
INSERT INTO `base_log` VALUES (1242, '1张晓明', 'POST', '/user/login', 1620640993126);
INSERT INTO `base_log` VALUES (1243, '1张晓明', 'POST', '/user/login', 1620641010844);
INSERT INTO `base_log` VALUES (1244, '1张晓明', 'POST', '/user/login', 1620641045794);
INSERT INTO `base_log` VALUES (1245, '1张晓明', 'POST', '/user/login', 1620641052792);
INSERT INTO `base_log` VALUES (1246, '1张晓明', 'POST', '/user/login', 1620641060238);
INSERT INTO `base_log` VALUES (1247, '1张晓明', 'POST', '/user/login', 1620641108873);
INSERT INTO `base_log` VALUES (1248, '1张晓明', 'POST', '/user/login', 1620641121351);
INSERT INTO `base_log` VALUES (1249, '1张晓明', 'POST', '/user/login', 1620641155769);
INSERT INTO `base_log` VALUES (1250, '1张晓明', 'POST', '/user/login', 1620641189798);
INSERT INTO `base_log` VALUES (1251, '1张晓明', 'GET', '/user/info', 1620641473336);
INSERT INTO `base_log` VALUES (1252, '1张晓明', 'GET', '/user/info', 1620641473496);
INSERT INTO `base_log` VALUES (1253, '1张晓明', 'GET', '/user/info', 1620641697834);
INSERT INTO `base_log` VALUES (1254, '1张晓明', 'GET', '/user/info', 1620641754957);
INSERT INTO `base_log` VALUES (1255, '1张晓明', 'GET', '/user/info', 1620641762739);
INSERT INTO `base_log` VALUES (1256, '1张晓明', 'GET', '/user/info', 1620641765261);
INSERT INTO `base_log` VALUES (1257, '1张晓明', 'GET', '/user/info', 1620641773608);
INSERT INTO `base_log` VALUES (1258, '1张晓明', 'GET', '/user/info', 1620641776600);
INSERT INTO `base_log` VALUES (1259, '1张晓明', 'GET', '/user/info', 1620641798899);
INSERT INTO `base_log` VALUES (1260, '1张晓明', 'GET', '/user/info', 1620641801507);
INSERT INTO `base_log` VALUES (1261, '1张晓明', 'GET', '/user/info', 1620641812981);
INSERT INTO `base_log` VALUES (1262, '1张晓明', 'GET', '/user/info', 1620641863409);
INSERT INTO `base_log` VALUES (1263, '1张晓明', 'GET', '/user/info', 1620641863460);
INSERT INTO `base_log` VALUES (1264, '1张晓明', 'GET', '/user/info', 1620641866654);
INSERT INTO `base_log` VALUES (1265, '1张晓明', 'GET', '/user/info', 1620641866703);
INSERT INTO `base_log` VALUES (1266, '1张晓明', 'GET', '/user/info', 1620641874836);
INSERT INTO `base_log` VALUES (1267, '1张晓明', 'GET', '/user/info', 1620641875334);
INSERT INTO `base_log` VALUES (1268, '1张晓明', 'POST', '/user/login', 1620668537683);
INSERT INTO `base_log` VALUES (1269, '1张晓明', 'POST', '/user/login', 1620669176985);
INSERT INTO `base_log` VALUES (1270, '1张晓明', 'POST', '/user/login', 1620669190201);
INSERT INTO `base_log` VALUES (1271, '1张晓明', 'POST', '/user/login', 1620669197474);
INSERT INTO `base_log` VALUES (1272, '1张晓明', 'POST', '/user/login', 1620669216142);
INSERT INTO `base_log` VALUES (1273, '1张晓明', 'POST', '/user/login', 1620669221646);
INSERT INTO `base_log` VALUES (1274, '1张晓明', 'GET', '/user/info', 1620695807863);
INSERT INTO `base_log` VALUES (1275, '1张晓明', 'GET', '/user/info', 1620696609121);
INSERT INTO `base_log` VALUES (1276, '1张晓明', 'POST', '/user/login', 1620696688540);
INSERT INTO `base_log` VALUES (1277, '1张晓明', 'GET', '/user/info', 1620696737445);
INSERT INTO `base_log` VALUES (1278, '1张晓明', 'GET', '/user/info', 1620696778859);
INSERT INTO `base_log` VALUES (1279, '1张晓明', 'GET', '/user/info', 1620696838157);
INSERT INTO `base_log` VALUES (1280, '1张晓明', 'GET', '/user/info', 1620696870972);
INSERT INTO `base_log` VALUES (1281, '1张晓明', 'GET', '/user/info', 1620697114674);
INSERT INTO `base_log` VALUES (1282, '1张晓明', 'GET', '/user/info', 1620697398569);
INSERT INTO `base_log` VALUES (1283, '1张晓明', 'GET', '/user/info', 1620697482530);
INSERT INTO `base_log` VALUES (1284, '1张晓明', 'GET', '/user/info', 1620697572966);
INSERT INTO `base_log` VALUES (1285, '1张晓明', 'POST', '/user/register', 1620697575135);
INSERT INTO `base_log` VALUES (1286, '1张晓明', 'POST', '/user/register', 1620697643191);
INSERT INTO `base_log` VALUES (1287, '1张晓明', 'POST', '/user/register', 1620697679059);
INSERT INTO `base_log` VALUES (1288, '1张晓明', 'GET', '/user/info', 1620697686914);
INSERT INTO `base_log` VALUES (1289, '1张晓明', 'GET', '/user/info', 1620697698011);
INSERT INTO `base_log` VALUES (1290, '1张晓明', 'POST', '/user/register', 1620697787628);
INSERT INTO `base_log` VALUES (1291, '1张晓明', 'POST', '/user/register', 1620697792745);
INSERT INTO `base_log` VALUES (1292, '1张晓明', 'GET', '/user/info', 1620697838096);
INSERT INTO `base_log` VALUES (1293, '1张晓明', 'GET', '/user/info', 1620697904919);
INSERT INTO `base_log` VALUES (1294, '1张晓明', 'GET', '/baseUser/deleteById', 1620697956634);
INSERT INTO `base_log` VALUES (1295, '1张晓明', 'GET', '/user/info', 1620697993151);
INSERT INTO `base_log` VALUES (1296, '1张晓明', 'GET', '/baseUser/deleteById', 1620698008921);
INSERT INTO `base_log` VALUES (1297, '1张晓明', 'GET', '/baseUser/deleteById', 1620698025029);
INSERT INTO `base_log` VALUES (1298, '1张晓明', 'GET', '/user/info', 1620698274538);
INSERT INTO `base_log` VALUES (1299, '1张晓明', 'GET', '/user/info', 1620698307544);
INSERT INTO `base_log` VALUES (1300, '1张晓明', 'GET', '/user/info', 1620698327389);
INSERT INTO `base_log` VALUES (1301, '1张晓明', 'GET', '/user/info', 1620698352111);
INSERT INTO `base_log` VALUES (1302, '1张晓明', 'POST', '/user/register', 1620698597651);
INSERT INTO `base_log` VALUES (1303, '1张晓明', 'POST', '/user/register', 1620698737493);
INSERT INTO `base_log` VALUES (1304, '1张晓明', 'GET', '/user/info', 1620699219536);
INSERT INTO `base_log` VALUES (1305, '1张晓明', 'GET', '/user/info', 1620699223294);
INSERT INTO `base_log` VALUES (1306, '1张晓明', 'GET', '/user/info', 1620699235588);
INSERT INTO `base_log` VALUES (1307, '1张晓明', 'GET', '/user/info', 1620699256049);
INSERT INTO `base_log` VALUES (1308, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620699342671);
INSERT INTO `base_log` VALUES (1309, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620699355142);
INSERT INTO `base_log` VALUES (1310, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620699364067);
INSERT INTO `base_log` VALUES (1311, '1张晓明', 'POST', '/role/authorization', 1620699370646);
INSERT INTO `base_log` VALUES (1312, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620699371050);
INSERT INTO `base_log` VALUES (1313, '1张晓明', 'GET', '/user/info', 1620699373669);
INSERT INTO `base_log` VALUES (1314, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620699374133);
INSERT INTO `base_log` VALUES (1315, '1张晓明', 'GET', '/user/info', 1620699496874);
INSERT INTO `base_log` VALUES (1316, '1张晓明', 'GET', '/user/info', 1620699498629);
INSERT INTO `base_log` VALUES (1317, '1张晓明', 'GET', '/user/info', 1620699608305);
INSERT INTO `base_log` VALUES (1318, '1张晓明', 'POST', '/user/login', 1620699624485);
INSERT INTO `base_log` VALUES (1319, '1张晓明', 'GET', '/user/info', 1620699628159);
INSERT INTO `base_log` VALUES (1320, '1张晓明', 'GET', '/user/info', 1620699701957);
INSERT INTO `base_log` VALUES (1321, '1张晓明', 'GET', '/user/info', 1620699823424);
INSERT INTO `base_log` VALUES (1322, '1张晓明', 'GET', '/user/info', 1620699873267);
INSERT INTO `base_log` VALUES (1323, '1张晓明', 'GET', '/user/info', 1620699883375);
INSERT INTO `base_log` VALUES (1324, '1张晓明', 'GET', '/baseUser/deleteById', 1620699902768);
INSERT INTO `base_log` VALUES (1325, '1张晓明', 'GET', '/baseUser/deleteById', 1620699904826);
INSERT INTO `base_log` VALUES (1326, '1张晓明', 'GET', '/baseUser/deleteById', 1620699906762);
INSERT INTO `base_log` VALUES (1327, '1张晓明', 'GET', '/baseUser/deleteById', 1620699908693);
INSERT INTO `base_log` VALUES (1328, '1张晓明', 'GET', '/user/info', 1620699916725);
INSERT INTO `base_log` VALUES (1329, '1张晓明', 'GET', '/user/info', 1620700137080);
INSERT INTO `base_log` VALUES (1330, '1张晓明', 'GET', '/user/info', 1620700265694);
INSERT INTO `base_log` VALUES (1331, '1张晓明', 'GET', '/user/info', 1620700277748);
INSERT INTO `base_log` VALUES (1332, '1张晓明', 'GET', '/user/info', 1620700281148);
INSERT INTO `base_log` VALUES (1333, '1张晓明', 'GET', '/user/info', 1620700418187);
INSERT INTO `base_log` VALUES (1334, '1张晓明', 'GET', '/user/info', 1620700421675);
INSERT INTO `base_log` VALUES (1335, '1张晓明', 'GET', '/user/info', 1620700517065);
INSERT INTO `base_log` VALUES (1336, '1张晓明', 'GET', '/user/info', 1620700544904);
INSERT INTO `base_log` VALUES (1337, '1张晓明', 'GET', '/user/info', 1620700566046);
INSERT INTO `base_log` VALUES (1338, '1张晓明', 'GET', '/user/info', 1620700573284);
INSERT INTO `base_log` VALUES (1339, '1张晓明', 'GET', '/user/info', 1620700581725);
INSERT INTO `base_log` VALUES (1340, '1张晓明', 'GET', '/user/info', 1620700587929);
INSERT INTO `base_log` VALUES (1341, '1张晓明', 'GET', '/user/info', 1620700630669);
INSERT INTO `base_log` VALUES (1342, '1张晓明', 'GET', '/user/info', 1620700738584);
INSERT INTO `base_log` VALUES (1343, '1张晓明', 'GET', '/user/info', 1620700786933);
INSERT INTO `base_log` VALUES (1344, '1张晓明', 'GET', '/user/info', 1620700899821);
INSERT INTO `base_log` VALUES (1345, '1张晓明', 'GET', '/user/info', 1620700909887);
INSERT INTO `base_log` VALUES (1346, NULL, 'POST', '/user/login', 1620700933044);
INSERT INTO `base_log` VALUES (1347, '1张晓明', 'GET', '/user/info', 1620700945495);
INSERT INTO `base_log` VALUES (1348, '1张晓明', 'POST', '/user/logout', 1620700945778);
INSERT INTO `base_log` VALUES (1349, '1张晓明', 'GET', '/user/info', 1620700997733);
INSERT INTO `base_log` VALUES (1350, '1张晓明', 'POST', '/user/logout', 1620700997930);
INSERT INTO `base_log` VALUES (1351, '1张晓明', 'POST', '/user/login', 1620701025036);
INSERT INTO `base_log` VALUES (1352, '1张晓明', 'GET', '/user/info', 1620701050883);
INSERT INTO `base_log` VALUES (1353, '1张晓明', 'POST', '/user/logout', 1620701050957);
INSERT INTO `base_log` VALUES (1354, '1张晓明', 'GET', '/user/info', 1620701078524);
INSERT INTO `base_log` VALUES (1355, '1张晓明', 'POST', '/user/login', 1620701222727);
INSERT INTO `base_log` VALUES (1356, '1张晓明', 'GET', '/user/info', 1620701235637);
INSERT INTO `base_log` VALUES (1357, '1张晓明', 'POST', '/user/logout', 1620701243447);
INSERT INTO `base_log` VALUES (1358, '1张晓明', 'GET', '/user/info', 1620701250991);
INSERT INTO `base_log` VALUES (1359, '1张晓明', 'POST', '/user/login', 1620701261961);
INSERT INTO `base_log` VALUES (1360, '1张晓明', 'GET', '/user/info', 1620701263163);
INSERT INTO `base_log` VALUES (1361, '1张晓明', 'GET', '/user/info', 1620701273028);
INSERT INTO `base_log` VALUES (1362, '1张晓明', 'GET', '/user/info', 1620701305187);
INSERT INTO `base_log` VALUES (1363, '1张晓明', 'GET', '/user/info', 1620701314840);
INSERT INTO `base_log` VALUES (1364, '1张晓明', 'GET', '/user/info', 1620701323809);
INSERT INTO `base_log` VALUES (1365, '1张晓明', 'POST', '/user/login', 1620701482908);
INSERT INTO `base_log` VALUES (1366, '1张晓明', 'GET', '/user/info', 1620701508762);
INSERT INTO `base_log` VALUES (1367, '1张晓明', 'POST', '/user/login', 1620701518795);
INSERT INTO `base_log` VALUES (1368, '1张晓明', 'POST', '/user/login', 1620701584138);
INSERT INTO `base_log` VALUES (1369, '1张晓明', 'GET', '/user/info', 1620701602070);
INSERT INTO `base_log` VALUES (1370, '1张晓明', 'POST', '/user/login', 1620701615306);
INSERT INTO `base_log` VALUES (1371, '1张晓明', 'POST', '/user/login', 1620701635421);
INSERT INTO `base_log` VALUES (1372, '1张晓明', 'POST', '/user/login', 1620701749786);
INSERT INTO `base_log` VALUES (1373, '1张晓明', 'POST', '/user/login', 1620701858912);
INSERT INTO `base_log` VALUES (1374, '1张晓明', 'POST', '/user/login', 1620701897260);
INSERT INTO `base_log` VALUES (1375, '1张晓明', 'POST', '/user/login', 1620701938250);
INSERT INTO `base_log` VALUES (1376, '1张晓明', 'POST', '/user/login', 1620702290282);
INSERT INTO `base_log` VALUES (1377, '1张晓明', 'POST', '/user/login', 1620702655227);
INSERT INTO `base_log` VALUES (1378, '1张晓明', 'POST', '/user/login', 1620702680831);
INSERT INTO `base_log` VALUES (1379, '1张晓明', 'POST', '/user/login', 1620702735566);
INSERT INTO `base_log` VALUES (1380, '1张晓明', 'POST', '/user/login', 1620702977569);
INSERT INTO `base_log` VALUES (1381, '1张晓明', 'POST', '/user/login', 1620703142763);
INSERT INTO `base_log` VALUES (1382, '1张晓明', 'POST', '/user/login', 1620703168808);
INSERT INTO `base_log` VALUES (1383, '1张晓明', 'POST', '/user/login', 1620703241131);
INSERT INTO `base_log` VALUES (1384, '1张晓明', 'POST', '/user/login', 1620703297292);
INSERT INTO `base_log` VALUES (1385, '1张晓明', 'POST', '/user/login', 1620703339826);
INSERT INTO `base_log` VALUES (1386, '1张晓明', 'POST', '/user/login', 1620703559252);
INSERT INTO `base_log` VALUES (1387, '1张晓明', 'POST', '/user/login', 1620703580524);
INSERT INTO `base_log` VALUES (1388, '1张晓明', 'GET', '/alipay/test', 1620705261504);
INSERT INTO `base_log` VALUES (1389, '1张晓明', 'GET', '/alipay/test', 1620705338533);
INSERT INTO `base_log` VALUES (1390, '1张晓明', 'POST', '/user/login', 1620707179539);
INSERT INTO `base_log` VALUES (1391, '1张晓明', 'POST', '/user/login', 1620707193352);
INSERT INTO `base_log` VALUES (1392, '1张晓明', 'GET', '/user/info', 1620707221087);
INSERT INTO `base_log` VALUES (1393, '1张晓明', 'GET', '/user/info', 1620707478861);
INSERT INTO `base_log` VALUES (1394, '1张晓明', 'POST', '/baseUser/addUserWidthRole', 1620707547775);
INSERT INTO `base_log` VALUES (1395, '1张晓明', 'GET', '/user/info', 1620707557112);
INSERT INTO `base_log` VALUES (1396, '1张晓明', 'POST', '/baseUser/addUserWidthRole', 1620707573739);
INSERT INTO `base_log` VALUES (1397, '1张晓明', 'POST', '/baseUser/addUserWidthRole', 1620707608119);
INSERT INTO `base_log` VALUES (1398, '1张晓明', 'GET', '/user/info', 1620707874267);
INSERT INTO `base_log` VALUES (1399, '1张晓明', 'GET', '/user/info', 1620707874487);
INSERT INTO `base_log` VALUES (1400, '1张晓明', 'GET', '/user/info', 1620707950606);
INSERT INTO `base_log` VALUES (1401, '1张晓明', 'GET', '/user/info', 1620707951108);
INSERT INTO `base_log` VALUES (1402, '1张晓明', 'GET', '/user/info', 1620707977808);
INSERT INTO `base_log` VALUES (1403, '1张晓明', 'GET', '/user/info', 1620707977965);
INSERT INTO `base_log` VALUES (1404, '1张晓明', 'GET', '/user/info', 1620707985521);
INSERT INTO `base_log` VALUES (1405, '1张晓明', 'GET', '/user/info', 1620707985590);
INSERT INTO `base_log` VALUES (1406, '1张晓明', 'GET', '/user/info', 1620707994467);
INSERT INTO `base_log` VALUES (1407, '1张晓明', 'GET', '/user/info', 1620707995065);
INSERT INTO `base_log` VALUES (1408, '1张晓明', 'GET', '/user/info', 1620708000689);
INSERT INTO `base_log` VALUES (1409, '1张晓明', 'GET', '/user/info', 1620708000772);
INSERT INTO `base_log` VALUES (1410, '1张晓明', 'GET', '/user/info', 1620708331068);
INSERT INTO `base_log` VALUES (1411, '1张晓明', 'GET', '/user/info', 1620708331470);
INSERT INTO `base_log` VALUES (1412, '1张晓明', 'GET', '/user/info', 1620708338530);
INSERT INTO `base_log` VALUES (1413, '1张晓明', 'GET', '/user/info', 1620708338876);
INSERT INTO `base_log` VALUES (1414, '1张晓明', 'GET', '/user/info', 1620708345011);
INSERT INTO `base_log` VALUES (1415, '1张晓明', 'GET', '/user/info', 1620708345027);
INSERT INTO `base_log` VALUES (1416, '1张晓明', 'GET', '/user/info', 1620708356219);
INSERT INTO `base_log` VALUES (1417, '1张晓明', 'GET', '/user/info', 1620708356289);
INSERT INTO `base_log` VALUES (1418, '1张晓明', 'GET', '/user/info', 1620708372148);
INSERT INTO `base_log` VALUES (1419, '1张晓明', 'GET', '/user/info', 1620708372249);
INSERT INTO `base_log` VALUES (1420, '1张晓明', 'GET', '/user/info', 1620708409708);
INSERT INTO `base_log` VALUES (1421, '1张晓明', 'GET', '/user/info', 1620708409743);
INSERT INTO `base_log` VALUES (1422, '1张晓明', 'GET', '/user/info', 1620708431432);
INSERT INTO `base_log` VALUES (1423, '1张晓明', 'GET', '/user/info', 1620708431794);
INSERT INTO `base_log` VALUES (1424, '1张晓明', 'GET', '/user/info', 1620708451319);
INSERT INTO `base_log` VALUES (1425, '1张晓明', 'GET', '/user/info', 1620708451350);
INSERT INTO `base_log` VALUES (1426, '1张晓明', 'GET', '/user/info', 1620708515024);
INSERT INTO `base_log` VALUES (1427, '1张晓明', 'GET', '/user/info', 1620708515033);
INSERT INTO `base_log` VALUES (1428, '1张晓明', 'GET', '/user/info', 1620708598878);
INSERT INTO `base_log` VALUES (1429, '1张晓明', 'GET', '/user/info', 1620708599062);
INSERT INTO `base_log` VALUES (1430, '1张晓明', 'GET', '/user/info', 1620708605309);
INSERT INTO `base_log` VALUES (1431, '1张晓明', 'GET', '/user/info', 1620708605799);
INSERT INTO `base_log` VALUES (1432, '1张晓明', 'GET', '/user/info', 1620708780690);
INSERT INTO `base_log` VALUES (1433, '1张晓明', 'GET', '/user/info', 1620708834089);
INSERT INTO `base_log` VALUES (1434, '1张晓明', 'GET', '/user/info', 1620708894321);
INSERT INTO `base_log` VALUES (1435, '1张晓明', 'GET', '/user/info', 1620708938358);
INSERT INTO `base_log` VALUES (1436, '1张晓明', 'GET', '/user/info', 1620708982793);
INSERT INTO `base_log` VALUES (1437, '1张晓明', 'GET', '/user/info', 1620709002793);
INSERT INTO `base_log` VALUES (1438, '1张晓明', 'GET', '/user/info', 1620709117468);
INSERT INTO `base_log` VALUES (1439, '1张晓明', 'GET', '/user/info', 1620709118045);
INSERT INTO `base_log` VALUES (1440, '1张晓明', 'GET', '/baseUser/deleteById', 1620709123376);
INSERT INTO `base_log` VALUES (1441, '1张晓明', 'POST', '/baseUser/addUserWidthRole', 1620709137892);
INSERT INTO `base_log` VALUES (1442, '1张晓明', 'GET', '/user/info', 1620709233827);
INSERT INTO `base_log` VALUES (1443, '1张晓明', 'GET', '/user/info', 1620709235641);
INSERT INTO `base_log` VALUES (1444, '1张晓明', 'GET', '/user/info', 1620709243756);
INSERT INTO `base_log` VALUES (1445, '1张晓明', 'GET', '/user/info', 1620709243890);
INSERT INTO `base_log` VALUES (1446, '1张晓明', 'POST', '/baseUser/addUserWidthRole', 1620709258545);
INSERT INTO `base_log` VALUES (1447, '1张晓明', 'POST', '/baseUser/addUserWidthRole', 1620709272205);
INSERT INTO `base_log` VALUES (1448, '1张晓明', 'GET', '/user/info', 1620712267266);
INSERT INTO `base_log` VALUES (1449, '1张晓明', 'POST', '/baseUser/addUserWidthRole', 1620712289728);
INSERT INTO `base_log` VALUES (1450, '1张晓明', 'POST', '/baseUser/addUserWidthRole', 1620712298643);
INSERT INTO `base_log` VALUES (1451, '1张晓明', 'POST', '/baseUser/addUserWidthRole', 1620712305541);
INSERT INTO `base_log` VALUES (1452, '1张晓明', 'POST', '/user/register', 1620712318164);
INSERT INTO `base_log` VALUES (1453, '1张晓明', 'POST', '/baseUser/addUserWidthRole', 1620712432564);
INSERT INTO `base_log` VALUES (1454, '1张晓明', 'POST', '/baseUser/addUserWidthRole', 1620712448347);
INSERT INTO `base_log` VALUES (1455, '1张晓明', 'POST', '/baseUser/addUserWidthRole', 1620712460984);
INSERT INTO `base_log` VALUES (1456, '1张晓明', 'GET', '/user/info', 1620712512856);
INSERT INTO `base_log` VALUES (1457, '1张晓明', 'GET', '/user/info', 1620713042071);
INSERT INTO `base_log` VALUES (1458, '1张晓明', 'GET', '/user/info', 1620713060138);
INSERT INTO `base_log` VALUES (1459, '1张晓明', 'GET', '/user/info', 1620713288625);
INSERT INTO `base_log` VALUES (1460, '1张晓明', 'GET', '/user/info', 1620713289201);
INSERT INTO `base_log` VALUES (1461, '1张晓明', 'POST', '/baseUser/saveOrUpdate', 1620713305121);
INSERT INTO `base_log` VALUES (1462, '1张晓明', 'GET', '/user/info', 1620713309249);
INSERT INTO `base_log` VALUES (1463, '1张晓明', 'GET', '/user/info', 1620713535797);
INSERT INTO `base_log` VALUES (1464, '1张晓明', 'POST', '/baseUser/saveOrUpdate', 1620713554712);
INSERT INTO `base_log` VALUES (1465, '1张晓明', 'GET', '/user/info', 1620713712998);
INSERT INTO `base_log` VALUES (1466, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620715059416);
INSERT INTO `base_log` VALUES (1467, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620715070330);
INSERT INTO `base_log` VALUES (1468, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620715081301);
INSERT INTO `base_log` VALUES (1469, '1张晓明', 'GET', '/user/info', 1620715098741);
INSERT INTO `base_log` VALUES (1470, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620715152615);
INSERT INTO `base_log` VALUES (1471, '1张晓明', 'DELETE', '/privilege/deleteById', 1620715159661);
INSERT INTO `base_log` VALUES (1472, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620715194528);
INSERT INTO `base_log` VALUES (1473, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620715375076);
INSERT INTO `base_log` VALUES (1474, '1张晓明', 'GET', '/user/info', 1620715405664);
INSERT INTO `base_log` VALUES (1475, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620715451930);
INSERT INTO `base_log` VALUES (1476, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620715464113);
INSERT INTO `base_log` VALUES (1477, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620715495000);
INSERT INTO `base_log` VALUES (1478, '1张晓明', 'POST', '/role/authorization', 1620715509503);
INSERT INTO `base_log` VALUES (1479, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620715510060);
INSERT INTO `base_log` VALUES (1480, '1张晓明', 'GET', '/user/info', 1620715512178);
INSERT INTO `base_log` VALUES (1481, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620715513052);
INSERT INTO `base_log` VALUES (1482, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620715562500);
INSERT INTO `base_log` VALUES (1483, '1张晓明', 'GET', '/user/info', 1620715565432);
INSERT INTO `base_log` VALUES (1484, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620715590525);
INSERT INTO `base_log` VALUES (1485, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620715597706);
INSERT INTO `base_log` VALUES (1486, '1张晓明', 'GET', '/user/info', 1620715820445);
INSERT INTO `base_log` VALUES (1487, '1张晓明', 'GET', '/user/info', 1620716045248);
INSERT INTO `base_log` VALUES (1488, '1张晓明', 'GET', '/user/info', 1620716085080);
INSERT INTO `base_log` VALUES (1489, '1张晓明', 'GET', '/user/info', 1620716141580);
INSERT INTO `base_log` VALUES (1490, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620716306663);
INSERT INTO `base_log` VALUES (1491, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620716319456);
INSERT INTO `base_log` VALUES (1492, '1张晓明', 'GET', '/user/info', 1620716322314);
INSERT INTO `base_log` VALUES (1493, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620716337970);
INSERT INTO `base_log` VALUES (1494, '1张晓明', 'POST', '/role/authorization', 1620716345713);
INSERT INTO `base_log` VALUES (1495, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620716345800);
INSERT INTO `base_log` VALUES (1496, '1张晓明', 'POST', '/role/authorization', 1620716367009);
INSERT INTO `base_log` VALUES (1497, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620716367594);
INSERT INTO `base_log` VALUES (1498, '1张晓明', 'GET', '/user/info', 1620716466818);
INSERT INTO `base_log` VALUES (1499, '1张晓明', 'POST', '/baseUser/setRoles', 1620716477887);
INSERT INTO `base_log` VALUES (1500, '1张晓明', 'GET', '/user/info', 1620716588988);
INSERT INTO `base_log` VALUES (1501, '1张晓明', 'POST', '/baseUser/setRoles', 1620716600493);
INSERT INTO `base_log` VALUES (1502, '1张晓明', 'POST', '/baseUser/setRoles', 1620716619962);
INSERT INTO `base_log` VALUES (1503, '1张晓明', 'POST', '/baseUser/setRoles', 1620716641051);
INSERT INTO `base_log` VALUES (1504, '1张晓明', 'POST', '/baseUser/setRoles', 1620716646381);
INSERT INTO `base_log` VALUES (1505, '1张晓明', 'POST', '/baseUser/setRoles', 1620716651637);
INSERT INTO `base_log` VALUES (1506, '1张晓明', 'POST', '/baseUser/setRoles', 1620716661739);
INSERT INTO `base_log` VALUES (1507, '1张晓明', 'POST', '/baseUser/setRoles', 1620716700388);
INSERT INTO `base_log` VALUES (1508, '1张晓明', 'POST', '/user/logout', 1620716706648);
INSERT INTO `base_log` VALUES (1509, '刘亚蓉', 'GET', '/user/info', 1620716714723);
INSERT INTO `base_log` VALUES (1510, '刘亚蓉', 'GET', '/user/info', 1620716759604);
INSERT INTO `base_log` VALUES (1511, '刘亚蓉', 'POST', '/baseUser/saveOrUpdate', 1620716778200);
INSERT INTO `base_log` VALUES (1512, '刘亚蓉', 'GET', '/user/info', 1620716805674);
INSERT INTO `base_log` VALUES (1513, '刘亚蓉', 'GET', '/user/info', 1620716877427);
INSERT INTO `base_log` VALUES (1514, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620716895610);
INSERT INTO `base_log` VALUES (1515, '刘亚蓉', 'POST', '/role/authorization', 1620716905351);
INSERT INTO `base_log` VALUES (1516, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620716906330);
INSERT INTO `base_log` VALUES (1517, '刘亚蓉', 'POST', '/role/authorization', 1620716916616);
INSERT INTO `base_log` VALUES (1518, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620716917166);
INSERT INTO `base_log` VALUES (1519, '刘亚蓉', 'GET', '/user/info', 1620716921479);
INSERT INTO `base_log` VALUES (1520, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620716922179);
INSERT INTO `base_log` VALUES (1521, '刘亚蓉', 'GET', '/user/info', 1620716955310);
INSERT INTO `base_log` VALUES (1522, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620717071419);
INSERT INTO `base_log` VALUES (1523, '刘亚蓉', 'GET', '/user/info', 1620717143032);
INSERT INTO `base_log` VALUES (1524, '1张晓明', 'POST', '/user/login', 1620717421638);
INSERT INTO `base_log` VALUES (1525, '1张晓明', 'GET', '/user/info', 1620717673871);
INSERT INTO `base_log` VALUES (1526, '1张晓明', 'GET', '/user/info', 1620717696130);
INSERT INTO `base_log` VALUES (1527, '1张晓明', 'GET', '/user/info', 1620717770588);
INSERT INTO `base_log` VALUES (1528, '1张晓明', 'GET', '/user/info', 1620717801810);
INSERT INTO `base_log` VALUES (1529, '1张晓明', 'GET', '/user/info', 1620717811222);
INSERT INTO `base_log` VALUES (1530, '1张晓明', 'GET', '/user/info', 1620717852427);
INSERT INTO `base_log` VALUES (1531, '1张晓明', 'GET', '/user/info', 1620717870630);
INSERT INTO `base_log` VALUES (1532, '1张晓明', 'GET', '/user/info', 1620717875696);
INSERT INTO `base_log` VALUES (1533, '刘亚蓉', 'GET', '/user/info', 1620718068167);
INSERT INTO `base_log` VALUES (1534, '刘亚蓉', 'GET', '/user/info', 1620718068169);
INSERT INTO `base_log` VALUES (1535, '刘亚蓉', 'GET', '/user/info', 1620718071105);
INSERT INTO `base_log` VALUES (1536, '刘亚蓉', 'GET', '/user/info', 1620718091498);
INSERT INTO `base_log` VALUES (1537, '刘亚蓉', 'GET', '/user/info', 1620718091571);
INSERT INTO `base_log` VALUES (1538, '刘亚蓉', 'GET', '/user/info', 1620718110047);
INSERT INTO `base_log` VALUES (1539, '刘亚蓉', 'GET', '/user/info', 1620718110052);
INSERT INTO `base_log` VALUES (1540, '刘亚蓉', 'GET', '/user/info', 1620718119015);
INSERT INTO `base_log` VALUES (1541, '刘亚蓉', 'GET', '/user/info', 1620718119182);
INSERT INTO `base_log` VALUES (1542, '1张晓明', 'GET', '/user/info', 1620718350363);
INSERT INTO `base_log` VALUES (1543, '1张晓明', 'GET', '/user/info', 1620718492526);
INSERT INTO `base_log` VALUES (1544, '1张晓明', 'GET', '/user/info', 1620718499141);
INSERT INTO `base_log` VALUES (1545, '刘亚蓉', 'GET', '/user/info', 1620718835293);
INSERT INTO `base_log` VALUES (1546, '1张晓明', 'GET', '/user/info', 1620718866030);
INSERT INTO `base_log` VALUES (1547, '1张晓明', 'GET', '/user/info', 1620718867748);
INSERT INTO `base_log` VALUES (1548, '刘亚蓉', 'GET', '/user/info', 1620718940964);
INSERT INTO `base_log` VALUES (1549, '刘亚蓉', 'GET', '/user/info', 1620719193791);
INSERT INTO `base_log` VALUES (1550, '刘亚蓉', 'GET', '/user/info', 1620719232032);
INSERT INTO `base_log` VALUES (1551, '刘亚蓉', 'GET', '/user/info', 1620719332870);
INSERT INTO `base_log` VALUES (1552, '刘亚蓉', 'GET', '/user/info', 1620719333159);
INSERT INTO `base_log` VALUES (1553, '刘亚蓉', 'GET', '/user/info', 1620719339976);
INSERT INTO `base_log` VALUES (1554, '刘亚蓉', 'GET', '/user/info', 1620719340187);
INSERT INTO `base_log` VALUES (1555, '刘亚蓉', 'GET', '/user/info', 1620719347479);
INSERT INTO `base_log` VALUES (1556, '刘亚蓉', 'GET', '/user/info', 1620719347508);
INSERT INTO `base_log` VALUES (1557, '刘亚蓉', 'GET', '/user/info', 1620719354913);
INSERT INTO `base_log` VALUES (1558, '刘亚蓉', 'GET', '/user/info', 1620719356255);
INSERT INTO `base_log` VALUES (1559, '刘亚蓉', 'GET', '/user/info', 1620719387230);
INSERT INTO `base_log` VALUES (1560, '刘亚蓉', 'GET', '/user/info', 1620719387339);
INSERT INTO `base_log` VALUES (1561, '刘亚蓉', 'GET', '/user/info', 1620719401356);
INSERT INTO `base_log` VALUES (1562, '刘亚蓉', 'GET', '/user/info', 1620719401702);
INSERT INTO `base_log` VALUES (1563, '1张晓明', 'POST', '/user/register', 1620719537821);
INSERT INTO `base_log` VALUES (1564, '1张晓明', 'POST', '/user/login', 1620719545695);
INSERT INTO `base_log` VALUES (1565, '1张晓明', 'POST', '/user/register', 1620719563654);
INSERT INTO `base_log` VALUES (1566, '1张晓明', 'POST', '/user/login', 1620719574523);
INSERT INTO `base_log` VALUES (1567, NULL, 'GET', '/user/info', 1620719576792);
INSERT INTO `base_log` VALUES (1568, '刘亚蓉', 'GET', '/user/info', 1620719795678);
INSERT INTO `base_log` VALUES (1569, '刘亚蓉', 'GET', '/user/info', 1620719796052);
INSERT INTO `base_log` VALUES (1570, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620719849547);
INSERT INTO `base_log` VALUES (1571, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620719875913);
INSERT INTO `base_log` VALUES (1572, NULL, 'GET', '/user/info', 1620719888557);
INSERT INTO `base_log` VALUES (1573, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620719890802);
INSERT INTO `base_log` VALUES (1574, '刘亚蓉', 'POST', '/role/authorization', 1620719896696);
INSERT INTO `base_log` VALUES (1575, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620719897204);
INSERT INTO `base_log` VALUES (1576, '刘亚蓉', 'GET', '/user/info', 1620719900542);
INSERT INTO `base_log` VALUES (1577, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620719901521);
INSERT INTO `base_log` VALUES (1578, NULL, 'POST', '/user/login', 1620719937304);
INSERT INTO `base_log` VALUES (1579, '1张晓明', 'GET', '/user/info', 1620719938666);
INSERT INTO `base_log` VALUES (1580, '刘亚蓉', 'GET', '/user/info', 1620719941000);
INSERT INTO `base_log` VALUES (1581, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620719942430);
INSERT INTO `base_log` VALUES (1582, '1张晓明', 'GET', '/user/info', 1620719961502);
INSERT INTO `base_log` VALUES (1583, '1张晓明', 'GET', '/user/info', 1620719967717);
INSERT INTO `base_log` VALUES (1584, '刘亚蓉', 'GET', '/user/info', 1620719972763);
INSERT INTO `base_log` VALUES (1585, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620719973812);
INSERT INTO `base_log` VALUES (1586, '刘亚蓉', 'POST', '/baseUser/saveOrUpdate', 1620719986765);
INSERT INTO `base_log` VALUES (1587, '刘亚蓉', 'POST', '/baseUser/saveOrUpdate', 1620719998172);
INSERT INTO `base_log` VALUES (1588, '刘亚蓉', 'POST', '/baseUser/saveOrUpdate', 1620720004901);
INSERT INTO `base_log` VALUES (1589, '刘亚蓉', 'POST', '/baseUser/saveOrUpdate', 1620720010951);
INSERT INTO `base_log` VALUES (1590, '1张晓明', 'GET', '/user/info', 1620720032286);
INSERT INTO `base_log` VALUES (1591, '刘亚蓉', 'POST', '/baseUser/saveOrUpdate', 1620720045694);
INSERT INTO `base_log` VALUES (1592, '1张晓明', 'GET', '/user/info', 1620720110118);
INSERT INTO `base_log` VALUES (1593, '1张晓明', 'GET', '/user/info', 1620720123262);
INSERT INTO `base_log` VALUES (1594, '1张晓明', 'GET', '/user/info', 1620720216477);
INSERT INTO `base_log` VALUES (1595, '刘亚蓉', 'GET', '/user/info', 1620720253627);
INSERT INTO `base_log` VALUES (1596, '刘亚蓉', 'POST', '/baseUser/saveOrUpdate', 1620720268841);
INSERT INTO `base_log` VALUES (1597, '刘亚蓉', 'GET', '/user/info', 1620720312458);
INSERT INTO `base_log` VALUES (1598, '刘亚蓉', 'GET', '/user/info', 1620720312754);
INSERT INTO `base_log` VALUES (1599, '1张晓明', 'GET', '/user/info', 1620720359235);
INSERT INTO `base_log` VALUES (1600, '刘亚蓉', 'POST', '/baseUser/saveOrUpdate', 1620720374143);
INSERT INTO `base_log` VALUES (1601, '1张晓明', 'GET', '/user/info', 1620720405632);
INSERT INTO `base_log` VALUES (1602, '刘亚蓉', 'GET', '/user/info', 1620720441281);
INSERT INTO `base_log` VALUES (1603, '刘亚蓉', 'POST', '/baseUser/saveOrUpdate', 1620720456240);
INSERT INTO `base_log` VALUES (1604, '刘亚蓉', 'GET', '/user/info', 1620720484435);
INSERT INTO `base_log` VALUES (1605, '刘亚蓉', 'POST', '/baseUser/addUserWidthRole', 1620720500351);
INSERT INTO `base_log` VALUES (1606, '刘亚蓉', 'POST', '/baseUser/addUserWidthRole', 1620720507969);
INSERT INTO `base_log` VALUES (1607, '刘亚蓉', 'POST', '/baseUser/addUserWidthRole', 1620720515640);
INSERT INTO `base_log` VALUES (1608, '刘亚蓉', 'POST', '/baseUser/addUserWidthRole', 1620720550316);
INSERT INTO `base_log` VALUES (1609, '刘亚蓉', 'POST', '/baseUser/saveOrUpdate', 1620720562949);
INSERT INTO `base_log` VALUES (1610, '刘亚蓉', 'POST', '/baseUser/saveOrUpdate', 1620720573114);
INSERT INTO `base_log` VALUES (1611, '刘亚蓉', 'GET', '/user/info', 1620720753521);
INSERT INTO `base_log` VALUES (1612, '1张晓明', 'GET', '/user/info', 1620721129431);
INSERT INTO `base_log` VALUES (1613, '刘亚蓉', 'GET', '/user/info', 1620721151206);
INSERT INTO `base_log` VALUES (1614, '刘亚蓉', 'GET', '/user/info', 1620721243673);
INSERT INTO `base_log` VALUES (1615, '刘亚蓉', 'DELETE', '/privilege/deleteById', 1620721284420);
INSERT INTO `base_log` VALUES (1616, '1张晓明', 'GET', '/user/info', 1620721323739);
INSERT INTO `base_log` VALUES (1617, '1张晓明', 'GET', '/user/info', 1620721334470);
INSERT INTO `base_log` VALUES (1618, '刘亚蓉', 'GET', '/user/info', 1620721340406);
INSERT INTO `base_log` VALUES (1619, '1张晓明', 'GET', '/user/info', 1620721363839);
INSERT INTO `base_log` VALUES (1620, '1张晓明', 'GET', '/user/info', 1620721378513);
INSERT INTO `base_log` VALUES (1621, '1张晓明', 'GET', '/user/info', 1620721436503);
INSERT INTO `base_log` VALUES (1622, '1张晓明', 'GET', '/user/info', 1620721453957);
INSERT INTO `base_log` VALUES (1623, '刘亚蓉', 'GET', '/user/info', 1620721542907);
INSERT INTO `base_log` VALUES (1624, '1张晓明', 'GET', '/user/info', 1620721559487);
INSERT INTO `base_log` VALUES (1625, '刘亚蓉', 'GET', '/user/info', 1620721646617);
INSERT INTO `base_log` VALUES (1626, '刘亚蓉', 'GET', '/user/info', 1620721659277);
INSERT INTO `base_log` VALUES (1627, '1张晓明', 'POST', '/user/login', 1620721704566);
INSERT INTO `base_log` VALUES (1628, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620721712637);
INSERT INTO `base_log` VALUES (1629, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620721723696);
INSERT INTO `base_log` VALUES (1630, '1张晓明', 'GET', '/user/info', 1620721725312);
INSERT INTO `base_log` VALUES (1631, '刘亚蓉', 'GET', '/user/info', 1620721728596);
INSERT INTO `base_log` VALUES (1632, '刘亚蓉', 'GET', '/user/info', 1620721731594);
INSERT INTO `base_log` VALUES (1633, '刘亚蓉', 'GET', '/user/info', 1620721733910);
INSERT INTO `base_log` VALUES (1634, '刘亚蓉', 'GET', '/user/info', 1620721755050);
INSERT INTO `base_log` VALUES (1635, '刘亚蓉', 'GET', '/user/info', 1620721809656);
INSERT INTO `base_log` VALUES (1636, '刘亚蓉', 'GET', '/user/info', 1620721853425);
INSERT INTO `base_log` VALUES (1637, '刘亚蓉', 'GET', '/user/info', 1620721892417);
INSERT INTO `base_log` VALUES (1638, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620721919050);
INSERT INTO `base_log` VALUES (1639, '刘亚蓉', 'POST', '/role/authorization', 1620721925970);
INSERT INTO `base_log` VALUES (1640, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620721926068);
INSERT INTO `base_log` VALUES (1641, '刘亚蓉', 'GET', '/user/info', 1620721929213);
INSERT INTO `base_log` VALUES (1642, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620721930032);
INSERT INTO `base_log` VALUES (1643, '刘亚蓉', 'POST', '/user/logout', 1620721940213);
INSERT INTO `base_log` VALUES (1644, '1张晓明', 'GET', '/user/info', 1620721943845);
INSERT INTO `base_log` VALUES (1645, '1张晓明', 'GET', '/user/info', 1620722040870);
INSERT INTO `base_log` VALUES (1646, '1张晓明', 'GET', '/user/info', 1620722045103);
INSERT INTO `base_log` VALUES (1647, '1张晓明', 'GET', '/user/info', 1620722178276);
INSERT INTO `base_log` VALUES (1648, '1张晓明', 'GET', '/user/info', 1620722215714);
INSERT INTO `base_log` VALUES (1649, '1张晓明', 'GET', '/user/info', 1620722239698);
INSERT INTO `base_log` VALUES (1650, '1张晓明', 'GET', '/user/info', 1620722272527);
INSERT INTO `base_log` VALUES (1651, '1张晓明', 'GET', '/user/info', 1620722297987);
INSERT INTO `base_log` VALUES (1652, '1张晓明', 'GET', '/user/info', 1620722301167);
INSERT INTO `base_log` VALUES (1653, '1张晓明', 'GET', '/user/info', 1620722305562);
INSERT INTO `base_log` VALUES (1654, '1张晓明', 'GET', '/user/info', 1620722315086);
INSERT INTO `base_log` VALUES (1655, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620722368344);
INSERT INTO `base_log` VALUES (1656, '1张晓明', 'GET', '/user/info', 1620722375416);
INSERT INTO `base_log` VALUES (1657, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620722380679);
INSERT INTO `base_log` VALUES (1658, '1张晓明', 'POST', '/role/authorization', 1620722386815);
INSERT INTO `base_log` VALUES (1659, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620722387083);
INSERT INTO `base_log` VALUES (1660, '1张晓明', 'GET', '/user/info', 1620722389586);
INSERT INTO `base_log` VALUES (1661, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620722390748);
INSERT INTO `base_log` VALUES (1662, '1张晓明', 'GET', '/user/info', 1620722415160);
INSERT INTO `base_log` VALUES (1663, '1张晓明', 'GET', '/user/info', 1620722432351);
INSERT INTO `base_log` VALUES (1664, '1张晓明', 'GET', '/user/info', 1620722450524);
INSERT INTO `base_log` VALUES (1665, '1张晓明', 'GET', '/user/info', 1620722548327);
INSERT INTO `base_log` VALUES (1666, '1张晓明', 'GET', '/user/info', 1620722549045);
INSERT INTO `base_log` VALUES (1667, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620723328127);
INSERT INTO `base_log` VALUES (1668, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620723343942);
INSERT INTO `base_log` VALUES (1669, '1张晓明', 'POST', '/privilege/saveOrUpdate', 1620723366640);
INSERT INTO `base_log` VALUES (1670, '1张晓明', 'GET', '/user/info', 1620723402093);
INSERT INTO `base_log` VALUES (1671, '1张晓明', 'GET', '/user/info', 1620723410345);
INSERT INTO `base_log` VALUES (1672, '1张晓明', 'GET', '/user/info', 1620724041997);
INSERT INTO `base_log` VALUES (1673, '1张晓明', 'GET', '/user/info', 1620724045961);
INSERT INTO `base_log` VALUES (1674, '1张晓明', 'GET', '/user/info', 1620724053422);
INSERT INTO `base_log` VALUES (1675, '1张晓明', 'GET', '/user/info', 1620724088452);
INSERT INTO `base_log` VALUES (1676, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620724100567);
INSERT INTO `base_log` VALUES (1677, '1张晓明', 'POST', '/role/authorization', 1620724106347);
INSERT INTO `base_log` VALUES (1678, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620724106564);
INSERT INTO `base_log` VALUES (1679, '1张晓明', 'GET', '/user/info', 1620724108785);
INSERT INTO `base_log` VALUES (1680, '1张晓明', 'GET', '/role/cascadePrivilegeFindAll', 1620724109815);
INSERT INTO `base_log` VALUES (1681, '1张晓明', 'GET', '/user/info', 1620724309125);
INSERT INTO `base_log` VALUES (1682, '1张晓明', 'GET', '/user/info', 1620724316669);
INSERT INTO `base_log` VALUES (1683, '1张晓明', 'GET', '/user/info', 1620724422447);
INSERT INTO `base_log` VALUES (1684, '1张晓明', 'GET', '/user/info', 1620724507216);
INSERT INTO `base_log` VALUES (1685, '1张晓明', 'POST', '/file/upload', 1620724687868);
INSERT INTO `base_log` VALUES (1686, '1张晓明', 'GET', '/user/info', 1620724723111);
INSERT INTO `base_log` VALUES (1687, '1张晓明', 'GET', '/user/info', 1620724724922);
INSERT INTO `base_log` VALUES (1688, '1张晓明', 'GET', '/user/info', 1620725054911);
INSERT INTO `base_log` VALUES (1689, '1张晓明', 'GET', '/user/info', 1620725066968);
INSERT INTO `base_log` VALUES (1690, '1张晓明', 'GET', '/user/info', 1620725121834);
INSERT INTO `base_log` VALUES (1691, '1张晓明', 'GET', '/user/info', 1620725166839);
INSERT INTO `base_log` VALUES (1692, '1张晓明', 'GET', '/user/info', 1620725167255);
INSERT INTO `base_log` VALUES (1693, '1张晓明', 'GET', '/user/info', 1620725192090);
INSERT INTO `base_log` VALUES (1694, '1张晓明', 'GET', '/user/info', 1620725192090);
INSERT INTO `base_log` VALUES (1695, '1张晓明', 'GET', '/user/info', 1620725207546);
INSERT INTO `base_log` VALUES (1696, '1张晓明', 'GET', '/user/info', 1620725207744);
INSERT INTO `base_log` VALUES (1697, '1张晓明', 'GET', '/user/info', 1620725311519);
INSERT INTO `base_log` VALUES (1698, '1张晓明', 'GET', '/user/info', 1620725313313);
INSERT INTO `base_log` VALUES (1699, '1张晓明', 'GET', '/user/info', 1620725411398);
INSERT INTO `base_log` VALUES (1700, '1张晓明', 'GET', '/user/info', 1620725538835);
INSERT INTO `base_log` VALUES (1701, '1张晓明', 'GET', '/user/info', 1620725538927);
INSERT INTO `base_log` VALUES (1702, '1张晓明', 'GET', '/user/info', 1620725580063);
INSERT INTO `base_log` VALUES (1703, '1张晓明', 'GET', '/user/info', 1620725652494);
INSERT INTO `base_log` VALUES (1704, '1张晓明', 'GET', '/user/info', 1620725924572);
INSERT INTO `base_log` VALUES (1705, '1张晓明', 'GET', '/user/info', 1620725940748);
INSERT INTO `base_log` VALUES (1706, '1张晓明', 'GET', '/user/info', 1620726009148);
INSERT INTO `base_log` VALUES (1707, '1张晓明', 'GET', '/user/info', 1620726033630);
INSERT INTO `base_log` VALUES (1708, '1张晓明', 'GET', '/user/info', 1620726135514);
INSERT INTO `base_log` VALUES (1709, '1张晓明', 'POST', '/productCategory/saveOrUpdate', 1620726148481);
INSERT INTO `base_log` VALUES (1710, '1张晓明', 'GET', '/user/info', 1620726219481);
INSERT INTO `base_log` VALUES (1711, '1张晓明', 'POST', '/productCategory/saveOrUpdate', 1620726259588);
INSERT INTO `base_log` VALUES (1712, '1张晓明', 'GET', '/user/info', 1620726317068);
INSERT INTO `base_log` VALUES (1713, '1张晓明', 'GET', '/user/info', 1620726326526);
INSERT INTO `base_log` VALUES (1714, '1张晓明', 'POST', '/productCategory/saveOrUpdate', 1620726326683);
INSERT INTO `base_log` VALUES (1715, '1张晓明', 'GET', '/productCategory/deleteById', 1620726330592);
INSERT INTO `base_log` VALUES (1716, '1张晓明', 'GET', '/user/info', 1620726356365);
INSERT INTO `base_log` VALUES (1717, '1张晓明', 'GET', '/productCategory/deleteById', 1620726360421);
INSERT INTO `base_log` VALUES (1718, '1张晓明', 'GET', '/user/info', 1620726516640);
INSERT INTO `base_log` VALUES (1719, '1张晓明', 'GET', '/user/info', 1620726607549);
INSERT INTO `base_log` VALUES (1720, '1张晓明', 'GET', '/user/info', 1620726708245);
INSERT INTO `base_log` VALUES (1721, '1张晓明', 'GET', '/user/info', 1620781622058);
INSERT INTO `base_log` VALUES (1722, '1张晓明', 'GET', '/user/info', 1620781622058);
INSERT INTO `base_log` VALUES (1723, '1张晓明', 'POST', '/baseUser/saveOrUpdate', 1620781816968);
INSERT INTO `base_log` VALUES (1724, '张晓明', 'GET', '/user/info', 1620781820958);
INSERT INTO `base_log` VALUES (1725, '张晓明', 'POST', '/baseUser/saveOrUpdate', 1620781836296);
INSERT INTO `base_log` VALUES (1726, '杰普', 'GET', '/user/info', 1620781841066);
INSERT INTO `base_log` VALUES (1727, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620781888473);
INSERT INTO `base_log` VALUES (1728, '杰普', 'POST', '/user/login', 1620782074534);
INSERT INTO `base_log` VALUES (1729, '杰普', 'POST', '/user/login', 1620782182757);
INSERT INTO `base_log` VALUES (1730, '杰普', 'GET', '/user/info', 1620782190855);
INSERT INTO `base_log` VALUES (1731, '杰普', 'GET', '/user/info', 1620782348198);
INSERT INTO `base_log` VALUES (1732, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620782569770);
INSERT INTO `base_log` VALUES (1733, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620782586294);
INSERT INTO `base_log` VALUES (1734, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620782605503);
INSERT INTO `base_log` VALUES (1735, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620782613992);
INSERT INTO `base_log` VALUES (1736, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620782623162);
INSERT INTO `base_log` VALUES (1737, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620782643680);
INSERT INTO `base_log` VALUES (1738, '杰普', 'GET', '/user/info', 1620782665463);
INSERT INTO `base_log` VALUES (1739, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620782781174);
INSERT INTO `base_log` VALUES (1740, '杰普', 'GET', '/user/info', 1620782798192);
INSERT INTO `base_log` VALUES (1741, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620782851240);
INSERT INTO `base_log` VALUES (1742, '杰普', 'GET', '/user/info', 1620782904626);
INSERT INTO `base_log` VALUES (1743, '杰普', 'GET', '/user/info', 1620783142337);
INSERT INTO `base_log` VALUES (1744, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620783144140);
INSERT INTO `base_log` VALUES (1745, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620783161961);
INSERT INTO `base_log` VALUES (1746, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620783179890);
INSERT INTO `base_log` VALUES (1747, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620783193012);
INSERT INTO `base_log` VALUES (1748, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620783204576);
INSERT INTO `base_log` VALUES (1749, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620783214239);
INSERT INTO `base_log` VALUES (1750, '杰普', 'GET', '/user/info', 1620783327383);
INSERT INTO `base_log` VALUES (1751, '杰普', 'GET', '/user/info', 1620783348812);
INSERT INTO `base_log` VALUES (1752, '杰普', 'GET', '/user/info', 1620783569029);
INSERT INTO `base_log` VALUES (1753, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620783785713);
INSERT INTO `base_log` VALUES (1754, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620783793078);
INSERT INTO `base_log` VALUES (1755, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620783799545);
INSERT INTO `base_log` VALUES (1756, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620783807180);
INSERT INTO `base_log` VALUES (1757, '杰普', 'GET', '/productCategory/deleteById', 1620783817749);
INSERT INTO `base_log` VALUES (1758, '杰普', 'GET', '/productCategory/deleteById', 1620783820109);
INSERT INTO `base_log` VALUES (1759, '杰普', 'GET', '/productCategory/deleteById', 1620783822080);
INSERT INTO `base_log` VALUES (1760, '杰普', 'GET', '/productCategory/deleteById', 1620783824153);
INSERT INTO `base_log` VALUES (1761, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620783865899);
INSERT INTO `base_log` VALUES (1762, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620783896280);
INSERT INTO `base_log` VALUES (1763, '杰普', 'POST', '/user/login', 1620784009428);
INSERT INTO `base_log` VALUES (1764, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620784049325);
INSERT INTO `base_log` VALUES (1765, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620784104745);
INSERT INTO `base_log` VALUES (1766, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620784114559);
INSERT INTO `base_log` VALUES (1767, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620784125788);
INSERT INTO `base_log` VALUES (1768, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620784136326);
INSERT INTO `base_log` VALUES (1769, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620784171547);
INSERT INTO `base_log` VALUES (1770, '杰普', 'GET', '/user/info', 1620784219723);
INSERT INTO `base_log` VALUES (1771, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620784245089);
INSERT INTO `base_log` VALUES (1772, '杰普', 'GET', '/user/info', 1620784354331);
INSERT INTO `base_log` VALUES (1773, '杰普', 'GET', '/user/info', 1620784446459);
INSERT INTO `base_log` VALUES (1774, '杰普', 'GET', '/user/info', 1620784820501);
INSERT INTO `base_log` VALUES (1775, '杰普', 'POST', '/user/login', 1620785084834);
INSERT INTO `base_log` VALUES (1776, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620785148066);
INSERT INTO `base_log` VALUES (1777, '杰普', 'GET', '/user/info', 1620785209729);
INSERT INTO `base_log` VALUES (1778, '杰普', 'POST', '/user/login', 1620785252265);
INSERT INTO `base_log` VALUES (1779, '杰普', 'POST', '/user/login', 1620785268337);
INSERT INTO `base_log` VALUES (1780, '杰普', 'GET', '/user/info', 1620785341660);
INSERT INTO `base_log` VALUES (1781, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620785353347);
INSERT INTO `base_log` VALUES (1782, '杰普', 'GET', '/user/info', 1620785452093);
INSERT INTO `base_log` VALUES (1783, '杰普', 'POST', '/privilege/saveOrUpdate', 1620785644175);
INSERT INTO `base_log` VALUES (1784, '杰普', 'POST', '/privilege/saveOrUpdate', 1620785698317);
INSERT INTO `base_log` VALUES (1785, '杰普', 'POST', '/privilege/saveOrUpdate', 1620785709603);
INSERT INTO `base_log` VALUES (1786, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620785712878);
INSERT INTO `base_log` VALUES (1787, '杰普', 'POST', '/role/authorization', 1620785719687);
INSERT INTO `base_log` VALUES (1788, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620785720121);
INSERT INTO `base_log` VALUES (1789, '杰普', 'GET', '/user/info', 1620785722249);
INSERT INTO `base_log` VALUES (1790, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620785723232);
INSERT INTO `base_log` VALUES (1791, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620785736188);
INSERT INTO `base_log` VALUES (1792, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620785750499);
INSERT INTO `base_log` VALUES (1793, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620785759472);
INSERT INTO `base_log` VALUES (1794, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620785768881);
INSERT INTO `base_log` VALUES (1795, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620785781525);
INSERT INTO `base_log` VALUES (1796, '杰普', 'GET', '/user/info', 1620786484632);
INSERT INTO `base_log` VALUES (1797, '杰普', 'GET', '/user/info', 1620786883645);
INSERT INTO `base_log` VALUES (1798, '杰普', 'GET', '/user/info', 1620786916771);
INSERT INTO `base_log` VALUES (1799, '杰普', 'GET', '/user/info', 1620787165920);
INSERT INTO `base_log` VALUES (1800, '杰普', 'GET', '/user/info', 1620787173945);
INSERT INTO `base_log` VALUES (1801, '杰普', 'GET', '/user/info', 1620787177453);
INSERT INTO `base_log` VALUES (1802, '杰普', 'POST', '/user/login', 1620787201347);
INSERT INTO `base_log` VALUES (1803, '杰普', 'GET', '/user/info', 1620787205761);
INSERT INTO `base_log` VALUES (1804, '杰普', 'GET', '/user/info', 1620787213326);
INSERT INTO `base_log` VALUES (1805, '杰普', 'GET', '/user/info', 1620787642588);
INSERT INTO `base_log` VALUES (1806, '杰普', 'GET', '/user/info', 1620787667783);
INSERT INTO `base_log` VALUES (1807, '杰普', 'GET', '/user/info', 1620787722571);
INSERT INTO `base_log` VALUES (1808, '杰普', 'GET', '/user/info', 1620787751257);
INSERT INTO `base_log` VALUES (1809, '杰普', 'GET', '/productCategory/deleteById', 1620788174341);
INSERT INTO `base_log` VALUES (1810, '杰普', 'GET', '/productCategory/deleteById', 1620788176011);
INSERT INTO `base_log` VALUES (1811, '杰普', 'GET', '/productCategory/deleteById', 1620788177812);
INSERT INTO `base_log` VALUES (1812, '杰普', 'GET', '/productCategory/deleteById', 1620788180286);
INSERT INTO `base_log` VALUES (1813, '杰普', 'GET', '/productCategory/deleteById', 1620788182497);
INSERT INTO `base_log` VALUES (1814, '杰普', 'GET', '/productCategory/deleteById', 1620788184333);
INSERT INTO `base_log` VALUES (1815, '杰普', 'GET', '/productCategory/deleteById', 1620788186540);
INSERT INTO `base_log` VALUES (1816, '刘亚蓉', 'GET', '/user/info', 1620788286519);
INSERT INTO `base_log` VALUES (1817, '刘亚蓉', 'GET', '/user/info', 1620788319236);
INSERT INTO `base_log` VALUES (1818, '刘亚蓉', 'GET', '/user/info', 1620788491127);
INSERT INTO `base_log` VALUES (1819, '刘亚蓉', 'GET', '/user/info', 1620788568384);
INSERT INTO `base_log` VALUES (1820, '刘亚蓉', 'GET', '/user/info', 1620788587389);
INSERT INTO `base_log` VALUES (1821, '刘亚蓉', 'GET', '/user/info', 1620788654538);
INSERT INTO `base_log` VALUES (1822, '杰普', 'GET', '/user/info', 1620788665741);
INSERT INTO `base_log` VALUES (1823, '刘亚蓉', 'GET', '/user/info', 1620788757938);
INSERT INTO `base_log` VALUES (1824, '刘亚蓉', 'GET', '/user/info', 1620788973153);
INSERT INTO `base_log` VALUES (1825, '刘亚蓉', 'GET', '/user/info', 1620789019963);
INSERT INTO `base_log` VALUES (1826, '刘亚蓉', 'GET', '/user/info', 1620789094030);
INSERT INTO `base_log` VALUES (1827, '杰普', 'POST', '/user/login', 1620789797511);
INSERT INTO `base_log` VALUES (1828, '杰普', 'POST', '/user/login', 1620789800149);
INSERT INTO `base_log` VALUES (1829, '杰普', 'GET', '/user/info', 1620789985247);
INSERT INTO `base_log` VALUES (1830, '杰普', 'GET', '/user/info', 1620789990204);
INSERT INTO `base_log` VALUES (1831, '杰普', 'POST', '/product/saveOrUpdate', 1620790056132);
INSERT INTO `base_log` VALUES (1832, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620790216557);
INSERT INTO `base_log` VALUES (1833, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620790220996);
INSERT INTO `base_log` VALUES (1834, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620790225304);
INSERT INTO `base_log` VALUES (1835, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620790230978);
INSERT INTO `base_log` VALUES (1836, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620790236388);
INSERT INTO `base_log` VALUES (1837, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620790244626);
INSERT INTO `base_log` VALUES (1838, '杰普', 'POST', '/product/saveOrUpdate', 1620790323531);
INSERT INTO `base_log` VALUES (1839, '杰普', 'POST', '/product/saveOrUpdate', 1620790337822);
INSERT INTO `base_log` VALUES (1840, '杰普', 'POST', '/product/saveOrUpdate', 1620790489044);
INSERT INTO `base_log` VALUES (1841, '杰普', 'GET', '/user/info', 1620790654150);
INSERT INTO `base_log` VALUES (1842, '杰普', 'POST', '/product/saveOrUpdate', 1620790672508);
INSERT INTO `base_log` VALUES (1843, '杰普', 'POST', '/product/saveOrUpdate', 1620790676380);
INSERT INTO `base_log` VALUES (1844, '杰普', 'POST', '/product/saveOrUpdate', 1620790679231);
INSERT INTO `base_log` VALUES (1845, '杰普', 'POST', '/product/saveOrUpdate', 1620790682003);
INSERT INTO `base_log` VALUES (1846, '杰普', 'GET', '/user/info', 1620790960301);
INSERT INTO `base_log` VALUES (1847, '杰普', 'GET', '/product/offline', 1620791074899);
INSERT INTO `base_log` VALUES (1848, '杰普', 'POST', '/user/login', 1620791105138);
INSERT INTO `base_log` VALUES (1849, '杰普', 'GET', '/user/info', 1620791141680);
INSERT INTO `base_log` VALUES (1850, '杰普', 'GET', '/user/info', 1620792443822);
INSERT INTO `base_log` VALUES (1851, '杰普', 'GET', '/user/info', 1620792471963);
INSERT INTO `base_log` VALUES (1852, '杰普', 'POST', '/baseUser/saveOrUpdate', 1620792524433);
INSERT INTO `base_log` VALUES (1853, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620792534560);
INSERT INTO `base_log` VALUES (1854, '杰普', 'POST', '/baseUser/saveOrUpdate', 1620792555995);
INSERT INTO `base_log` VALUES (1855, '杰普', 'POST', '/baseUser/saveOrUpdate', 1620792579322);
INSERT INTO `base_log` VALUES (1856, '杰普', 'GET', '/user/info', 1620792582698);
INSERT INTO `base_log` VALUES (1857, '杰普', 'GET', '/user/info', 1620792583535);
INSERT INTO `base_log` VALUES (1858, '杰普', 'GET', '/user/info', 1620792633850);
INSERT INTO `base_log` VALUES (1859, '杰普', 'GET', '/user/info', 1620792698652);
INSERT INTO `base_log` VALUES (1860, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620792712096);
INSERT INTO `base_log` VALUES (1861, '杰普', 'GET', '/user/info', 1620792901123);
INSERT INTO `base_log` VALUES (1862, '杰普', 'GET', '/user/info', 1620792920926);
INSERT INTO `base_log` VALUES (1863, '杰普', 'GET', '/user/info', 1620793023096);
INSERT INTO `base_log` VALUES (1864, '杰普', 'GET', '/user/info', 1620793023137);
INSERT INTO `base_log` VALUES (1865, '杰普', 'POST', '/user/login', 1620794522232);
INSERT INTO `base_log` VALUES (1866, '杰普', 'GET', '/user/info', 1620795702045);
INSERT INTO `base_log` VALUES (1867, '杰普', 'GET', '/user/info', 1620795708253);
INSERT INTO `base_log` VALUES (1868, '杰普', 'POST', '/user/login', 1620796715726);
INSERT INTO `base_log` VALUES (1869, '杰普', 'GET', '/user/info', 1620799843464);
INSERT INTO `base_log` VALUES (1870, '杰普', 'GET', '/user/info', 1620800070343);
INSERT INTO `base_log` VALUES (1871, '杰普', 'GET', '/user/info', 1620800091934);
INSERT INTO `base_log` VALUES (1872, '杰普', 'GET', '/productCategory/deleteById', 1620800122883);
INSERT INTO `base_log` VALUES (1873, '杰普', 'GET', '/productCategory/deleteById', 1620800125385);
INSERT INTO `base_log` VALUES (1874, '杰普', 'GET', '/productCategory/deleteById', 1620800127421);
INSERT INTO `base_log` VALUES (1875, '杰普', 'GET', '/productCategory/deleteById', 1620800129558);
INSERT INTO `base_log` VALUES (1876, '杰普', 'GET', '/productCategory/deleteById', 1620800134564);
INSERT INTO `base_log` VALUES (1877, '杰普', 'GET', '/productCategory/deleteById', 1620800136769);
INSERT INTO `base_log` VALUES (1878, '杰普', 'GET', '/productCategory/deleteById', 1620800139216);
INSERT INTO `base_log` VALUES (1879, '杰普', 'GET', '/productCategory/deleteById', 1620800141929);
INSERT INTO `base_log` VALUES (1880, '杰普', 'GET', '/user/info', 1620800144894);
INSERT INTO `base_log` VALUES (1881, '杰普', 'GET', '/productCategory/deleteById', 1620800145279);
INSERT INTO `base_log` VALUES (1882, '杰普', 'GET', '/productCategory/deleteById', 1620800147497);
INSERT INTO `base_log` VALUES (1883, '杰普', 'GET', '/user/info', 1620800158283);
INSERT INTO `base_log` VALUES (1884, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620800561832);
INSERT INTO `base_log` VALUES (1885, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620800578929);
INSERT INTO `base_log` VALUES (1886, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620800626752);
INSERT INTO `base_log` VALUES (1887, '杰普', 'POST', '/productCategory/saveOrUpdate', 1620800707378);
INSERT INTO `base_log` VALUES (1888, '杰普', 'POST', '/product/saveOrUpdate', 1620800955936);
INSERT INTO `base_log` VALUES (1889, '杰普', 'GET', '/user/info', 1620801344001);
INSERT INTO `base_log` VALUES (1890, '杰普', 'POST', '/privilege/saveOrUpdate', 1620801837317);
INSERT INTO `base_log` VALUES (1891, '杰普', 'POST', '/privilege/saveOrUpdate', 1620801854567);
INSERT INTO `base_log` VALUES (1892, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620801895207);
INSERT INTO `base_log` VALUES (1893, '杰普', 'POST', '/role/authorization', 1620801901572);
INSERT INTO `base_log` VALUES (1894, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620801902021);
INSERT INTO `base_log` VALUES (1895, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620801905017);
INSERT INTO `base_log` VALUES (1896, '杰普', 'GET', '/user/info', 1620801907751);
INSERT INTO `base_log` VALUES (1897, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620801908421);
INSERT INTO `base_log` VALUES (1898, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620801910922);
INSERT INTO `base_log` VALUES (1899, '杰普', 'GET', '/user/info', 1620801953505);
INSERT INTO `base_log` VALUES (1900, '杰普', 'POST', '/product/saveOrUpdate', 1620801972836);
INSERT INTO `base_log` VALUES (1901, '杰普', 'GET', '/user/info', 1620801980479);
INSERT INTO `base_log` VALUES (1902, '杰普', 'GET', '/user/info', 1620802007255);
INSERT INTO `base_log` VALUES (1903, '杰普', 'POST', '/product/saveOrUpdate', 1620802007567);
INSERT INTO `base_log` VALUES (1904, '杰普', 'POST', '/user/login', 1620802019653);
INSERT INTO `base_log` VALUES (1905, '杰普', 'POST', '/product/saveOrUpdate', 1620802026465);
INSERT INTO `base_log` VALUES (1906, '杰普', 'GET', '/user/info', 1620802030833);
INSERT INTO `base_log` VALUES (1907, '杰普', 'GET', '/user/info', 1620802065680);
INSERT INTO `base_log` VALUES (1908, '杰普', 'GET', '/user/info', 1620802110778);
INSERT INTO `base_log` VALUES (1909, '杰普', 'POST', '/product/saveOrUpdate', 1620802138966);
INSERT INTO `base_log` VALUES (1910, '杰普', 'GET', '/user/info', 1620802193339);
INSERT INTO `base_log` VALUES (1911, '杰普', 'POST', '/product/saveOrUpdate', 1620802202318);
INSERT INTO `base_log` VALUES (1912, '杰普', 'POST', '/product/saveOrUpdate', 1620802249633);
INSERT INTO `base_log` VALUES (1913, '杰普', 'GET', '/user/info', 1620802287060);
INSERT INTO `base_log` VALUES (1914, '杰普', 'GET', '/user/info', 1620802313403);
INSERT INTO `base_log` VALUES (1915, '杰普', 'POST', '/product/saveOrUpdate', 1620802400839);
INSERT INTO `base_log` VALUES (1916, '杰普', 'POST', '/product/saveOrUpdate', 1620802442490);
INSERT INTO `base_log` VALUES (1917, '杰普', 'POST', '/product/saveOrUpdate', 1620802487035);
INSERT INTO `base_log` VALUES (1918, '杰普', 'POST', '/product/saveOrUpdate', 1620802526014);
INSERT INTO `base_log` VALUES (1919, '杰普', 'POST', '/user/login', 1620802794684);
INSERT INTO `base_log` VALUES (1920, '杰普', 'GET', '/user/info', 1620803310934);
INSERT INTO `base_log` VALUES (1921, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620803317415);
INSERT INTO `base_log` VALUES (1922, '杰普', 'GET', '/user/info', 1620803519113);
INSERT INTO `base_log` VALUES (1923, '杰普', 'GET', '/user/info', 1620803543688);
INSERT INTO `base_log` VALUES (1924, '杰普', 'POST', '/product/saveOrUpdate', 1620803570969);
INSERT INTO `base_log` VALUES (1925, '杰普', 'GET', '/user/info', 1620803612746);
INSERT INTO `base_log` VALUES (1926, '杰普', 'GET', '/user/info', 1620803619623);
INSERT INTO `base_log` VALUES (1927, '杰普', 'POST', '/product/saveOrUpdate', 1620803620189);
INSERT INTO `base_log` VALUES (1928, '杰普', 'GET', '/user/info', 1620803623058);
INSERT INTO `base_log` VALUES (1929, '杰普', 'GET', '/user/info', 1620803717594);
INSERT INTO `base_log` VALUES (1930, '杰普', 'GET', '/user/info', 1620803723891);
INSERT INTO `base_log` VALUES (1931, '杰普', 'GET', '/user/info', 1620803800273);
INSERT INTO `base_log` VALUES (1932, '杰普', 'POST', '/product/saveOrUpdate', 1620803808640);
INSERT INTO `base_log` VALUES (1933, '杰普', 'POST', '/product/saveOrUpdate', 1620803823411);
INSERT INTO `base_log` VALUES (1934, '杰普', 'POST', '/product/saveOrUpdate', 1620803831538);
INSERT INTO `base_log` VALUES (1935, '杰普', 'GET', '/user/info', 1620803832395);
INSERT INTO `base_log` VALUES (1936, '杰普', 'POST', '/product/saveOrUpdate', 1620803839584);
INSERT INTO `base_log` VALUES (1937, '杰普', 'POST', '/product/saveOrUpdate', 1620803855179);
INSERT INTO `base_log` VALUES (1938, '杰普', 'POST', '/product/saveOrUpdate', 1620803862922);
INSERT INTO `base_log` VALUES (1939, '杰普', 'POST', '/product/saveOrUpdate', 1620803874705);
INSERT INTO `base_log` VALUES (1940, '杰普', 'POST', '/product/saveOrUpdate', 1620803882548);
INSERT INTO `base_log` VALUES (1941, '杰普', 'POST', '/product/saveOrUpdate', 1620803891912);
INSERT INTO `base_log` VALUES (1942, '杰普', 'GET', '/user/info', 1620803893860);
INSERT INTO `base_log` VALUES (1943, '杰普', 'GET', '/user/info', 1620803974047);
INSERT INTO `base_log` VALUES (1944, '杰普', 'POST', '/carousel/saveOrUpdate', 1620803986598);
INSERT INTO `base_log` VALUES (1945, '杰普', 'POST', '/carousel/saveOrUpdate', 1620803994361);
INSERT INTO `base_log` VALUES (1946, '杰普', 'POST', '/product/saveOrUpdate', 1620803997431);
INSERT INTO `base_log` VALUES (1947, '杰普', 'POST', '/carousel/saveOrUpdate', 1620804004091);
INSERT INTO `base_log` VALUES (1948, '杰普', 'GET', '/user/info', 1620804007152);
INSERT INTO `base_log` VALUES (1949, '杰普', 'POST', '/carousel/saveOrUpdate', 1620804021606);
INSERT INTO `base_log` VALUES (1950, '杰普', 'POST', '/product/saveOrUpdate', 1620804134659);
INSERT INTO `base_log` VALUES (1951, '杰普', 'GET', '/user/info', 1620804187771);
INSERT INTO `base_log` VALUES (1952, '杰普', 'POST', '/product/saveOrUpdate', 1620804208781);
INSERT INTO `base_log` VALUES (1953, '杰普', 'GET', '/carousel/deleteById', 1620804447900);
INSERT INTO `base_log` VALUES (1954, '杰普', 'GET', '/user/info', 1620804484861);
INSERT INTO `base_log` VALUES (1955, '杰普', 'GET', '/carousel/deleteById', 1620804489738);
INSERT INTO `base_log` VALUES (1956, '杰普', 'POST', '/carousel/saveOrUpdate', 1620804503871);
INSERT INTO `base_log` VALUES (1957, '杰普', 'POST', '/carousel/saveOrUpdate', 1620804509948);
INSERT INTO `base_log` VALUES (1958, '杰普', 'POST', '/carousel/saveOrUpdate', 1620804512560);
INSERT INTO `base_log` VALUES (1959, '杰普', 'POST', '/privilege/saveOrUpdate', 1620804685824);
INSERT INTO `base_log` VALUES (1960, '杰普', 'POST', '/privilege/saveOrUpdate', 1620804697764);
INSERT INTO `base_log` VALUES (1961, '杰普', 'POST', '/privilege/saveOrUpdate', 1620804704346);
INSERT INTO `base_log` VALUES (1962, '杰普', 'POST', '/privilege/saveOrUpdate', 1620804712257);
INSERT INTO `base_log` VALUES (1963, '杰普', 'POST', '/privilege/saveOrUpdate', 1620804718755);
INSERT INTO `base_log` VALUES (1964, '杰普', 'POST', '/privilege/saveOrUpdate', 1620804738605);
INSERT INTO `base_log` VALUES (1965, '杰普', 'GET', '/user/info', 1620804743449);
INSERT INTO `base_log` VALUES (1966, '杰普', 'GET', '/user/info', 1620804747810);
INSERT INTO `base_log` VALUES (1967, '杰普', 'GET', '/user/info', 1620804910658);
INSERT INTO `base_log` VALUES (1968, '杰普', 'POST', '/product/saveOrUpdate', 1620804924109);
INSERT INTO `base_log` VALUES (1969, '杰普', 'POST', '/product/saveOrUpdate', 1620804951161);
INSERT INTO `base_log` VALUES (1970, '杰普', 'POST', '/product/saveOrUpdate', 1620804960366);
INSERT INTO `base_log` VALUES (1971, '杰普', 'POST', '/product/saveOrUpdate', 1620804973046);
INSERT INTO `base_log` VALUES (1972, '杰普', 'POST', '/product/saveOrUpdate', 1620804985248);
INSERT INTO `base_log` VALUES (1973, '杰普', 'GET', '/user/info', 1620804988413);
INSERT INTO `base_log` VALUES (1974, '杰普', 'POST', '/product/saveOrUpdate', 1620805005036);
INSERT INTO `base_log` VALUES (1975, '杰普', 'POST', '/product/saveOrUpdate', 1620805017955);
INSERT INTO `base_log` VALUES (1976, '杰普', 'GET', '/user/info', 1620805030714);
INSERT INTO `base_log` VALUES (1977, '杰普', 'POST', '/product/saveOrUpdate', 1620805040958);
INSERT INTO `base_log` VALUES (1978, '杰普', 'POST', '/product/saveOrUpdate', 1620805195235);
INSERT INTO `base_log` VALUES (1979, '杰普', 'POST', '/privilege/saveOrUpdate', 1620805198989);
INSERT INTO `base_log` VALUES (1980, '杰普', 'POST', '/product/saveOrUpdate', 1620805206120);
INSERT INTO `base_log` VALUES (1981, '杰普', 'POST', '/privilege/saveOrUpdate', 1620805208471);
INSERT INTO `base_log` VALUES (1982, '杰普', 'POST', '/privilege/saveOrUpdate', 1620805234100);
INSERT INTO `base_log` VALUES (1983, '杰普', 'GET', '/user/info', 1620805262980);
INSERT INTO `base_log` VALUES (1984, '杰普', 'GET', '/user/info', 1620805271236);
INSERT INTO `base_log` VALUES (1985, '杰普', 'GET', '/user/info', 1620805338786);
INSERT INTO `base_log` VALUES (1986, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805366668);
INSERT INTO `base_log` VALUES (1987, '杰普', 'POST', '/role/authorization', 1620805376726);
INSERT INTO `base_log` VALUES (1988, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805377055);
INSERT INTO `base_log` VALUES (1989, '杰普', 'GET', '/user/info', 1620805380253);
INSERT INTO `base_log` VALUES (1990, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805381809);
INSERT INTO `base_log` VALUES (1991, '杰普', 'POST', '/category/saveOrUpdate', 1620805390362);
INSERT INTO `base_log` VALUES (1992, '杰普', 'POST', '/category/saveOrUpdate', 1620805417174);
INSERT INTO `base_log` VALUES (1993, '杰普', 'POST', '/privilege/saveOrUpdate', 1620805418733);
INSERT INTO `base_log` VALUES (1994, '杰普', 'GET', '/user/info', 1620805422689);
INSERT INTO `base_log` VALUES (1995, '杰普', 'POST', '/privilege/saveOrUpdate', 1620805447568);
INSERT INTO `base_log` VALUES (1996, '杰普', 'POST', '/category/saveOrUpdate', 1620805462428);
INSERT INTO `base_log` VALUES (1997, '杰普', 'POST', '/privilege/saveOrUpdate', 1620805491242);
INSERT INTO `base_log` VALUES (1998, '杰普', 'POST', '/category/saveOrUpdate', 1620805493609);
INSERT INTO `base_log` VALUES (1999, '杰普', 'GET', '/user/info', 1620805497291);
INSERT INTO `base_log` VALUES (2000, '杰普', 'GET', '/user/info', 1620805541122);
INSERT INTO `base_log` VALUES (2001, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805551637);
INSERT INTO `base_log` VALUES (2002, '杰普', 'POST', '/role/authorization', 1620805560035);
INSERT INTO `base_log` VALUES (2003, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805560197);
INSERT INTO `base_log` VALUES (2004, '杰普', 'GET', '/user/info', 1620805563677);
INSERT INTO `base_log` VALUES (2005, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805565251);
INSERT INTO `base_log` VALUES (2006, '杰普', 'GET', '/user/info', 1620805583943);
INSERT INTO `base_log` VALUES (2007, '杰普', 'POST', '/privilege/saveOrUpdate', 1620805607041);
INSERT INTO `base_log` VALUES (2008, '杰普', 'GET', '/user/info', 1620805610219);
INSERT INTO `base_log` VALUES (2009, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805629900);
INSERT INTO `base_log` VALUES (2010, '杰普', 'GET', '/user/info', 1620805630445);
INSERT INTO `base_log` VALUES (2011, '杰普', 'GET', '/user/info', 1620805664041);
INSERT INTO `base_log` VALUES (2012, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805666784);
INSERT INTO `base_log` VALUES (2013, '杰普', 'POST', '/privilege/saveOrUpdate', 1620805717632);
INSERT INTO `base_log` VALUES (2014, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805725002);
INSERT INTO `base_log` VALUES (2015, '杰普', 'POST', '/role/authorization', 1620805734666);
INSERT INTO `base_log` VALUES (2016, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805734851);
INSERT INTO `base_log` VALUES (2017, '杰普', 'GET', '/user/info', 1620805738463);
INSERT INTO `base_log` VALUES (2018, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805741151);
INSERT INTO `base_log` VALUES (2019, '杰普', 'GET', '/user/info', 1620805751778);
INSERT INTO `base_log` VALUES (2020, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805753564);
INSERT INTO `base_log` VALUES (2021, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805756097);
INSERT INTO `base_log` VALUES (2022, '杰普', 'POST', '/role/authorization', 1620805778549);
INSERT INTO `base_log` VALUES (2023, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805778787);
INSERT INTO `base_log` VALUES (2024, '杰普', 'GET', '/user/info', 1620805867224);
INSERT INTO `base_log` VALUES (2025, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805870590);
INSERT INTO `base_log` VALUES (2026, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805882107);
INSERT INTO `base_log` VALUES (2027, '杰普', 'POST', '/privilege/saveOrUpdate', 1620805917159);
INSERT INTO `base_log` VALUES (2028, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805920384);
INSERT INTO `base_log` VALUES (2029, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805923062);
INSERT INTO `base_log` VALUES (2030, '杰普', 'POST', '/role/authorization', 1620805931870);
INSERT INTO `base_log` VALUES (2031, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805932005);
INSERT INTO `base_log` VALUES (2032, '杰普', 'GET', '/user/info', 1620805934689);
INSERT INTO `base_log` VALUES (2033, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805935980);
INSERT INTO `base_log` VALUES (2034, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805942919);
INSERT INTO `base_log` VALUES (2035, '杰普', 'GET', '/user/info', 1620805961828);
INSERT INTO `base_log` VALUES (2036, '杰普', 'POST', '/baseUser/saveOrUpdate', 1620805980616);
INSERT INTO `base_log` VALUES (2037, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805984584);
INSERT INTO `base_log` VALUES (2038, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620805989704);
INSERT INTO `base_log` VALUES (2039, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620806000769);
INSERT INTO `base_log` VALUES (2040, '杰普', 'POST', '/role/authorization', 1620806008599);
INSERT INTO `base_log` VALUES (2041, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620806008963);
INSERT INTO `base_log` VALUES (2042, '杰普', 'POST', '/article/saveOrUpdate', 1620806011406);
INSERT INTO `base_log` VALUES (2043, '杰普', 'GET', '/user/info', 1620806013148);
INSERT INTO `base_log` VALUES (2044, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620806014732);
INSERT INTO `base_log` VALUES (2045, '杰普', 'POST', '/privilege/saveOrUpdate', 1620806041705);
INSERT INTO `base_log` VALUES (2046, '杰普', 'DELETE', '/privilege/deleteById', 1620806050623);
INSERT INTO `base_log` VALUES (2047, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620806063113);
INSERT INTO `base_log` VALUES (2048, '杰普', 'POST', '/privilege/saveOrUpdate', 1620806089823);
INSERT INTO `base_log` VALUES (2049, '杰普', 'GET', '/user/info', 1620806095986);
INSERT INTO `base_log` VALUES (2050, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620806101513);
INSERT INTO `base_log` VALUES (2051, '杰普', 'GET', '/user/info', 1620806277774);
INSERT INTO `base_log` VALUES (2052, '杰普', 'GET', '/baseUser/deleteById', 1620806286592);
INSERT INTO `base_log` VALUES (2053, '杰普', 'POST', '/baseUser/saveOrUpdate', 1620806294008);
INSERT INTO `base_log` VALUES (2054, '杰普', 'POST', '/baseUser/saveOrUpdate', 1620806295258);
INSERT INTO `base_log` VALUES (2055, '杰普', 'POST', '/baseUser/saveOrUpdate', 1620806321722);
INSERT INTO `base_log` VALUES (2056, '杰普', 'POST', '/product/saveOrUpdate', 1620806423701);
INSERT INTO `base_log` VALUES (2057, '杰普', 'POST', '/baseUser/saveOrUpdate', 1620806496186);
INSERT INTO `base_log` VALUES (2058, '杰普', 'POST', '/baseUser/saveOrUpdate', 1620806504565);
INSERT INTO `base_log` VALUES (2059, '杰普', 'POST', '/product/saveOrUpdate', 1620806514658);
INSERT INTO `base_log` VALUES (2060, '杰普', 'GET', '/user/info', 1620806552499);
INSERT INTO `base_log` VALUES (2061, '杰普', 'GET', '/user/info', 1620806592876);
INSERT INTO `base_log` VALUES (2062, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620806604201);
INSERT INTO `base_log` VALUES (2063, '杰普', 'GET', '/user/info', 1620806638870);
INSERT INTO `base_log` VALUES (2064, '杰普', 'GET', '/user/info', 1620806770265);
INSERT INTO `base_log` VALUES (2065, '杰普', 'POST', '/product/saveOrUpdate', 1620806858142);
INSERT INTO `base_log` VALUES (2066, '杰普', 'GET', '/user/info', 1620807071522);
INSERT INTO `base_log` VALUES (2067, '杰普', 'GET', '/user/info', 1620807081615);
INSERT INTO `base_log` VALUES (2068, '杰普', 'POST', '/product/saveOrUpdate', 1620807245365);
INSERT INTO `base_log` VALUES (2069, '杰普', 'GET', '/user/info', 1620807332978);
INSERT INTO `base_log` VALUES (2070, '杰普', 'GET', '/user/info', 1620807334889);
INSERT INTO `base_log` VALUES (2071, '杰普', 'POST', '/product/saveOrUpdate', 1620807386453);
INSERT INTO `base_log` VALUES (2072, '杰普', 'GET', '/user/info', 1620807582970);
INSERT INTO `base_log` VALUES (2073, '杰普', 'GET', '/user/info', 1620807591453);
INSERT INTO `base_log` VALUES (2074, '杰普', 'POST', '/product/saveOrUpdate', 1620807596233);
INSERT INTO `base_log` VALUES (2075, '杰普', 'GET', '/user/info', 1620807698429);
INSERT INTO `base_log` VALUES (2076, '杰普', 'POST', '/product/saveOrUpdate', 1620807723182);
INSERT INTO `base_log` VALUES (2077, '杰普', 'GET', '/user/info', 1620807735234);
INSERT INTO `base_log` VALUES (2078, '杰普', 'POST', '/product/saveOrUpdate', 1620807744183);
INSERT INTO `base_log` VALUES (2079, '杰普', 'POST', '/product/saveOrUpdate', 1620807756640);
INSERT INTO `base_log` VALUES (2080, '杰普', 'GET', '/user/info', 1620807805299);
INSERT INTO `base_log` VALUES (2081, '杰普', 'GET', '/user/info', 1620807826806);
INSERT INTO `base_log` VALUES (2082, '杰普', 'GET', '/user/info', 1620807832220);
INSERT INTO `base_log` VALUES (2083, '杰普', 'GET', '/user/info', 1620807905584);
INSERT INTO `base_log` VALUES (2084, '杰普', 'POST', '/privilege/saveOrUpdate', 1620807922301);
INSERT INTO `base_log` VALUES (2085, '杰普', 'POST', '/privilege/saveOrUpdate', 1620807934856);
INSERT INTO `base_log` VALUES (2086, '杰普', 'POST', '/privilege/saveOrUpdate', 1620807952703);
INSERT INTO `base_log` VALUES (2087, '杰普', 'POST', '/privilege/saveOrUpdate', 1620807957217);
INSERT INTO `base_log` VALUES (2088, '杰普', 'POST', '/privilege/saveOrUpdate', 1620807965511);
INSERT INTO `base_log` VALUES (2089, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620808131535);
INSERT INTO `base_log` VALUES (2090, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620808177307);
INSERT INTO `base_log` VALUES (2091, '杰普', 'GET', '/user/info', 1620808292356);
INSERT INTO `base_log` VALUES (2092, '杰普', 'GET', '/user/info', 1620808680905);
INSERT INTO `base_log` VALUES (2093, '杰普', 'GET', '/user/info', 1620808686601);
INSERT INTO `base_log` VALUES (2094, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620808704542);
INSERT INTO `base_log` VALUES (2095, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620808723821);
INSERT INTO `base_log` VALUES (2096, '杰普', 'GET', '/user/info', 1620808827621);
INSERT INTO `base_log` VALUES (2097, '杰普', 'GET', '/user/info', 1620808846137);
INSERT INTO `base_log` VALUES (2098, '杰普', 'GET', '/user/info', 1620808915621);
INSERT INTO `base_log` VALUES (2099, '杰普', 'GET', '/user/info', 1620810323201);
INSERT INTO `base_log` VALUES (2100, '杰普', 'GET', '/user/info', 1620810364885);
INSERT INTO `base_log` VALUES (2101, '杰普', 'GET', '/user/info', 1620810381648);
INSERT INTO `base_log` VALUES (2102, '杰普', 'GET', '/user/info', 1620810846017);
INSERT INTO `base_log` VALUES (2103, '杰普', 'GET', '/user/info', 1620810866914);
INSERT INTO `base_log` VALUES (2104, '杰普', 'GET', '/user/info', 1620811010749);
INSERT INTO `base_log` VALUES (2105, '杰普', 'GET', '/user/info', 1620811053751);
INSERT INTO `base_log` VALUES (2106, '杰普', 'GET', '/user/info', 1620811116392);
INSERT INTO `base_log` VALUES (2107, '杰普', 'GET', '/user/info', 1620811222742);
INSERT INTO `base_log` VALUES (2108, '杰普', 'GET', '/user/info', 1620811289408);
INSERT INTO `base_log` VALUES (2109, '杰普', 'GET', '/user/info', 1620811319763);
INSERT INTO `base_log` VALUES (2110, '杰普', 'GET', '/user/info', 1620811467819);
INSERT INTO `base_log` VALUES (2111, '杰普', 'POST', '/user/login', 1620817756444);
INSERT INTO `base_log` VALUES (2112, '杰普', 'POST', '/user/login', 1620817785923);
INSERT INTO `base_log` VALUES (2113, '杰普', 'GET', '/user/info', 1620817837778);
INSERT INTO `base_log` VALUES (2114, '杰普', 'POST', '/user/login', 1620820803640);
INSERT INTO `base_log` VALUES (2115, '杰普', 'GET', '/user/info', 1620867822829);
INSERT INTO `base_log` VALUES (2116, '杰普', 'GET', '/user/info', 1620868220465);
INSERT INTO `base_log` VALUES (2117, '杰普', 'GET', '/user/info', 1620868226157);
INSERT INTO `base_log` VALUES (2118, '杰普', 'GET', '/user/info', 1620868230725);
INSERT INTO `base_log` VALUES (2119, '杰普', 'GET', '/user/info', 1620868259715);
INSERT INTO `base_log` VALUES (2120, '杰普', 'POST', '/user/login', 1620868273598);
INSERT INTO `base_log` VALUES (2121, '杰普', 'POST', '/user/login', 1620868295703);
INSERT INTO `base_log` VALUES (2122, '杰普', 'GET', '/user/info', 1620868400939);
INSERT INTO `base_log` VALUES (2123, '杰普', 'GET', '/user/info', 1620868525290);
INSERT INTO `base_log` VALUES (2124, '杰普', 'GET', '/user/info', 1620868525492);
INSERT INTO `base_log` VALUES (2125, '杰普', 'GET', '/user/info', 1620868595832);
INSERT INTO `base_log` VALUES (2126, '杰普', 'POST', '/user/login', 1620868653411);
INSERT INTO `base_log` VALUES (2127, '杰普', 'POST', '/user/login', 1620868667217);
INSERT INTO `base_log` VALUES (2128, '杰普', 'POST', '/user/login', 1620869562333);
INSERT INTO `base_log` VALUES (2129, '杰普', 'GET', '/user/info', 1620869680108);
INSERT INTO `base_log` VALUES (2130, '杰普', 'GET', '/user/info', 1620869680863);
INSERT INTO `base_log` VALUES (2131, '杰普', 'GET', '/user/info', 1620869775182);
INSERT INTO `base_log` VALUES (2132, '杰普', 'GET', '/user/info', 1620869852317);
INSERT INTO `base_log` VALUES (2133, '杰普', 'GET', '/user/info', 1620869981321);
INSERT INTO `base_log` VALUES (2134, '杰普', 'POST', '/user/login', 1620869998621);
INSERT INTO `base_log` VALUES (2135, '杰普', 'GET', '/user/info', 1620870089869);
INSERT INTO `base_log` VALUES (2136, '杰普', 'GET', '/user/info', 1620870105716);
INSERT INTO `base_log` VALUES (2137, '杰普', 'GET', '/user/info', 1620870553452);
INSERT INTO `base_log` VALUES (2138, '杰普', 'GET', '/user/info', 1620870723645);
INSERT INTO `base_log` VALUES (2139, '杰普', 'GET', '/user/info', 1620870829373);
INSERT INTO `base_log` VALUES (2140, '杰普', 'GET', '/user/info', 1620870968786);
INSERT INTO `base_log` VALUES (2141, '杰普', 'GET', '/user/info', 1620870981120);
INSERT INTO `base_log` VALUES (2142, '杰普', 'GET', '/user/info', 1620871040471);
INSERT INTO `base_log` VALUES (2143, '杰普', 'GET', '/user/info', 1620871150911);
INSERT INTO `base_log` VALUES (2144, '杰普', 'GET', '/user/info', 1620871569104);
INSERT INTO `base_log` VALUES (2145, '杰普', 'GET', '/user/info', 1620871746175);
INSERT INTO `base_log` VALUES (2146, '杰普', 'GET', '/user/info', 1620871764140);
INSERT INTO `base_log` VALUES (2147, '杰普', 'GET', '/user/info', 1620871803309);
INSERT INTO `base_log` VALUES (2148, '杰普', 'GET', '/user/info', 1620872238450);
INSERT INTO `base_log` VALUES (2149, '杰普', 'GET', '/user/info', 1620872287665);
INSERT INTO `base_log` VALUES (2150, '杰普', 'GET', '/user/info', 1620872699525);
INSERT INTO `base_log` VALUES (2151, '杰普', 'GET', '/user/info', 1620872884532);
INSERT INTO `base_log` VALUES (2152, '杰普', 'GET', '/user/info', 1620872921985);
INSERT INTO `base_log` VALUES (2153, '杰普', 'GET', '/user/info', 1620872972228);
INSERT INTO `base_log` VALUES (2154, '杰普', 'GET', '/user/info', 1620873100350);
INSERT INTO `base_log` VALUES (2155, '杰普', 'GET', '/user/info', 1620873100779);
INSERT INTO `base_log` VALUES (2156, '杰普', 'GET', '/user/info', 1620873189663);
INSERT INTO `base_log` VALUES (2157, '杰普', 'GET', '/user/info', 1620873296082);
INSERT INTO `base_log` VALUES (2158, '杰普', 'GET', '/user/info', 1620873672453);
INSERT INTO `base_log` VALUES (2159, '杰普', 'GET', '/user/info', 1620873799573);
INSERT INTO `base_log` VALUES (2160, '杰普', 'GET', '/user/info', 1620873820732);
INSERT INTO `base_log` VALUES (2161, '杰普', 'POST', '/baseUser/setRoles', 1620873833326);
INSERT INTO `base_log` VALUES (2162, '杰普', 'POST', '/baseUser/setRoles', 1620873905252);
INSERT INTO `base_log` VALUES (2163, '杰普', 'GET', '/user/info', 1620873929479);
INSERT INTO `base_log` VALUES (2164, '杰普', 'GET', '/user/info', 1620873963598);
INSERT INTO `base_log` VALUES (2165, '杰普', 'POST', '/baseUser/setRoles', 1620873974244);
INSERT INTO `base_log` VALUES (2166, '杰普', 'POST', '/baseUser/setRoles', 1620874010384);
INSERT INTO `base_log` VALUES (2167, '杰普', 'POST', '/baseUser/setRoles', 1620874019052);
INSERT INTO `base_log` VALUES (2168, '杰普', 'GET', '/user/info', 1620874053413);
INSERT INTO `base_log` VALUES (2169, '杰普', 'GET', '/user/info', 1620874105666);
INSERT INTO `base_log` VALUES (2170, '杰普', 'GET', '/user/info', 1620874192656);
INSERT INTO `base_log` VALUES (2171, '杰普', 'GET', '/user/info', 1620874281549);
INSERT INTO `base_log` VALUES (2172, '杰普', 'POST', '/baseUser/setRoles', 1620874293071);
INSERT INTO `base_log` VALUES (2173, '杰普', 'POST', '/baseUser/setRoles', 1620874298352);
INSERT INTO `base_log` VALUES (2174, '杰普', 'POST', '/baseUser/setRoles', 1620874306640);
INSERT INTO `base_log` VALUES (2175, '杰普', 'POST', '/baseUser/setRoles', 1620874312699);
INSERT INTO `base_log` VALUES (2176, '杰普', 'POST', '/baseUser/setRoles', 1620874317445);
INSERT INTO `base_log` VALUES (2177, '杰普', 'POST', '/baseUser/setRoles', 1620874322764);
INSERT INTO `base_log` VALUES (2178, '杰普', 'POST', '/baseUser/setRoles', 1620874328648);
INSERT INTO `base_log` VALUES (2179, '杰普', 'POST', '/baseUser/saveOrUpdate', 1620874347301);
INSERT INTO `base_log` VALUES (2180, '杰普', 'GET', '/user/info', 1620874449977);
INSERT INTO `base_log` VALUES (2181, '杰普', 'GET', '/user/info', 1620874507765);
INSERT INTO `base_log` VALUES (2182, '杰普', 'POST', '/baseUser/addUserWidthRole', 1620874858683);
INSERT INTO `base_log` VALUES (2183, '杰普', 'POST', '/baseUser/saveOrUpdate', 1620874867494);
INSERT INTO `base_log` VALUES (2184, '杰普', 'GET', '/user/info', 1620874878731);
INSERT INTO `base_log` VALUES (2185, '杰普', 'GET', '/user/info', 1620875016711);
INSERT INTO `base_log` VALUES (2186, '杰普', 'POST', '/baseUser/saveOrUpdate', 1620875038699);
INSERT INTO `base_log` VALUES (2187, '杰普', 'POST', '/baseUser/saveOrUpdate', 1620875043336);
INSERT INTO `base_log` VALUES (2188, '杰普', 'POST', '/baseUser/addUserWidthRole', 1620875054099);
INSERT INTO `base_log` VALUES (2189, '杰普', 'GET', '/user/info', 1620875088817);
INSERT INTO `base_log` VALUES (2190, '杰普', 'POST', '/baseUser/saveOrUpdate', 1620875099570);
INSERT INTO `base_log` VALUES (2191, '杰普', 'GET', '/user/info', 1620875170883);
INSERT INTO `base_log` VALUES (2192, '杰普', 'GET', '/user/info', 1620875219807);
INSERT INTO `base_log` VALUES (2193, '杰普', 'GET', '/user/info', 1620875314089);
INSERT INTO `base_log` VALUES (2194, '杰普', 'GET', '/user/info', 1620875355336);
INSERT INTO `base_log` VALUES (2195, '杰普', 'GET', '/user/info', 1620875417613);
INSERT INTO `base_log` VALUES (2196, '杰普', 'GET', '/user/info', 1620875475408);
INSERT INTO `base_log` VALUES (2197, '杰普', 'POST', '/user/login', 1620875487961);
INSERT INTO `base_log` VALUES (2198, '杰普', 'GET', '/user/info', 1620875489597);
INSERT INTO `base_log` VALUES (2199, '杰普', 'GET', '/user/info', 1620875514647);
INSERT INTO `base_log` VALUES (2200, '杰普', 'GET', '/user/info', 1620875520261);
INSERT INTO `base_log` VALUES (2201, '杰普', 'GET', '/user/info', 1620875552674);
INSERT INTO `base_log` VALUES (2202, '杰普', 'GET', '/user/info', 1620875585826);
INSERT INTO `base_log` VALUES (2203, '杰普', 'GET', '/user/info', 1620875601411);
INSERT INTO `base_log` VALUES (2204, '杰普', 'GET', '/user/info', 1620875609794);
INSERT INTO `base_log` VALUES (2205, '杰普', 'GET', '/user/info', 1620875639752);
INSERT INTO `base_log` VALUES (2206, '杰普', 'GET', '/user/info', 1620875655172);
INSERT INTO `base_log` VALUES (2207, '杰普', 'GET', '/user/info', 1620875729089);
INSERT INTO `base_log` VALUES (2208, '杰普', 'GET', '/user/info', 1620875773831);
INSERT INTO `base_log` VALUES (2209, '杰普', 'GET', '/user/info', 1620875785353);
INSERT INTO `base_log` VALUES (2210, '杰普', 'GET', '/user/info', 1620875887202);
INSERT INTO `base_log` VALUES (2211, '杰普', 'GET', '/user/info', 1620875961031);
INSERT INTO `base_log` VALUES (2212, '杰普', 'GET', '/user/info', 1620876040098);
INSERT INTO `base_log` VALUES (2213, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876058892);
INSERT INTO `base_log` VALUES (2214, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876064119);
INSERT INTO `base_log` VALUES (2215, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876069023);
INSERT INTO `base_log` VALUES (2216, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876103254);
INSERT INTO `base_log` VALUES (2217, '杰普', 'GET', '/user/info', 1620876141392);
INSERT INTO `base_log` VALUES (2218, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876141953);
INSERT INTO `base_log` VALUES (2219, '杰普', 'GET', '/user/info', 1620876147964);
INSERT INTO `base_log` VALUES (2220, '杰普', 'GET', '/user/info', 1620876150659);
INSERT INTO `base_log` VALUES (2221, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876402384);
INSERT INTO `base_log` VALUES (2222, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876425955);
INSERT INTO `base_log` VALUES (2223, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876429252);
INSERT INTO `base_log` VALUES (2224, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876434497);
INSERT INTO `base_log` VALUES (2225, '杰普', 'GET', '/user/info', 1620876471643);
INSERT INTO `base_log` VALUES (2226, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876472199);
INSERT INTO `base_log` VALUES (2227, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876480705);
INSERT INTO `base_log` VALUES (2228, '杰普', 'GET', '/user/info', 1620876528006);
INSERT INTO `base_log` VALUES (2229, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876531979);
INSERT INTO `base_log` VALUES (2230, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876536553);
INSERT INTO `base_log` VALUES (2231, '杰普', 'GET', '/user/info', 1620876584435);
INSERT INTO `base_log` VALUES (2232, '杰普', 'GET', '/user/info', 1620876621699);
INSERT INTO `base_log` VALUES (2233, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876642013);
INSERT INTO `base_log` VALUES (2234, '杰普', 'GET', '/user/info', 1620876686601);
INSERT INTO `base_log` VALUES (2235, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876689851);
INSERT INTO `base_log` VALUES (2236, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876692995);
INSERT INTO `base_log` VALUES (2237, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876704195);
INSERT INTO `base_log` VALUES (2238, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876709298);
INSERT INTO `base_log` VALUES (2239, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620876715857);
INSERT INTO `base_log` VALUES (2240, '杰普', 'GET', '/user/info', 1620876726063);
INSERT INTO `base_log` VALUES (2241, '杰普', 'GET', '/user/info', 1620876923795);
INSERT INTO `base_log` VALUES (2242, '杰普', 'GET', '/user/info', 1620877346284);
INSERT INTO `base_log` VALUES (2243, '杰普', 'GET', '/user/info', 1620877436463);
INSERT INTO `base_log` VALUES (2244, '杰普', 'GET', '/user/info', 1620877457799);
INSERT INTO `base_log` VALUES (2245, '杰普', 'POST', '/category/saveOrUpdate', 1620877467122);
INSERT INTO `base_log` VALUES (2246, '杰普', 'POST', '/category/saveOrUpdate', 1620877468157);
INSERT INTO `base_log` VALUES (2247, '杰普', 'POST', '/category/saveOrUpdate', 1620877474601);
INSERT INTO `base_log` VALUES (2248, '杰普', 'POST', '/category/saveOrUpdate', 1620877478558);
INSERT INTO `base_log` VALUES (2249, '杰普', 'GET', '/category/deleteById', 1620877484560);
INSERT INTO `base_log` VALUES (2250, '杰普', 'GET', '/user/info', 1620877560874);
INSERT INTO `base_log` VALUES (2251, '杰普', 'GET', '/user/info', 1620877563379);
INSERT INTO `base_log` VALUES (2252, '杰普', 'GET', '/user/info', 1620877571428);
INSERT INTO `base_log` VALUES (2253, '杰普', 'GET', '/user/info', 1620877573774);
INSERT INTO `base_log` VALUES (2254, '杰普', 'GET', '/user/info', 1620877576077);
INSERT INTO `base_log` VALUES (2255, '杰普', 'GET', '/user/info', 1620877616781);
INSERT INTO `base_log` VALUES (2256, '杰普', 'GET', '/user/info', 1620877825492);
INSERT INTO `base_log` VALUES (2257, '杰普', 'GET', '/user/info', 1620886403393);
INSERT INTO `base_log` VALUES (2258, '杰普', 'GET', '/user/info', 1620886653417);
INSERT INTO `base_log` VALUES (2259, '刘亚蓉', 'GET', '/user/info', 1620886675521);
INSERT INTO `base_log` VALUES (2260, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620886692164);
INSERT INTO `base_log` VALUES (2261, '杰普', 'GET', '/user/info', 1620887427738);
INSERT INTO `base_log` VALUES (2262, '杰普', 'GET', '/user/info', 1620887542067);
INSERT INTO `base_log` VALUES (2263, '杰普', 'GET', '/user/info', 1620887556871);
INSERT INTO `base_log` VALUES (2264, '刘亚蓉', 'GET', '/user/info', 1620887557310);
INSERT INTO `base_log` VALUES (2265, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620887560734);
INSERT INTO `base_log` VALUES (2266, '杰普', 'POST', '/user/login', 1620887564193);
INSERT INTO `base_log` VALUES (2267, '杰普', 'GET', '/user/info', 1620887566284);
INSERT INTO `base_log` VALUES (2268, '杰普', 'GET', '/baseUser/deleteById', 1620887570853);
INSERT INTO `base_log` VALUES (2269, '杰普', 'GET', '/user/info', 1620887584398);
INSERT INTO `base_log` VALUES (2270, '杰普', 'GET', '/user/info', 1620887653265);
INSERT INTO `base_log` VALUES (2271, '杰普', 'GET', '/user/info', 1620887760809);
INSERT INTO `base_log` VALUES (2272, '刘亚蓉', 'GET', '/user/info', 1620887762983);
INSERT INTO `base_log` VALUES (2273, '杰普', 'GET', '/user/info', 1620887952080);
INSERT INTO `base_log` VALUES (2274, '杰普', 'GET', '/user/info', 1620888013979);
INSERT INTO `base_log` VALUES (2275, '杰普', 'GET', '/user/info', 1620888020765);
INSERT INTO `base_log` VALUES (2276, '杰普', 'GET', '/user/info', 1620888025668);
INSERT INTO `base_log` VALUES (2277, '杰普', 'GET', '/user/info', 1620888027681);
INSERT INTO `base_log` VALUES (2278, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620888173132);
INSERT INTO `base_log` VALUES (2279, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620888242269);
INSERT INTO `base_log` VALUES (2280, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620888253290);
INSERT INTO `base_log` VALUES (2281, '杰普', 'GET', '/user/info', 1620888288361);
INSERT INTO `base_log` VALUES (2282, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620888288976);
INSERT INTO `base_log` VALUES (2283, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620888314726);
INSERT INTO `base_log` VALUES (2284, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620888328004);
INSERT INTO `base_log` VALUES (2285, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620888343912);
INSERT INTO `base_log` VALUES (2286, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620888353934);
INSERT INTO `base_log` VALUES (2287, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620888358765);
INSERT INTO `base_log` VALUES (2288, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620888372416);
INSERT INTO `base_log` VALUES (2289, '杰普', 'GET', '/user/info', 1620888375685);
INSERT INTO `base_log` VALUES (2290, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620888376239);
INSERT INTO `base_log` VALUES (2291, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620888523731);
INSERT INTO `base_log` VALUES (2292, '杰普', 'GET', '/user/info', 1620888582675);
INSERT INTO `base_log` VALUES (2293, '杰普', 'GET', '/user/info', 1620888659545);
INSERT INTO `base_log` VALUES (2294, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620888661456);
INSERT INTO `base_log` VALUES (2295, '杰普', 'GET', '/user/info', 1620888681572);
INSERT INTO `base_log` VALUES (2296, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620888682579);
INSERT INTO `base_log` VALUES (2297, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620888937967);
INSERT INTO `base_log` VALUES (2298, '杰普', 'GET', '/user/info', 1620889125552);
INSERT INTO `base_log` VALUES (2299, '杰普', 'GET', '/user/info', 1620889158923);
INSERT INTO `base_log` VALUES (2300, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620889160544);
INSERT INTO `base_log` VALUES (2301, '杰普', 'POST', '/role/saveOrUpdate', 1620889168257);
INSERT INTO `base_log` VALUES (2302, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620889168484);
INSERT INTO `base_log` VALUES (2303, '杰普', 'POST', '/role/authorization', 1620889177700);
INSERT INTO `base_log` VALUES (2304, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620889178535);
INSERT INTO `base_log` VALUES (2305, '杰普', 'POST', '/baseUser/saveOrUpdate', 1620889190936);
INSERT INTO `base_log` VALUES (2306, '杰普', 'POST', '/user/logout', 1620889196131);
INSERT INTO `base_log` VALUES (2307, '刘亮', 'GET', '/user/info', 1620889203081);
INSERT INTO `base_log` VALUES (2308, '刘亮', 'POST', '/user/logout', 1620889210015);
INSERT INTO `base_log` VALUES (2309, '刘亚蓉', 'GET', '/user/info', 1620889223533);
INSERT INTO `base_log` VALUES (2310, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889228798);
INSERT INTO `base_log` VALUES (2311, '刘亚蓉', 'GET', '/user/info', 1620889238048);
INSERT INTO `base_log` VALUES (2312, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889239371);
INSERT INTO `base_log` VALUES (2313, '刘亚蓉', 'POST', '/role/saveOrUpdate', 1620889242596);
INSERT INTO `base_log` VALUES (2314, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889242768);
INSERT INTO `base_log` VALUES (2315, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889270957);
INSERT INTO `base_log` VALUES (2316, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889281112);
INSERT INTO `base_log` VALUES (2317, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889289973);
INSERT INTO `base_log` VALUES (2318, '刘亚蓉', 'GET', '/user/info', 1620889321564);
INSERT INTO `base_log` VALUES (2319, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889322377);
INSERT INTO `base_log` VALUES (2320, '刘亚蓉', 'POST', '/role/saveOrUpdate', 1620889326081);
INSERT INTO `base_log` VALUES (2321, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889326318);
INSERT INTO `base_log` VALUES (2322, '刘亚蓉', 'POST', '/role/saveOrUpdate', 1620889329787);
INSERT INTO `base_log` VALUES (2323, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889329989);
INSERT INTO `base_log` VALUES (2324, '刘亚蓉', 'POST', '/role/authorization', 1620889340286);
INSERT INTO `base_log` VALUES (2325, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889340779);
INSERT INTO `base_log` VALUES (2326, '刘亚蓉', 'GET', '/role/deleteById', 1620889344773);
INSERT INTO `base_log` VALUES (2327, '刘亚蓉', 'POST', '/role/authorization', 1620889367947);
INSERT INTO `base_log` VALUES (2328, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889401997);
INSERT INTO `base_log` VALUES (2329, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889464050);
INSERT INTO `base_log` VALUES (2330, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889529023);
INSERT INTO `base_log` VALUES (2331, '刘亚蓉', 'GET', '/role/deleteById', 1620889542093);
INSERT INTO `base_log` VALUES (2332, '刘亚蓉', 'POST', '/role/authorization', 1620889574168);
INSERT INTO `base_log` VALUES (2333, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889574258);
INSERT INTO `base_log` VALUES (2334, '刘亚蓉', 'GET', '/user/info', 1620889616479);
INSERT INTO `base_log` VALUES (2335, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889617605);
INSERT INTO `base_log` VALUES (2336, '刘亚蓉', 'POST', '/role/authorization', 1620889635039);
INSERT INTO `base_log` VALUES (2337, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889748025);
INSERT INTO `base_log` VALUES (2338, '刘亚蓉', 'GET', '/user/info', 1620889754019);
INSERT INTO `base_log` VALUES (2339, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889755284);
INSERT INTO `base_log` VALUES (2340, '刘亚蓉', 'POST', '/role/authorization', 1620889769762);
INSERT INTO `base_log` VALUES (2341, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889770426);
INSERT INTO `base_log` VALUES (2342, '刘亚蓉', 'GET', '/role/deleteById', 1620889806051);
INSERT INTO `base_log` VALUES (2343, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889806433);
INSERT INTO `base_log` VALUES (2344, '刘亚蓉', 'GET', '/user/info', 1620889876534);
INSERT INTO `base_log` VALUES (2345, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889877640);
INSERT INTO `base_log` VALUES (2346, '刘亚蓉', 'GET', '/user/info', 1620889889054);
INSERT INTO `base_log` VALUES (2347, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889891351);
INSERT INTO `base_log` VALUES (2348, '刘亚蓉', 'GET', '/user/info', 1620889989107);
INSERT INTO `base_log` VALUES (2349, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620889990203);
INSERT INTO `base_log` VALUES (2350, '刘亚蓉', 'GET', '/user/info', 1620890471098);
INSERT INTO `base_log` VALUES (2351, '刘亚蓉', 'GET', '/user/info', 1620890714864);
INSERT INTO `base_log` VALUES (2352, '刘亚蓉', 'GET', '/user/info', 1620890728870);
INSERT INTO `base_log` VALUES (2353, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620890745189);
INSERT INTO `base_log` VALUES (2354, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620890767625);
INSERT INTO `base_log` VALUES (2355, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620890778091);
INSERT INTO `base_log` VALUES (2356, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620890784247);
INSERT INTO `base_log` VALUES (2357, '刘亚蓉', 'GET', '/user/info', 1620890899273);
INSERT INTO `base_log` VALUES (2358, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620890916944);
INSERT INTO `base_log` VALUES (2359, '刘亚蓉', 'DELETE', '/privilege/deleteById', 1620890922671);
INSERT INTO `base_log` VALUES (2360, '刘亚蓉', 'DELETE', '/privilege/deleteById', 1620890925530);
INSERT INTO `base_log` VALUES (2361, '刘亚蓉', 'POST', '/baseUser/saveOrUpdate', 1620890970594);
INSERT INTO `base_log` VALUES (2362, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620890984637);
INSERT INTO `base_log` VALUES (2363, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620890988967);
INSERT INTO `base_log` VALUES (2364, '刘亚蓉', 'GET', '/user/info', 1620891168064);
INSERT INTO `base_log` VALUES (2365, '杰普', 'POST', '/user/login', 1620891260962);
INSERT INTO `base_log` VALUES (2366, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620891265155);
INSERT INTO `base_log` VALUES (2367, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620891288980);
INSERT INTO `base_log` VALUES (2368, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620891301924);
INSERT INTO `base_log` VALUES (2369, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620891305694);
INSERT INTO `base_log` VALUES (2370, '刘亚蓉', 'POST', '/role/authorization', 1620891311310);
INSERT INTO `base_log` VALUES (2371, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620891311506);
INSERT INTO `base_log` VALUES (2372, '刘亚蓉', 'GET', '/user/info', 1620891315779);
INSERT INTO `base_log` VALUES (2373, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620891316359);
INSERT INTO `base_log` VALUES (2374, '刘亚蓉', 'GET', '/user/info', 1620891329720);
INSERT INTO `base_log` VALUES (2375, '刘亚蓉', 'GET', '/user/info', 1620891430142);
INSERT INTO `base_log` VALUES (2376, '杰普', 'POST', '/user/login', 1620891539623);
INSERT INTO `base_log` VALUES (2377, '刘亚蓉', 'GET', '/user/info', 1620892305602);
INSERT INTO `base_log` VALUES (2378, '刘亚蓉', 'GET', '/user/info', 1620892330864);
INSERT INTO `base_log` VALUES (2379, '刘亚蓉', 'GET', '/user/info', 1620892840139);
INSERT INTO `base_log` VALUES (2380, '杰普', 'GET', '/user/info', 1620892860022);
INSERT INTO `base_log` VALUES (2381, '刘亚蓉', 'GET', '/user/info', 1620893361763);
INSERT INTO `base_log` VALUES (2382, '刘亚蓉', 'GET', '/user/info', 1620893554711);
INSERT INTO `base_log` VALUES (2383, '杰普', 'POST', '/user/login', 1620893590444);
INSERT INTO `base_log` VALUES (2384, '刘亚蓉', 'GET', '/user/info', 1620893848303);
INSERT INTO `base_log` VALUES (2385, '刘亚蓉', 'GET', '/alipay/test', 1620895320064);
INSERT INTO `base_log` VALUES (2386, '刘亚蓉', 'GET', '/user/info', 1620895898276);
INSERT INTO `base_log` VALUES (2387, '刘亚蓉', 'GET', '/user/info', 1620895976345);
INSERT INTO `base_log` VALUES (2388, '刘亚蓉', 'GET', '/user/info', 1620895989485);
INSERT INTO `base_log` VALUES (2389, '杰普', 'POST', '/user/login', 1620895994933);
INSERT INTO `base_log` VALUES (2390, '刘亚蓉', 'GET', '/user/info', 1620896004106);
INSERT INTO `base_log` VALUES (2391, '杰普', 'POST', '/user/login', 1620896008595);
INSERT INTO `base_log` VALUES (2392, '杰普', 'POST', '/user/login', 1620896061877);
INSERT INTO `base_log` VALUES (2393, '刘亚蓉', 'GET', '/user/info', 1620896102080);
INSERT INTO `base_log` VALUES (2394, '杰普', 'POST', '/user/login', 1620896789597);
INSERT INTO `base_log` VALUES (2395, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620897034750);
INSERT INTO `base_log` VALUES (2396, '杰普', 'POST', '/user/login', 1620897066447);
INSERT INTO `base_log` VALUES (2397, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620897071555);
INSERT INTO `base_log` VALUES (2398, '杰普', 'POST', '/user/login', 1620897077942);
INSERT INTO `base_log` VALUES (2399, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620897087493);
INSERT INTO `base_log` VALUES (2400, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620897116502);
INSERT INTO `base_log` VALUES (2401, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620897144060);
INSERT INTO `base_log` VALUES (2402, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620897151377);
INSERT INTO `base_log` VALUES (2403, '刘亚蓉', 'POST', '/role/authorization', 1620897158731);
INSERT INTO `base_log` VALUES (2404, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620897159337);
INSERT INTO `base_log` VALUES (2405, '刘亚蓉', 'GET', '/user/info', 1620897162978);
INSERT INTO `base_log` VALUES (2406, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620897163734);
INSERT INTO `base_log` VALUES (2407, '刘亚蓉', 'GET', '/user/info', 1620897217807);
INSERT INTO `base_log` VALUES (2408, '杰普', 'POST', '/user/login', 1620897273181);
INSERT INTO `base_log` VALUES (2409, '刘亚蓉', 'GET', '/user/info', 1620897274163);
INSERT INTO `base_log` VALUES (2410, '杰普', 'POST', '/user/login', 1620897286466);
INSERT INTO `base_log` VALUES (2411, '杰普', 'POST', '/user/login', 1620897374754);
INSERT INTO `base_log` VALUES (2412, '杰普', 'POST', '/user/login', 1620897379761);
INSERT INTO `base_log` VALUES (2413, '杰普', 'POST', '/user/login', 1620897716696);
INSERT INTO `base_log` VALUES (2414, '刘亚蓉', 'GET', '/user/info', 1620897844614);
INSERT INTO `base_log` VALUES (2415, '杰普', 'GET', '/user/info', 1620898104565);
INSERT INTO `base_log` VALUES (2416, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620898110410);
INSERT INTO `base_log` VALUES (2417, '杰普', 'POST', '/user/login', 1620898239519);
INSERT INTO `base_log` VALUES (2418, '杰普', 'GET', '/user/info', 1620898244062);
INSERT INTO `base_log` VALUES (2419, '杰普', 'POST', '/order/submitOrder', 1620908185668);
INSERT INTO `base_log` VALUES (2420, '杰普', 'POST', '/order/submitOrder', 1620908198424);
INSERT INTO `base_log` VALUES (2421, '杰普', 'POST', '/order/submitOrder', 1620944873939);
INSERT INTO `base_log` VALUES (2422, '杰普', 'GET', '/account/recharge', 1620944910437);
INSERT INTO `base_log` VALUES (2423, '杰普', 'GET', '/account/recharge', 1620944916030);
INSERT INTO `base_log` VALUES (2424, '杰普', 'POST', '/order/submitOrder', 1620944949492);
INSERT INTO `base_log` VALUES (2425, '杰普', 'POST', '/order/submitOrder', 1620945262411);
INSERT INTO `base_log` VALUES (2426, '杰普', 'GET', '/account/recharge', 1620945276260);
INSERT INTO `base_log` VALUES (2427, '杰普', 'POST', '/order/submitOrder', 1620945287143);
INSERT INTO `base_log` VALUES (2428, '刘亚蓉', 'GET', '/user/info', 1620955137947);
INSERT INTO `base_log` VALUES (2429, '杰普', 'GET', '/user/info', 1620955140747);
INSERT INTO `base_log` VALUES (2430, '杰普', 'DELETE', '/privilege/deleteById', 1620955195031);
INSERT INTO `base_log` VALUES (2431, '杰普', 'DELETE', '/privilege/deleteById', 1620955197898);
INSERT INTO `base_log` VALUES (2432, '杰普', 'GET', '/user/info', 1620955202611);
INSERT INTO `base_log` VALUES (2433, '杰普', 'GET', '/role/cascadePrivilegeFindAll', 1620955212141);
INSERT INTO `base_log` VALUES (2434, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620955530951);
INSERT INTO `base_log` VALUES (2435, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620955598683);
INSERT INTO `base_log` VALUES (2436, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620955633392);
INSERT INTO `base_log` VALUES (2437, '杰普', 'POST', '/user/login', 1620955717576);
INSERT INTO `base_log` VALUES (2438, '杰普', 'POST', '/user/login', 1620956089270);
INSERT INTO `base_log` VALUES (2439, '刘亚蓉', 'GET', '/user/info', 1620956500195);
INSERT INTO `base_log` VALUES (2440, '刘亚蓉', 'GET', '/user/info', 1620956677779);
INSERT INTO `base_log` VALUES (2441, '刘亚蓉', 'GET', '/user/info', 1620956863755);
INSERT INTO `base_log` VALUES (2442, '刘亚蓉', 'GET', '/user/info', 1620956948681);
INSERT INTO `base_log` VALUES (2443, '刘亚蓉', 'GET', '/user/info', 1620957074571);
INSERT INTO `base_log` VALUES (2444, '刘亚蓉', 'GET', '/user/info', 1620958633795);
INSERT INTO `base_log` VALUES (2445, '刘亚蓉', 'GET', '/user/info', 1620958763293);
INSERT INTO `base_log` VALUES (2446, '刘亚蓉', 'GET', '/user/info', 1620958813377);
INSERT INTO `base_log` VALUES (2447, '刘亚蓉', 'GET', '/user/info', 1620959468781);
INSERT INTO `base_log` VALUES (2448, '刘亚蓉', 'GET', '/user/info', 1620959763651);
INSERT INTO `base_log` VALUES (2449, '刘亚蓉', 'GET', '/user/info', 1620960255963);
INSERT INTO `base_log` VALUES (2450, '杰普', 'POST', '/user/registerEmployee', 1620960413534);
INSERT INTO `base_log` VALUES (2451, '杰普', 'POST', '/user/registerEmployee', 1620960841566);
INSERT INTO `base_log` VALUES (2452, '杰普', 'POST', '/user/login', 1620960857761);
INSERT INTO `base_log` VALUES (2453, '杰普', 'POST', '/user/login', 1620960867933);
INSERT INTO `base_log` VALUES (2454, '杰普', 'POST', '/user/login', 1620960976618);
INSERT INTO `base_log` VALUES (2455, NULL, 'POST', '/user/registerEmployee', 1620961016504);
INSERT INTO `base_log` VALUES (2456, NULL, 'POST', '/user/login', 1620961033093);
INSERT INTO `base_log` VALUES (2457, NULL, 'POST', '/user/login', 1620961076276);
INSERT INTO `base_log` VALUES (2458, '杰普', 'GET', '/user/info', 1620961110606);
INSERT INTO `base_log` VALUES (2459, NULL, 'POST', '/user/registerEmployee', 1620961116076);
INSERT INTO `base_log` VALUES (2460, NULL, 'POST', '/user/login', 1620961131637);
INSERT INTO `base_log` VALUES (2461, NULL, 'GET', '/user/info', 1620961173691);
INSERT INTO `base_log` VALUES (2462, NULL, 'POST', '/user/login', 1620961211742);
INSERT INTO `base_log` VALUES (2463, NULL, 'POST', '/user/registerEmployee', 1620961572476);
INSERT INTO `base_log` VALUES (2464, NULL, 'POST', '/user/registerEmployee', 1620961700870);
INSERT INTO `base_log` VALUES (2465, NULL, 'POST', '/user/registerEmployee', 1620961717163);
INSERT INTO `base_log` VALUES (2466, NULL, 'POST', '/user/login', 1620961731336);
INSERT INTO `base_log` VALUES (2467, NULL, 'POST', '/user/login', 1620961746029);
INSERT INTO `base_log` VALUES (2468, NULL, 'POST', '/user/registerEmployee', 1620961762741);
INSERT INTO `base_log` VALUES (2469, NULL, 'POST', '/user/login', 1620961770247);
INSERT INTO `base_log` VALUES (2470, NULL, 'POST', '/user/registerEmployee', 1620961961743);
INSERT INTO `base_log` VALUES (2471, NULL, 'POST', '/user/login', 1620961969622);
INSERT INTO `base_log` VALUES (2472, NULL, 'POST', '/user/registerEmployee', 1620962015567);
INSERT INTO `base_log` VALUES (2473, NULL, 'POST', '/user/login', 1620962024527);
INSERT INTO `base_log` VALUES (2474, '刘亚蓉', 'GET', '/user/info', 1620962379282);
INSERT INTO `base_log` VALUES (2475, NULL, 'POST', '/user/login', 1620962875307);
INSERT INTO `base_log` VALUES (2476, '杰普', 'POST', '/user/login', 1620962983897);
INSERT INTO `base_log` VALUES (2477, '杰普', 'POST', '/user/login', 1620963001106);
INSERT INTO `base_log` VALUES (2478, '杰普', 'GET', '/user/info', 1620963207931);
INSERT INTO `base_log` VALUES (2479, '杰普', 'GET', '/user/info', 1620963546796);
INSERT INTO `base_log` VALUES (2480, NULL, 'GET', '/user/info', 1620963851307);
INSERT INTO `base_log` VALUES (2481, NULL, 'GET', '/user/info', 1620963853045);
INSERT INTO `base_log` VALUES (2482, '杰普', 'GET', '/user/info', 1620972753644);
INSERT INTO `base_log` VALUES (2483, '杰普', 'GET', '/user/info', 1620976517133);
INSERT INTO `base_log` VALUES (2484, '杰普', 'GET', '/user/info', 1620976519306);
INSERT INTO `base_log` VALUES (2485, '杰普', 'POST', '/user/login', 1620977216843);
INSERT INTO `base_log` VALUES (2486, '杰普', 'GET', '/user/info', 1620977385546);
INSERT INTO `base_log` VALUES (2487, '杰普', 'GET', '/user/info', 1620980698120);
INSERT INTO `base_log` VALUES (2488, '杰普', 'GET', '/user/info', 1620980698192);
INSERT INTO `base_log` VALUES (2489, '杰普', 'GET', '/user/info', 1620980764853);
INSERT INTO `base_log` VALUES (2490, '杰普', 'POST', '/user/logout', 1620981716575);
INSERT INTO `base_log` VALUES (2491, '刘亚蓉', 'GET', '/user/info', 1620981722478);
INSERT INTO `base_log` VALUES (2492, '刘亚蓉', 'GET', '/user/info', 1620982591521);
INSERT INTO `base_log` VALUES (2493, '刘亚蓉', 'GET', '/user/info', 1620982652452);
INSERT INTO `base_log` VALUES (2494, '刘亚蓉', 'GET', '/user/info', 1620982661427);
INSERT INTO `base_log` VALUES (2495, '杰普', 'POST', '/user/login', 1620982666321);
INSERT INTO `base_log` VALUES (2496, '杰普', 'GET', '/user/info', 1620982669718);
INSERT INTO `base_log` VALUES (2497, '刘亚蓉', 'GET', '/user/info', 1620982671406);
INSERT INTO `base_log` VALUES (2498, '刘亚蓉', 'GET', '/user/info', 1620982679823);
INSERT INTO `base_log` VALUES (2499, '刘亚蓉', 'GET', '/user/info', 1620983348533);
INSERT INTO `base_log` VALUES (2500, '杰普', 'POST', '/user/login', 1620983659595);
INSERT INTO `base_log` VALUES (2501, '杰普', 'POST', '/user/login', 1620983671028);
INSERT INTO `base_log` VALUES (2502, '杰普', 'GET', '/user/info', 1620983671254);
INSERT INTO `base_log` VALUES (2503, '杰普', 'GET', '/user/info', 1620984037874);
INSERT INTO `base_log` VALUES (2504, '杰普', 'GET', '/user/info', 1620984055531);
INSERT INTO `base_log` VALUES (2505, '杰普', 'GET', '/user/info', 1620984128461);
INSERT INTO `base_log` VALUES (2506, '杰普', 'GET', '/user/info', 1620984320657);
INSERT INTO `base_log` VALUES (2507, '杰普', 'GET', '/user/info', 1620984339014);
INSERT INTO `base_log` VALUES (2508, '杰普', 'GET', '/user/info', 1620984361662);
INSERT INTO `base_log` VALUES (2509, '杰普', 'GET', '/user/info', 1620984373458);
INSERT INTO `base_log` VALUES (2510, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620984746393);
INSERT INTO `base_log` VALUES (2511, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620984764189);
INSERT INTO `base_log` VALUES (2512, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620984794377);
INSERT INTO `base_log` VALUES (2513, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620984806683);
INSERT INTO `base_log` VALUES (2514, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1620984811520);
INSERT INTO `base_log` VALUES (2515, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620984819949);
INSERT INTO `base_log` VALUES (2516, '刘亚蓉', 'POST', '/role/authorization', 1620984837569);
INSERT INTO `base_log` VALUES (2517, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620984838255);
INSERT INTO `base_log` VALUES (2518, '刘亚蓉', 'GET', '/user/info', 1620984841766);
INSERT INTO `base_log` VALUES (2519, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1620984842690);
INSERT INTO `base_log` VALUES (2520, '刘亚蓉', 'GET', '/user/info', 1620984967257);
INSERT INTO `base_log` VALUES (2521, '刘亚蓉', 'GET', '/user/info', 1620985083315);
INSERT INTO `base_log` VALUES (2522, '刘亚蓉', 'GET', '/user/info', 1620985225101);
INSERT INTO `base_log` VALUES (2523, '刘亚蓉', 'GET', '/user/info', 1620985412369);
INSERT INTO `base_log` VALUES (2524, '刘亚蓉', 'GET', '/user/info', 1620985445864);
INSERT INTO `base_log` VALUES (2525, '刘亚蓉', 'GET', '/user/info', 1620985563970);
INSERT INTO `base_log` VALUES (2526, '杰普', 'GET', '/user/info', 1620985601125);
INSERT INTO `base_log` VALUES (2527, '杰普', 'GET', '/user/info', 1620985613367);
INSERT INTO `base_log` VALUES (2528, '杰普', 'GET', '/user/info', 1620985616618);
INSERT INTO `base_log` VALUES (2529, '杰普', 'GET', '/user/info', 1620985687521);
INSERT INTO `base_log` VALUES (2530, '刘亚蓉', 'GET', '/user/info', 1620985781562);
INSERT INTO `base_log` VALUES (2531, '刘亚蓉', 'GET', '/user/info', 1620985795685);
INSERT INTO `base_log` VALUES (2532, '刘亚蓉', 'GET', '/user/info', 1620985825410);
INSERT INTO `base_log` VALUES (2533, '刘亚蓉', 'GET', '/user/info', 1620985854277);
INSERT INTO `base_log` VALUES (2534, '杰普', 'GET', '/user/info', 1620985897600);
INSERT INTO `base_log` VALUES (2535, '刘亚蓉', 'GET', '/user/info', 1620985931119);
INSERT INTO `base_log` VALUES (2536, '刘亚蓉', 'GET', '/user/info', 1620985934235);
INSERT INTO `base_log` VALUES (2537, '刘亚蓉', 'GET', '/user/info', 1620985989067);
INSERT INTO `base_log` VALUES (2538, '刘亚蓉', 'GET', '/user/info', 1620986040970);
INSERT INTO `base_log` VALUES (2539, '刘亚蓉', 'GET', '/user/info', 1620986233257);
INSERT INTO `base_log` VALUES (2540, '刘亚蓉', 'GET', '/user/info', 1620986408238);
INSERT INTO `base_log` VALUES (2541, '刘亚蓉', 'GET', '/user/info', 1620986418254);
INSERT INTO `base_log` VALUES (2542, '刘亚蓉', 'GET', '/user/info', 1620986419414);
INSERT INTO `base_log` VALUES (2543, '刘亚蓉', 'GET', '/user/info', 1620986441365);
INSERT INTO `base_log` VALUES (2544, '刘亚蓉', 'GET', '/user/info', 1620986504435);
INSERT INTO `base_log` VALUES (2545, '杰普', 'GET', '/user/info', 1620986796147);
INSERT INTO `base_log` VALUES (2546, '杰普', 'GET', '/user/info', 1621077254262);
INSERT INTO `base_log` VALUES (2547, '杰普', 'POST', '/user/login', 1621077656181);
INSERT INTO `base_log` VALUES (2548, '杰普', 'GET', '/user/info', 1621077674086);
INSERT INTO `base_log` VALUES (2549, '杰普', 'GET', '/user/info', 1621077679991);
INSERT INTO `base_log` VALUES (2550, '杰普', 'GET', '/user/info', 1621078829193);
INSERT INTO `base_log` VALUES (2551, '杰普', 'GET', '/user/info', 1621080345398);
INSERT INTO `base_log` VALUES (2552, '杰普', 'GET', '/user/info', 1621080347979);
INSERT INTO `base_log` VALUES (2553, '杰普', 'POST', '/user/login', 1621080634586);
INSERT INTO `base_log` VALUES (2554, '杰普', 'POST', '/user/login', 1621080645929);
INSERT INTO `base_log` VALUES (2555, '杰普', 'POST', '/user/login', 1621081587573);
INSERT INTO `base_log` VALUES (2556, '杰普', 'POST', '/user/login', 1621081644152);
INSERT INTO `base_log` VALUES (2557, '杰普', 'GET', '/user/info', 1621084562080);
INSERT INTO `base_log` VALUES (2558, '杰普', 'GET', '/user/info', 1621164605927);
INSERT INTO `base_log` VALUES (2559, '杰普', 'GET', '/user/info', 1621164605927);
INSERT INTO `base_log` VALUES (2560, '杰普', 'POST', '/user/login', 1621164843400);
INSERT INTO `base_log` VALUES (2561, '杰普', 'GET', '/user/info', 1621164843773);
INSERT INTO `base_log` VALUES (2562, '杰普', 'GET', '/user/info', 1621164844093);
INSERT INTO `base_log` VALUES (2563, '杰普', 'POST', '/user/login', 1621164928147);
INSERT INTO `base_log` VALUES (2564, '杰普', 'POST', '/user/login', 1621165017737);
INSERT INTO `base_log` VALUES (2565, '杰普', 'GET', '/user/info', 1621165017976);
INSERT INTO `base_log` VALUES (2566, '杰普', 'POST', '/user/login', 1621165057062);
INSERT INTO `base_log` VALUES (2567, '杰普', 'GET', '/user/info', 1621165057383);
INSERT INTO `base_log` VALUES (2568, '杰普', 'POST', '/user/login', 1621165137937);
INSERT INTO `base_log` VALUES (2569, '杰普', 'GET', '/user/info', 1621165138193);
INSERT INTO `base_log` VALUES (2570, '杰普', 'POST', '/user/login', 1621165158993);
INSERT INTO `base_log` VALUES (2571, '杰普', 'GET', '/user/info', 1621165159300);
INSERT INTO `base_log` VALUES (2572, '杰普', 'POST', '/user/login', 1621165174883);
INSERT INTO `base_log` VALUES (2573, '杰普', 'GET', '/user/info', 1621165175117);
INSERT INTO `base_log` VALUES (2574, '杰普', 'POST', '/user/login', 1621165183247);
INSERT INTO `base_log` VALUES (2575, '杰普', 'GET', '/user/info', 1621165183936);
INSERT INTO `base_log` VALUES (2576, '杰普', 'POST', '/user/login', 1621165429273);
INSERT INTO `base_log` VALUES (2577, '杰普', 'GET', '/user/info', 1621165429474);
INSERT INTO `base_log` VALUES (2578, '杰普', 'POST', '/user/login', 1621165474088);
INSERT INTO `base_log` VALUES (2579, '杰普', 'GET', '/user/info', 1621165474420);
INSERT INTO `base_log` VALUES (2580, '杰普', 'POST', '/user/login', 1621165493817);
INSERT INTO `base_log` VALUES (2581, '杰普', 'GET', '/user/info', 1621165494065);
INSERT INTO `base_log` VALUES (2582, '杰普', 'POST', '/user/login', 1621165514341);
INSERT INTO `base_log` VALUES (2583, '杰普', 'GET', '/user/info', 1621165514634);
INSERT INTO `base_log` VALUES (2584, '杰普', 'POST', '/user/login', 1621165922515);
INSERT INTO `base_log` VALUES (2585, '杰普', 'POST', '/user/login', 1621166194107);
INSERT INTO `base_log` VALUES (2586, '杰普', 'POST', '/user/login', 1621166335163);
INSERT INTO `base_log` VALUES (2587, '杰普', 'GET', '/user/info', 1621166335414);
INSERT INTO `base_log` VALUES (2588, '杰普', 'POST', '/user/login', 1621166543171);
INSERT INTO `base_log` VALUES (2589, '杰普', 'GET', '/user/info', 1621166543352);
INSERT INTO `base_log` VALUES (2590, '杰普', 'POST', '/user/login', 1621166561053);
INSERT INTO `base_log` VALUES (2591, '杰普', 'GET', '/user/info', 1621166561415);
INSERT INTO `base_log` VALUES (2592, '杰普', 'POST', '/user/login', 1621166680930);
INSERT INTO `base_log` VALUES (2593, '杰普', 'GET', '/user/info', 1621166681291);
INSERT INTO `base_log` VALUES (2594, '杰普', 'POST', '/user/login', 1621166849373);
INSERT INTO `base_log` VALUES (2595, '杰普', 'GET', '/user/info', 1621166849660);
INSERT INTO `base_log` VALUES (2596, '杰普', 'POST', '/user/login', 1621166878653);
INSERT INTO `base_log` VALUES (2597, '杰普', 'GET', '/user/info', 1621166878953);
INSERT INTO `base_log` VALUES (2598, '杰普', 'GET', '/user/info', 1621167005831);
INSERT INTO `base_log` VALUES (2599, '杰普', 'POST', '/user/login', 1621167010969);
INSERT INTO `base_log` VALUES (2600, '杰普', 'GET', '/user/info', 1621167011259);
INSERT INTO `base_log` VALUES (2601, '杰普', 'GET', '/user/info', 1621167011561);
INSERT INTO `base_log` VALUES (2602, '杰普', 'GET', '/user/info', 1621167041977);
INSERT INTO `base_log` VALUES (2603, '杰普', 'GET', '/order/sendOrder', 1621167196999);
INSERT INTO `base_log` VALUES (2604, '杰普', 'GET', '/user/info', 1621167361805);
INSERT INTO `base_log` VALUES (2605, '杰普', 'GET', '/user/info', 1621167394125);
INSERT INTO `base_log` VALUES (2606, '杰普', 'GET', '/user/info', 1621167576289);
INSERT INTO `base_log` VALUES (2607, '杰普', 'GET', '/user/info', 1621167647700);
INSERT INTO `base_log` VALUES (2608, '杰普', 'GET', '/user/info', 1621167804996);
INSERT INTO `base_log` VALUES (2609, '杰普', 'GET', '/user/info', 1621167868446);
INSERT INTO `base_log` VALUES (2610, '杰普', 'GET', '/user/info', 1621167945732);
INSERT INTO `base_log` VALUES (2611, '杰普', 'GET', '/user/info', 1621168036968);
INSERT INTO `base_log` VALUES (2612, '杰普', 'GET', '/user/info', 1621168047247);
INSERT INTO `base_log` VALUES (2613, '杰普', 'GET', '/user/info', 1621168077533);
INSERT INTO `base_log` VALUES (2614, '杰普', 'GET', '/user/info', 1621168152124);
INSERT INTO `base_log` VALUES (2615, '杰普', 'GET', '/user/info', 1621168161752);
INSERT INTO `base_log` VALUES (2616, '杰普', 'GET', '/user/info', 1621168176195);
INSERT INTO `base_log` VALUES (2617, '杰普', 'GET', '/user/info', 1621168207448);
INSERT INTO `base_log` VALUES (2618, '杰普', 'GET', '/user/info', 1621168226430);
INSERT INTO `base_log` VALUES (2619, '杰普', 'GET', '/user/info', 1621168235297);
INSERT INTO `base_log` VALUES (2620, '杰普', 'GET', '/user/info', 1621168245350);
INSERT INTO `base_log` VALUES (2621, '杰普', 'GET', '/user/info', 1621168323827);
INSERT INTO `base_log` VALUES (2622, '杰普', 'POST', '/user/login', 1621168709143);
INSERT INTO `base_log` VALUES (2623, '杰普', 'GET', '/user/info', 1621168709325);
INSERT INTO `base_log` VALUES (2624, '杰普', 'GET', '/user/info', 1621168760992);
INSERT INTO `base_log` VALUES (2625, '杰普', 'GET', '/user/info', 1621168776326);
INSERT INTO `base_log` VALUES (2626, '杰普', 'GET', '/user/info', 1621168829027);
INSERT INTO `base_log` VALUES (2627, '杰普', 'GET', '/user/info', 1621168855513);
INSERT INTO `base_log` VALUES (2628, '杰普', 'GET', '/user/info', 1621168877447);
INSERT INTO `base_log` VALUES (2629, '杰普', 'GET', '/user/info', 1621168917681);
INSERT INTO `base_log` VALUES (2630, '杰普', 'GET', '/user/info', 1621168929988);
INSERT INTO `base_log` VALUES (2631, '杰普', 'GET', '/user/info', 1621169016750);
INSERT INTO `base_log` VALUES (2632, '杰普', 'GET', '/user/info', 1621169023900);
INSERT INTO `base_log` VALUES (2633, '杰普', 'GET', '/user/info', 1621169095403);
INSERT INTO `base_log` VALUES (2634, '杰普', 'GET', '/user/info', 1621169147440);
INSERT INTO `base_log` VALUES (2635, '杰普', 'GET', '/user/info', 1621169188611);
INSERT INTO `base_log` VALUES (2636, '杰普', 'GET', '/user/info', 1621169199591);
INSERT INTO `base_log` VALUES (2637, '杰普', 'GET', '/user/info', 1621169242766);
INSERT INTO `base_log` VALUES (2638, '杰普', 'GET', '/user/info', 1621169275250);
INSERT INTO `base_log` VALUES (2639, '杰普', 'GET', '/user/info', 1621169295826);
INSERT INTO `base_log` VALUES (2640, '杰普', 'GET', '/user/info', 1621169313765);
INSERT INTO `base_log` VALUES (2641, '杰普', 'GET', '/order/sendOrder', 1621169360818);
INSERT INTO `base_log` VALUES (2642, '杰普', 'GET', '/user/info', 1621169366283);
INSERT INTO `base_log` VALUES (2643, '杰普', 'GET', '/user/info', 1621169445932);
INSERT INTO `base_log` VALUES (2644, '杰普', 'GET', '/user/info', 1621169509311);
INSERT INTO `base_log` VALUES (2645, '杰普', 'GET', '/user/info', 1621169663187);
INSERT INTO `base_log` VALUES (2646, '杰普', 'POST', '/user/login', 1621169760856);
INSERT INTO `base_log` VALUES (2647, '杰普', 'GET', '/user/info', 1621169761007);
INSERT INTO `base_log` VALUES (2648, '杰普', 'POST', '/user/login', 1621169799407);
INSERT INTO `base_log` VALUES (2649, '杰普', 'GET', '/user/info', 1621169799627);
INSERT INTO `base_log` VALUES (2650, '杰普', 'POST', '/user/login', 1621169946927);
INSERT INTO `base_log` VALUES (2651, '杰普', 'GET', '/user/info', 1621169947108);
INSERT INTO `base_log` VALUES (2652, '杰普', 'POST', '/user/login', 1621169961029);
INSERT INTO `base_log` VALUES (2653, '杰普', 'GET', '/user/info', 1621169961358);
INSERT INTO `base_log` VALUES (2654, '杰普', 'POST', '/user/login', 1621170078305);
INSERT INTO `base_log` VALUES (2655, '杰普', 'GET', '/user/info', 1621170078444);
INSERT INTO `base_log` VALUES (2656, '杰普', 'POST', '/user/login', 1621170084961);
INSERT INTO `base_log` VALUES (2657, '杰普', 'GET', '/user/info', 1621170085167);
INSERT INTO `base_log` VALUES (2658, '杰普', 'POST', '/user/login', 1621170154477);
INSERT INTO `base_log` VALUES (2659, '杰普', 'GET', '/user/info', 1621170154852);
INSERT INTO `base_log` VALUES (2660, '杰普', 'POST', '/user/login', 1621170240948);
INSERT INTO `base_log` VALUES (2661, '杰普', 'GET', '/user/info', 1621170241092);
INSERT INTO `base_log` VALUES (2662, '杰普', 'POST', '/user/login', 1621170448235);
INSERT INTO `base_log` VALUES (2663, '杰普', 'GET', '/user/info', 1621170448446);
INSERT INTO `base_log` VALUES (2664, '杰普', 'POST', '/user/login', 1621170466162);
INSERT INTO `base_log` VALUES (2665, '杰普', 'GET', '/user/info', 1621170466507);
INSERT INTO `base_log` VALUES (2666, '杰普', 'POST', '/user/login', 1621170481379);
INSERT INTO `base_log` VALUES (2667, '杰普', 'GET', '/user/info', 1621170481640);
INSERT INTO `base_log` VALUES (2668, '杰普', 'GET', '/user/info', 1621170484367);
INSERT INTO `base_log` VALUES (2669, '杰普', 'GET', '/user/info', 1621170490294);
INSERT INTO `base_log` VALUES (2670, '杰普', 'GET', '/user/info', 1621170539935);
INSERT INTO `base_log` VALUES (2671, '杰普', 'GET', '/user/info', 1621170612035);
INSERT INTO `base_log` VALUES (2672, '杰普', 'GET', '/user/info', 1621170635046);
INSERT INTO `base_log` VALUES (2673, '杰普', 'GET', '/user/info', 1621171142700);
INSERT INTO `base_log` VALUES (2674, '杰普', 'GET', '/user/info', 1621171208458);
INSERT INTO `base_log` VALUES (2675, '杰普', 'POST', '/user/login', 1621171596773);
INSERT INTO `base_log` VALUES (2676, '杰普', 'GET', '/user/info', 1621171596938);
INSERT INTO `base_log` VALUES (2677, '杰普', 'GET', '/user/info', 1621171597161);
INSERT INTO `base_log` VALUES (2678, '杰普', 'POST', '/user/login', 1621171678665);
INSERT INTO `base_log` VALUES (2679, '杰普', 'GET', '/user/info', 1621171678880);
INSERT INTO `base_log` VALUES (2680, '杰普', 'GET', '/user/info', 1621171679021);
INSERT INTO `base_log` VALUES (2681, '杰普', 'GET', '/user/info', 1621172547552);
INSERT INTO `base_log` VALUES (2682, '杰普', 'POST', '/user/login', 1621173193549);
INSERT INTO `base_log` VALUES (2683, '杰普', 'GET', '/user/info', 1621173193747);
INSERT INTO `base_log` VALUES (2684, '杰普', 'GET', '/user/info', 1621173193868);
INSERT INTO `base_log` VALUES (2685, '杰普', 'GET', '/user/info', 1621173195238);
INSERT INTO `base_log` VALUES (2686, '杰普', 'GET', '/user/info', 1621173246864);
INSERT INTO `base_log` VALUES (2687, '杰普', 'GET', '/user/info', 1621173295415);
INSERT INTO `base_log` VALUES (2688, '杰普', 'GET', '/user/info', 1621173392114);
INSERT INTO `base_log` VALUES (2689, '杰普', 'GET', '/user/info', 1621173416460);
INSERT INTO `base_log` VALUES (2690, '杰普', 'GET', '/order/serviceCompleteOrder', 1621173460221);
INSERT INTO `base_log` VALUES (2691, '杰普', 'GET', '/user/info', 1621173465795);
INSERT INTO `base_log` VALUES (2692, '杰普', 'GET', '/order/confirmOrder', 1621173531725);
INSERT INTO `base_log` VALUES (2693, '杰普', 'GET', '/user/info', 1621173541466);
INSERT INTO `base_log` VALUES (2694, '杰普', 'GET', '/alipay/test', 1621173611265);
INSERT INTO `base_log` VALUES (2695, '杰普', 'POST', '/user/login', 1621173732616);
INSERT INTO `base_log` VALUES (2696, '杰普', 'GET', '/user/info', 1621173732873);
INSERT INTO `base_log` VALUES (2697, '杰普', 'GET', '/user/info', 1621173733029);
INSERT INTO `base_log` VALUES (2698, '杰普', 'GET', '/user/info', 1621173770371);
INSERT INTO `base_log` VALUES (2699, '杰普', 'GET', '/user/info', 1621173917436);
INSERT INTO `base_log` VALUES (2700, '杰普', 'GET', '/user/info', 1621173919406);
INSERT INTO `base_log` VALUES (2701, '杰普', 'GET', '/user/info', 1621173939383);
INSERT INTO `base_log` VALUES (2702, '杰普', 'GET', '/user/info', 1621174055242);
INSERT INTO `base_log` VALUES (2703, '杰普', 'GET', '/user/info', 1621174236733);
INSERT INTO `base_log` VALUES (2704, '杰普', 'GET', '/user/info', 1621174330392);
INSERT INTO `base_log` VALUES (2705, '杰普', 'GET', '/user/info', 1621174332513);
INSERT INTO `base_log` VALUES (2706, '杰普', 'GET', '/user/info', 1621174341611);
INSERT INTO `base_log` VALUES (2707, '杰普', 'GET', '/user/info', 1621219968724);
INSERT INTO `base_log` VALUES (2708, '杰普', 'POST', '/user/login', 1621220482832);
INSERT INTO `base_log` VALUES (2709, '杰普', 'GET', '/user/info', 1621220483051);
INSERT INTO `base_log` VALUES (2710, '杰普', 'GET', '/user/info', 1621220483308);
INSERT INTO `base_log` VALUES (2711, '杰普', 'GET', '/user/info', 1621220485470);
INSERT INTO `base_log` VALUES (2712, '杰普', 'GET', '/user/info', 1621220492376);
INSERT INTO `base_log` VALUES (2713, '杰普', 'GET', '/error', 1621220493927);
INSERT INTO `base_log` VALUES (2714, '杰普', 'GET', '/error', 1621220584091);
INSERT INTO `base_log` VALUES (2715, '杰普', 'GET', '/error', 1621220688081);
INSERT INTO `base_log` VALUES (2716, '杰普', 'GET', '/user/info', 1621232581673);
INSERT INTO `base_log` VALUES (2717, '杰普', 'GET', '/user/info', 1621232635906);
INSERT INTO `base_log` VALUES (2718, '杰普', 'POST', '/user/login', 1621232906546);
INSERT INTO `base_log` VALUES (2719, '杰普', 'GET', '/user/info', 1621232906798);
INSERT INTO `base_log` VALUES (2720, '杰普', 'GET', '/user/info', 1621232907047);
INSERT INTO `base_log` VALUES (2721, '杰普', 'GET', '/user/info', 1621232909159);
INSERT INTO `base_log` VALUES (2722, '杰普', 'GET', '/user/info', 1621232911080);
INSERT INTO `base_log` VALUES (2723, '杰普', 'GET', '/user/info', 1621233100034);
INSERT INTO `base_log` VALUES (2724, '杰普', 'GET', '/user/info', 1621233111740);
INSERT INTO `base_log` VALUES (2725, '杰普', 'GET', '/user/info', 1621233166141);
INSERT INTO `base_log` VALUES (2726, '杰普', 'GET', '/user/info', 1621233203532);
INSERT INTO `base_log` VALUES (2727, '杰普', 'GET', '/user/info', 1621233224603);
INSERT INTO `base_log` VALUES (2728, '杰普', 'GET', '/user/info', 1621233234471);
INSERT INTO `base_log` VALUES (2729, '杰普', 'GET', '/user/info', 1621233282743);
INSERT INTO `base_log` VALUES (2730, '杰普', 'GET', '/user/info', 1621233323799);
INSERT INTO `base_log` VALUES (2731, '杰普', 'GET', '/user/info', 1621233374470);
INSERT INTO `base_log` VALUES (2732, '杰普', 'GET', '/user/info', 1621233391582);
INSERT INTO `base_log` VALUES (2733, '杰普', 'GET', '/user/info', 1621233418312);
INSERT INTO `base_log` VALUES (2734, '杰普', 'GET', '/user/info', 1621233635725);
INSERT INTO `base_log` VALUES (2735, '杰普', 'GET', '/user/info', 1621233640436);
INSERT INTO `base_log` VALUES (2736, '杰普', 'GET', '/user/info', 1621233641579);
INSERT INTO `base_log` VALUES (2737, '杰普', 'GET', '/user/info', 1621233700913);
INSERT INTO `base_log` VALUES (2738, '杰普', 'POST', '/user/login', 1621234018210);
INSERT INTO `base_log` VALUES (2739, '杰普', 'GET', '/user/info', 1621234018345);
INSERT INTO `base_log` VALUES (2740, '杰普', 'GET', '/user/info', 1621234018425);
INSERT INTO `base_log` VALUES (2741, '杰普', 'GET', '/user/info', 1621234034509);
INSERT INTO `base_log` VALUES (2742, '杰普', 'GET', '/user/info', 1621234161573);
INSERT INTO `base_log` VALUES (2743, '杰普', 'GET', '/user/info', 1621234178240);
INSERT INTO `base_log` VALUES (2744, '杰普', 'GET', '/user/info', 1621234191117);
INSERT INTO `base_log` VALUES (2745, '杰普', 'GET', '/user/info', 1621234194165);
INSERT INTO `base_log` VALUES (2746, '杰普', 'GET', '/user/info', 1621234194965);
INSERT INTO `base_log` VALUES (2747, '杰普', 'GET', '/user/info', 1621234196404);
INSERT INTO `base_log` VALUES (2748, '杰普', 'GET', '/user/info', 1621234504533);
INSERT INTO `base_log` VALUES (2749, '杰普', 'GET', '/user/info', 1621234713543);
INSERT INTO `base_log` VALUES (2750, '杰普', 'GET', '/user/info', 1621234717084);
INSERT INTO `base_log` VALUES (2751, '杰普', 'GET', '/user/info', 1621235291601);
INSERT INTO `base_log` VALUES (2752, '杰普', 'GET', '/user/info', 1621236545405);
INSERT INTO `base_log` VALUES (2753, '杰普', 'GET', '/user/info', 1621236548037);
INSERT INTO `base_log` VALUES (2754, '杰普', 'POST', '/file/upload', 1621236659256);
INSERT INTO `base_log` VALUES (2755, '杰普', 'POST', '/file/upload', 1621236665102);
INSERT INTO `base_log` VALUES (2756, '杰普', 'GET', '/user/info', 1621238027026);
INSERT INTO `base_log` VALUES (2757, '杰普', 'POST', '/order/submitOrder', 1621238346767);
INSERT INTO `base_log` VALUES (2758, '杰普', 'GET', '/user/info', 1621238397347);
INSERT INTO `base_log` VALUES (2759, '杰普', 'GET', '/user/info', 1621238406912);
INSERT INTO `base_log` VALUES (2760, '杰普', 'GET', '/user/info', 1621238437333);
INSERT INTO `base_log` VALUES (2761, '杰普', 'GET', '/user/info', 1621238505348);
INSERT INTO `base_log` VALUES (2762, '杰普', 'GET', '/user/info', 1621238515089);
INSERT INTO `base_log` VALUES (2763, '杰普', 'GET', '/user/info', 1621238538415);
INSERT INTO `base_log` VALUES (2764, '杰普', 'GET', '/user/info', 1621238546019);
INSERT INTO `base_log` VALUES (2765, '杰普', 'GET', '/user/info', 1621238560731);
INSERT INTO `base_log` VALUES (2766, '杰普', 'GET', '/user/info', 1621238577662);
INSERT INTO `base_log` VALUES (2767, '杰普', 'GET', '/user/info', 1621238586003);
INSERT INTO `base_log` VALUES (2768, '杰普', 'GET', '/user/info', 1621238599520);
INSERT INTO `base_log` VALUES (2769, '杰普', 'GET', '/user/info', 1621238604417);
INSERT INTO `base_log` VALUES (2770, '杰普', 'GET', '/user/info', 1621238635909);
INSERT INTO `base_log` VALUES (2771, '杰普', 'GET', '/user/info', 1621238645804);
INSERT INTO `base_log` VALUES (2772, '杰普', 'POST', '/order/submitOrder', 1621238656942);
INSERT INTO `base_log` VALUES (2773, '杰普', 'GET', '/user/info', 1621238671509);
INSERT INTO `base_log` VALUES (2774, '杰普', 'GET', '/user/info', 1621238677435);
INSERT INTO `base_log` VALUES (2775, '杰普', 'POST', '/order/submitOrder', 1621238681893);
INSERT INTO `base_log` VALUES (2776, '杰普', 'GET', '/user/info', 1621238722519);
INSERT INTO `base_log` VALUES (2777, '杰普', 'GET', '/user/info', 1621238726943);
INSERT INTO `base_log` VALUES (2778, '杰普', 'POST', '/order/submitOrder', 1621238728169);
INSERT INTO `base_log` VALUES (2779, '杰普', 'GET', '/user/info', 1621238963265);
INSERT INTO `base_log` VALUES (2780, '杰普', 'GET', '/user/info', 1621238967857);
INSERT INTO `base_log` VALUES (2781, '杰普', 'GET', '/user/info', 1621238975308);
INSERT INTO `base_log` VALUES (2782, '杰普', 'POST', '/order/submitOrder', 1621238977179);
INSERT INTO `base_log` VALUES (2783, '杰普', 'GET', '/user/info', 1621238985186);
INSERT INTO `base_log` VALUES (2784, '杰普', 'POST', '/order/submitOrder', 1621238991434);
INSERT INTO `base_log` VALUES (2785, '杰普', 'GET', '/user/info', 1621238997624);
INSERT INTO `base_log` VALUES (2786, '杰普', 'POST', '/order/submitOrder', 1621238998688);
INSERT INTO `base_log` VALUES (2787, '杰普', 'GET', '/user/info', 1621239020355);
INSERT INTO `base_log` VALUES (2788, '杰普', 'POST', '/order/submitOrder', 1621239021315);
INSERT INTO `base_log` VALUES (2789, '杰普', 'POST', '/user/login', 1621239360822);
INSERT INTO `base_log` VALUES (2790, '杰普', 'GET', '/user/info', 1621239367751);
INSERT INTO `base_log` VALUES (2791, '杰普', 'POST', '/order/submitOrder', 1621239372216);
INSERT INTO `base_log` VALUES (2792, '杰普', 'GET', '/user/info', 1621239387589);
INSERT INTO `base_log` VALUES (2793, '杰普', 'GET', '/user/info', 1621239443649);
INSERT INTO `base_log` VALUES (2794, '杰普', 'GET', '/user/info', 1621239469481);
INSERT INTO `base_log` VALUES (2795, '杰普', 'POST', '/order/submitOrder', 1621239480613);
INSERT INTO `base_log` VALUES (2796, '杰普', 'GET', '/user/info', 1621239521484);
INSERT INTO `base_log` VALUES (2797, '杰普', 'GET', '/user/info', 1621239552193);
INSERT INTO `base_log` VALUES (2798, '杰普', 'POST', '/order/submitOrder', 1621239553468);
INSERT INTO `base_log` VALUES (2799, '杰普', 'POST', '/order/submitOrder', 1621239656629);
INSERT INTO `base_log` VALUES (2800, '杰普', 'GET', '/user/info', 1621239672715);
INSERT INTO `base_log` VALUES (2801, '杰普', 'GET', '/account/recharge', 1621239703383);
INSERT INTO `base_log` VALUES (2802, '杰普', 'GET', '/account/recharge', 1621239709812);
INSERT INTO `base_log` VALUES (2803, '杰普', 'GET', '/user/info', 1621239715234);
INSERT INTO `base_log` VALUES (2804, '杰普', 'GET', '/user/info', 1621239761981);
INSERT INTO `base_log` VALUES (2805, '杰普', 'GET', '/user/info', 1621239769945);
INSERT INTO `base_log` VALUES (2806, '杰普', 'GET', '/user/info', 1621239783408);
INSERT INTO `base_log` VALUES (2807, '杰普', 'GET', '/user/info', 1621239787890);
INSERT INTO `base_log` VALUES (2808, '杰普', 'POST', '/order/submitOrder', 1621239796036);
INSERT INTO `base_log` VALUES (2809, '杰普', 'GET', '/user/info', 1621239836496);
INSERT INTO `base_log` VALUES (2810, '杰普', 'GET', '/user/info', 1621240267189);
INSERT INTO `base_log` VALUES (2811, '杰普', 'GET', '/user/info', 1621240268163);
INSERT INTO `base_log` VALUES (2812, '杰普', 'GET', '/user/info', 1621240281642);
INSERT INTO `base_log` VALUES (2813, '杰普', 'GET', '/user/info', 1621240282156);
INSERT INTO `base_log` VALUES (2814, '杰普', 'GET', '/user/info', 1621240295225);
INSERT INTO `base_log` VALUES (2815, '杰普', 'POST', '/order/submitOrder', 1621240299688);
INSERT INTO `base_log` VALUES (2816, '杰普', 'GET', '/user/info', 1621240336832);
INSERT INTO `base_log` VALUES (2817, '杰普', 'GET', '/user/info', 1621240340486);
INSERT INTO `base_log` VALUES (2818, '杰普', 'POST', '/order/submitOrder', 1621240341436);
INSERT INTO `base_log` VALUES (2819, '杰普', 'GET', '/user/info', 1621240382820);
INSERT INTO `base_log` VALUES (2820, '杰普', 'POST', '/order/submitOrder', 1621240385336);
INSERT INTO `base_log` VALUES (2821, '杰普', 'GET', '/user/info', 1621240388387);
INSERT INTO `base_log` VALUES (2822, '杰普', 'GET', '/account/recharge', 1621240464642);
INSERT INTO `base_log` VALUES (2823, '杰普', 'GET', '/account/recharge', 1621240466406);
INSERT INTO `base_log` VALUES (2824, '杰普', 'GET', '/account/recharge', 1621240467057);
INSERT INTO `base_log` VALUES (2825, '杰普', 'GET', '/account/recharge', 1621240467658);
INSERT INTO `base_log` VALUES (2826, '杰普', 'GET', '/account/recharge', 1621240468270);
INSERT INTO `base_log` VALUES (2827, '杰普', 'GET', '/user/info', 1621240471886);
INSERT INTO `base_log` VALUES (2828, '杰普', 'GET', '/user/info', 1621240532242);
INSERT INTO `base_log` VALUES (2829, '杰普', 'GET', '/user/info', 1621240716583);
INSERT INTO `base_log` VALUES (2830, '杰普', 'GET', '/user/info', 1621240740047);
INSERT INTO `base_log` VALUES (2831, '杰普', 'GET', '/user/info', 1621240747220);
INSERT INTO `base_log` VALUES (2832, '杰普', 'GET', '/user/info', 1621240753568);
INSERT INTO `base_log` VALUES (2833, '杰普', 'GET', '/user/info', 1621240762604);
INSERT INTO `base_log` VALUES (2834, '杰普', 'GET', '/user/info', 1621240767716);
INSERT INTO `base_log` VALUES (2835, '杰普', 'POST', '/order/submitOrder', 1621240769048);
INSERT INTO `base_log` VALUES (2836, '杰普', 'GET', '/user/info', 1621240807672);
INSERT INTO `base_log` VALUES (2837, '杰普', 'GET', '/user/info', 1621240862426);
INSERT INTO `base_log` VALUES (2838, '杰普', 'POST', '/order/submitOrder', 1621240865322);
INSERT INTO `base_log` VALUES (2839, '杰普', 'GET', '/user/info', 1621240896604);
INSERT INTO `base_log` VALUES (2840, '杰普', 'GET', '/user/info', 1621241028370);
INSERT INTO `base_log` VALUES (2841, '杰普', 'POST', '/order/submitOrder', 1621241030026);
INSERT INTO `base_log` VALUES (2842, '杰普', 'GET', '/user/info', 1621241060174);
INSERT INTO `base_log` VALUES (2843, '杰普', 'POST', '/order/submitOrder', 1621241061366);
INSERT INTO `base_log` VALUES (2844, '杰普', 'GET', '/user/info', 1621241083295);
INSERT INTO `base_log` VALUES (2845, '杰普', 'POST', '/order/submitOrder', 1621241084210);
INSERT INTO `base_log` VALUES (2846, '杰普', 'GET', '/user/info', 1621241090248);
INSERT INTO `base_log` VALUES (2847, '杰普', 'GET', '/user/info', 1621241101013);
INSERT INTO `base_log` VALUES (2848, '杰普', 'POST', '/order/submitOrder', 1621241102196);
INSERT INTO `base_log` VALUES (2849, '杰普', 'GET', '/user/info', 1621241157436);
INSERT INTO `base_log` VALUES (2850, '杰普', 'GET', '/user/info', 1621241184322);
INSERT INTO `base_log` VALUES (2851, '杰普', 'GET', '/user/info', 1621241193256);
INSERT INTO `base_log` VALUES (2852, '杰普', 'GET', '/user/info', 1621241207241);
INSERT INTO `base_log` VALUES (2853, '杰普', 'GET', '/user/info', 1621241222524);
INSERT INTO `base_log` VALUES (2854, '杰普', 'GET', '/user/info', 1621241238367);
INSERT INTO `base_log` VALUES (2855, '杰普', 'GET', '/user/info', 1621241245212);
INSERT INTO `base_log` VALUES (2856, '杰普', 'POST', '/order/submitOrder', 1621241246317);
INSERT INTO `base_log` VALUES (2857, '杰普', 'GET', '/user/info', 1621241271543);
INSERT INTO `base_log` VALUES (2858, '杰普', 'GET', '/user/info', 1621241278527);
INSERT INTO `base_log` VALUES (2859, '杰普', 'POST', '/order/submitOrder', 1621241279374);
INSERT INTO `base_log` VALUES (2860, '杰普', 'GET', '/user/info', 1621241292391);
INSERT INTO `base_log` VALUES (2861, '杰普', 'GET', '/user/info', 1621241296838);
INSERT INTO `base_log` VALUES (2862, '杰普', 'POST', '/order/submitOrder', 1621241298044);
INSERT INTO `base_log` VALUES (2863, '杰普', 'GET', '/user/info', 1621241303454);
INSERT INTO `base_log` VALUES (2864, '杰普', 'GET', '/user/info', 1621241304483);
INSERT INTO `base_log` VALUES (2865, '杰普', 'GET', '/user/info', 1621241366638);
INSERT INTO `base_log` VALUES (2866, '杰普', 'GET', '/user/info', 1621241379819);
INSERT INTO `base_log` VALUES (2867, '杰普', 'GET', '/user/info', 1621241384072);
INSERT INTO `base_log` VALUES (2868, '杰普', 'POST', '/order/submitOrder', 1621241384936);
INSERT INTO `base_log` VALUES (2869, '杰普', 'GET', '/user/info', 1621241399831);
INSERT INTO `base_log` VALUES (2870, '杰普', 'GET', '/user/info', 1621241441400);
INSERT INTO `base_log` VALUES (2871, '杰普', 'GET', '/user/info', 1621241446068);
INSERT INTO `base_log` VALUES (2872, '杰普', 'POST', '/order/submitOrder', 1621241446817);
INSERT INTO `base_log` VALUES (2873, '杰普', 'GET', '/user/info', 1621241488152);
INSERT INTO `base_log` VALUES (2874, '杰普', 'GET', '/user/info', 1621241489358);
INSERT INTO `base_log` VALUES (2875, '杰普', 'GET', '/user/info', 1621241498217);
INSERT INTO `base_log` VALUES (2876, '杰普', 'POST', '/order/submitOrder', 1621241498965);
INSERT INTO `base_log` VALUES (2877, '杰普', 'GET', '/user/info', 1621241570050);
INSERT INTO `base_log` VALUES (2878, '杰普', 'POST', '/order/submitOrder', 1621241580434);
INSERT INTO `base_log` VALUES (2879, '杰普', 'GET', '/user/info', 1621241582499);
INSERT INTO `base_log` VALUES (2880, '杰普', 'GET', '/user/info', 1621241585281);
INSERT INTO `base_log` VALUES (2881, '杰普', 'POST', '/order/submitOrder', 1621241586229);
INSERT INTO `base_log` VALUES (2882, '杰普', 'GET', '/user/info', 1621241592446);
INSERT INTO `base_log` VALUES (2883, '杰普', 'POST', '/order/submitOrder', 1621241593329);
INSERT INTO `base_log` VALUES (2884, '杰普', 'GET', '/user/info', 1621241648216);
INSERT INTO `base_log` VALUES (2885, '杰普', 'GET', '/user/info', 1621241652977);
INSERT INTO `base_log` VALUES (2886, '杰普', 'GET', '/user/info', 1621241665237);
INSERT INTO `base_log` VALUES (2887, '杰普', 'GET', '/user/info', 1621241689756);
INSERT INTO `base_log` VALUES (2888, '杰普', 'GET', '/user/info', 1621241710315);
INSERT INTO `base_log` VALUES (2889, '杰普', 'GET', '/user/info', 1621241784514);
INSERT INTO `base_log` VALUES (2890, '杰普', 'POST', '/order/submitOrder', 1621241785722);
INSERT INTO `base_log` VALUES (2891, '杰普', 'GET', '/user/info', 1621241815016);
INSERT INTO `base_log` VALUES (2892, '杰普', 'POST', '/order/submitOrder', 1621241818674);
INSERT INTO `base_log` VALUES (2893, '杰普', 'GET', '/user/info', 1621241823797);
INSERT INTO `base_log` VALUES (2894, '杰普', 'GET', '/user/info', 1621241835173);
INSERT INTO `base_log` VALUES (2895, '杰普', 'GET', '/user/info', 1621241844032);
INSERT INTO `base_log` VALUES (2896, '杰普', 'POST', '/order/submitOrder', 1621241845009);
INSERT INTO `base_log` VALUES (2897, '杰普', 'GET', '/user/info', 1621241899964);
INSERT INTO `base_log` VALUES (2898, '杰普', 'GET', '/user/info', 1621241903896);
INSERT INTO `base_log` VALUES (2899, '杰普', 'POST', '/order/submitOrder', 1621241904768);
INSERT INTO `base_log` VALUES (2900, '杰普', 'POST', '/order/submitOrder', 1621241908509);
INSERT INTO `base_log` VALUES (2901, '杰普', 'GET', '/user/info', 1621241967921);
INSERT INTO `base_log` VALUES (2902, '杰普', 'GET', '/user/info', 1621242108033);
INSERT INTO `base_log` VALUES (2903, '杰普', 'GET', '/user/info', 1621242135753);
INSERT INTO `base_log` VALUES (2904, '杰普', 'GET', '/user/info', 1621242146072);
INSERT INTO `base_log` VALUES (2905, '杰普', 'POST', '/order/submitOrder', 1621242150670);
INSERT INTO `base_log` VALUES (2906, '杰普', 'GET', '/user/info', 1621242222463);
INSERT INTO `base_log` VALUES (2907, '杰普', 'POST', '/certification/submitCertificationApply', 1621242244489);
INSERT INTO `base_log` VALUES (2908, '杰普', 'GET', '/user/info', 1621242358097);
INSERT INTO `base_log` VALUES (2909, '杰普', 'POST', '/certification/submitCertificationApply', 1621242362638);
INSERT INTO `base_log` VALUES (2910, '杰普', 'GET', '/user/info', 1621242367366);
INSERT INTO `base_log` VALUES (2911, '杰普', 'GET', '/user/info', 1621242394923);
INSERT INTO `base_log` VALUES (2912, '杰普', 'POST', '/order/submitOrder', 1621242397889);
INSERT INTO `base_log` VALUES (2913, '杰普', 'GET', '/user/info', 1621242448194);
INSERT INTO `base_log` VALUES (2914, '杰普', 'POST', '/order/submitOrder', 1621242450435);
INSERT INTO `base_log` VALUES (2915, '杰普', 'GET', '/user/info', 1621242474449);
INSERT INTO `base_log` VALUES (2916, '杰普', 'POST', '/order/submitOrder', 1621242476859);
INSERT INTO `base_log` VALUES (2917, '杰普', 'POST', '/certification/submitCertificationApply', 1621242835015);
INSERT INTO `base_log` VALUES (2918, '杰普', 'GET', '/user/info', 1621242852714);
INSERT INTO `base_log` VALUES (2919, '杰普', 'GET', '/user/info', 1621242870997);
INSERT INTO `base_log` VALUES (2920, '杰普', 'GET', '/user/info', 1621242877437);
INSERT INTO `base_log` VALUES (2921, '杰普', 'GET', '/user/info', 1621242881246);
INSERT INTO `base_log` VALUES (2922, '杰普', 'POST', '/certification/submitCertificationApply', 1621242881756);
INSERT INTO `base_log` VALUES (2923, '杰普', 'POST', '/order/submitOrder', 1621242882103);
INSERT INTO `base_log` VALUES (2924, '杰普', 'GET', '/user/info', 1621242976646);
INSERT INTO `base_log` VALUES (2925, '杰普', 'GET', '/user/info', 1621243046259);
INSERT INTO `base_log` VALUES (2926, '杰普', 'POST', '/order/submitOrder', 1621243048526);
INSERT INTO `base_log` VALUES (2927, '杰普', 'GET', '/user/info', 1621243096881);
INSERT INTO `base_log` VALUES (2928, '杰普', 'POST', '/order/submitOrder', 1621243099341);
INSERT INTO `base_log` VALUES (2929, '杰普', 'POST', '/certification/submitCertificationApply', 1621243140663);
INSERT INTO `base_log` VALUES (2930, '杰普', 'POST', '/certification/submitCertificationApply', 1621243145120);
INSERT INTO `base_log` VALUES (2931, '杰普', 'GET', '/user/info', 1621243145406);
INSERT INTO `base_log` VALUES (2932, '杰普', 'GET', '/user/info', 1621243290050);
INSERT INTO `base_log` VALUES (2933, '杰普', 'POST', '/order/submitOrder', 1621243293156);
INSERT INTO `base_log` VALUES (2934, '杰普', 'GET', '/user/info', 1621243353715);
INSERT INTO `base_log` VALUES (2935, '杰普', 'POST', '/order/submitOrder', 1621243357022);
INSERT INTO `base_log` VALUES (2936, '杰普', 'GET', '/user/info', 1621243459415);
INSERT INTO `base_log` VALUES (2937, '杰普', 'POST', '/order/submitOrder', 1621243462875);
INSERT INTO `base_log` VALUES (2938, '杰普', 'GET', '/user/info', 1621243482647);
INSERT INTO `base_log` VALUES (2939, '杰普', 'GET', '/user/info', 1621243507052);
INSERT INTO `base_log` VALUES (2940, '杰普', 'GET', '/user/info', 1621243584329);
INSERT INTO `base_log` VALUES (2941, '杰普', 'POST', '/order/submitOrder', 1621243594354);
INSERT INTO `base_log` VALUES (2942, '杰普', 'GET', '/user/info', 1621243625084);
INSERT INTO `base_log` VALUES (2943, '杰普', 'GET', '/user/info', 1621243629245);
INSERT INTO `base_log` VALUES (2944, '杰普', 'POST', '/order/submitOrder', 1621243630100);
INSERT INTO `base_log` VALUES (2945, '杰普', 'GET', '/user/info', 1621243663279);
INSERT INTO `base_log` VALUES (2946, '杰普', 'POST', '/order/submitOrder', 1621243665571);
INSERT INTO `base_log` VALUES (2947, '杰普', 'GET', '/user/info', 1621243738121);
INSERT INTO `base_log` VALUES (2948, '杰普', 'POST', '/order/submitOrder', 1621243740503);
INSERT INTO `base_log` VALUES (2949, '杰普', 'GET', '/user/info', 1621243843115);
INSERT INTO `base_log` VALUES (2950, '杰普', 'POST', '/order/submitOrder', 1621243844544);
INSERT INTO `base_log` VALUES (2951, '杰普', 'GET', '/user/info', 1621243869446);
INSERT INTO `base_log` VALUES (2952, '杰普', 'POST', '/order/submitOrder', 1621243870908);
INSERT INTO `base_log` VALUES (2953, '杰普', 'GET', '/user/info', 1621243944018);
INSERT INTO `base_log` VALUES (2954, '杰普', 'GET', '/user/info', 1621243948657);
INSERT INTO `base_log` VALUES (2955, '杰普', 'POST', '/order/submitOrder', 1621243971546);
INSERT INTO `base_log` VALUES (2956, '杰普', 'GET', '/user/info', 1621243985017);
INSERT INTO `base_log` VALUES (2957, '杰普', 'GET', '/user/info', 1621244002024);
INSERT INTO `base_log` VALUES (2958, '杰普', 'POST', '/order/submitOrder', 1621244003571);
INSERT INTO `base_log` VALUES (2959, '杰普', 'GET', '/user/info', 1621317685424);
INSERT INTO `base_log` VALUES (2960, '杰普', 'GET', '/user/info', 1621317688735);
INSERT INTO `base_log` VALUES (2961, '杰普', 'GET', '/user/info', 1621317690545);
INSERT INTO `base_log` VALUES (2962, '杰普', 'GET', '/user/info', 1621317691601);
INSERT INTO `base_log` VALUES (2963, '杰普', 'GET', '/user/info', 1621317694140);
INSERT INTO `base_log` VALUES (2964, '杰普', 'GET', '/user/info', 1621317696386);
INSERT INTO `base_log` VALUES (2965, '杰普', 'GET', '/user/info', 1621317712618);
INSERT INTO `base_log` VALUES (2966, '杰普', 'GET', '/certification/checkPass', 1621317820283);
INSERT INTO `base_log` VALUES (2967, '小点点', 'GET', '/user/info', 1621317826723);
INSERT INTO `base_log` VALUES (2968, '小点点', 'GET', '/user/info', 1621317909619);
INSERT INTO `base_log` VALUES (2969, '小点点', 'GET', '/user/info', 1621317919223);
INSERT INTO `base_log` VALUES (2970, '小点点', 'GET', '/user/info', 1621318057034);
INSERT INTO `base_log` VALUES (2971, '小点点', 'GET', '/user/info', 1621318250281);
INSERT INTO `base_log` VALUES (2972, '小点点', 'GET', '/user/info', 1621318283250);
INSERT INTO `base_log` VALUES (2973, '小点点', 'GET', '/user/info', 1621318460676);
INSERT INTO `base_log` VALUES (2974, '小点点', 'GET', '/user/info', 1621318478260);
INSERT INTO `base_log` VALUES (2975, '小点点', 'GET', '/user/info', 1621318903216);
INSERT INTO `base_log` VALUES (2976, '小点点', 'GET', '/user/info', 1621319162086);
INSERT INTO `base_log` VALUES (2977, '小点点', 'GET', '/user/info', 1621319174240);
INSERT INTO `base_log` VALUES (2978, '小点点', 'GET', '/user/info', 1621319184296);
INSERT INTO `base_log` VALUES (2979, '小点点', 'GET', '/user/info', 1621319344882);
INSERT INTO `base_log` VALUES (2980, '小点点', 'GET', '/user/info', 1621319349582);
INSERT INTO `base_log` VALUES (2981, '小点点', 'GET', '/user/info', 1621319408747);
INSERT INTO `base_log` VALUES (2982, '小点点', 'GET', '/user/info', 1621319416651);
INSERT INTO `base_log` VALUES (2983, '小点点', 'GET', '/user/info', 1621319419509);
INSERT INTO `base_log` VALUES (2984, '小点点', 'GET', '/user/info', 1621319421363);
INSERT INTO `base_log` VALUES (2985, '小点点', 'GET', '/user/info', 1621319425011);
INSERT INTO `base_log` VALUES (2986, '小点点', 'GET', '/user/info', 1621320101978);
INSERT INTO `base_log` VALUES (2987, '小点点', 'GET', '/user/info', 1621320859226);
INSERT INTO `base_log` VALUES (2988, '小点点', 'GET', '/user/info', 1621321145974);
INSERT INTO `base_log` VALUES (2989, '小点点', 'GET', '/user/info', 1621322989584);
INSERT INTO `base_log` VALUES (2990, '小点点', 'POST', '/user/login', 1621386388881);
INSERT INTO `base_log` VALUES (2991, '小点点', 'GET', '/user/info', 1621386405384);
INSERT INTO `base_log` VALUES (2992, '小点点', 'POST', '/order/submitOrder', 1621386407512);
INSERT INTO `base_log` VALUES (2993, '小点点', 'POST', '/order/submitOrder', 1621386451885);
INSERT INTO `base_log` VALUES (2994, '小点点', 'POST', '/order/submitOrder', 1621386469166);
INSERT INTO `base_log` VALUES (2995, '小点点', 'GET', '/user/info', 1621386690251);
INSERT INTO `base_log` VALUES (2996, '小点点', 'POST', '/order/submitOrder', 1621386692598);
INSERT INTO `base_log` VALUES (2997, '小点点', 'GET', '/user/info', 1621386728153);
INSERT INTO `base_log` VALUES (2998, '小点点', 'POST', '/order/submitOrder', 1621386738011);
INSERT INTO `base_log` VALUES (2999, '小点点', 'POST', '/order/submitOrder', 1621386741149);
INSERT INTO `base_log` VALUES (3000, '小点点', 'POST', '/order/submitOrder', 1621386752605);
INSERT INTO `base_log` VALUES (3001, '小点点', 'POST', '/order/submitOrder', 1621386755555);
INSERT INTO `base_log` VALUES (3002, '小点点', 'GET', '/user/info', 1621386774258);
INSERT INTO `base_log` VALUES (3003, '小点点', 'GET', '/user/info', 1621386786032);
INSERT INTO `base_log` VALUES (3004, '小点点', 'POST', '/user/login', 1621386788578);
INSERT INTO `base_log` VALUES (3005, '小点点', 'GET', '/user/info', 1621386788761);
INSERT INTO `base_log` VALUES (3006, '小点点', 'GET', '/user/info', 1621386788842);
INSERT INTO `base_log` VALUES (3007, '小点点', 'GET', '/user/info', 1621386790650);
INSERT INTO `base_log` VALUES (3008, '小点点', 'GET', '/user/info', 1621386791402);
INSERT INTO `base_log` VALUES (3009, '小点点', 'GET', '/user/info', 1621386797217);
INSERT INTO `base_log` VALUES (3010, '小点点', 'GET', '/user/info', 1621386800523);
INSERT INTO `base_log` VALUES (3011, '小点点', 'GET', '/user/info', 1621386802349);
INSERT INTO `base_log` VALUES (3012, '小点点', 'GET', '/user/info', 1621386803955);
INSERT INTO `base_log` VALUES (3013, '小点点', 'GET', '/user/info', 1621386809162);
INSERT INTO `base_log` VALUES (3014, '小点点', 'POST', '/order/submitOrder', 1621386809945);
INSERT INTO `base_log` VALUES (3015, '小点点', 'POST', '/order/submitOrder', 1621386816367);
INSERT INTO `base_log` VALUES (3016, '小点点', 'GET', '/user/info', 1621386842503);
INSERT INTO `base_log` VALUES (3017, '小点点', 'GET', '/user/info', 1621386848261);
INSERT INTO `base_log` VALUES (3018, '小点点', 'GET', '/user/info', 1621386887968);
INSERT INTO `base_log` VALUES (3019, '小点点', 'GET', '/user/info', 1621386964426);
INSERT INTO `base_log` VALUES (3020, '小点点', 'GET', '/user/info', 1621386981042);
INSERT INTO `base_log` VALUES (3021, '小点点', 'GET', '/user/info', 1621386983416);
INSERT INTO `base_log` VALUES (3022, '小点点', 'GET', '/user/info', 1621387005220);
INSERT INTO `base_log` VALUES (3023, '小点点', 'GET', '/user/info', 1621387103972);
INSERT INTO `base_log` VALUES (3024, '小点点', 'GET', '/user/info', 1621387112406);
INSERT INTO `base_log` VALUES (3025, '小点点', 'GET', '/user/info', 1621387150260);
INSERT INTO `base_log` VALUES (3026, '小点点', 'GET', '/user/info', 1621387188412);
INSERT INTO `base_log` VALUES (3027, '小点点', 'GET', '/user/info', 1621387200044);
INSERT INTO `base_log` VALUES (3028, '小点点', 'POST', '/order/submitOrder', 1621387205769);
INSERT INTO `base_log` VALUES (3029, '小点点', 'GET', '/user/info', 1621387213430);
INSERT INTO `base_log` VALUES (3030, '小点点', 'GET', '/user/info', 1621387215699);
INSERT INTO `base_log` VALUES (3031, '小点点', 'POST', '/order/submitOrder', 1621387216858);
INSERT INTO `base_log` VALUES (3032, '小点点', 'GET', '/account/recharge', 1621387221963);
INSERT INTO `base_log` VALUES (3033, '小点点', 'GET', '/user/info', 1621387233610);
INSERT INTO `base_log` VALUES (3034, '小点点', 'GET', '/user/info', 1621387247781);
INSERT INTO `base_log` VALUES (3035, '小点点', 'GET', '/user/info', 1621387251304);
INSERT INTO `base_log` VALUES (3036, '小点点', 'GET', '/user/info', 1621387270383);
INSERT INTO `base_log` VALUES (3037, '小点点', 'GET', '/user/info', 1621387283409);
INSERT INTO `base_log` VALUES (3038, '小点点', 'GET', '/user/info', 1621387288199);
INSERT INTO `base_log` VALUES (3039, '小点点', 'POST', '/order/submitOrder', 1621387289005);
INSERT INTO `base_log` VALUES (3040, '小点点', 'GET', '/account/recharge', 1621387292593);
INSERT INTO `base_log` VALUES (3041, '小点点', 'GET', '/user/info', 1621387314293);
INSERT INTO `base_log` VALUES (3042, '小点点', 'GET', '/user/info', 1621387322605);
INSERT INTO `base_log` VALUES (3043, '小点点', 'GET', '/user/info', 1621387324342);
INSERT INTO `base_log` VALUES (3044, '小点点', 'GET', '/user/info', 1621387326878);
INSERT INTO `base_log` VALUES (3045, '小点点', 'GET', '/user/info', 1621387331844);
INSERT INTO `base_log` VALUES (3046, '小点点', 'GET', '/user/info', 1621387337245);
INSERT INTO `base_log` VALUES (3047, '小点点', 'GET', '/user/info', 1621387412338);
INSERT INTO `base_log` VALUES (3048, '小点点', 'POST', '/order/submitOrder', 1621387416945);
INSERT INTO `base_log` VALUES (3049, '小点点', 'POST', '/order/submitOrder', 1621387425140);
INSERT INTO `base_log` VALUES (3050, '小点点', 'GET', '/account/recharge', 1621387430107);
INSERT INTO `base_log` VALUES (3051, '小点点', 'GET', '/user/info', 1621387440962);
INSERT INTO `base_log` VALUES (3052, '小点点', 'POST', '/order/submitOrder', 1621387442466);
INSERT INTO `base_log` VALUES (3053, '小点点', 'GET', '/account/recharge', 1621387446364);
INSERT INTO `base_log` VALUES (3054, '小点点', 'GET', '/user/info', 1621387480051);
INSERT INTO `base_log` VALUES (3055, '小点点', 'POST', '/order/submitOrder', 1621387482183);
INSERT INTO `base_log` VALUES (3056, '小点点', 'GET', '/account/recharge', 1621387486554);
INSERT INTO `base_log` VALUES (3057, '小点点', 'GET', '/user/info', 1621387492918);
INSERT INTO `base_log` VALUES (3058, '小点点', 'GET', '/user/info', 1621387591452);
INSERT INTO `base_log` VALUES (3059, '小点点', 'GET', '/user/info', 1621387638719);
INSERT INTO `base_log` VALUES (3060, '小点点', 'GET', '/user/info', 1621387642676);
INSERT INTO `base_log` VALUES (3061, '小点点', 'GET', '/user/info', 1621387710338);
INSERT INTO `base_log` VALUES (3062, '小点点', 'GET', '/user/info', 1621387726634);
INSERT INTO `base_log` VALUES (3063, '小点点', 'GET', '/account/recharge', 1621387739828);
INSERT INTO `base_log` VALUES (3064, '小点点', 'GET', '/account/recharge', 1621387752632);
INSERT INTO `base_log` VALUES (3065, '小点点', 'GET', '/user/info', 1621387779598);
INSERT INTO `base_log` VALUES (3066, '小点点', 'GET', '/account/recharge', 1621387785800);
INSERT INTO `base_log` VALUES (3067, '小点点', 'GET', '/user/info', 1621387786174);
INSERT INTO `base_log` VALUES (3068, '小点点', 'GET', '/user/info', 1621389028294);
INSERT INTO `base_log` VALUES (3069, '小点点', 'GET', '/user/info', 1621389040902);
INSERT INTO `base_log` VALUES (3070, '小点点', 'GET', '/user/info', 1621389047416);
INSERT INTO `base_log` VALUES (3071, '小点点', 'GET', '/user/info', 1621389839244);
INSERT INTO `base_log` VALUES (3072, '小点点', 'GET', '/user/info', 1621391842735);
INSERT INTO `base_log` VALUES (3073, '小点点', 'GET', '/user/info', 1621391859028);
INSERT INTO `base_log` VALUES (3074, '小点点', 'GET', '/user/info', 1621391862764);
INSERT INTO `base_log` VALUES (3075, '小点点', 'GET', '/user/info', 1621391866743);
INSERT INTO `base_log` VALUES (3076, '小点点', 'GET', '/user/info', 1621392561468);
INSERT INTO `base_log` VALUES (3077, '小点点', 'GET', '/user/info', 1621392565967);
INSERT INTO `base_log` VALUES (3078, '小点点', 'POST', '/certification/submitCertificationApply', 1621392743422);
INSERT INTO `base_log` VALUES (3079, '小点点', 'GET', '/user/info', 1621392744027);
INSERT INTO `base_log` VALUES (3080, '小点点', 'GET', '/user/info', 1621392748327);
INSERT INTO `base_log` VALUES (3081, '小点点', 'GET', '/user/info', 1621392758790);
INSERT INTO `base_log` VALUES (3082, '小点点', 'GET', '/certification/checkPass', 1621392820124);
INSERT INTO `base_log` VALUES (3083, '赵大伟', 'GET', '/user/info', 1621392825593);
INSERT INTO `base_log` VALUES (3084, '赵大伟', 'GET', '/user/info', 1621392862013);
INSERT INTO `base_log` VALUES (3085, '赵大伟', 'GET', '/user/info', 1621392865533);
INSERT INTO `base_log` VALUES (3086, '赵大伟', 'GET', '/user/info', 1621392868968);
INSERT INTO `base_log` VALUES (3087, '赵大伟', 'GET', '/user/info', 1621392877822);
INSERT INTO `base_log` VALUES (3088, '赵大伟', 'GET', '/user/info', 1621392882561);
INSERT INTO `base_log` VALUES (3089, '赵大伟', 'GET', '/user/info', 1621392905890);
INSERT INTO `base_log` VALUES (3090, '赵大伟', 'GET', '/user/info', 1621393134326);
INSERT INTO `base_log` VALUES (3091, '赵大伟', 'GET', '/user/info', 1621393137742);
INSERT INTO `base_log` VALUES (3092, '赵大伟', 'GET', '/user/info', 1621393149255);
INSERT INTO `base_log` VALUES (3093, '赵大伟', 'POST', '/accountApply/submitAccountApply', 1621393374210);
INSERT INTO `base_log` VALUES (3094, '赵大伟', 'GET', '/user/info', 1621393384626);
INSERT INTO `base_log` VALUES (3095, '赵大伟', 'GET', '/user/info', 1621393392109);
INSERT INTO `base_log` VALUES (3096, '赵大伟', 'GET', '/accountApply/checkPass', 1621393426540);
INSERT INTO `base_log` VALUES (3097, '赵大伟', 'GET', '/user/info', 1621393431539);
INSERT INTO `base_log` VALUES (3098, '赵大伟', 'GET', '/user/info', 1621393632704);
INSERT INTO `base_log` VALUES (3099, '赵大伟', 'GET', '/user/info', 1621393868818);
INSERT INTO `base_log` VALUES (3100, '赵大伟', 'GET', '/user/info', 1621394133047);
INSERT INTO `base_log` VALUES (3101, '赵大伟', 'GET', '/user/info', 1621394240712);
INSERT INTO `base_log` VALUES (3102, '赵大伟', 'GET', '/user/info', 1621394331222);
INSERT INTO `base_log` VALUES (3103, '赵大伟', 'GET', '/user/info', 1621394392739);
INSERT INTO `base_log` VALUES (3104, '赵大伟', 'GET', '/user/info', 1621394478019);
INSERT INTO `base_log` VALUES (3105, '赵大伟', 'GET', '/user/info', 1621394510700);
INSERT INTO `base_log` VALUES (3106, '赵大伟', 'GET', '/user/info', 1621394524259);
INSERT INTO `base_log` VALUES (3107, '赵大伟', 'GET', '/user/info', 1621394525193);
INSERT INTO `base_log` VALUES (3108, '赵大伟', 'POST', '/user/login', 1621394588781);
INSERT INTO `base_log` VALUES (3109, '赵大伟', 'GET', '/user/info', 1621394589094);
INSERT INTO `base_log` VALUES (3110, '赵大伟', 'GET', '/user/info', 1621394589187);
INSERT INTO `base_log` VALUES (3111, '赵大伟', 'GET', '/user/info', 1621394590814);
INSERT INTO `base_log` VALUES (3112, '赵大伟', 'POST', '/address/saveOrUpdate', 1621394755328);
INSERT INTO `base_log` VALUES (3113, '赵大伟', 'GET', '/user/info', 1621395036097);
INSERT INTO `base_log` VALUES (3114, '赵大伟', 'GET', '/user/info', 1621405360108);
INSERT INTO `base_log` VALUES (3115, '赵大伟', 'GET', '/user/info', 1621405362503);
INSERT INTO `base_log` VALUES (3116, '赵大伟', 'GET', '/user/info', 1621405364600);
INSERT INTO `base_log` VALUES (3117, '赵大伟', 'POST', '/user/login', 1621405509304);
INSERT INTO `base_log` VALUES (3118, '赵大伟', 'GET', '/user/info', 1621405513066);
INSERT INTO `base_log` VALUES (3119, '赵大伟', 'GET', '/user/info', 1621406094207);
INSERT INTO `base_log` VALUES (3120, '赵大伟', 'POST', '/user/login', 1621411759325);
INSERT INTO `base_log` VALUES (3121, '赵大伟', 'GET', '/user/info', 1621411762356);
INSERT INTO `base_log` VALUES (3122, '赵大伟', 'POST', '/address/saveOrUpdate', 1621413395514);
INSERT INTO `base_log` VALUES (3123, '赵大伟', 'GET', '/user/info', 1621413401326);
INSERT INTO `base_log` VALUES (3124, '赵大伟', 'POST', '/address/saveOrUpdate', 1621413576388);
INSERT INTO `base_log` VALUES (3125, '赵大伟', 'POST', '/address/saveOrUpdate', 1621413647559);
INSERT INTO `base_log` VALUES (3126, '赵大伟', 'GET', '/user/info', 1621413655088);
INSERT INTO `base_log` VALUES (3127, '赵大伟', 'GET', '/user/info', 1621822755670);
INSERT INTO `base_log` VALUES (3128, '刘亚蓉', 'GET', '/user/info', 1621923433940);
INSERT INTO `base_log` VALUES (3129, '赵大伟', 'GET', '/user/info', 1621926978494);
INSERT INTO `base_log` VALUES (3130, '赵大伟', 'POST', '/order/submitOrder', 1621926992067);
INSERT INTO `base_log` VALUES (3131, '赵大伟', 'GET', '/user/info', 1621927006350);
INSERT INTO `base_log` VALUES (3132, '刘亚蓉', 'GET', '/order/sendOrder', 1621927080312);
INSERT INTO `base_log` VALUES (3133, '刘亚蓉', 'GET', '/user/info', 1621927089339);
INSERT INTO `base_log` VALUES (3134, '刘亚蓉', 'GET', '/order/sendOrder', 1621927115889);
INSERT INTO `base_log` VALUES (3135, '刘亚蓉', 'GET', '/order/sendOrder', 1621927166848);
INSERT INTO `base_log` VALUES (3136, '刘亚蓉', 'GET', '/user/info', 1621927219457);
INSERT INTO `base_log` VALUES (3137, '刘亚蓉', 'GET', '/order/sendOrder', 1621927291868);
INSERT INTO `base_log` VALUES (3138, '刘亚蓉', 'GET', '/user/info', 1621927344631);
INSERT INTO `base_log` VALUES (3139, '刘亚蓉', 'GET', '/order/sendOrder', 1621927360742);
INSERT INTO `base_log` VALUES (3140, '刘亚蓉', 'GET', '/user/info', 1621927379760);
INSERT INTO `base_log` VALUES (3141, '刘亚蓉', 'GET', '/user/info', 1621927386187);
INSERT INTO `base_log` VALUES (3142, '刘亚蓉', 'GET', '/order/sendOrder', 1621927407260);
INSERT INTO `base_log` VALUES (3143, '刘亚蓉', 'GET', '/user/info', 1621927422139);
INSERT INTO `base_log` VALUES (3144, '赵大伟', 'GET', '/user/info', 1621927656667);
INSERT INTO `base_log` VALUES (3145, '赵大伟', 'POST', '/address/saveOrUpdate', 1621927709330);
INSERT INTO `base_log` VALUES (3146, '赵大伟', 'POST', '/address/saveOrUpdate', 1621927717097);
INSERT INTO `base_log` VALUES (3147, '刘亚蓉', 'GET', '/user/info', 1621927924733);
INSERT INTO `base_log` VALUES (3148, '刘亚蓉', 'GET', '/user/info', 1621928091758);
INSERT INTO `base_log` VALUES (3149, '刘亚蓉', 'GET', '/order/sendOrder', 1621928315083);
INSERT INTO `base_log` VALUES (3150, '刘亚蓉', 'GET', '/user/info', 1621929638753);
INSERT INTO `base_log` VALUES (3151, '刘亚蓉', 'GET', '/user/info', 1621929660192);
INSERT INTO `base_log` VALUES (3152, '刘亚蓉', 'GET', '/user/info', 1621929864255);
INSERT INTO `base_log` VALUES (3153, '刘亚蓉', 'GET', '/order/cancelSendOrder', 1621929918076);
INSERT INTO `base_log` VALUES (3154, '刘亚蓉', 'GET', '/user/info', 1621929936850);
INSERT INTO `base_log` VALUES (3155, '刘亚蓉', 'GET', '/order/cancelSendOrder', 1621929941487);
INSERT INTO `base_log` VALUES (3156, '刘亚蓉', 'GET', '/user/info', 1621929987627);
INSERT INTO `base_log` VALUES (3157, '刘亚蓉', 'GET', '/user/info', 1621930027553);
INSERT INTO `base_log` VALUES (3158, '刘亚蓉', 'GET', '/user/info', 1621930056662);
INSERT INTO `base_log` VALUES (3159, '刘亚蓉', 'GET', '/user/info', 1621930201618);
INSERT INTO `base_log` VALUES (3160, '刘亚蓉', 'GET', '/user/info', 1621930233816);
INSERT INTO `base_log` VALUES (3161, '刘亚蓉', 'GET', '/user/info', 1621930255715);
INSERT INTO `base_log` VALUES (3162, '刘亚蓉', 'GET', '/order/sendOrder', 1621930280984);
INSERT INTO `base_log` VALUES (3163, '刘亚蓉', 'GET', '/order/cancelSendOrder', 1621930288875);
INSERT INTO `base_log` VALUES (3164, '刘亚蓉', 'GET', '/user/info', 1621930339614);
INSERT INTO `base_log` VALUES (3165, '刘亚蓉', 'GET', '/order/cancelSendOrder', 1621930348573);
INSERT INTO `base_log` VALUES (3166, '刘亚蓉', 'GET', '/order/cancelSendOrder', 1621930372602);
INSERT INTO `base_log` VALUES (3167, '刘亚蓉', 'GET', '/order/sendOrder', 1621930375934);
INSERT INTO `base_log` VALUES (3168, '刘亚蓉', 'GET', '/user/info', 1621930422583);
INSERT INTO `base_log` VALUES (3169, '刘亚蓉', 'GET', '/user/info', 1621930497509);
INSERT INTO `base_log` VALUES (3170, '刘亚蓉', 'GET', '/user/info', 1621930506858);
INSERT INTO `base_log` VALUES (3171, '刘亚蓉', 'GET', '/order/sendOrder', 1621930512072);
INSERT INTO `base_log` VALUES (3172, '刘亚蓉', 'GET', '/user/info', 1621930545841);
INSERT INTO `base_log` VALUES (3173, '刘亚蓉', 'GET', '/user/info', 1621930771570);
INSERT INTO `base_log` VALUES (3174, '刘亚蓉', 'GET', '/user/info', 1621930876881);
INSERT INTO `base_log` VALUES (3175, '刘亚蓉', 'GET', '/user/info', 1621930890471);
INSERT INTO `base_log` VALUES (3176, '刘亚蓉', 'GET', '/user/info', 1621930985092);
INSERT INTO `base_log` VALUES (3177, '刘亚蓉', 'GET', '/user/info', 1621931169712);
INSERT INTO `base_log` VALUES (3178, '刘亚蓉', 'GET', '/user/info', 1621931628174);
INSERT INTO `base_log` VALUES (3179, '刘亚蓉', 'GET', '/user/info', 1621996463367);
INSERT INTO `base_log` VALUES (3180, '刘亚蓉', 'GET', '/user/info', 1621996512572);
INSERT INTO `base_log` VALUES (3181, '刘亚蓉', 'GET', '/user/info', 1621996536745);
INSERT INTO `base_log` VALUES (3182, '刘亚蓉', 'GET', '/user/info', 1621996561469);
INSERT INTO `base_log` VALUES (3183, '刘亚蓉', 'GET', '/user/info', 1621996852884);
INSERT INTO `base_log` VALUES (3184, '刘亚蓉', 'GET', '/user/info', 1621996868912);
INSERT INTO `base_log` VALUES (3185, '刘亚蓉', 'GET', '/user/info', 1621996885366);
INSERT INTO `base_log` VALUES (3186, '刘亚蓉', 'GET', '/user/info', 1621996960585);
INSERT INTO `base_log` VALUES (3187, '刘亚蓉', 'GET', '/user/info', 1621997015058);
INSERT INTO `base_log` VALUES (3188, '刘亚蓉', 'GET', '/user/info', 1621997040979);
INSERT INTO `base_log` VALUES (3189, '刘亚蓉', 'GET', '/user/info', 1621997108211);
INSERT INTO `base_log` VALUES (3190, '刘亚蓉', 'GET', '/user/info', 1621997150324);
INSERT INTO `base_log` VALUES (3191, '刘亚蓉', 'GET', '/user/info', 1621997251921);
INSERT INTO `base_log` VALUES (3192, '刘亚蓉', 'GET', '/user/info', 1621997276374);
INSERT INTO `base_log` VALUES (3193, '刘亚蓉', 'GET', '/user/info', 1621997320530);
INSERT INTO `base_log` VALUES (3194, '刘亚蓉', 'GET', '/user/info', 1621997335293);
INSERT INTO `base_log` VALUES (3195, '刘亚蓉', 'GET', '/user/info', 1621997352198);
INSERT INTO `base_log` VALUES (3196, '刘亚蓉', 'GET', '/user/info', 1621997408990);
INSERT INTO `base_log` VALUES (3197, '刘亚蓉', 'GET', '/user/info', 1621997428523);
INSERT INTO `base_log` VALUES (3198, '刘亚蓉', 'GET', '/user/info', 1621997697784);
INSERT INTO `base_log` VALUES (3199, '刘亚蓉', 'GET', '/user/info', 1621997705310);
INSERT INTO `base_log` VALUES (3200, '刘亚蓉', 'GET', '/user/info', 1621997786450);
INSERT INTO `base_log` VALUES (3201, '刘亚蓉', 'GET', '/user/info', 1621998171694);
INSERT INTO `base_log` VALUES (3202, '刘亚蓉', 'GET', '/user/info', 1621998227494);
INSERT INTO `base_log` VALUES (3203, '刘亚蓉', 'GET', '/user/info', 1621998380870);
INSERT INTO `base_log` VALUES (3204, '刘亚蓉', 'GET', '/user/info', 1621998446005);
INSERT INTO `base_log` VALUES (3205, '刘亚蓉', 'GET', '/user/info', 1621998472882);
INSERT INTO `base_log` VALUES (3206, '刘亚蓉', 'GET', '/user/info', 1621998912545);
INSERT INTO `base_log` VALUES (3207, '刘亚蓉', 'GET', '/user/info', 1621999319989);
INSERT INTO `base_log` VALUES (3208, '刘亚蓉', 'GET', '/user/info', 1621999470262);
INSERT INTO `base_log` VALUES (3209, '刘亚蓉', 'GET', '/user/info', 1622000261720);
INSERT INTO `base_log` VALUES (3210, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1622005155360);
INSERT INTO `base_log` VALUES (3211, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1622005196027);
INSERT INTO `base_log` VALUES (3212, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1622005211145);
INSERT INTO `base_log` VALUES (3213, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1622005215332);
INSERT INTO `base_log` VALUES (3214, '刘亚蓉', 'POST', '/role/authorization', 1622005223155);
INSERT INTO `base_log` VALUES (3215, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1622005224053);
INSERT INTO `base_log` VALUES (3216, '刘亚蓉', 'GET', '/user/info', 1622005226134);
INSERT INTO `base_log` VALUES (3217, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1622005226936);
INSERT INTO `base_log` VALUES (3218, '刘亚蓉', 'GET', '/user/info', 1622005240138);
INSERT INTO `base_log` VALUES (3219, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1622005240832);
INSERT INTO `base_log` VALUES (3220, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1622005268038);
INSERT INTO `base_log` VALUES (3221, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1622005284082);
INSERT INTO `base_log` VALUES (3222, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1622005286611);
INSERT INTO `base_log` VALUES (3223, '刘亚蓉', 'POST', '/role/authorization', 1622005298687);
INSERT INTO `base_log` VALUES (3224, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1622005298867);
INSERT INTO `base_log` VALUES (3225, '刘亚蓉', 'GET', '/user/info', 1622005307149);
INSERT INTO `base_log` VALUES (3226, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1622005307715);
INSERT INTO `base_log` VALUES (3227, '刘亚蓉', 'POST', '/baseUser/setRoles', 1622005323334);
INSERT INTO `base_log` VALUES (3228, '刘亚蓉', 'GET', '/user/info', 1622005330250);
INSERT INTO `base_log` VALUES (3229, '刘亚蓉', 'GET', '/user/info', 1622005343689);
INSERT INTO `base_log` VALUES (3230, '刘亚蓉', 'GET', '/user/info', 1622005461224);
INSERT INTO `base_log` VALUES (3231, '刘亚蓉', 'GET', '/user/info', 1622005473382);
INSERT INTO `base_log` VALUES (3232, '刘亚蓉', 'GET', '/user/info', 1622006046971);
INSERT INTO `base_log` VALUES (3233, '刘亚蓉', 'GET', '/certification/checkNoPass', 1622006061233);
INSERT INTO `base_log` VALUES (3234, '刘亚蓉', 'GET', '/certification/checkNoPass', 1622006073152);
INSERT INTO `base_log` VALUES (3235, '刘亚蓉', 'GET', '/certification/checkPass', 1622006086420);
INSERT INTO `base_log` VALUES (3236, '刘亚蓉', 'GET', '/user/info', 1622006349974);
INSERT INTO `base_log` VALUES (3237, '刘亚蓉', 'GET', '/user/info', 1622006358094);
INSERT INTO `base_log` VALUES (3238, '刘亚蓉', 'POST', '/privilege/saveOrUpdate', 1622006596726);
INSERT INTO `base_log` VALUES (3239, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1622006599340);
INSERT INTO `base_log` VALUES (3240, '刘亚蓉', 'POST', '/role/authorization', 1622006607220);
INSERT INTO `base_log` VALUES (3241, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1622006607449);
INSERT INTO `base_log` VALUES (3242, '刘亚蓉', 'GET', '/user/info', 1622006611030);
INSERT INTO `base_log` VALUES (3243, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1622006612065);
INSERT INTO `base_log` VALUES (3244, '刘亚蓉', 'GET', '/user/info', 1622006644052);
INSERT INTO `base_log` VALUES (3245, '刘亚蓉', 'GET', '/user/info', 1622006786068);
INSERT INTO `base_log` VALUES (3246, '刘亚蓉', 'GET', '/user/info', 1622006802870);
INSERT INTO `base_log` VALUES (3247, '刘亚蓉', 'GET', '/accountApply/checkNoPass', 1622006807833);
INSERT INTO `base_log` VALUES (3248, '刘亚蓉', 'GET', '/user/info', 1622006841468);
INSERT INTO `base_log` VALUES (3249, '刘亚蓉', 'GET', '/user/info', 1622006866663);
INSERT INTO `base_log` VALUES (3250, '刘亚蓉', 'GET', '/accountApply/checkPass', 1622006872863);
INSERT INTO `base_log` VALUES (3251, NULL, 'POST', '/user/login', 1622009509696);
INSERT INTO `base_log` VALUES (3252, NULL, 'GET', '/user/info', 1622009702940);
INSERT INTO `base_log` VALUES (3253, '刘亚蓉', 'GET', '/user/info', 1622010214287);
INSERT INTO `base_log` VALUES (3254, '刘亚蓉', 'GET', '/accountApply/checkNoPass', 1622010519728);
INSERT INTO `base_log` VALUES (3255, '刘亚蓉', 'GET', '/user/info', 1622010542209);
INSERT INTO `base_log` VALUES (3256, NULL, 'GET', '/user/info', 1622010764087);
INSERT INTO `base_log` VALUES (3257, NULL, 'GET', '/user/info', 1622010770807);
INSERT INTO `base_log` VALUES (3258, NULL, 'GET', '/user/info', 1622010776417);
INSERT INTO `base_log` VALUES (3259, NULL, 'GET', '/user/info', 1622010789587);
INSERT INTO `base_log` VALUES (3260, NULL, 'GET', '/user/info', 1622010802559);
INSERT INTO `base_log` VALUES (3261, NULL, 'GET', '/user/info', 1622010808581);
INSERT INTO `base_log` VALUES (3262, NULL, 'GET', '/user/info', 1622010828554);
INSERT INTO `base_log` VALUES (3263, NULL, 'GET', '/user/info', 1622010848199);
INSERT INTO `base_log` VALUES (3264, NULL, 'GET', '/user/info', 1622010873373);
INSERT INTO `base_log` VALUES (3265, '刘亚蓉', 'GET', '/user/info', 1622010931588);
INSERT INTO `base_log` VALUES (3266, NULL, 'GET', '/user/info', 1622010943739);
INSERT INTO `base_log` VALUES (3267, NULL, 'GET', '/user/info', 1622010969191);
INSERT INTO `base_log` VALUES (3268, '刘亚蓉', 'GET', '/user/info', 1622011038416);
INSERT INTO `base_log` VALUES (3269, '刘亚蓉', 'GET', '/user/info', 1622011106314);
INSERT INTO `base_log` VALUES (3270, '刘亚蓉', 'GET', '/user/info', 1622011163560);
INSERT INTO `base_log` VALUES (3271, '刘亚蓉', 'GET', '/user/info', 1622011188238);
INSERT INTO `base_log` VALUES (3272, NULL, 'GET', '/user/info', 1622011202174);
INSERT INTO `base_log` VALUES (3273, NULL, 'GET', '/user/info', 1622011218621);
INSERT INTO `base_log` VALUES (3274, NULL, 'GET', '/user/info', 1622011236998);
INSERT INTO `base_log` VALUES (3275, NULL, 'GET', '/user/info', 1622011239124);
INSERT INTO `base_log` VALUES (3276, NULL, 'GET', '/user/info', 1622011250525);
INSERT INTO `base_log` VALUES (3277, '刘亚蓉', 'GET', '/user/info', 1622011271112);
INSERT INTO `base_log` VALUES (3278, NULL, 'GET', '/user/info', 1622011272008);
INSERT INTO `base_log` VALUES (3279, NULL, 'GET', '/user/info', 1622011290718);
INSERT INTO `base_log` VALUES (3280, NULL, 'GET', '/user/info', 1622011300447);
INSERT INTO `base_log` VALUES (3281, NULL, 'GET', '/user/info', 1622011324896);
INSERT INTO `base_log` VALUES (3282, NULL, 'GET', '/user/info', 1622011350801);
INSERT INTO `base_log` VALUES (3283, NULL, 'GET', '/user/info', 1622011398199);
INSERT INTO `base_log` VALUES (3284, NULL, 'GET', '/user/info', 1622011416306);
INSERT INTO `base_log` VALUES (3285, NULL, 'GET', '/user/info', 1622011419763);
INSERT INTO `base_log` VALUES (3286, NULL, 'GET', '/user/info', 1622011451288);
INSERT INTO `base_log` VALUES (3287, NULL, 'GET', '/user/info', 1622011476801);
INSERT INTO `base_log` VALUES (3288, NULL, 'POST', '/user/login', 1622011511887);
INSERT INTO `base_log` VALUES (3289, NULL, 'GET', '/user/info', 1622011516469);
INSERT INTO `base_log` VALUES (3290, NULL, 'GET', '/user/info', 1622011550853);
INSERT INTO `base_log` VALUES (3291, NULL, 'GET', '/user/info', 1622011573522);
INSERT INTO `base_log` VALUES (3292, NULL, 'GET', '/user/info', 1622011577807);
INSERT INTO `base_log` VALUES (3293, NULL, 'GET', '/user/info', 1622011600571);
INSERT INTO `base_log` VALUES (3294, NULL, 'GET', '/user/info', 1622011601737);
INSERT INTO `base_log` VALUES (3295, NULL, 'GET', '/user/info', 1622011653126);
INSERT INTO `base_log` VALUES (3296, NULL, 'GET', '/user/info', 1622011660461);
INSERT INTO `base_log` VALUES (3297, NULL, 'GET', '/user/info', 1622011666122);
INSERT INTO `base_log` VALUES (3298, NULL, 'GET', '/user/info', 1622011722416);
INSERT INTO `base_log` VALUES (3299, NULL, 'GET', '/user/info', 1622011796884);
INSERT INTO `base_log` VALUES (3300, NULL, 'GET', '/user/info', 1622011801822);
INSERT INTO `base_log` VALUES (3301, NULL, 'GET', '/user/info', 1622011855013);
INSERT INTO `base_log` VALUES (3302, NULL, 'GET', '/user/info', 1622011894445);
INSERT INTO `base_log` VALUES (3303, NULL, 'GET', '/user/info', 1622011906248);
INSERT INTO `base_log` VALUES (3304, NULL, 'GET', '/user/info', 1622011922432);
INSERT INTO `base_log` VALUES (3305, NULL, 'GET', '/user/info', 1622011942746);
INSERT INTO `base_log` VALUES (3306, NULL, 'GET', '/user/info', 1622011949313);
INSERT INTO `base_log` VALUES (3307, NULL, 'GET', '/user/info', 1622011952895);
INSERT INTO `base_log` VALUES (3308, NULL, 'GET', '/user/info', 1622011956519);
INSERT INTO `base_log` VALUES (3309, NULL, 'GET', '/user/info', 1622012057997);
INSERT INTO `base_log` VALUES (3310, NULL, 'GET', '/user/info', 1622012062695);
INSERT INTO `base_log` VALUES (3311, NULL, 'GET', '/user/info', 1622012066470);
INSERT INTO `base_log` VALUES (3312, NULL, 'GET', '/user/info', 1622012077857);
INSERT INTO `base_log` VALUES (3313, '刘亚蓉', 'GET', '/user/info', 1622012131531);
INSERT INTO `base_log` VALUES (3314, NULL, 'GET', '/user/info', 1622012137650);
INSERT INTO `base_log` VALUES (3315, '刘亚蓉', 'GET', '/user/info', 1622012147286);
INSERT INTO `base_log` VALUES (3316, NULL, 'GET', '/user/info', 1622012172553);
INSERT INTO `base_log` VALUES (3317, NULL, 'GET', '/user/info', 1622012286407);
INSERT INTO `base_log` VALUES (3318, '刘亚蓉', 'GET', '/user/info', 1622012286695);
INSERT INTO `base_log` VALUES (3319, '刘亚蓉', 'GET', '/user/info', 1622012291668);
INSERT INTO `base_log` VALUES (3320, NULL, 'GET', '/user/info', 1622012306052);
INSERT INTO `base_log` VALUES (3321, NULL, 'GET', '/user/info', 1622012359198);
INSERT INTO `base_log` VALUES (3322, NULL, 'GET', '/user/info', 1622012373593);
INSERT INTO `base_log` VALUES (3323, NULL, 'GET', '/user/info', 1622012379479);
INSERT INTO `base_log` VALUES (3324, NULL, 'GET', '/user/info', 1622012418885);
INSERT INTO `base_log` VALUES (3325, NULL, 'GET', '/user/info', 1622012427551);
INSERT INTO `base_log` VALUES (3326, NULL, 'GET', '/user/info', 1622012434741);
INSERT INTO `base_log` VALUES (3327, NULL, 'GET', '/user/info', 1622012443967);
INSERT INTO `base_log` VALUES (3328, NULL, 'GET', '/user/info', 1622012454724);
INSERT INTO `base_log` VALUES (3329, '刘亚蓉', 'GET', '/user/info', 1622012619794);
INSERT INTO `base_log` VALUES (3330, NULL, 'GET', '/user/info', 1622012635033);
INSERT INTO `base_log` VALUES (3331, '刘亚蓉', 'GET', '/user/info', 1622012645755);
INSERT INTO `base_log` VALUES (3332, '刘亚蓉', 'GET', '/user/info', 1622012657985);
INSERT INTO `base_log` VALUES (3333, NULL, 'GET', '/user/info', 1622012719539);
INSERT INTO `base_log` VALUES (3334, NULL, 'GET', '/user/info', 1622012765732);
INSERT INTO `base_log` VALUES (3335, NULL, 'GET', '/user/info', 1622012794616);
INSERT INTO `base_log` VALUES (3336, NULL, 'GET', '/user/info', 1622012804708);
INSERT INTO `base_log` VALUES (3337, NULL, 'GET', '/user/info', 1622012909183);
INSERT INTO `base_log` VALUES (3338, NULL, 'GET', '/user/info', 1622012918346);
INSERT INTO `base_log` VALUES (3339, NULL, 'GET', '/user/info', 1622012976784);
INSERT INTO `base_log` VALUES (3340, NULL, 'GET', '/user/info', 1622013004425);
INSERT INTO `base_log` VALUES (3341, NULL, 'GET', '/user/info', 1622013047611);
INSERT INTO `base_log` VALUES (3342, NULL, 'GET', '/user/info', 1622013158781);
INSERT INTO `base_log` VALUES (3343, NULL, 'GET', '/user/info', 1622013195633);
INSERT INTO `base_log` VALUES (3344, NULL, 'GET', '/user/info', 1622013682419);
INSERT INTO `base_log` VALUES (3345, NULL, 'GET', '/user/info', 1622013688407);
INSERT INTO `base_log` VALUES (3346, NULL, 'GET', '/user/info', 1622013810151);
INSERT INTO `base_log` VALUES (3347, '刘亚蓉', 'GET', '/user/info', 1622013917994);
INSERT INTO `base_log` VALUES (3348, '刘亚蓉', 'GET', '/user/info', 1622014184053);
INSERT INTO `base_log` VALUES (3349, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1622019179308);
INSERT INTO `base_log` VALUES (3350, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1622019196445);
INSERT INTO `base_log` VALUES (3351, '刘亚蓉', 'GET', '/role/cascadePrivilegeFindAll', 1622019213380);
INSERT INTO `base_log` VALUES (3352, '刘亚蓉', 'GET', '/user/info', 1622019954684);
INSERT INTO `base_log` VALUES (3353, '刘亚蓉', 'GET', '/user/info', 1622019963275);
INSERT INTO `base_log` VALUES (3354, '刘亚蓉', 'GET', '/user/info', 1622020138241);
INSERT INTO `base_log` VALUES (3355, '刘亚蓉', 'POST', '/article/changeStatus', 1622020159902);
INSERT INTO `base_log` VALUES (3356, '刘亚蓉', 'GET', '/article/deleteById', 1622020215815);
INSERT INTO `base_log` VALUES (3357, '刘亚蓉', 'POST', '/article/changeStatus', 1622020387007);
INSERT INTO `base_log` VALUES (3358, '刘亚蓉', 'GET', '/user/info', 1622020399699);
INSERT INTO `base_log` VALUES (3359, '刘亚蓉', 'GET', '/user/info', 1622020413120);
INSERT INTO `base_log` VALUES (3360, '刘亚蓉', 'POST', '/article/changeStatus', 1622020419586);
INSERT INTO `base_log` VALUES (3361, '刘亚蓉', 'POST', '/article/changeStatus', 1622020425564);
INSERT INTO `base_log` VALUES (3362, '刘亚蓉', 'GET', '/user/info', 1622020486997);
INSERT INTO `base_log` VALUES (3363, '刘亚蓉', 'GET', '/user/info', 1622020490866);
INSERT INTO `base_log` VALUES (3364, '刘亚蓉', 'POST', '/article/changeStatus', 1622020506811);
INSERT INTO `base_log` VALUES (3365, '刘亚蓉', 'POST', '/article/changeStatus', 1622020517016);
INSERT INTO `base_log` VALUES (3366, '刘亚蓉', 'GET', '/user/info', 1622020718910);
INSERT INTO `base_log` VALUES (3367, '刘亚蓉', 'GET', '/article/deleteById', 1622020742110);
INSERT INTO `base_log` VALUES (3368, '刘亚蓉', 'GET', '/user/info', 1622020801852);
INSERT INTO `base_log` VALUES (3369, '刘亚蓉', 'GET', '/user/info', 1622020827965);
INSERT INTO `base_log` VALUES (3370, '刘亚蓉', 'GET', '/user/info', 1622021095037);
INSERT INTO `base_log` VALUES (3371, '刘亚蓉', 'GET', '/user/info', 1622021470412);
INSERT INTO `base_log` VALUES (3372, '刘亚蓉', 'GET', '/user/info', 1622021480061);
INSERT INTO `base_log` VALUES (3373, '刘亚蓉', 'GET', '/user/info', 1622021736901);
INSERT INTO `base_log` VALUES (3374, '刘亚蓉', 'GET', '/user/info', 1622021842690);
INSERT INTO `base_log` VALUES (3375, '刘亚蓉', 'POST', '/article/saveOrUpdate', 1622021870604);
INSERT INTO `base_log` VALUES (3376, '刘亚蓉', 'GET', '/user/info', 1622021887930);
INSERT INTO `base_log` VALUES (3377, '刘亚蓉', 'POST', '/article/saveOrUpdate', 1622021916320);
INSERT INTO `base_log` VALUES (3378, '刘亚蓉', 'POST', '/article/changeStatus', 1622022094148);
INSERT INTO `base_log` VALUES (3379, '刘亚蓉', 'GET', '/user/info', 1622078486190);
INSERT INTO `base_log` VALUES (3380, '刘亚蓉', 'GET', '/user/info', 1622078740785);
INSERT INTO `base_log` VALUES (3381, '刘亚蓉', 'GET', '/user/info', 1622079094694);
INSERT INTO `base_log` VALUES (3382, '刘亚蓉', 'GET', '/user/info', 1622079432131);
INSERT INTO `base_log` VALUES (3383, '刘亚蓉', 'GET', '/user/info', 1622079459384);
INSERT INTO `base_log` VALUES (3384, '刘亚蓉', 'GET', '/user/info', 1622082524822);
INSERT INTO `base_log` VALUES (3385, '刘亚蓉', 'POST', '/baseUser/alterUserface', 1622082576319);
INSERT INTO `base_log` VALUES (3386, '刘亚蓉', 'GET', '/user/info', 1622082624491);
INSERT INTO `base_log` VALUES (3387, '刘亚蓉', 'GET', '/user/info', 1622082652302);
INSERT INTO `base_log` VALUES (3388, '刘亚蓉', 'POST', '/baseUser/alterUserface', 1622082664794);
INSERT INTO `base_log` VALUES (3389, '刘亚蓉', 'GET', '/user/info', 1622082665350);
INSERT INTO `base_log` VALUES (3390, '刘亚蓉', 'GET', '/user/info', 1622082774778);
INSERT INTO `base_log` VALUES (3391, '刘亚蓉', 'GET', '/user/info', 1622082870672);
INSERT INTO `base_log` VALUES (3392, '刘亚蓉', 'POST', '/baseUser/alterUserface', 1622082889541);
INSERT INTO `base_log` VALUES (3393, '刘亚蓉', 'GET', '/user/info', 1622082889770);
INSERT INTO `base_log` VALUES (3394, '刘亚蓉', 'GET', '/user/info', 1622082946718);
INSERT INTO `base_log` VALUES (3395, '刘亚蓉', 'GET', '/user/info', 1622082991557);
INSERT INTO `base_log` VALUES (3396, '刘亚蓉', 'POST', '/baseUser/alterUserface', 1622083009614);
INSERT INTO `base_log` VALUES (3397, '刘亚蓉', 'GET', '/user/info', 1622083010137);
INSERT INTO `base_log` VALUES (3398, '刘亚蓉', 'POST', '/baseUser/alterUserface', 1622083042671);
INSERT INTO `base_log` VALUES (3399, '刘亚蓉', 'GET', '/user/info', 1622083042935);
INSERT INTO `base_log` VALUES (3400, '刘亚蓉', 'GET', '/user/info', 1622083064009);
INSERT INTO `base_log` VALUES (3401, '刘亚蓉', 'POST', '/baseUser/alterUserface', 1622083096119);
INSERT INTO `base_log` VALUES (3402, '刘亚蓉', 'GET', '/user/info', 1622083097442);
INSERT INTO `base_log` VALUES (3403, '刘亚蓉', 'GET', '/user/info', 1622083113048);
INSERT INTO `base_log` VALUES (3404, '刘亚蓉', 'POST', '/baseUser/saveOrUpdate', 1622083127250);
INSERT INTO `base_log` VALUES (3405, '刘亚蓉2', 'GET', '/user/info', 1622083128079);
INSERT INTO `base_log` VALUES (3406, '刘亚蓉2', 'GET', '/user/info', 1622083154997);
INSERT INTO `base_log` VALUES (3407, '刘亚蓉2', 'GET', '/user/info', 1622083173978);
INSERT INTO `base_log` VALUES (3408, '刘亚蓉2', 'GET', '/user/info', 1622083242699);
INSERT INTO `base_log` VALUES (3409, '刘亚蓉2', 'POST', '/baseUser/saveOrUpdate', 1622083254741);
INSERT INTO `base_log` VALUES (3410, '刘亚蓉2', 'POST', '/baseUser/alterUserface', 1622083264774);
INSERT INTO `base_log` VALUES (3411, '刘亚蓉2', 'GET', '/user/info', 1622083276703);
INSERT INTO `base_log` VALUES (3412, '刘亚蓉2', 'GET', '/user/info', 1622083399656);
INSERT INTO `base_log` VALUES (3413, '刘亚蓉2', 'GET', '/user/info', 1622083579801);
INSERT INTO `base_log` VALUES (3414, '刘亚蓉2', 'POST', '/baseUser/alterUserface', 1622083588447);
INSERT INTO `base_log` VALUES (3415, '刘亚蓉2', 'POST', '/baseUser/alterUserface', 1622083602289);
INSERT INTO `base_log` VALUES (3416, '刘亚蓉2', 'POST', '/baseUser/alterUserface', 1622083621862);
INSERT INTO `base_log` VALUES (3417, '刘亚蓉2', 'GET', '/user/info', 1622083869877);
INSERT INTO `base_log` VALUES (3418, '刘亚蓉2', 'GET', '/user/info', 1622083898209);
INSERT INTO `base_log` VALUES (3419, '刘亚蓉2', 'POST', '/privilege/saveOrUpdate', 1622084082599);
INSERT INTO `base_log` VALUES (3420, '刘亚蓉2', 'POST', '/privilege/saveOrUpdate', 1622084125259);
INSERT INTO `base_log` VALUES (3421, '刘亚蓉2', 'GET', '/role/cascadePrivilegeFindAll', 1622084139009);
INSERT INTO `base_log` VALUES (3422, '刘亚蓉2', 'POST', '/role/authorization', 1622084147150);
INSERT INTO `base_log` VALUES (3423, '刘亚蓉2', 'GET', '/role/cascadePrivilegeFindAll', 1622084148037);
INSERT INTO `base_log` VALUES (3424, '刘亚蓉2', 'GET', '/user/info', 1622084190452);
INSERT INTO `base_log` VALUES (3425, '刘亚蓉2', 'GET', '/user/info', 1622084387658);
INSERT INTO `base_log` VALUES (3426, '刘亚蓉2', 'GET', '/user/info', 1622084393583);
INSERT INTO `base_log` VALUES (3427, '刘亚蓉2', 'GET', '/user/info', 1622084404109);
INSERT INTO `base_log` VALUES (3428, '刘亚蓉2', 'GET', '/user/info', 1622084414013);
INSERT INTO `base_log` VALUES (3429, '刘亚蓉2', 'GET', '/user/info', 1622084421388);
INSERT INTO `base_log` VALUES (3430, '刘亚蓉2', 'GET', '/user/info', 1622084446791);
INSERT INTO `base_log` VALUES (3431, '刘亚蓉2', 'GET', '/user/info', 1622084519967);
INSERT INTO `base_log` VALUES (3432, '刘亚蓉2', 'GET', '/user/info', 1622084670441);
INSERT INTO `base_log` VALUES (3433, '刘亚蓉2', 'GET', '/user/info', 1622084859547);
INSERT INTO `base_log` VALUES (3434, '刘亚蓉2', 'GET', '/user/info', 1622084865664);
INSERT INTO `base_log` VALUES (3435, '刘亚蓉2', 'GET', '/user/info', 1622085084018);
INSERT INTO `base_log` VALUES (3436, '刘亚蓉2', 'GET', '/user/info', 1622085138069);
INSERT INTO `base_log` VALUES (3437, '刘亚蓉2', 'GET', '/user/info', 1622085163342);
INSERT INTO `base_log` VALUES (3438, '刘亚蓉2', 'GET', '/user/info', 1622085271891);
INSERT INTO `base_log` VALUES (3439, '刘亚蓉2', 'GET', '/user/info', 1622085289312);
INSERT INTO `base_log` VALUES (3440, '刘亚蓉2', 'GET', '/comment/check', 1622085293889);
INSERT INTO `base_log` VALUES (3441, '刘亚蓉2', 'GET', '/user/info', 1622085344284);
INSERT INTO `base_log` VALUES (3442, '刘亚蓉2', 'GET', '/comment/check', 1622085349676);
INSERT INTO `base_log` VALUES (3443, '刘亚蓉2', 'GET', '/comment/check', 1622085352371);
INSERT INTO `base_log` VALUES (3444, '刘亚蓉2', 'GET', '/comment/check', 1622085355201);
INSERT INTO `base_log` VALUES (3445, '刘亚蓉2', 'GET', '/user/info', 1622085385573);
INSERT INTO `base_log` VALUES (3446, '刘亚蓉2', 'GET', '/user/info', 1622085421878);
INSERT INTO `base_log` VALUES (3447, '刘亚蓉2', 'GET', '/user/info', 1622085454965);
INSERT INTO `base_log` VALUES (3448, '刘亚蓉2', 'GET', '/comment/check', 1622085459761);
INSERT INTO `base_log` VALUES (3449, '刘亚蓉2', 'GET', '/user/info', 1622085756318);
INSERT INTO `base_log` VALUES (3450, '刘亚蓉2', 'GET', '/comment/deleteById', 1622085766789);
INSERT INTO `base_log` VALUES (3451, '刘亚蓉2', 'GET', '/user/info', 1622085794308);
INSERT INTO `base_log` VALUES (3452, '刘亚蓉2', 'GET', '/comment/deleteById', 1622085801239);
INSERT INTO `base_log` VALUES (3453, '刘亚蓉2', 'POST', '/comment/batchDelete', 1622085830967);
INSERT INTO `base_log` VALUES (3454, '刘亚蓉2', 'GET', '/user/info', 1622085911712);
INSERT INTO `base_log` VALUES (3455, '刘亚蓉2', 'GET', '/user/info', 1622085924929);
INSERT INTO `base_log` VALUES (3456, '刘亚蓉2', 'GET', '/user/info', 1622085976490);
INSERT INTO `base_log` VALUES (3457, '刘亚蓉2', 'GET', '/comment/deleteById', 1622085992277);
INSERT INTO `base_log` VALUES (3458, '刘亚蓉2', 'GET', '/user/info', 1622086224571);
INSERT INTO `base_log` VALUES (3459, '刘亚蓉2', 'POST', '/privilege/saveOrUpdate', 1622086288764);
INSERT INTO `base_log` VALUES (3460, '刘亚蓉2', 'POST', '/privilege/saveOrUpdate', 1622086307260);
INSERT INTO `base_log` VALUES (3461, '刘亚蓉2', 'POST', '/privilege/saveOrUpdate', 1622086318597);
INSERT INTO `base_log` VALUES (3462, '刘亚蓉2', 'GET', '/role/cascadePrivilegeFindAll', 1622086321498);
INSERT INTO `base_log` VALUES (3463, '刘亚蓉2', 'POST', '/role/authorization', 1622086332105);
INSERT INTO `base_log` VALUES (3464, '刘亚蓉2', 'GET', '/role/cascadePrivilegeFindAll', 1622086332677);
INSERT INTO `base_log` VALUES (3465, '刘亚蓉2', 'GET', '/user/info', 1622086344598);
INSERT INTO `base_log` VALUES (3466, '刘亚蓉2', 'GET', '/role/cascadePrivilegeFindAll', 1622086345990);
INSERT INTO `base_log` VALUES (3467, '刘亚蓉2', 'GET', '/user/info', 1622086369247);
INSERT INTO `base_log` VALUES (3468, '刘亚蓉2', 'GET', '/role/cascadePrivilegeFindAll', 1622086370416);
INSERT INTO `base_log` VALUES (3469, '刘亚蓉2', 'GET', '/user/info', 1622086484474);
INSERT INTO `base_log` VALUES (3470, '刘亚蓉2', 'GET', '/user/info', 1622086844847);
INSERT INTO `base_log` VALUES (3471, NULL, 'POST', '/user/login', 1622087849449);
INSERT INTO `base_log` VALUES (3472, NULL, 'GET', '/user/info', 1622087855560);
INSERT INTO `base_log` VALUES (3473, NULL, 'POST', '/order/submitOrder', 1622087856846);
INSERT INTO `base_log` VALUES (3474, NULL, 'GET', '/user/info', 1622087857354);
INSERT INTO `base_log` VALUES (3475, '刘亚蓉2', 'GET', '/user/info', 1622090006747);
INSERT INTO `base_log` VALUES (3476, '刘亚蓉2', 'GET', '/user/info', 1622090053624);
INSERT INTO `base_log` VALUES (3477, '刘亚蓉2', 'GET', '/user/info', 1622090216768);
INSERT INTO `base_log` VALUES (3478, '刘亚蓉2', 'POST', '/baseConfig/saveOrUpdate', 1622090237913);
INSERT INTO `base_log` VALUES (3479, '刘亚蓉2', 'GET', '/user/info', 1622090372029);
INSERT INTO `base_log` VALUES (3480, '刘亚蓉2', 'POST', '/baseConfig/saveOrUpdate', 1622090387309);
INSERT INTO `base_log` VALUES (3481, '刘亚蓉2', 'POST', '/baseConfig/saveOrUpdate', 1622090396242);
INSERT INTO `base_log` VALUES (3482, '刘亚蓉2', 'GET', '/user/info', 1622090454717);
INSERT INTO `base_log` VALUES (3483, '刘亚蓉2', 'GET', '/baseConfig/deleteById', 1622090462151);
INSERT INTO `base_log` VALUES (3484, '刘亚蓉2', 'POST', '/baseConfig/saveOrUpdate', 1622090495492);
INSERT INTO `base_log` VALUES (3485, '刘亚蓉2', 'GET', '/user/info', 1622090532020);
INSERT INTO `base_log` VALUES (3486, '刘亚蓉2', 'GET', '/user/info', 1622090594697);
INSERT INTO `base_log` VALUES (3487, '刘亚蓉2', 'GET', '/user/info', 1622090732696);
INSERT INTO `base_log` VALUES (3488, '刘亚蓉2', 'GET', '/user/info', 1622090744264);
INSERT INTO `base_log` VALUES (3489, NULL, 'GET', '/user/info', 1622096087396);
INSERT INTO `base_log` VALUES (3490, NULL, 'GET', '/user/info', 1622096087755);
INSERT INTO `base_log` VALUES (3491, NULL, 'GET', '/user/info', 1622096091250);
INSERT INTO `base_log` VALUES (3492, NULL, 'GET', '/user/info', 1622096093159);
INSERT INTO `base_log` VALUES (3493, NULL, 'GET', '/user/info', 1622096095282);
INSERT INTO `base_log` VALUES (3494, NULL, 'GET', '/user/info', 1622096107595);
INSERT INTO `base_log` VALUES (3495, NULL, 'GET', '/user/info', 1622096133946);
INSERT INTO `base_log` VALUES (3496, NULL, 'GET', '/user/info', 1622096223147);
INSERT INTO `base_log` VALUES (3497, NULL, 'GET', '/user/info', 1622096301715);
INSERT INTO `base_log` VALUES (3498, NULL, 'GET', '/user/info', 1622096475096);
INSERT INTO `base_log` VALUES (3499, NULL, 'GET', '/user/info', 1622096496585);
INSERT INTO `base_log` VALUES (3500, NULL, 'GET', '/user/info', 1622096523060);
INSERT INTO `base_log` VALUES (3501, NULL, 'GET', '/user/info', 1622096553440);
INSERT INTO `base_log` VALUES (3502, NULL, 'GET', '/user/info', 1622096561992);
INSERT INTO `base_log` VALUES (3503, NULL, 'GET', '/user/info', 1622096619159);
INSERT INTO `base_log` VALUES (3504, NULL, 'GET', '/user/info', 1622096629322);
INSERT INTO `base_log` VALUES (3505, NULL, 'GET', '/user/info', 1622096643619);
INSERT INTO `base_log` VALUES (3506, NULL, 'POST', '/accountApply/submitAccountApply', 1622096653112);
INSERT INTO `base_log` VALUES (3507, NULL, 'GET', '/user/info', 1622096841139);
INSERT INTO `base_log` VALUES (3508, NULL, 'GET', '/user/info', 1622096858459);
INSERT INTO `base_log` VALUES (3509, NULL, 'POST', '/accountApply/submitAccountApply', 1622096862868);
INSERT INTO `base_log` VALUES (3510, NULL, 'GET', '/user/info', 1622096885987);
INSERT INTO `base_log` VALUES (3511, NULL, 'GET', '/user/info', 1622096914643);
INSERT INTO `base_log` VALUES (3512, NULL, 'GET', '/user/info', 1622096921024);
INSERT INTO `base_log` VALUES (3513, NULL, 'POST', '/accountApply/submitAccountApply', 1622096926777);
INSERT INTO `base_log` VALUES (3514, NULL, 'GET', '/user/info', 1622096963059);
INSERT INTO `base_log` VALUES (3515, NULL, 'GET', '/user/info', 1622097296841);
INSERT INTO `base_log` VALUES (3516, NULL, 'GET', '/user/info', 1622097314709);
INSERT INTO `base_log` VALUES (3517, NULL, 'GET', '/user/info', 1622097324072);
INSERT INTO `base_log` VALUES (3518, NULL, 'GET', '/user/info', 1622097331778);
INSERT INTO `base_log` VALUES (3519, NULL, 'GET', '/user/info', 1622097425659);
INSERT INTO `base_log` VALUES (3520, NULL, 'GET', '/user/info', 1622097438817);
INSERT INTO `base_log` VALUES (3521, NULL, 'GET', '/user/info', 1622097453740);
INSERT INTO `base_log` VALUES (3522, NULL, 'GET', '/user/info', 1622097474112);
INSERT INTO `base_log` VALUES (3523, NULL, 'GET', '/user/info', 1622097531016);
INSERT INTO `base_log` VALUES (3524, NULL, 'GET', '/user/info', 1622097546223);
INSERT INTO `base_log` VALUES (3525, NULL, 'GET', '/user/info', 1622097558503);
INSERT INTO `base_log` VALUES (3526, NULL, 'GET', '/user/info', 1622097583565);
INSERT INTO `base_log` VALUES (3527, NULL, 'GET', '/user/info', 1622097593616);
INSERT INTO `base_log` VALUES (3528, NULL, 'GET', '/user/info', 1622097614365);
INSERT INTO `base_log` VALUES (3529, NULL, 'GET', '/user/info', 1622097644566);
INSERT INTO `base_log` VALUES (3530, NULL, 'GET', '/user/info', 1622097650370);
INSERT INTO `base_log` VALUES (3531, NULL, 'GET', '/user/info', 1622097825092);
INSERT INTO `base_log` VALUES (3532, NULL, 'POST', '/user/login', 1622097886306);
INSERT INTO `base_log` VALUES (3533, NULL, 'GET', '/user/info', 1622097886897);
INSERT INTO `base_log` VALUES (3534, NULL, 'GET', '/user/info', 1622097886999);
INSERT INTO `base_log` VALUES (3535, NULL, 'GET', '/user/info', 1622097903299);
INSERT INTO `base_log` VALUES (3536, NULL, 'GET', '/user/info', 1622097960419);
INSERT INTO `base_log` VALUES (3537, NULL, 'GET', '/user/info', 1622097972244);
INSERT INTO `base_log` VALUES (3538, NULL, 'GET', '/user/info', 1622098002004);
INSERT INTO `base_log` VALUES (3539, NULL, 'GET', '/user/info', 1622098202379);
INSERT INTO `base_log` VALUES (3540, NULL, 'GET', '/user/info', 1622098320067);
INSERT INTO `base_log` VALUES (3541, NULL, 'GET', '/user/info', 1622098480182);
INSERT INTO `base_log` VALUES (3542, NULL, 'GET', '/user/info', 1622098577924);
INSERT INTO `base_log` VALUES (3543, NULL, 'GET', '/user/info', 1622098754987);
INSERT INTO `base_log` VALUES (3544, NULL, 'POST', '/accountApply/submitAccountApply', 1622098904404);
INSERT INTO `base_log` VALUES (3545, NULL, 'GET', '/user/info', 1622098906286);
INSERT INTO `base_log` VALUES (3546, NULL, 'GET', '/user/info', 1622098913939);
INSERT INTO `base_log` VALUES (3547, NULL, 'GET', '/user/info', 1622099045641);
INSERT INTO `base_log` VALUES (3548, NULL, 'GET', '/user/info', 1622099075138);
INSERT INTO `base_log` VALUES (3549, NULL, 'GET', '/user/info', 1622099076784);
INSERT INTO `base_log` VALUES (3550, NULL, 'GET', '/user/info', 1622099081716);
INSERT INTO `base_log` VALUES (3551, NULL, 'GET', '/user/info', 1622099085349);
INSERT INTO `base_log` VALUES (3552, NULL, 'GET', '/user/info', 1622099087703);
INSERT INTO `base_log` VALUES (3553, NULL, 'GET', '/user/info', 1622099097454);
INSERT INTO `base_log` VALUES (3554, NULL, 'GET', '/user/info', 1622099111798);
INSERT INTO `base_log` VALUES (3555, NULL, 'GET', '/user/info', 1622099127298);
INSERT INTO `base_log` VALUES (3556, NULL, 'GET', '/order/sendOrder', 1622099240441);
INSERT INTO `base_log` VALUES (3557, NULL, 'GET', '/user/info', 1622099246316);
INSERT INTO `base_log` VALUES (3558, NULL, 'GET', '/user/info', 1622099363078);
INSERT INTO `base_log` VALUES (3559, NULL, 'GET', '/user/info', 1622099575616);
INSERT INTO `base_log` VALUES (3560, NULL, 'GET', '/user/info', 1622099597396);
INSERT INTO `base_log` VALUES (3561, NULL, 'GET', '/user/info', 1622099699047);
INSERT INTO `base_log` VALUES (3562, NULL, 'GET', '/user/info', 1622099740690);
INSERT INTO `base_log` VALUES (3563, NULL, 'GET', '/user/info', 1622099785498);
INSERT INTO `base_log` VALUES (3564, NULL, 'GET', '/user/info', 1622099921461);
INSERT INTO `base_log` VALUES (3565, NULL, 'GET', '/user/info', 1622099923589);
INSERT INTO `base_log` VALUES (3566, NULL, 'GET', '/user/info', 1622099949658);
INSERT INTO `base_log` VALUES (3567, NULL, 'GET', '/user/info', 1622099951881);
INSERT INTO `base_log` VALUES (3568, NULL, 'GET', '/user/info', 1622100187633);
INSERT INTO `base_log` VALUES (3569, NULL, 'GET', '/user/info', 1622100193411);
INSERT INTO `base_log` VALUES (3570, NULL, 'GET', '/user/info', 1622100206069);
INSERT INTO `base_log` VALUES (3571, NULL, 'GET', '/user/info', 1622100382211);
INSERT INTO `base_log` VALUES (3572, NULL, 'GET', '/user/info', 1622100459541);
INSERT INTO `base_log` VALUES (3573, NULL, 'GET', '/user/info', 1622100594668);
INSERT INTO `base_log` VALUES (3574, NULL, 'GET', '/user/info', 1622100597233);
INSERT INTO `base_log` VALUES (3575, NULL, 'GET', '/user/info', 1622100600802);
INSERT INTO `base_log` VALUES (3576, '刘亚蓉2', 'GET', '/user/info', 1622101134233);
INSERT INTO `base_log` VALUES (3577, '刘亚蓉2', 'GET', '/user/info', 1622101214756);
INSERT INTO `base_log` VALUES (3578, '刘亚蓉2', 'GET', '/user/info', 1622101275974);
INSERT INTO `base_log` VALUES (3579, NULL, 'GET', '/user/info', 1622102600718);
INSERT INTO `base_log` VALUES (3580, NULL, 'GET', '/user/info', 1622102600943);
INSERT INTO `base_log` VALUES (3581, NULL, 'GET', '/user/info', 1622102633959);
INSERT INTO `base_log` VALUES (3582, '刘亚蓉2', 'GET', '/user/info', 1622103675892);
INSERT INTO `base_log` VALUES (3583, NULL, 'GET', '/user/info', 1622105442430);
INSERT INTO `base_log` VALUES (3584, NULL, 'GET', '/user/info', 1622105462000);
INSERT INTO `base_log` VALUES (3585, '刘亚蓉2', 'GET', '/baseConfig/deleteById', 1622105666388);
INSERT INTO `base_log` VALUES (3586, '刘亚蓉2', 'POST', '/baseConfig/saveOrUpdate', 1622105956014);
INSERT INTO `base_log` VALUES (3587, NULL, 'GET', '/user/info', 1622106173364);
INSERT INTO `base_log` VALUES (3588, '刘亚蓉2', 'POST', '/user/login', 1622106310531);
INSERT INTO `base_log` VALUES (3589, '刘亚蓉2', 'POST', '/user/login', 1622106317476);
INSERT INTO `base_log` VALUES (3590, NULL, 'GET', '/order/takeOrder', 1622106355285);
INSERT INTO `base_log` VALUES (3591, NULL, 'GET', '/user/info', 1622106422244);
INSERT INTO `base_log` VALUES (3592, NULL, 'GET', '/user/info', 1622106438223);
INSERT INTO `base_log` VALUES (3593, NULL, 'GET', '/order/sendOrder', 1622106515120);
INSERT INTO `base_log` VALUES (3594, NULL, 'GET', '/user/info', 1622106520978);
INSERT INTO `base_log` VALUES (3595, NULL, 'GET', '/user/info', 1622106537865);
INSERT INTO `base_log` VALUES (3596, NULL, 'GET', '/order/takeOrder', 1622106574933);
INSERT INTO `base_log` VALUES (3597, NULL, 'GET', '/user/info', 1622106581285);
INSERT INTO `base_log` VALUES (3598, NULL, 'GET', '/order/sendOrder', 1622106605000);
INSERT INTO `base_log` VALUES (3599, NULL, 'GET', '/user/info', 1622106610126);
INSERT INTO `base_log` VALUES (3600, NULL, 'GET', '/order/rejectOrder', 1622106792698);
INSERT INTO `base_log` VALUES (3601, NULL, 'GET', '/user/info', 1622106800951);
INSERT INTO `base_log` VALUES (3602, '刘亚蓉2', 'GET', '/user/info', 1622106803416);
INSERT INTO `base_log` VALUES (3603, '刘亚蓉2', 'POST', '/orderComment/checkPass', 1622106830411);
INSERT INTO `base_log` VALUES (3604, '刘亚蓉2', 'POST', '/orderComment/checkNoPass', 1622106832962);
INSERT INTO `base_log` VALUES (3605, '刘亚蓉2', 'GET', '/orderComment/deleteById', 1622106841528);
INSERT INTO `base_log` VALUES (3606, NULL, 'GET', '/user/info', 1622106862640);
INSERT INTO `base_log` VALUES (3607, NULL, 'GET', '/user/info', 1622106867134);
INSERT INTO `base_log` VALUES (3608, NULL, 'GET', '/user/info', 1622106871358);
INSERT INTO `base_log` VALUES (3609, '刘亚蓉2', 'GET', '/user/info', 1622106879038);
INSERT INTO `base_log` VALUES (3610, NULL, 'GET', '/user/info', 1622106879240);
INSERT INTO `base_log` VALUES (3611, NULL, 'GET', '/user/info', 1622106883991);
INSERT INTO `base_log` VALUES (3612, NULL, 'GET', '/user/info', 1622106885773);
INSERT INTO `base_log` VALUES (3613, NULL, 'GET', '/user/info', 1622106894653);
INSERT INTO `base_log` VALUES (3614, NULL, 'GET', '/user/info', 1622106909254);
INSERT INTO `base_log` VALUES (3615, NULL, 'GET', '/user/info', 1622106928359);
INSERT INTO `base_log` VALUES (3616, NULL, 'GET', '/user/info', 1622106947485);
INSERT INTO `base_log` VALUES (3617, NULL, 'GET', '/user/info', 1622106949271);
INSERT INTO `base_log` VALUES (3618, NULL, 'GET', '/user/info', 1622107025702);
INSERT INTO `base_log` VALUES (3619, NULL, 'GET', '/user/info', 1622107030601);
INSERT INTO `base_log` VALUES (3620, NULL, 'GET', '/user/info', 1622107034486);
INSERT INTO `base_log` VALUES (3621, NULL, 'GET', '/user/info', 1622107072580);
INSERT INTO `base_log` VALUES (3622, NULL, 'GET', '/user/info', 1622107075396);
INSERT INTO `base_log` VALUES (3623, NULL, 'GET', '/user/info', 1622107078278);
INSERT INTO `base_log` VALUES (3624, NULL, 'GET', '/user/info', 1622107080059);
INSERT INTO `base_log` VALUES (3625, NULL, 'GET', '/user/info', 1622107163997);
INSERT INTO `base_log` VALUES (3626, NULL, 'GET', '/user/info', 1622107165284);
INSERT INTO `base_log` VALUES (3627, NULL, 'GET', '/user/info', 1622107171134);
INSERT INTO `base_log` VALUES (3628, '刘亚蓉2', 'GET', '/orderComment/deleteById', 1622107178979);
INSERT INTO `base_log` VALUES (3629, NULL, 'GET', '/user/info', 1622107255883);
INSERT INTO `base_log` VALUES (3630, NULL, 'GET', '/user/info', 1622107265121);
INSERT INTO `base_log` VALUES (3631, NULL, 'GET', '/user/info', 1622107354712);
INSERT INTO `base_log` VALUES (3632, NULL, 'GET', '/user/info', 1622107360616);
INSERT INTO `base_log` VALUES (3633, NULL, 'GET', '/user/info', 1622107362195);
INSERT INTO `base_log` VALUES (3634, NULL, 'GET', '/user/info', 1622107385139);
INSERT INTO `base_log` VALUES (3635, NULL, 'GET', '/user/info', 1622107389487);
INSERT INTO `base_log` VALUES (3636, NULL, 'GET', '/user/info', 1622107392419);
INSERT INTO `base_log` VALUES (3637, NULL, 'GET', '/user/info', 1622107393725);
INSERT INTO `base_log` VALUES (3638, NULL, 'GET', '/user/info', 1622107401961);
INSERT INTO `base_log` VALUES (3639, NULL, 'GET', '/user/info', 1622107406850);
INSERT INTO `base_log` VALUES (3640, NULL, 'GET', '/user/info', 1622107408484);
INSERT INTO `base_log` VALUES (3641, '刘亚蓉2', 'POST', '/article/saveOrUpdate', 1622107469128);
INSERT INTO `base_log` VALUES (3642, '刘亚蓉2', 'POST', '/article/saveOrUpdate', 1622107473135);
INSERT INTO `base_log` VALUES (3643, NULL, 'GET', '/user/info', 1622107539373);
INSERT INTO `base_log` VALUES (3644, NULL, 'GET', '/user/info', 1622107580074);
INSERT INTO `base_log` VALUES (3645, NULL, 'GET', '/user/info', 1622107584077);
INSERT INTO `base_log` VALUES (3646, NULL, 'GET', '/user/info', 1622107586192);
INSERT INTO `base_log` VALUES (3647, NULL, 'GET', '/user/info', 1622107588997);
INSERT INTO `base_log` VALUES (3648, NULL, 'GET', '/user/info', 1622107596078);
INSERT INTO `base_log` VALUES (3649, NULL, 'GET', '/user/info', 1622107692799);
INSERT INTO `base_log` VALUES (3650, NULL, 'GET', '/user/info', 1622107701132);
INSERT INTO `base_log` VALUES (3651, NULL, 'GET', '/user/info', 1622107713800);
INSERT INTO `base_log` VALUES (3652, NULL, 'GET', '/user/info', 1622107728486);
INSERT INTO `base_log` VALUES (3653, NULL, 'GET', '/user/info', 1622107737725);
INSERT INTO `base_log` VALUES (3654, NULL, 'GET', '/user/info', 1622107746418);
INSERT INTO `base_log` VALUES (3655, NULL, 'GET', '/user/info', 1622107815330);
INSERT INTO `base_log` VALUES (3656, NULL, 'GET', '/user/info', 1622107821021);
INSERT INTO `base_log` VALUES (3657, NULL, 'GET', '/user/info', 1622107847437);
INSERT INTO `base_log` VALUES (3658, NULL, 'GET', '/user/info', 1622107884238);
INSERT INTO `base_log` VALUES (3659, NULL, 'GET', '/user/info', 1622107907802);
INSERT INTO `base_log` VALUES (3660, NULL, 'GET', '/user/info', 1622107918602);
INSERT INTO `base_log` VALUES (3661, NULL, 'GET', '/user/info', 1622107931957);
INSERT INTO `base_log` VALUES (3662, NULL, 'GET', '/user/info', 1622107935162);
INSERT INTO `base_log` VALUES (3663, NULL, 'GET', '/user/info', 1622107939245);
INSERT INTO `base_log` VALUES (3664, NULL, 'GET', '/user/info', 1622107951958);
INSERT INTO `base_log` VALUES (3665, NULL, 'GET', '/user/info', 1622107957080);
INSERT INTO `base_log` VALUES (3666, NULL, 'GET', '/user/info', 1622107963799);
INSERT INTO `base_log` VALUES (3667, '刘亚蓉2', 'POST', '/privilege/saveOrUpdate', 1622107976029);
INSERT INTO `base_log` VALUES (3668, '刘亚蓉2', 'POST', '/privilege/saveOrUpdate', 1622107988309);
INSERT INTO `base_log` VALUES (3669, NULL, 'GET', '/user/info', 1622107993691);
INSERT INTO `base_log` VALUES (3670, NULL, 'GET', '/user/info', 1622107996398);
INSERT INTO `base_log` VALUES (3671, NULL, 'GET', '/user/info', 1622108006138);
INSERT INTO `base_log` VALUES (3672, '刘亚蓉2', 'POST', '/privilege/saveOrUpdate', 1622108008583);
INSERT INTO `base_log` VALUES (3673, '刘亚蓉2', 'POST', '/privilege/saveOrUpdate', 1622108022652);
INSERT INTO `base_log` VALUES (3674, '刘亚蓉2', 'POST', '/privilege/saveOrUpdate', 1622108028993);
INSERT INTO `base_log` VALUES (3675, '刘亚蓉2', 'POST', '/privilege/saveOrUpdate', 1622108035958);
INSERT INTO `base_log` VALUES (3676, '刘亚蓉2', 'POST', '/privilege/saveOrUpdate', 1622108049485);
INSERT INTO `base_log` VALUES (3677, '刘亚蓉2', 'GET', '/user/info', 1622108053183);
INSERT INTO `base_log` VALUES (3678, '刘亚蓉2', 'POST', '/privilege/saveOrUpdate', 1622108083443);
INSERT INTO `base_log` VALUES (3679, '刘亚蓉2', 'POST', '/privilege/saveOrUpdate', 1622108092367);
INSERT INTO `base_log` VALUES (3680, '刘亚蓉2', 'POST', '/privilege/saveOrUpdate', 1622108118981);
INSERT INTO `base_log` VALUES (3681, '刘亚蓉2', 'DELETE', '/privilege/deleteById', 1622108137697);
INSERT INTO `base_log` VALUES (3682, NULL, 'GET', '/user/info', 1622108138163);
INSERT INTO `base_log` VALUES (3683, '刘亚蓉2', 'DELETE', '/privilege/deleteById', 1622108141287);
INSERT INTO `base_log` VALUES (3684, '刘亚蓉2', 'DELETE', '/privilege/deleteById', 1622108149844);
INSERT INTO `base_log` VALUES (3685, '刘亚蓉2', 'GET', '/user/info', 1622108177592);
INSERT INTO `base_log` VALUES (3686, NULL, 'GET', '/user/info', 1622108414162);
INSERT INTO `base_log` VALUES (3687, NULL, 'GET', '/user/info', 1622108442737);
INSERT INTO `base_log` VALUES (3688, NULL, 'GET', '/user/info', 1622108467679);
INSERT INTO `base_log` VALUES (3689, NULL, 'GET', '/user/info', 1622108514946);
INSERT INTO `base_log` VALUES (3690, NULL, 'GET', '/user/info', 1622108553367);
INSERT INTO `base_log` VALUES (3691, NULL, 'GET', '/user/info', 1622108588319);
INSERT INTO `base_log` VALUES (3692, '刘亚蓉2', 'GET', '/user/info', 1622108597499);
INSERT INTO `base_log` VALUES (3693, '刘亚蓉2', 'POST', '/baseUser/saveOrUpdate', 1622108606774);
INSERT INTO `base_log` VALUES (3694, '刘亚蓉', 'GET', '/user/info', 1622108606940);
INSERT INTO `base_log` VALUES (3695, '刘亚蓉', 'POST', '/user/logout', 1622108623938);
INSERT INTO `base_log` VALUES (3696, NULL, 'GET', '/user/info', 1622108629434);
INSERT INTO `base_log` VALUES (3697, NULL, 'GET', '/user/info', 1622108629792);
INSERT INTO `base_log` VALUES (3698, NULL, 'GET', '/user/info', 1622108643453);
INSERT INTO `base_log` VALUES (3699, NULL, 'POST', '/baseUser/saveOrUpdate', 1622108659943);
INSERT INTO `base_log` VALUES (3700, '杰普', 'GET', '/user/info', 1622108662602);
INSERT INTO `base_log` VALUES (3701, '杰普', 'POST', '/user/logout', 1622108667771);
INSERT INTO `base_log` VALUES (3702, '杰普', 'GET', '/user/info', 1622108670147);
INSERT INTO `base_log` VALUES (3703, '杰普', 'GET', '/user/info', 1622108672702);
INSERT INTO `base_log` VALUES (3704, '杰普', 'GET', '/user/info', 1622108684120);
INSERT INTO `base_log` VALUES (3705, '杰普', 'GET', '/user/info', 1622108695183);
INSERT INTO `base_log` VALUES (3706, '杰普', 'GET', '/user/info', 1622108706605);
INSERT INTO `base_log` VALUES (3707, '杰普', 'POST', '/baseUser/alterUserface', 1622108715974);
INSERT INTO `base_log` VALUES (3708, '杰普', 'GET', '/user/info', 1622108716119);
INSERT INTO `base_log` VALUES (3709, '杰普', 'GET', '/user/info', 1622108762400);
INSERT INTO `base_log` VALUES (3710, '杰普', 'GET', '/user/info', 1622108778642);
INSERT INTO `base_log` VALUES (3711, '杰普', 'GET', '/user/info', 1622108808051);
INSERT INTO `base_log` VALUES (3712, '杰普', 'GET', '/user/info', 1622108822427);
INSERT INTO `base_log` VALUES (3713, '杰普', 'GET', '/user/info', 1622108860683);
INSERT INTO `base_log` VALUES (3714, '杰普', 'GET', '/user/info', 1622108896931);
INSERT INTO `base_log` VALUES (3715, '杰普', 'GET', '/user/info', 1622108941891);
INSERT INTO `base_log` VALUES (3716, '杰普', 'POST', '/baseUser/alterUserface', 1622108969224);
INSERT INTO `base_log` VALUES (3717, '杰普', 'GET', '/user/info', 1622108969400);
INSERT INTO `base_log` VALUES (3718, '杰普', 'GET', '/user/info', 1622108971774);
INSERT INTO `base_log` VALUES (3719, '杰普', 'GET', '/user/info', 1622109116833);
INSERT INTO `base_log` VALUES (3720, '杰普', 'GET', '/user/info', 1622109222555);
INSERT INTO `base_log` VALUES (3721, '杰普', 'GET', '/user/info', 1622109270627);
INSERT INTO `base_log` VALUES (3722, '杰普', 'GET', '/user/info', 1622109272461);
INSERT INTO `base_log` VALUES (3723, '杰普', 'GET', '/user/info', 1622109278457);
INSERT INTO `base_log` VALUES (3724, '杰普', 'GET', '/user/info', 1622109461404);
INSERT INTO `base_log` VALUES (3725, '杰普', 'GET', '/user/info', 1622109461485);
INSERT INTO `base_log` VALUES (3726, '杰普', 'GET', '/user/info', 1622109714211);
INSERT INTO `base_log` VALUES (3727, '杰普', 'GET', '/user/info', 1622109714265);
INSERT INTO `base_log` VALUES (3728, '杰普', 'GET', '/user/info', 1622109720633);
INSERT INTO `base_log` VALUES (3729, '杰普', 'GET', '/user/info', 1622109722727);
INSERT INTO `base_log` VALUES (3730, '杰普', 'GET', '/user/info', 1622109725225);
INSERT INTO `base_log` VALUES (3731, '杰普', 'GET', '/user/info', 1622109726205);
INSERT INTO `base_log` VALUES (3732, '杰普', 'GET', '/user/info', 1622109809911);
INSERT INTO `base_log` VALUES (3733, '杰普', 'GET', '/user/info', 1622109809989);
INSERT INTO `base_log` VALUES (3734, '杰普', 'GET', '/user/info', 1622109818269);
INSERT INTO `base_log` VALUES (3735, '杰普', 'GET', '/user/info', 1622109827199);
INSERT INTO `base_log` VALUES (3736, '杰普', 'GET', '/user/info', 1622109834977);
INSERT INTO `base_log` VALUES (3737, '杰普', 'GET', '/user/info', 1622109836525);
INSERT INTO `base_log` VALUES (3738, '杰普', 'GET', '/user/info', 1622109841741);
INSERT INTO `base_log` VALUES (3739, '杰普', 'GET', '/user/info', 1622109842292);
INSERT INTO `base_log` VALUES (3740, '杰普', 'GET', '/user/info', 1622109844003);
INSERT INTO `base_log` VALUES (3741, '杰普', 'GET', '/user/info', 1622109847460);
INSERT INTO `base_log` VALUES (3742, '杰普', 'GET', '/user/info', 1622109850476);
INSERT INTO `base_log` VALUES (3743, '杰普', 'GET', '/user/info', 1622109851403);
INSERT INTO `base_log` VALUES (3744, '杰普', 'GET', '/user/info', 1622109864250);
INSERT INTO `base_log` VALUES (3745, '杰普', 'GET', '/user/info', 1622110203829);
INSERT INTO `base_log` VALUES (3746, '杰普', 'GET', '/user/info', 1622110210833);
INSERT INTO `base_log` VALUES (3747, '杰普', 'GET', '/user/info', 1622110214944);
INSERT INTO `base_log` VALUES (3748, '杰普', 'GET', '/user/info', 1622110220093);
INSERT INTO `base_log` VALUES (3749, '杰普', 'GET', '/user/info', 1622110222030);
INSERT INTO `base_log` VALUES (3750, '杰普', 'GET', '/user/info', 1622110288256);
INSERT INTO `base_log` VALUES (3751, '杰普', 'GET', '/user/info', 1622110447860);
INSERT INTO `base_log` VALUES (3752, '杰普', 'GET', '/user/info', 1622110722845);
INSERT INTO `base_log` VALUES (3753, '杰普', 'GET', '/user/info', 1622110736080);
INSERT INTO `base_log` VALUES (3754, '杰普', 'GET', '/user/info', 1622110784944);
INSERT INTO `base_log` VALUES (3755, '杰普', 'GET', '/user/info', 1622110805746);
INSERT INTO `base_log` VALUES (3756, '杰普', 'GET', '/user/info', 1622110810313);
INSERT INTO `base_log` VALUES (3757, '杰普', 'POST', '/user/login', 1622110989152);
INSERT INTO `base_log` VALUES (3758, '杰普', 'GET', '/user/info', 1622110989261);
INSERT INTO `base_log` VALUES (3759, '杰普', 'GET', '/user/info', 1622110989342);
INSERT INTO `base_log` VALUES (3760, '杰普', 'GET', '/user/info', 1622111541301);
INSERT INTO `base_log` VALUES (3761, '杰普', 'GET', '/user/info', 1622111546111);
INSERT INTO `base_log` VALUES (3762, '杰普', 'GET', '/user/info', 1622111547959);
INSERT INTO `base_log` VALUES (3763, '杰普', 'GET', '/user/info', 1622111549674);
INSERT INTO `base_log` VALUES (3764, '杰普', 'GET', '/user/info', 1622111551414);
INSERT INTO `base_log` VALUES (3765, '杰普', 'GET', '/user/info', 1622111553245);
INSERT INTO `base_log` VALUES (3766, '杰普', 'GET', '/user/info', 1622111594226);
INSERT INTO `base_log` VALUES (3767, '杰普', 'GET', '/user/info', 1622111680543);
INSERT INTO `base_log` VALUES (3768, '杰普', 'GET', '/user/info', 1622111683503);
INSERT INTO `base_log` VALUES (3769, '杰普', 'GET', '/user/info', 1622111690435);
INSERT INTO `base_log` VALUES (3770, '杰普', 'GET', '/user/info', 1622111693969);
INSERT INTO `base_log` VALUES (3771, '杰普', 'GET', '/user/info', 1622111696386);
INSERT INTO `base_log` VALUES (3772, '杰普', 'GET', '/user/info', 1622111851248);
INSERT INTO `base_log` VALUES (3773, '杰普', 'GET', '/user/info', 1622111856178);
INSERT INTO `base_log` VALUES (3774, '杰普', 'GET', '/user/info', 1622111880157);
INSERT INTO `base_log` VALUES (3775, '杰普', 'GET', '/user/info', 1622111883214);
INSERT INTO `base_log` VALUES (3776, '杰普', 'GET', '/user/info', 1622111885311);
INSERT INTO `base_log` VALUES (3777, '杰普', 'GET', '/user/info', 1622111887213);
INSERT INTO `base_log` VALUES (3778, '杰普', 'GET', '/user/info', 1622111889531);
INSERT INTO `base_log` VALUES (3779, '杰普', 'GET', '/user/info', 1622111891923);
INSERT INTO `base_log` VALUES (3780, '杰普', 'GET', '/user/info', 1622111939528);
INSERT INTO `base_log` VALUES (3781, '杰普', 'GET', '/user/info', 1622111941757);
INSERT INTO `base_log` VALUES (3782, '杰普', 'GET', '/user/info', 1622112291356);
INSERT INTO `base_log` VALUES (3783, '杰普', 'GET', '/user/info', 1622112292768);
INSERT INTO `base_log` VALUES (3784, '杰普', 'GET', '/user/info', 1622112294391);
INSERT INTO `base_log` VALUES (3785, '杰普', 'GET', '/user/info', 1622112297287);
INSERT INTO `base_log` VALUES (3786, '杰普', 'GET', '/user/info', 1622112299088);
INSERT INTO `base_log` VALUES (3787, '杰普', 'GET', '/user/info', 1622112332058);
INSERT INTO `base_log` VALUES (3788, '杰普', 'GET', '/user/info', 1622112333310);
INSERT INTO `base_log` VALUES (3789, '杰普', 'GET', '/user/info', 1622112335711);
INSERT INTO `base_log` VALUES (3790, '杰普', 'GET', '/user/info', 1622112337403);
INSERT INTO `base_log` VALUES (3791, '杰普', 'GET', '/user/info', 1622112802967);
INSERT INTO `base_log` VALUES (3792, '杰普', 'GET', '/user/info', 1622112809654);
INSERT INTO `base_log` VALUES (3793, '杰普', 'GET', '/user/info', 1622112860498);
INSERT INTO `base_log` VALUES (3794, '杰普', 'GET', '/user/info', 1622113058630);
INSERT INTO `base_log` VALUES (3795, '杰普', 'GET', '/user/info', 1622113061747);
INSERT INTO `base_log` VALUES (3796, '杰普', 'GET', '/user/info', 1622113064517);
INSERT INTO `base_log` VALUES (3797, '杰普', 'GET', '/user/info', 1622113073135);
INSERT INTO `base_log` VALUES (3798, '杰普', 'GET', '/user/info', 1622113155470);
INSERT INTO `base_log` VALUES (3799, '杰普', 'GET', '/user/info', 1622113157467);
INSERT INTO `base_log` VALUES (3800, '杰普', 'GET', '/user/info', 1622113159232);
INSERT INTO `base_log` VALUES (3801, '杰普', 'GET', '/user/info', 1622113161746);
INSERT INTO `base_log` VALUES (3802, '杰普', 'GET', '/user/info', 1622113170109);
INSERT INTO `base_log` VALUES (3803, '杰普', 'GET', '/user/info', 1622113173458);
INSERT INTO `base_log` VALUES (3804, '杰普', 'GET', '/user/info', 1622113177005);
INSERT INTO `base_log` VALUES (3805, '杰普', 'GET', '/user/info', 1622113180262);
INSERT INTO `base_log` VALUES (3806, '杰普', 'GET', '/user/info', 1622113183326);
INSERT INTO `base_log` VALUES (3807, '杰普', 'GET', '/user/info', 1622113185845);
INSERT INTO `base_log` VALUES (3808, '杰普', 'GET', '/user/info', 1622113198638);
INSERT INTO `base_log` VALUES (3809, '杰普', 'GET', '/user/info', 1622113201880);
INSERT INTO `base_log` VALUES (3810, '杰普', 'GET', '/user/info', 1622113211497);
INSERT INTO `base_log` VALUES (3811, '杰普', 'GET', '/user/info', 1622113213868);
INSERT INTO `base_log` VALUES (3812, '杰普', 'GET', '/user/info', 1622113225675);
INSERT INTO `base_log` VALUES (3813, '杰普', 'GET', '/user/info', 1622113232803);
INSERT INTO `base_log` VALUES (3814, '杰普', 'GET', '/user/info', 1622113234658);
INSERT INTO `base_log` VALUES (3815, '杰普', 'GET', '/user/info', 1622113239371);
INSERT INTO `base_log` VALUES (3816, '杰普', 'POST', '/user/login', 1622114782600);
INSERT INTO `base_log` VALUES (3817, '杰普', 'POST', '/user/login', 1622114799118);
INSERT INTO `base_log` VALUES (3818, '杰普', 'GET', '/user/info', 1622114802198);
INSERT INTO `base_log` VALUES (3819, '杰普', 'GET', '/user/info', 1622114806129);
INSERT INTO `base_log` VALUES (3820, '杰普', 'GET', '/user/info', 1622114807763);
INSERT INTO `base_log` VALUES (3821, '杰普', 'GET', '/user/info', 1622114810233);
INSERT INTO `base_log` VALUES (3822, '杰普', 'GET', '/user/info', 1622114812735);
INSERT INTO `base_log` VALUES (3823, '杰普', 'GET', '/user/info', 1622114825131);
INSERT INTO `base_log` VALUES (3824, '杰普', 'GET', '/user/info', 1622114827613);
INSERT INTO `base_log` VALUES (3825, '杰普', 'GET', '/user/info', 1622114833901);
INSERT INTO `base_log` VALUES (3826, '杰普', 'GET', '/user/info', 1622114838808);
INSERT INTO `base_log` VALUES (3827, '杰普', 'POST', '/order/submitOrder', 1622114839948);
INSERT INTO `base_log` VALUES (3828, '杰普', 'GET', '/user/info', 1622114840214);
INSERT INTO `base_log` VALUES (3829, '杰普', 'GET', '/user/info', 1622114843439);
INSERT INTO `base_log` VALUES (3830, '杰普', 'GET', '/user/info', 1622114847239);
INSERT INTO `base_log` VALUES (3831, '杰普', 'GET', '/user/info', 1622114849998);
INSERT INTO `base_log` VALUES (3832, '杰普', 'GET', '/user/info', 1622114856063);
INSERT INTO `base_log` VALUES (3833, '杰普', 'POST', '/user/login', 1622114897122);
INSERT INTO `base_log` VALUES (3834, '杰普', 'GET', '/user/info', 1622125437239);
INSERT INTO `base_log` VALUES (3835, '杰普', 'GET', '/user/info', 1622125444679);
INSERT INTO `base_log` VALUES (3836, '杰普', 'GET', '/user/info', 1622125449120);
INSERT INTO `base_log` VALUES (3837, '杰普', 'GET', '/user/info', 1622125455579);
INSERT INTO `base_log` VALUES (3838, '杰普', 'GET', '/user/info', 1622167629649);
INSERT INTO `base_log` VALUES (3839, '杰普', 'GET', '/user/info', 1622169625929);
INSERT INTO `base_log` VALUES (3840, '杰普', 'GET', '/user/info', 1622169648072);
INSERT INTO `base_log` VALUES (3841, '杰普', 'GET', '/user/info', 1622169650810);
INSERT INTO `base_log` VALUES (3842, '杰普', 'GET', '/user/info', 1622169653067);
INSERT INTO `base_log` VALUES (3843, '杰普', 'GET', '/user/info', 1622169657612);
INSERT INTO `base_log` VALUES (3844, '杰普', 'GET', '/user/info', 1622169666627);
INSERT INTO `base_log` VALUES (3845, '杰普', 'POST', '/user/login', 1622169676927);
INSERT INTO `base_log` VALUES (3846, '杰普', 'GET', '/user/info', 1622169677125);
INSERT INTO `base_log` VALUES (3847, '杰普', 'GET', '/user/info', 1622169678456);
INSERT INTO `base_log` VALUES (3848, '杰普', 'GET', '/user/info', 1622169682770);
INSERT INTO `base_log` VALUES (3849, '杰普', 'GET', '/user/info', 1622169722101);
INSERT INTO `base_log` VALUES (3850, '杰普', 'GET', '/user/info', 1622169736614);
INSERT INTO `base_log` VALUES (3851, '杰普', 'GET', '/user/info', 1622169747352);
INSERT INTO `base_log` VALUES (3852, '杰普', 'POST', '/order/submitOrder', 1622169748577);
INSERT INTO `base_log` VALUES (3853, '杰普', 'GET', '/user/info', 1622169749942);
INSERT INTO `base_log` VALUES (3854, '杰普', 'GET', '/user/info', 1622169753112);
INSERT INTO `base_log` VALUES (3855, '杰普', 'GET', '/user/info', 1622169754558);
INSERT INTO `base_log` VALUES (3856, '杰普', 'POST', '/user/login', 1622169754910);
INSERT INTO `base_log` VALUES (3857, '杰普', 'GET', '/user/info', 1622169755175);
INSERT INTO `base_log` VALUES (3858, '杰普', 'GET', '/user/info', 1622169757685);
INSERT INTO `base_log` VALUES (3859, '杰普', 'GET', '/user/info', 1622169770128);
INSERT INTO `base_log` VALUES (3860, '杰普', 'GET', '/user/info', 1622169771809);
INSERT INTO `base_log` VALUES (3861, '杰普', 'GET', '/user/info', 1622169772295);
INSERT INTO `base_log` VALUES (3862, '杰普', 'GET', '/user/info', 1622169773067);
INSERT INTO `base_log` VALUES (3863, '杰普', 'GET', '/user/info', 1622169783013);
INSERT INTO `base_log` VALUES (3864, '杰普', 'GET', '/user/info', 1625569675254);
INSERT INTO `base_log` VALUES (3865, '杰普', 'GET', '/user/info', 1625569820426);
INSERT INTO `base_log` VALUES (3866, '杰普', 'GET', '/user/info', 1625569830002);
INSERT INTO `base_log` VALUES (3867, '杰普', 'GET', '/user/info', 1625569915306);
INSERT INTO `base_log` VALUES (3868, '杰普', 'GET', '/user/info', 1625569917959);
INSERT INTO `base_log` VALUES (3869, '杰普', 'GET', '/user/info', 1625570542402);
INSERT INTO `base_log` VALUES (3870, '杰普', 'GET', '/user/info', 1625571278273);
INSERT INTO `base_log` VALUES (3871, '杰普', 'GET', '/user/info', 1625571287434);
INSERT INTO `base_log` VALUES (3872, '杰普', 'GET', '/user/info', 1625571358257);
INSERT INTO `base_log` VALUES (3873, '杰普', 'GET', '/user/info', 1625571375225);
INSERT INTO `base_log` VALUES (3874, '杰普', 'GET', '/user/info', 1625571392927);
INSERT INTO `base_log` VALUES (3875, '杰普', 'GET', '/user/info', 1625571530566);
INSERT INTO `base_log` VALUES (3876, '杰普', 'GET', '/user/info', 1625571641721);
INSERT INTO `base_log` VALUES (3877, '杰普', 'GET', '/user/info', 1625571658556);
INSERT INTO `base_log` VALUES (3878, '杰普', 'GET', '/user/info', 1625571661633);
INSERT INTO `base_log` VALUES (3879, '杰普', 'GET', '/user/info', 1625571696683);
INSERT INTO `base_log` VALUES (3880, '杰普', 'GET', '/user/info', 1625572647057);
INSERT INTO `base_log` VALUES (3881, '杰普', 'GET', '/user/info', 1625572687789);
INSERT INTO `base_log` VALUES (3882, '杰普', 'POST', '/user/logout', 1625572691746);
INSERT INTO `base_log` VALUES (3883, '杰普', 'GET', '/user/info', 1625572700890);
INSERT INTO `base_log` VALUES (3884, '杰普', 'POST', '/comment/saveOrUpdate', 1625579229658);
INSERT INTO `base_log` VALUES (3885, '杰普', 'GET', '/user/info', 1625799728267);
INSERT INTO `base_log` VALUES (3886, '杰普', 'POST', '/user/logout', 1625799784425);
INSERT INTO `base_log` VALUES (3887, '杰普', 'GET', '/user/info', 1627891190673);
INSERT INTO `base_log` VALUES (3888, '杰普', 'POST', '/user/login', 1627891194001);
INSERT INTO `base_log` VALUES (3889, '杰普', 'POST', '/user/login', 1627891194865);
INSERT INTO `base_log` VALUES (3890, '杰普', 'GET', '/carousel/query', 1627891237313);
INSERT INTO `base_log` VALUES (3891, '杰普', 'GET', '/user/info', 1627891259362);
INSERT INTO `base_log` VALUES (3892, '杰普', 'GET', '/user/info', 1628750245698);
INSERT INTO `base_log` VALUES (3893, '杰普', 'POST', '/baseUser/alterUserface', 1628750296175);
INSERT INTO `base_log` VALUES (3894, '杰普', 'GET', '/user/info', 1628750299456);
INSERT INTO `base_log` VALUES (3895, '杰普', 'GET', '/user/info', 1628750741590);
INSERT INTO `base_log` VALUES (3896, '杰普', 'GET', '/vue-admin-template/table/list', 1628750745161);
INSERT INTO `base_log` VALUES (3897, '杰普', 'GET', '/error', 1628750745193);
INSERT INTO `base_log` VALUES (3898, '杰普', 'GET', '/user/info', 1629185086893);
INSERT INTO `base_log` VALUES (3899, '杰普', 'GET', '/carousel/query', 1629185241657);
INSERT INTO `base_log` VALUES (3900, '杰普', 'GET', '/user/info', 1629355926033);
INSERT INTO `base_log` VALUES (3901, '杰普', 'GET', '/user/info', 1629361184764);
INSERT INTO `base_log` VALUES (3902, '杰普', 'GET', '/user/info', 1629362691924);
INSERT INTO `base_log` VALUES (3903, '杰普', 'GET', '/user/info', 1629421151676);
INSERT INTO `base_log` VALUES (3904, '杰普', 'GET', '/carousel/query', 1629421182992);
INSERT INTO `base_log` VALUES (3905, '杰普', 'GET', '/carousel/query', 1629421196829);
INSERT INTO `base_log` VALUES (3906, '杰普', 'GET', '/user/info', 1629421766332);
INSERT INTO `base_log` VALUES (3907, '杰普', 'GET', '/vue-admin-template/table/list', 1629421771685);
INSERT INTO `base_log` VALUES (3908, '杰普', 'GET', '/error', 1629421771730);
INSERT INTO `base_log` VALUES (3909, '杰普', 'GET', '/user/info', 1630485595557);
INSERT INTO `base_log` VALUES (3910, '杰普', 'GET', '/carousel/query', 1630493337206);
INSERT INTO `base_log` VALUES (3911, '杰普', 'GET', '/carousel/query', 1630493355728);
INSERT INTO `base_log` VALUES (3912, '杰普', 'GET', '/carousel/query', 1630493361166);
INSERT INTO `base_log` VALUES (3913, '杰普', 'GET', '/user/info', 1630494334638);
INSERT INTO `base_log` VALUES (3914, '杰普', 'POST', '/user/logout', 1630494345486);
INSERT INTO `base_log` VALUES (3915, '杰普', 'GET', '/user/info', 1631618499215);
INSERT INTO `base_log` VALUES (3916, '杰普', 'GET', '/user/info', 1631775444136);
INSERT INTO `base_log` VALUES (3917, '杰普', 'POST', '/baseUser/saveOrUpdate', 1631775493614);
INSERT INTO `base_log` VALUES (3918, 'admin', 'GET', '/user/info', 1631775501084);
INSERT INTO `base_log` VALUES (3919, 'admin', 'GET', '/user/info', 1631879797594);
INSERT INTO `base_log` VALUES (3920, 'admin', 'GET', '/carousel/query', 1631879810432);
INSERT INTO `base_log` VALUES (3921, 'admin', 'GET', '/carousel/query', 1631879832781);
INSERT INTO `base_log` VALUES (3922, 'admin', 'GET', '/user/info', 1631947981199);
INSERT INTO `base_log` VALUES (3923, 'admin', 'GET', '/user/info', 1632283698905);
INSERT INTO `base_log` VALUES (3924, 'admin', 'GET', '/user/info', 1632284571768);
INSERT INTO `base_log` VALUES (3925, 'admin', 'GET', '/user/info', 1632311051139);
INSERT INTO `base_log` VALUES (3926, 'admin', 'POST', '/user/logout', 1632311057683);
INSERT INTO `base_log` VALUES (3927, 'admin', 'GET', '/user/info', 1632663878947);
INSERT INTO `base_log` VALUES (3928, 'admin', 'GET', '/user/info', 1632663915839);
INSERT INTO `base_log` VALUES (3929, 'admin', 'GET', '/user/info', 1632882991364);
INSERT INTO `base_log` VALUES (3930, 'admin', 'GET', '/user/info', 1633943529708);
INSERT INTO `base_log` VALUES (3931, 'admin', 'GET', '/user/info', 1633943698356);
INSERT INTO `base_log` VALUES (3932, 'admin', 'GET', '/user/info', 1635150242993);
INSERT INTO `base_log` VALUES (3933, 'admin', 'GET', '/user/info', 1635150406199);
INSERT INTO `base_log` VALUES (3934, 'admin', 'GET', '/carousel/query', 1635150410614);
INSERT INTO `base_log` VALUES (3935, 'admin', 'GET', '/carousel/query', 1635150642634);
INSERT INTO `base_log` VALUES (3936, 'admin', 'GET', '/user/info', 1635230418197);
INSERT INTO `base_log` VALUES (3937, 'admin', 'GET', '/user/info', 1635230537615);
INSERT INTO `base_log` VALUES (3938, 'admin', 'GET', '/role/cascadePrivilegeFindAll', 1635230637331);
INSERT INTO `base_log` VALUES (3939, 'admin', 'GET', '/role/cascadePrivilegeFindAll', 1635230646948);
INSERT INTO `base_log` VALUES (3940, 'admin', 'GET', '/user/info', 1635231707808);
INSERT INTO `base_log` VALUES (3941, 'admin', 'GET', '/user/info', 1635304776231);
INSERT INTO `base_log` VALUES (3942, 'admin', 'GET', '/vue-admin-template/table/list', 1635304779087);
INSERT INTO `base_log` VALUES (3943, 'admin', 'GET', '/error', 1635304779290);
INSERT INTO `base_log` VALUES (3944, 'admin', 'GET', '/vue-admin-template/table/list', 1635304788171);
INSERT INTO `base_log` VALUES (3945, 'admin', 'GET', '/error', 1635304788276);
INSERT INTO `base_log` VALUES (3946, 'admin', 'GET', '/user/info', 1635314549972);
INSERT INTO `base_log` VALUES (3947, 'admin', 'GET', '/user/info', 1635314607084);
INSERT INTO `base_log` VALUES (3948, 'admin', 'GET', '/carousel/query', 1635314621723);
INSERT INTO `base_log` VALUES (3949, 'admin', 'POST', '/carousel/saveOrUpdate', 1635314648562);
INSERT INTO `base_log` VALUES (3950, 'admin', 'GET', '/carousel/query', 1635314648659);
INSERT INTO `base_log` VALUES (3951, 'admin', 'POST', '/carousel/saveOrUpdate', 1635314651886);
INSERT INTO `base_log` VALUES (3952, 'admin', 'GET', '/carousel/query', 1635314651957);
INSERT INTO `base_log` VALUES (3953, 'admin', 'GET', '/user/info', 1635402949647);
INSERT INTO `base_log` VALUES (3954, 'admin', 'GET', '/carousel/query', 1635402958209);
INSERT INTO `base_log` VALUES (3955, 'admin', 'GET', '/user/info', 1635746659786);
INSERT INTO `base_log` VALUES (3956, 'admin', 'GET', '/carousel/query', 1635746666989);
INSERT INTO `base_log` VALUES (3957, 'admin', 'GET', '/carousel/query', 1635748089392);
INSERT INTO `base_log` VALUES (3958, 'admin', 'GET', '/user/info', 1636255919253);
INSERT INTO `base_log` VALUES (3959, 'admin', 'GET', '/user/info', 1637834055543);
INSERT INTO `base_log` VALUES (3960, 'admin', 'GET', '/user/info', 1638546102828);
INSERT INTO `base_log` VALUES (3961, 'admin', 'POST', '/article/changeStatus', 1638546310492);
INSERT INTO `base_log` VALUES (3962, 'admin', 'POST', '/article/changeStatus', 1638546311694);
INSERT INTO `base_log` VALUES (3963, 'admin', 'GET', '/user/info', 1638548710001);
INSERT INTO `base_log` VALUES (3964, 'admin', 'GET', '/user/info', 1638549696098);
INSERT INTO `base_log` VALUES (3965, 'admin', 'GET', '/user/info', 1638583167356);
INSERT INTO `base_log` VALUES (3966, 'admin', 'GET', '/user/info', 1638583227002);
INSERT INTO `base_log` VALUES (3967, 'admin', 'GET', '/carousel/query', 1638583278783);
INSERT INTO `base_log` VALUES (3968, 'admin', 'GET', '/user/info', 1638586893003);
INSERT INTO `base_log` VALUES (3969, 'admin', 'GET', '/carousel/query', 1638587009038);
INSERT INTO `base_log` VALUES (3970, 'admin', 'GET', '/user/info', 1638587229583);
INSERT INTO `base_log` VALUES (3971, 'admin', 'GET', '/user/info', 1638587274358);
INSERT INTO `base_log` VALUES (3972, 'admin', 'GET', '/carousel/query', 1638587288178);
INSERT INTO `base_log` VALUES (3973, 'admin', 'GET', '/user/info', 1638587313355);
INSERT INTO `base_log` VALUES (3974, 'admin', 'GET', '/carousel/query', 1638587582036);
INSERT INTO `base_log` VALUES (3975, 'admin', 'POST', '/product/saveOrUpdate', 1638587630508);
INSERT INTO `base_log` VALUES (3976, 'admin', 'GET', '/order/sendOrder', 1638587663463);
INSERT INTO `base_log` VALUES (3977, 'admin', 'GET', '/order/sendOrder', 1638587665937);
INSERT INTO `base_log` VALUES (3978, 'admin', 'GET', '/order/cancelSendOrder', 1638587670692);
INSERT INTO `base_log` VALUES (3979, 'admin', 'POST', '/baseUser/saveOrUpdate', 1638587831023);
INSERT INTO `base_log` VALUES (3980, 'admin', 'POST', '/category/saveOrUpdate', 1638587883543);
INSERT INTO `base_log` VALUES (3981, 'admin', 'GET', '/role/cascadePrivilegeFindAll', 1638587920950);
INSERT INTO `base_log` VALUES (3982, 'admin', 'POST', '/privilege/saveOrUpdate', 1638587945060);
INSERT INTO `base_log` VALUES (3983, 'admin', 'GET', '/carousel/query', 1638587984583);
INSERT INTO `base_log` VALUES (3984, 'admin', 'POST', '/carousel/saveOrUpdate', 1638588013237);
INSERT INTO `base_log` VALUES (3985, 'admin', 'GET', '/carousel/query', 1638588013281);
INSERT INTO `base_log` VALUES (3986, 'admin', 'GET', '/carousel/query', 1638588149760);
INSERT INTO `base_log` VALUES (3987, 'admin', 'GET', '/user/info', 1640939541502);
INSERT INTO `base_log` VALUES (3988, 'admin', 'GET', '/vue-admin-template/table/list', 1640939548378);
INSERT INTO `base_log` VALUES (3989, 'admin', 'GET', '/error', 1640939548437);
INSERT INTO `base_log` VALUES (3990, 'admin', 'GET', '/vue-admin-template/table/list', 1640939554886);
INSERT INTO `base_log` VALUES (3991, 'admin', 'GET', '/error', 1640939554896);
INSERT INTO `base_log` VALUES (3992, 'admin', 'POST', '/user/logout', 1640939557971);
INSERT INTO `base_log` VALUES (3993, 'admin', 'GET', '/user/info', 1642045414446);
INSERT INTO `base_log` VALUES (3994, 'admin', 'GET', '/role/cascadePrivilegeFindAll', 1642045457133);
INSERT INTO `base_log` VALUES (3995, 'admin', 'GET', '/role/cascadePrivilegeFindAll', 1642045553173);
INSERT INTO `base_log` VALUES (3996, 'admin', 'GET', '/carousel/query', 1642045555287);
INSERT INTO `base_log` VALUES (3997, 'admin', 'GET', '/carousel/query', 1642045591000);
INSERT INTO `base_log` VALUES (3998, 'admin', 'POST', '/carousel/saveOrUpdate', 1642045606881);
INSERT INTO `base_log` VALUES (3999, 'admin', 'GET', '/carousel/query', 1642045606906);
INSERT INTO `base_log` VALUES (4000, 'admin', 'POST', '/user/logout', 1642045740660);
INSERT INTO `base_log` VALUES (4001, 'admin', 'GET', '/user/info', 1642045801746);
INSERT INTO `base_log` VALUES (4002, 'admin', 'GET', '/vue-admin-template/table/list', 1642045806032);
INSERT INTO `base_log` VALUES (4003, 'admin', 'GET', '/error', 1642045806083);
INSERT INTO `base_log` VALUES (4004, 'admin', 'GET', '/user/info', 1642211387073);
INSERT INTO `base_log` VALUES (4005, 'admin', 'POST', '/user/logout', 1642211392741);
INSERT INTO `base_log` VALUES (4006, 'admin', 'GET', '/user/info', 1642227990406);
INSERT INTO `base_log` VALUES (4007, 'admin', 'GET', '/vue-admin-template/table/list', 1642227994315);
INSERT INTO `base_log` VALUES (4008, 'admin', 'GET', '/error', 1642227994339);
INSERT INTO `base_log` VALUES (4009, 'admin', 'GET', '/vue-admin-template/table/list', 1642228003370);
INSERT INTO `base_log` VALUES (4010, 'admin', 'GET', '/error', 1642228003376);
INSERT INTO `base_log` VALUES (4011, 'admin', 'GET', '/user/info', 1642233417942);

-- ----------------------------
-- Table structure for base_privilege
-- ----------------------------
DROP TABLE IF EXISTS `base_privilege`;
CREATE TABLE `base_privilege`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `route` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `route_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hidden` int(255) NULL DEFAULT NULL,
  `num` int(11) NULL DEFAULT NULL,
  `parent_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_privilege_privilege`(`parent_id`) USING BTREE,
  CONSTRAINT `fk_privilege_privilege` FOREIGN KEY (`parent_id`) REFERENCES `base_privilege` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 160 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_privilege
-- ----------------------------
INSERT INTO `base_privilege` VALUES (87, '资讯分类', '', '/info/category/List', '', 'menu', '', 0, 20, 123);
INSERT INTO `base_privilege` VALUES (89, '网站资讯', '', '/admin/info', '', 'menu', 'icon_1', 0, 30, NULL);
INSERT INTO `base_privilege` VALUES (90, '资讯信息', '', '/admin/info/article/List', '', 'menu', '', 0, NULL, 89);
INSERT INTO `base_privilege` VALUES (97, '资讯详情', '', '/admin/info/article/Details', '', 'menu', '', 1, NULL, 89);
INSERT INTO `base_privilege` VALUES (112, '个人信息', '', '/owner/index', '', 'menu', 'icon_4', 0, 0, NULL);
INSERT INTO `base_privilege` VALUES (113, '个人信息', '', '/owner/index', '', 'menu', '', 0, NULL, 112);
INSERT INTO `base_privilege` VALUES (115, '发布资讯', '', '/admin/info/article/Publish', '', 'menu', '', 1, NULL, 89);
INSERT INTO `base_privilege` VALUES (120, '顾客管理', '', '/admin/custome', '', 'menu', 'icon_3', 0, 40, NULL);
INSERT INTO `base_privilege` VALUES (121, '顾客管理', NULL, '/admin/custome/List', NULL, 'menu', NULL, 0, NULL, 120);
INSERT INTO `base_privilege` VALUES (122, '顾客详情', '', '/admin/custome/Details', '', 'menu', '', 1, NULL, 120);
INSERT INTO `base_privilege` VALUES (123, '资讯详情', '', '/info/article/Details', '', 'menu', 'icon_3', 0, 30, 123);
INSERT INTO `base_privilege` VALUES (124, '资讯信息', NULL, '/info/article/List', NULL, 'menu', NULL, 0, NULL, 123);
INSERT INTO `base_privilege` VALUES (125, '资讯分类', '', '/admin/info/category/List', '', 'menu', '', 0, 16, 89);
INSERT INTO `base_privilege` VALUES (126, '员工管理', '', '/admin/waiter', '', 'menu', 'icon_4', 0, 40, NULL);
INSERT INTO `base_privilege` VALUES (127, '员工管理', NULL, '/admin/waiter/List', NULL, 'menu', NULL, 0, NULL, 126);
INSERT INTO `base_privilege` VALUES (128, '员工详情', NULL, '/admin/waiter/Details', NULL, 'menu', NULL, 1, NULL, 126);
INSERT INTO `base_privilege` VALUES (129, '栏目管理', '', '/admin/category', '', 'menu', 'icon_6', 0, 70, NULL);
INSERT INTO `base_privilege` VALUES (130, '栏目管理', NULL, '/admin/category/List', NULL, 'menu', NULL, 0, NULL, 129);
INSERT INTO `base_privilege` VALUES (131, '产品管理', '', '/admin/product', '', 'menu', 'icon_16', 0, 60, NULL);
INSERT INTO `base_privilege` VALUES (132, '产品管理', NULL, '/admin/product/List', NULL, 'menu', NULL, 0, NULL, 131);
INSERT INTO `base_privilege` VALUES (133, '轮播管理', '', '/admin/carousel', '', 'menu', 'icon_14', 0, 20, NULL);
INSERT INTO `base_privilege` VALUES (134, '轮播管理', NULL, '/admin/carousel/List', NULL, 'menu', NULL, 0, NULL, 133);
INSERT INTO `base_privilege` VALUES (135, '角色权限', '', '/admin/sys', '', 'menu', 'icon_3', 0, 24, NULL);
INSERT INTO `base_privilege` VALUES (136, '用户管理', NULL, '/admin/sys/user/List', NULL, 'menu', NULL, 0, NULL, 135);
INSERT INTO `base_privilege` VALUES (137, '用户详情', '', '/admin/sys/user/Details', '', 'menu', '', 1, NULL, 135);
INSERT INTO `base_privilege` VALUES (138, '角色管理', NULL, '/admin/sys/role/List', NULL, 'menu', NULL, 0, NULL, 135);
INSERT INTO `base_privilege` VALUES (139, '权限管理', NULL, '/admin/sys/privilege/List', NULL, 'menu', NULL, 0, NULL, 135);
INSERT INTO `base_privilege` VALUES (142, '日志管理', NULL, '/admin/log', NULL, 'menu', 'icon_17', 0, 20, NULL);
INSERT INTO `base_privilege` VALUES (143, '日志管理', '', '/admin/log/List', '', 'menu', '', 0, 20, 142);
INSERT INTO `base_privilege` VALUES (144, '账单管理', '', '/admin/account', '', 'menu', 'icon_14', 0, 50, NULL);
INSERT INTO `base_privilege` VALUES (145, '顾客账单', NULL, '/admin/account/Custome', NULL, 'menu', NULL, 0, NULL, 144);
INSERT INTO `base_privilege` VALUES (146, '员工账单', NULL, '/admin/account/Employee', NULL, 'menu', NULL, 0, NULL, 144);
INSERT INTO `base_privilege` VALUES (147, '系统账单', NULL, '/admin/account/System', NULL, 'menu', NULL, 0, NULL, 144);
INSERT INTO `base_privilege` VALUES (150, '订单管理', '', '/admin/order', '', 'menu', 'icon_10', 0, 50, 131);
INSERT INTO `base_privilege` VALUES (151, '订单管理', NULL, '/admin/order/List', NULL, 'menu', NULL, 0, NULL, 150);
INSERT INTO `base_privilege` VALUES (152, '订单详情', '', '/admin/order/Details', '', 'menu', '', 1, NULL, 150);
INSERT INTO `base_privilege` VALUES (153, '审核大厅', '', '/admin/check', '', 'menu', 'icon_13', 0, 50, NULL);
INSERT INTO `base_privilege` VALUES (154, '实名认证', '', '/admin/check/Certification', '', 'menu', '', 0, NULL, 153);
INSERT INTO `base_privilege` VALUES (155, '账户变更', NULL, '/admin/check/AccountApply', NULL, 'menu', NULL, 0, NULL, 153);
INSERT INTO `base_privilege` VALUES (156, '资讯评论', NULL, '/admin/check/InfoComment', NULL, 'menu', NULL, 0, NULL, 153);
INSERT INTO `base_privilege` VALUES (157, '订单评论', NULL, '/admin/check/OrderComment', NULL, 'menu', NULL, 0, NULL, 153);
INSERT INTO `base_privilege` VALUES (158, '系统配置', '', '/admin/baseConfig', '', 'menu', 'icon_10', 0, 20, NULL);
INSERT INTO `base_privilege` VALUES (159, '系统配置', NULL, '/admin/baseConfig/List', NULL, 'menu', NULL, 0, NULL, 158);

-- ----------------------------
-- Table structure for base_role
-- ----------------------------
DROP TABLE IF EXISTS `base_role`;
CREATE TABLE `base_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_role
-- ----------------------------
INSERT INTO `base_role` VALUES (1, '员工');
INSERT INTO `base_role` VALUES (2, '管理员');
INSERT INTO `base_role` VALUES (6, '顾客');

-- ----------------------------
-- Table structure for base_role_privilege
-- ----------------------------
DROP TABLE IF EXISTS `base_role_privilege`;
CREATE TABLE `base_role_privilege`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NULL DEFAULT NULL,
  `privilege_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_role_privilege_role`(`role_id`) USING BTREE,
  INDEX `fk_role_privilege_privilege`(`privilege_id`) USING BTREE,
  CONSTRAINT `fk_role_privilege_privilege` FOREIGN KEY (`privilege_id`) REFERENCES `base_privilege` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `fk_role_privilege_role` FOREIGN KEY (`role_id`) REFERENCES `base_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 332 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_role_privilege
-- ----------------------------
INSERT INTO `base_role_privilege` VALUES (241, 2, 89);
INSERT INTO `base_role_privilege` VALUES (242, 2, 90);
INSERT INTO `base_role_privilege` VALUES (249, 2, 97);
INSERT INTO `base_role_privilege` VALUES (263, 1, 112);
INSERT INTO `base_role_privilege` VALUES (264, 1, 113);
INSERT INTO `base_role_privilege` VALUES (265, 6, 112);
INSERT INTO `base_role_privilege` VALUES (266, 6, 113);
INSERT INTO `base_role_privilege` VALUES (267, 2, 112);
INSERT INTO `base_role_privilege` VALUES (268, 2, 113);
INSERT INTO `base_role_privilege` VALUES (278, 2, 115);
INSERT INTO `base_role_privilege` VALUES (284, 2, 120);
INSERT INTO `base_role_privilege` VALUES (285, 2, 121);
INSERT INTO `base_role_privilege` VALUES (286, 2, 122);
INSERT INTO `base_role_privilege` VALUES (287, 2, 125);
INSERT INTO `base_role_privilege` VALUES (290, 6, 89);
INSERT INTO `base_role_privilege` VALUES (291, 6, 90);
INSERT INTO `base_role_privilege` VALUES (292, 6, 97);
INSERT INTO `base_role_privilege` VALUES (293, 6, 115);
INSERT INTO `base_role_privilege` VALUES (298, 2, 126);
INSERT INTO `base_role_privilege` VALUES (299, 2, 127);
INSERT INTO `base_role_privilege` VALUES (300, 2, 128);
INSERT INTO `base_role_privilege` VALUES (301, 2, 129);
INSERT INTO `base_role_privilege` VALUES (302, 2, 130);
INSERT INTO `base_role_privilege` VALUES (303, 2, 131);
INSERT INTO `base_role_privilege` VALUES (304, 2, 132);
INSERT INTO `base_role_privilege` VALUES (305, 2, 133);
INSERT INTO `base_role_privilege` VALUES (306, 2, 134);
INSERT INTO `base_role_privilege` VALUES (307, 2, 135);
INSERT INTO `base_role_privilege` VALUES (308, 2, 136);
INSERT INTO `base_role_privilege` VALUES (309, 2, 137);
INSERT INTO `base_role_privilege` VALUES (310, 2, 138);
INSERT INTO `base_role_privilege` VALUES (311, 2, 139);
INSERT INTO `base_role_privilege` VALUES (316, 2, 142);
INSERT INTO `base_role_privilege` VALUES (317, 2, 143);
INSERT INTO `base_role_privilege` VALUES (318, 2, 144);
INSERT INTO `base_role_privilege` VALUES (319, 2, 145);
INSERT INTO `base_role_privilege` VALUES (320, 2, 146);
INSERT INTO `base_role_privilege` VALUES (321, 2, 147);
INSERT INTO `base_role_privilege` VALUES (322, 2, 150);
INSERT INTO `base_role_privilege` VALUES (323, 2, 151);
INSERT INTO `base_role_privilege` VALUES (324, 2, 152);
INSERT INTO `base_role_privilege` VALUES (325, 2, 153);
INSERT INTO `base_role_privilege` VALUES (326, 2, 154);
INSERT INTO `base_role_privilege` VALUES (327, 2, 155);
INSERT INTO `base_role_privilege` VALUES (328, 2, 156);
INSERT INTO `base_role_privilege` VALUES (329, 2, 157);
INSERT INTO `base_role_privilege` VALUES (330, 2, 158);
INSERT INTO `base_role_privilege` VALUES (331, 2, 159);

-- ----------------------------
-- Table structure for base_user
-- ----------------------------
DROP TABLE IF EXISTS `base_user`;
CREATE TABLE `base_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `telephone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `realname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `gender` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `birth` bigint(255) NULL DEFAULT NULL COMMENT '生日',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'email',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  `user_face` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像',
  `register_time` bigint(20) NULL DEFAULT NULL COMMENT '注册时间',
  `nation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '民族',
  `id_card` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `idcard_photo_positive` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证正面',
  `idcard_photo_negative` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证反面',
  `bank_card` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '银行卡号',
  `bank_card_photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '银行卡照片',
  `balance` double NULL DEFAULT NULL COMMENT '账户余额',
  `certification_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实名制状态',
  `certification_time` bigint(20) NULL DEFAULT NULL COMMENT '实名制时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 128 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_user
-- ----------------------------
INSERT INTO `base_user` VALUES (25, 'admin1', '123321', '15512348116', 'admin', 'male', 1065542400000, '1346678124@qq.com', '正常', 'http://121.199.29.84:8888/group1/M00/00/69/rBD-SWEUwdaAOroHAABPDW0-QVc268.gif', 1603443074531, '汉', '', 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCiM8OAHYA_AAEYdVeZwgs344.jpg', '', '', 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCiM86AKjJRAAC_TgKpeIw508.jpg', 5202.5, '已实名认证', 1622006086588);
INSERT INTO `base_user` VALUES (26, 'admin2', '123321', '15512348117', '胡晨', 'male', 876844800000, NULL, '正常', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-Td6-Aawn0AACq962TS9c719.jpg', 1603443074531, '汉', '51221199403145507', NULL, NULL, NULL, NULL, 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (81, 'employee1', '123321', '18812344321', '刘敏', 'male', 812822400000, '', '正常', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-Td6-Aawn0AACq962TS9c719.jpg', 1603443074531, '汉', '', NULL, NULL, NULL, NULL, 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (82, 'employee2', '123321', '18812344456', '张涛', 'male', 876844800000, '', '正常', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-Td6-Aawn0AACq962TS9c719.jpg', 1603443150088, '汉', '', NULL, NULL, NULL, NULL, 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (84, 'custome1', '123321', '18812344388', '张晓明', 'male', 876844800000, '', '正常', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-Td6-Aawn0AACq962TS9c719.jpg', 1603466902315, '汉', '', '', '', '', '', 638, '未认证', NULL);
INSERT INTO `base_user` VALUES (85, 'custome2', '123321', '18898231111', '王猛', 'male', 876844800000, '', '正常', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-Td6-Aawn0AACq962TS9c719.jpg', 1603466928033, '汉', '', NULL, NULL, NULL, NULL, 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (86, 'terry', '123321', '18812340092', '张铭', 'male', 1160409600000, NULL, '正常', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-Td6-Aawn0AACq962TS9c719.jpg', 1603587819899, '汉', NULL, NULL, NULL, NULL, NULL, 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (87, 'employee3', '123321', '18812344311', '王萌', 'female', 1065542400000, '', '正常', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-U076AMtEEAAEWA_4oI3k301.jpg', 1603588232687, '汉', '', NULL, NULL, NULL, NULL, 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (88, 'custome4', '123321', '18812349902', '顾客4', 'male', 1065542400000, '', '正常', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-VUgaAHORYAAEWA_4oI3k291.jpg', 1603621292766, '汉', '', NULL, NULL, NULL, NULL, 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (89, 'custome6', '123321', '17712345321', '张小龙', 'female', 1065542400000, '', '正常', 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCvCBGAb1-JAABJwfdNjls720.jpg', 1603624287362, '汉', '', '', '', '', '', 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (90, 'e1', '123321', '18876321342', '刘亮', 'male', 1128441600000, '', '禁用', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-Td6-Aawn0AACq962TS9c719.jpg', 1603624573317, '汉', '', '', '', '', '', 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (91, 'ronda', '123321', '13789976789', '刘亚蓉', '0', 1065542400000, '', '正常', 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCvBg-AMQtLAAx1nBeU9jo179.png', 1620636050350, '汉', '', '', '', '', '', 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (112, 'briup', 'briup', '13734105011', '杰普', 'male', 1065542400000, NULL, '正常', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-Td6-Aawn0AACq962TS9c719.jpg', 1620792524515, '汉', NULL, NULL, NULL, NULL, NULL, 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (114, 'test2', 'test2', 'test2', 'test2', 'female', NULL, '', '正常', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-Td6-Aawn0AACq962TS9c719.jpg', 1620875054162, '汉', '', '', '', '', '', 608, '未认证', NULL);
INSERT INTO `base_user` VALUES (117, 'curry', '123321', '188878149218', 'jj', 'female', NULL, '', '正常', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-Td6-Aawn0AACq962TS9c719.jpg', 1620960957300, '汉', '', '', '', '', '', 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (120, 'test123', '123', NULL, NULL, NULL, NULL, NULL, '正常', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-Td6-Aawn0AACq962TS9c719.jpg', 1620961157588, '汉', NULL, NULL, NULL, NULL, NULL, 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (121, 'test1', '123321', NULL, NULL, NULL, NULL, NULL, '正常', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-Td6-Aawn0AACq962TS9c719.jpg', 1620961204759, '汉', NULL, NULL, NULL, NULL, NULL, 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (123, 'test3', '123321', NULL, NULL, NULL, NULL, NULL, '正常', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-Td6-Aawn0AACq962TS9c719.jpg', 1620961717217, '汉', NULL, NULL, NULL, NULL, NULL, 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (125, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '正常', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-Td6-Aawn0AACq962TS9c719.jpg', 1620961961807, '汉', NULL, NULL, NULL, NULL, NULL, 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (126, 'curryg', '123321', NULL, NULL, NULL, NULL, NULL, '正常', 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCvCCSAWYb8AAG_cvBJFec36.jpeg', 1620962015618, '汉', NULL, NULL, NULL, NULL, NULL, 0, '未认证', NULL);
INSERT INTO `base_user` VALUES (127, 'ttt', '123', NULL, NULL, NULL, NULL, NULL, '正常', 'http://121.199.29.84:8888/group1/M00/00/12/rBD-SV-Td6-Aawn0AACq962TS9c719.jpg', 1620963839456, '汉', NULL, NULL, NULL, NULL, NULL, 0, '未认证', NULL);

-- ----------------------------
-- Table structure for base_user_role
-- ----------------------------
DROP TABLE IF EXISTS `base_user_role`;
CREATE TABLE `base_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL,
  `role_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_user_role_user`(`user_id`) USING BTREE,
  INDEX `fk_user_role_role`(`role_id`) USING BTREE,
  CONSTRAINT `fk_user_role_role` FOREIGN KEY (`role_id`) REFERENCES `base_role` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `fk_user_role_user` FOREIGN KEY (`user_id`) REFERENCES `base_user` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 149 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_user_role
-- ----------------------------
INSERT INTO `base_user_role` VALUES (37, 25, 2);
INSERT INTO `base_user_role` VALUES (38, 26, 2);
INSERT INTO `base_user_role` VALUES (80, 81, 1);
INSERT INTO `base_user_role` VALUES (81, 82, 1);
INSERT INTO `base_user_role` VALUES (82, 84, 6);
INSERT INTO `base_user_role` VALUES (87, 89, 6);
INSERT INTO `base_user_role` VALUES (124, 90, 1);
INSERT INTO `base_user_role` VALUES (127, 112, 2);
INSERT INTO `base_user_role` VALUES (128, 91, 2);
INSERT INTO `base_user_role` VALUES (129, 88, 6);
INSERT INTO `base_user_role` VALUES (131, 87, 1);
INSERT INTO `base_user_role` VALUES (132, 86, 2);
INSERT INTO `base_user_role` VALUES (133, 85, 6);
INSERT INTO `base_user_role` VALUES (135, 114, 6);
INSERT INTO `base_user_role` VALUES (138, 117, 6);
INSERT INTO `base_user_role` VALUES (141, 120, 6);
INSERT INTO `base_user_role` VALUES (142, 121, 6);
INSERT INTO `base_user_role` VALUES (144, 123, 1);
INSERT INTO `base_user_role` VALUES (146, 125, 1);
INSERT INTO `base_user_role` VALUES (147, 126, 1);
INSERT INTO `base_user_role` VALUES (148, 127, 6);

-- ----------------------------
-- Table structure for cms_article
-- ----------------------------
DROP TABLE IF EXISTS `cms_article`;
CREATE TABLE `cms_article`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `publish_time` bigint(20) NULL DEFAULT NULL,
  `read_times` bigint(20) NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thump_up` bigint(255) NULL DEFAULT NULL,
  `cover` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `project_id` bigint(20) NULL DEFAULT NULL,
  `author_id` bigint(20) NULL DEFAULT NULL,
  `category_id` bigint(20) NULL DEFAULT NULL,
  `compony_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_article_category`(`category_id`) USING BTREE,
  INDEX `fk_article_user`(`author_id`) USING BTREE,
  INDEX `photo_id`(`cover`) USING BTREE,
  INDEX `project_id`(`project_id`) USING BTREE,
  INDEX `compony_id`(`compony_id`) USING BTREE,
  CONSTRAINT `fk_article_category` FOREIGN KEY (`category_id`) REFERENCES `cms_category` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `fk_article_user` FOREIGN KEY (`author_id`) REFERENCES `base_user` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = InnoDB AUTO_INCREMENT = 93 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cms_article
-- ----------------------------
INSERT INTO `cms_article` VALUES (36, '【通知】教育部等11部门关于推进中小学生研学旅行的意见 教基一[2016]8号', '<p>　　近日，教育部等11部门印发了《关于推进中小学生研学旅行的意见》（以下简称《意见》），要求各地将研学旅行摆在更加重要的位置，推动1研学旅行健康快速发展。</p>\'<p>　　近日，教育部等11部门印发了《关于推进中小学生研学旅行的意见》（以下简称《意见》），要求各地将研学旅行摆在更加重要的位置，推动研学旅行健康快速发展。</p> <p>　　《意见》指出，中小学生研学旅行是由教育部门和学校有计划地组织安排，通过集体旅行、集中食宿方式开展的研究性学习和旅行体验相结合的校外教育活动。开展研学旅行，有利于促进学生培育和践行社会主义核心价值观，激发学生对党、对国家、对人民的热爱之情；有利于推动全面实施素质教育，促进书本知识和生活经验的深度融合；有利于满足学生日益增长的旅游需求，从小培养学生文明旅游意识。</p> <p>　　《意见》提出，要将研学旅行纳入中小学教育教学计划。各中小学要结合当地实际，把研学旅行纳入学校教育教学计划，与综合实践活动课程统筹考虑，促进研学旅行和学校课程有机融合。学校要根据教育教学计划灵活安排研学旅行时间，一般安排在小学四到六年级、初中一到二年级、高中一到二年级，并根据学段特点和地域特色，逐步建立小学阶段以乡土乡情为主、初中阶段以县情市情为主、高中阶段以省情国情为主的研学旅行活动课程体系。</p> <p>　　《意见》强调，要加强研学旅行基地建设。各地要根据研学旅行育人目标，依托自然和文化遗产资源，红色教育资源和综合实践基地等，建设一批安全适宜的中小学生研学旅行基地，并探索建立基地的准入标准、退出机制和评价体系。打造一批示范性研学旅行精品线路，形成布局合理、互联互通的研学旅行网络。各基地要将研学旅行作为重要的教育载体，根据小学、初中、高中不同学段的研学旅行目标，有针对性地开发多种类型的活动课程。</p> <p>　　《意见》要求，各地要规范研学旅行组织管理。各地教育行政部门和中小学要探索制定中小学生研学旅行工作规程，做到“活动有方案，行前有备案，应急有预案”。学校组织开展研学旅行可采取自行开展或委托开展的形式，但须按管理权限报教育行政部门备案，并做好学生活动管理和安全保障工作。学校自行开展研学旅行，要与家长签订协议书，明确学校、家长、学生的责任权利；学校采取委托开展研学旅行，要选择有资质、信誉好的企业合作，并与企业签订协议书，明确委托企业或机构承担学生研学旅行安全责任。</p> <p>　　《意见》还对研学旅行工作组织领导、经费保障、安全保障、督查评价、宣传引导等方面提出了明确要求。</p>', 1590150600197, 135, '推荐', 32, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV875N2AIYFSAAXEWQYzwG4810.jpg', 4, NULL, 5, 5);
INSERT INTO `cms_article` VALUES (57, '法国夏令营开始报名了', '<div id=\"u827\" class=\"ax_default label\">\n<div id=\"u827_text\" class=\"text \">\n<p>通过夏令营的学习，期望可以为孩子们打开一扇中国传统艺术的窗口，同时 为他们开启剪纸、灯彩、扎染、面塑等传统非物质文化</p>\n<p>艺术的殿堂，并且能够从 生活中发现传统艺术的点滴之美，最后还能学以致用，将自己所学到的技能用以 美化生活，具备创新设</p>\n<p>计的思维，将我们的传统艺术更好的传承下去。</p>\n</div>\n</div>\n<div id=\"u828\" class=\"ax_default image\"><img id=\"u828_img\" class=\"img \" src=\"http://39.106.16.56/huangyy/yxtx/images/文章/u828.png\" /></div>\n<div id=\"u829\" class=\"ax_default label\">\n<div id=\"u829_div\" class=\"\">&nbsp;</div>\n<div id=\"u829_text\" class=\"text \">\n<p>学生通过一周学习，能够知晓我国传统文化&ldquo;剪纸艺术＋上海灯彩&rdquo;的来龙 去脉，并能够感知剪纸、上海灯彩艺术独特的美学价值</p>\n<p>，借此表达丰富的视觉认 知与情感态度，同时能够循序渐进地锻炼手、脑、心的协调能力。在剪纸技巧上，能够剪出圆形纹、</p>\n<p>月牙形纹、锯齿纹、传统云纹、水滴纹等简单纹样，并能够剪出传统双喜字和上海窗花。 在扎灯技巧上，能够学会平接法、十字</p>\n<p>交叉法、勾合法、柱体骨架法等方法，并能够综合运用以上方法扎出传统兔子灯。</p>\n</div>\n</div>', 1598336993894, 0, '审核不通过', 0, 'http://121.199.29.84:8888/group1/M00/00/07/rBD-SV9Er92AaCQaAAAk47UyTaU076.png', 4, NULL, 4, 5);
INSERT INTO `cms_article` VALUES (66, '【公告】山西省少先队员“红领巾研学行”正式启动', '<p align=\"center\"><img style=\"border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px; border-left-width: 0px\" alt=\"\" src=\"http://www.shanxi.gov.cn/yw/tpxw/201901/W020190122208091035252.jpg\" OLDSRC=\"W020190122208091035252.jpg\" /></p> <p>　　“‘红领巾研学行’集结号已经吹响！戴着鲜艳的红领巾，走出课堂，探究实践，到更广阔的空间去学习成长！”1月18日，伴随着宣传片的播放，山西省少先队员“红领巾研学行”在太原市晋祠宾馆国际会议中心启动。团省委党组成员、团省委副书记苏涛出席活动，并为山西省少先队员“红领巾研学行”活动授旗。</p> <p>　　<strong>挖掘资源优势</strong></p> <p>　　“红领巾研学行”是为深入学习贯彻习近平总书记系列重要讲话精神，特别是关于青少年和共青团、少先队工作的重要论述，认真贯彻落实党的十九大和教育部等11部委 《关于推进中小学生研学活动的意见》（教基一[2016]8号）等文件精神，并且结合 《山西省少先队改革实施方案》要求，围绕我省工作实际，教育引导青少年了解党史国史国情省情、培育和践行社会主义核心价值观，紧紧围绕立德树人，培养担当民族复兴大任时代新人的根本任务。</p> <p>　　“红领巾研学行”将充分挖掘我省人文历史、生态环保、红色革命、国防科技等资源优势，特别设置了四条路线：红色基因传承，引导孩子们学习红色精神，继承红色传统，培养孩子们艰苦奋斗、不怕吃苦、百折不挠、团结友爱的优秀品德，唤醒红色记忆，激发爱国情怀；金色民俗感知，通过“民俗”感知研学活动，将传统文化的精髓渗透到少先队员的生活中，让他们着眼于小事，从小培养对传统文化的热爱，为实现中国梦而努力奋斗；绿色生态实践，通过自然研学，让少先队员亲近自然，开阔视野，增强自身的实践、创新能力，培养他们欣赏自然的能力，将生态环保的理念植入少先队员心中，提高少先队员自身的综合素质及环保意识；蓝色科技体验，通过科技体验活动，在学习并掌握了科学原理的同时，培养动手能力，开发创新思维，将书本上的知识应用到实际中，真正做到在玩中学，在学中做，把知识变得有趣、简单、有用。</p> <p>　　<strong>9部门联合开展</strong></p> <p>　　据了解，此次活动是由省委宣传部、省文明办、共青团山西省委、省教育厅、省文化和旅游厅、山西广播电视台、省扶贫开发办公室、省少工委、山西文旅集团9个部门联合开展，是山西省首创也是独具山西少先队特色的研学活动。上述单位相关负责人及教育部有关专家出席活动，并对 “红领巾研学行”活动作了重要指示。</p> <p>　　启动仪式上，团省委党组成员、省少工委主任丁国栋介绍了活动的目的、意义及开展情况。山西广播电视台党委委员、副台长王树勋就如何做好山西省少先队员“红领巾研学行”活动的宣传推广做了重要讲话。</p> <p>　　参与过活动的少先队员代表表示，期待有更多的少先队员加入“红领巾研学行”活动，有更多的少年儿童因 “红领巾研学行”活动受益，有更多人关注、支持少年儿童公益事业。（陈晓凌 赵云云）</p>', 1599751107354, 26, '推荐', 0, 'http://www.shanxi.gov.cn/yw/tpxw/201901/W020190122208091035252.jpg', NULL, NULL, 4, 5);
INSERT INTO `cms_article` VALUES (68, '嘉峪关夏令营开始报名了', '<p>嘉峪关夏令营开始报名了</p>', 1600000627631, 0, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/0E/rBD-SV9eEnCACsEwAAXEWQYzwG4756.jpg', 15, NULL, 4, NULL);
INSERT INTO `cms_article` VALUES (74, '观光农业', '<p><span style=\"font-size: 14px;\"><strong> 一、观光农业的概念：</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp; 观光农业也称休闲观光农业，是以农业为基础，以果树、农作物的风景以及文化和田园生活文化为核心来吸引客户来观光旅游，观光农业通过合理的规划和设计，把农业生产、自然风光、休闲娱乐等结合为一体的经营形态，使农业生产者既有农产品的收入，又可以从旅游产业中获得额外的经济回报。而目前休闲观光农业的发展对于提高农业经济以及开发农业发展潜力有着重要意义。</span></p>\n<p><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; 二、观光农业发展中存在的问题：</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp; 当前休闲观光农业发展过程中存在一些突出问题，缺乏引导重复建设问题、可持续发展能力差、缺少专业人才等问题影响着休闲观光农业的发展，需要引起各方注意。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp; 1、缺乏引导，重复建设问题突出</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp; 缺乏必要引导，进而导致重复建设问题较为突出。在休闲农业发展中，先行者发挥了引领作用，但后期政府缺少必要的引导和规划，导致休闲农业基础设施建设重复率较高，没有能够突出产业发展重点，不利于产业的均衡化和持续性发展。在实际的产业发展中，农户存在扎堆投入的现象，个别项目赚钱，参与经营的农户短期内激增，造成非良性竞争，导致盈利能力的弱化。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp; 2、可持续发展能力差</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp; 休闲观光农业是否能够取得理想发展效果，关键要保障其可持续发展。目前休闲观光农业缺乏可持续发展能力，产业缺乏深度开发和利用，整体依然处于现代农业的低级阶段和初级水平。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp; 3、缺少专业人才</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp; 休闲观光农业的发展要充分体现人才价值，在资源整合，对外推广，日常管理等各方面突出人才作用。现阶段在休闲观光农业发展过程中存在专业人才缺失等实际问题，导致休闲观光农业存在经营管理措施滞后、市场化发展效率低等实际问题。</span></p>\n<p><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; 三、如何发展观光农业？</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp; 休闲观光农业要围绕政府引导和创新探索、资源整合等方面发展，进而才能取得理想的发展成果。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp; 1、政府积极引导发挥观光休闲农业社会效应</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp; 政府要发挥引导作用，在区域观光休闲农业发展过程中要体现社会效应。围绕市场需求和本地区实际情况，推动观光休闲农业发展，避免发展模式单一、发展内容重叠等问题，打造有特色的休闲农业产品，积极完善品牌体系构建，力求取得发展长效。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp; 2、敢于创新丰富观光休闲农业经营内容</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp; 观光休闲农业经营内容要持续有效创新，在内容和服务标准方面加以突破，进而能够使观光休闲农业受到更多人关注，使该模式的经济效益得到充分发挥。观光休闲农业产业要实现深度的产业对接，通过利用互联网等技术，发挥互联网+ 效应，使观光休闲农业经营内容呈现出多元化和立体化发展特征。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp; 3、汇集资源实现观光休闲农业产业集聚</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp; 观光休闲农业要发挥区域合理，逐渐实现产业集聚，通过资源汇聚能够使观光休闲农业价值得到充分体现，利用本地区农业现代化发展，同时有助于消化剩余劳动力，解决实际就业问题。该地区的旅游资源、农业资源、交通资源等各类资源被整合到观光休闲农业这一产业当中，资源的价值得到充分体现，产业将取得持续稳定发展。</span></p>', 1614244777120, 0, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/1C/rBD-SWA3a6KACfJcAAIengRxp_I229.jpg', NULL, 25, 3, NULL);
INSERT INTO `cms_article` VALUES (75, '有机农业', '<p><span style=\"font-size: 14px;\">很多人听说过有机农业，但是却并不清楚什么是有机农业，下面就从它的定义以及特点简单的给大家介绍一下什么叫有机农业。</span></p>\n<p>&nbsp;</p>\n<p><span style=\"font-size: 14px;\"><strong>一、</strong><strong>有机农业</strong><strong>定义：</strong></span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; </span><span style=\"font-size: 14px;\">有机农业其实就是一种农业生产方式。我国颁布的国家标准《有机产品》（GB/T19630.1~19630.4--2005）对&ldquo;有机农业&rdquo;的定义为：&ldquo;遵照一定的有机农业生产标准，在生产过程中不采用基因工程获得的生物及产物，不使用化学合成的农药、化肥、生长调节剂、饲料添加剂等物质，遵循自然规律和生态学原理，协调种植业好养殖业的平衡，采用一系列可持续发展的农业技术以维持持续稳定的农业生产体系的一种农业生产方式。&rdquo;</span></p>\n<p><span style=\"font-size: 14px; white-space: normal;\">&nbsp; &nbsp;&nbsp;</span><span style=\"font-size: 14px;\">国际有机农业运动联盟（IFOAM）成立于1972年，是世界上最具权威性和广泛代表性的国际有机农业标准化机构，总部设立在德国。它给&ldquo;有机农业&rdquo;下的定义为：&ldquo; 有机农业包括所有能够促进环境、社会和经济良性发展的农业生产系统。通过尊重植物、动物和景观的自然能力，实现农业和环境各方面质量达到最完善。有机农业禁止使用化学合成的肥料、农药和生长调节剂等，极大地减少外部物质的投入，强调利用强有力的自然规律来增加农业产量和抗病能力。&rdquo; 从这个定义可以看出，发展有机农业的最终目的是达到环境、经济和社会三大效益的和谐发展。</span></p>\n<p>&nbsp;</p>\n<p><span style=\"font-size: 14px;\"><strong>二、</strong><strong>有机农业</strong><strong>特点：</strong></span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; </span><span style=\"font-size: 14px;\">有机农业与目前农业相比较，有以下特点：</span><br /><span style=\"font-size: 14px;\">&nbsp; &nbsp; </span><span style=\"font-size: 14px;\">1、可向社会提供无污染、好口味、食用安全环保食品，有利于人民身体健康，减少疾病发生。</span><br /><span style=\"font-size: 14px;\">&nbsp; &nbsp; </span><span style=\"font-size: 14px;\">2、可以减轻环境污染，有利恢复生态平衡。</span><br /><span style=\"font-size: 14px;\">&nbsp; &nbsp; </span><span style=\"font-size: 14px;\">3、有利提高我国农产品在国际上的竞争力，增加外汇收入。</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; </span><span style=\"font-size: 14px;\">4、有利于增加农村就业、农民收入，提高农业生产水平。</span></p>\n<p>&nbsp;</p>\n<p><span style=\"font-size: 14px;\"><strong>三、</strong><strong>有机农业</strong><strong>总结：</strong></span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp;&nbsp;<span style=\"font-size: 14px; white-space: normal;\">有机农业关注的不仅是产品，而是整个生产体系的综合效益，在减少农产品中的农药残留，提高农产品和食品的安全性的同时，还有效的保护了农田及其生态系统的生物多样性。同时，有机农业不使用化学肥料，避免了化学肥料带来的氮、磷流失，以及由此引起的水体富营养化，通过合理的耕作措施，切实防止了水土流失，土地荒漠化等问题，通过农业废弃物，例如秸秆，人畜粪便的综合利用，有效的减轻了农村废弃物不合理的利用带来的环境污染，因此，有机农业是包括我国在内的世界各国农业生产的必然发展趋势。</span></span></p>', 1614245473563, 0, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/1C/rBD-SWA3bl2AKHseAAO_gZCRNW0669.jpg', NULL, 25, 3, NULL);
INSERT INTO `cms_article` VALUES (76, '农产品追溯系统', '<p><strong><span style=\"font-family: 宋体; color: black;\"><span style=\"font-size: 14px; font-family: 宋体; line-height: 150%;\"><span style=\"font-size: 14px; font-family: 宋体;\">系统</span></span><span style=\"font-size: 14px;\">概述：</span></span></strong></p>\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style=\"font-size: 14px; font-family: 宋体;\"><span style=\"font-size: 14px; font-family: 宋体; line-height: 150%;\">农产品追溯系统又称农产品溯源系统、农产品质量安全溯源系统，</span></span><span style=\"font-size: 14px; font-family: 宋体; color: black;\">该农产品追溯系统围绕&ldquo;从农田到餐桌&rdquo;的安全管理理念，综合运用了多种网络技术、条码识别等前沿技术，具有生产企业（基地等）、农产品生产档案（产地环境、生产流程、质量检测）管理、检测数据（企业自检、检测中心抽检）管理、条形码标签设计和打印、基于网站和手机短信平台的质量安全溯源等功能，实现了对农业生产、流通等环节信息的溯源管理，为政府部门提供监督、管理、支持和决策的依据，为企业建立包含生产、物流、销售的可信流通体系。</span></p>\n<p style=\"text-indent: 24pt;\" align=\"center\"><span style=\"font-family: 宋体; color: black;\"><img title=\"农产品质量安全及管理溯源系统\" src=\"http://www.tpwlw.com/Public/Admin/js/kindeditor-4.1/attached/image/20130819/20130819133514_64322.jpg\" alt=\"农产品质量安全及管理溯源系统\" width=\"600\" height=\"462\" /></span></p>\n<p><strong><span style=\"font-family: 宋体;\"><span style=\"font-size: 14px; font-family: 宋体;\"><span style=\"font-size: 14px; font-family: 宋体; line-height: 150%;\">&nbsp;&nbsp;&nbsp;&nbsp;农产品追溯系统</span></span><span style=\"font-size: 14px;\">功能：</span></span></strong></p>\n<p><span style=\"font-size: 14px; font-family: 宋体; color: black; line-height: 150%;\">&nbsp;&nbsp;&nbsp; 1</span><span style=\"font-size: 12pt; font-family: 宋体; color: black; line-height: 150%;\"><span style=\"font-size: 14px;\">、农产品安全生产管理</span></span></p>\n<p style=\"text-indent: 24pt;\"><span style=\"font-size: 12pt; font-family: 宋体; color: black; line-height: 150%;\"><span style=\"font-size: 14px;\">以农业生产者的生产档案信息为基础，实现对基础信息、生产过程信息等的实时记、生产操作预警，生产档案查询和上传功能。</span></span></p>\n<p><span style=\"font-size: 14px; font-family: 宋体; color: black; line-height: 150%;\">&nbsp;&nbsp;&nbsp; 2</span><span style=\"font-size: 12pt; font-family: 宋体; color: black; line-height: 150%;\"><span style=\"font-size: 14px;\">、农产品流通管理</span></span></p>\n<p style=\"text-indent: 24pt;\"><span style=\"font-size: 12pt; font-family: 宋体; color: black; line-height: 150%;\"><span style=\"font-size: 14px;\">以市场准入控制为设计基础实行入市申报，对批发市场经营者进行管理，记录其经营产品的交易情况，实现批发市场的全程安全管理。</span></span></p>\n<p><span style=\"font-size: 14px; font-family: 宋体; color: black; line-height: 150%;\">&nbsp;&nbsp;&nbsp; 3</span><span style=\"font-size: 12pt; font-family: 宋体; color: black; line-height: 150%;\"><span style=\"font-size: 14px;\">、农产品质量监督管理</span></span></p>\n<p style=\"text-indent: 24pt;\"><span style=\"font-size: 12pt; font-family: 宋体; color: black; line-height: 150%;\"><span style=\"font-size: 14px;\">实现相关法律法规、政策措施的宣传与监督功能；同时完成企业、农产品信息库的组建、管理和查询及分配管理防伪条码等功能。</span></span></p>\n<p><span style=\"font-size: 14px; font-family: 宋体; color: black; line-height: 150%;\">&nbsp;&nbsp;&nbsp; 4</span><span style=\"font-size: 12pt; font-family: 宋体; color: black; line-height: 150%;\"><span style=\"font-size: 14px;\">、农产品质量追溯</span></span></p>\n<p style=\"text-indent: 24pt;\"><span style=\"font-size: 12pt; font-family: 宋体; color: black; line-height: 150%;\"><span style=\"font-size: 14px;\">追溯系统综合利用网路技术、短线技术、条码识别技术等，实现网站、</span><span style=\"font-size: 14px;\">POS</span><span style=\"font-size: 14px;\">机、短信和电话号码于一体的多终端农产品质量追溯。</span></span></p>\n<p style=\"text-indent: 24pt;\">&nbsp;</p>\n<p><strong><span style=\"font-size: 14pt; font-family: 宋体; color: black; line-height: 150%;\"><span style=\"font-size: 14px; font-family: 宋体; color: black; line-height: 150%;\"><span style=\"font-size: 14px; font-family: 宋体; line-height: 150%;\">&nbsp;&nbsp;&nbsp; 系统</span></span><span style=\"font-size: 14px;\">特点：</span></span></strong></p>\n<p style=\"text-indent: 24pt;\"><span style=\"font-size: 14px; font-family: 宋体; color: black; line-height: 150%;\">1</span><span style=\"font-size: 12pt; font-family: 宋体; color: black; line-height: 150%;\"><span style=\"font-size: 14px;\">、农产品追溯系统可将农业生产过程中的生产信息，包括产地环境、生产流程、病虫害防治、质量检测等信息进行记录。</span></span></p>\n<p style=\"text-indent: 24pt;\"><span style=\"font-size: 14px; font-family: 宋体; color: black; line-height: 150%;\">2</span><span style=\"font-size: 12pt; font-family: 宋体; color: black; line-height: 150%;\"><span style=\"font-size: 14px;\">、可将携带农产品信息的</span><span style=\"font-size: 14px;\">RFID</span><span style=\"font-size: 14px;\">标签的信息转换成含有农产品信息的一维或二维条码标签，保证信息链的流通。</span></span></p>\n<p>&nbsp;</p>\n<p><strong><span style=\"font-size: 14px; font-family: 宋体; color: black; line-height: 150%;\">&nbsp;&nbsp;&nbsp; 农产品追溯系统应用对象：</span></strong></p>\n<p><span style=\"font-size: 14px; font-family: 宋体; color: black; line-height: 150%;\">&nbsp;&nbsp;&nbsp; 1</span><span style=\"font-size: 12pt; font-family: 宋体; color: black; line-height: 150%;\"><span style=\"font-size: 14px;\">、农产品质量监管部门；</span></span></p>\n<p><span style=\"font-size: 14px; font-family: 宋体; color: black; line-height: 150%;\">&nbsp;&nbsp;&nbsp; 2</span><span style=\"font-size: 12pt; font-family: 宋体; color: black; line-height: 150%;\"><span style=\"font-size: 14px;\">、农业生产企业。</span></span></p>', 1614302165648, 0, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/1C/rBD-SWA4S9OALdFuAAIengRxp_I875.jpg', NULL, 25, 3, NULL);
INSERT INTO `cms_article` VALUES (77, '智慧农业', '<p><strong><span style=\"font-size: 14px;\">一、智慧农业概述：</span></strong><br /><span style=\"font-size: 14px;\">&nbsp; &nbsp; 什么是智慧农业？&ldquo;</span><a href=\"http://www.tpwlw.com/\" target=\"_blank\" rel=\"noopener\"><span style=\"font-size: 14px;\">智慧农业</span></a><span style=\"font-size: 14px;\">&rdquo;是由数字农业、智能农业等名词演化而来，其技术体系主要包括农业物联网、农业大数据和农业云平台等三个方面。充分应用现代信息技术成果，集成应用计算机与网络技术、物联网技术、音视频技术、传感器技术、无线通信技术及专家智慧与知识平台，实现农业可视化远程诊断、远程控制、灾变预警等智能管理、远程诊断交流、远程咨询、远程会诊，逐步建立农业信息服务的可视化传播与应用模式；实现对农业生产环境的远程精准监测和控制，提高设施农业建设管理水平，依靠存储在知识库中的农业专家的知识，运用推理、分析等机制，指导农牧业进行生产和流通作业。</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 智慧农业是农业生产的高级阶段，是集新兴的互联网、移动互联网、云计算和物联网技术为一体，依托部署在农业生产现场的各种传感节点（环境温湿度、土壤水分、二氧化碳、图像等）和无线通信网络实现农业生产环境的智能感知、智能预警、智能决策、智能分析、专家在线指导，为农业生产提供精准化种植、可视化管理、智能化决策。</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; &ldquo;智慧农业&rdquo;是云计算、传感网、3S等多种信息技术在农业中综合、全面的应用，实现更完备的信息化基础支撑、更透彻的农业信息感知、更集中的数据资源、更广泛的互联互通、更深入的智能控制、更贴心的公众服务。&ldquo;智慧农业&rdquo;与现代生物技术、种植技术等高新技术融合于一体，对建设世界水平农业具有重要意义。</span></p>\n<p><br /><strong><span style=\"font-size: 14px;\">二、智慧农业物联网解决方案组成：</span></strong></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; &ldquo;智慧农业&rdquo;系统及其整体物联网解决方案，可以实现农产品从选种、育苗，到生产管理、订购销售、物流配送、质量安全溯源等产、供、销全过程的的高效感知及可控，促进传统农业向智慧农业转变。它涵盖农业规划布局、生产、流通等环节，主要由以下三大子系统构成：精准农业生产管理系统、农产品质量溯源系统和农业专家服务系统。</span></p>\n<p><span style=\"font-size: 14px;\">1、&ldquo;智慧农业&rdquo;精准农业生产管理系统</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 利用温度、湿度、光照、二氧化碳气体等多种传感器对农牧产品（蔬菜、禽肉等）的生长过程进行全程监控和数据化管理，通过传感器和土壤成份检测感知生产过程中是否添加有机化学合成的肥料、农药、生长调节剂和饲料添加剂等物质；结合RFID电子标签对每批种苗来源、等级、培育场地以及在培育、生产、质检、运输等过程中具体实施人员等信息进行有效、可识别的实时数据存储和管理。系统以物联网平台技术为载体，提升有机农产品的质量及安全标准，从而让老百姓能够吃上放心菜。</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 系统主要功能：</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 1、农业现场数据采集功能（如温湿度、土壤酸碱度等）；</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 2、农业生产现场视频采集、生产过程监控功能；</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 3、生产过程中积累的大量数据分析功能；</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 4、远程卷帘、灌溉、风机等遥控功能；</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 5、手机监控、控制功能；</span></p>\n<p><span style=\"font-size: 14px;\">2、&ldquo;智慧农业&rdquo;农产品（猪肉）质量溯源系统</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 农产品质量管理系统，通过固定式专用RFID阅读器自动识别个体，进行自动分拣归栏，自动饲喂、自动追踪记录活动规律、饲养数据等，监控农产品（生猪）生长密度、环境参数，通过网络实时更新到档案数据库。进而通过RFID或条形码管理系统实现物流的追溯（通过包装条码查询产品物流状态）和产品质量的追溯（查询此批次产品的相关质量数据），为客户提供产品增值服务，同时也为企业生产管理者提供一手的现场数据。</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 产品主要功能：</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 1、农产品安全生产管理；</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 2、农产品流通管理；</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 3、农产品质量监督管理；</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 4、农产品质量追溯；</span></p>\n<p><span style=\"font-size: 14px;\">3、&ldquo;智慧农业&rdquo;农业专家远程诊断服务系统</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 农业专家远程诊断服务系统，采用3G无线传输技术、H.264网络视频压缩技术术将视频信息、控制信息等监控数据进行压缩编码，通过无线数据网络，传给专家，实现专家足不出户，即可远程实时指导、浏览和在线答疑、咨询等服务。并可记录视频信息的一整套远程专家诊断系统产品。</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 专家诊断平台的功能：</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 1、种养殖人员与专家双向音视频实时沟通功能；</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 2、远程传感提醒及遥控功能；</span><br /><span style=\"font-size: 14px;\">&nbsp; &nbsp; 3、多领域农业专家、多用户综合服务功能。</span></p>\n<p><br /><strong><span style=\"font-size: 14px;\">三、发展智慧农业带来的作用：</span></strong><br /><span style=\"font-size: 14px; white-space: normal;\">&nbsp; &nbsp;&nbsp;</span><span style=\"font-size: 14px;\">&ldquo;智慧农业&rdquo;能够有效改善农业生态环境。将农田、畜牧养殖场、水产养殖基地等生产单位和周边的生态环境视为整体，并通过对其物质交换和能量循环关系进行系统、精密运算，保障农业生产的生态环境在可承受范围内，如定量施肥不会造成土壤板结，经处理排放的畜禽粪便不会造成水和大气污染，反而能培肥地力等。</span></p>\n<p><span style=\"font-size: 14px; white-space: normal;\">&nbsp; &nbsp;&nbsp;</span><span style=\"font-size: 14px;\">&ldquo;智慧农业&rdquo;能够显着提高农业生产经营效率。基于精准的农业传感器进行实时监测，利用云计算、数据挖掘等技术进行多层次分析，并将分析指令与各种控制设备进行联动完成农业生产、管理。这种智能机械代替人的农业劳作，不仅解决了农业劳动力日益紧缺的问题，而且实现了农业生产高度规模化、集约化、工厂化，提高了农业生产对自然环境风险的应对能力，使弱势的传统农业成为具有高效率的现代产业。</span></p>\n<p><span style=\"font-size: 14px; white-space: normal;\">&nbsp; &nbsp;&nbsp;</span><span style=\"font-size: 14px;\">&ldquo;智慧农业&rdquo;能够彻底转变农业生产者、消费者观念和组织体系结构。完善的农业科技和电子商务网络服务体系，使农业相关人员足不出户就能够远程学习农业知识，获取各种科技和农产品供求信息；专家系统和信息化终端成为农业生产者的大脑，指导农业生产经营，改变了单纯依靠经验进行农业生产经营的模式，彻底转变了农业生产者和消费者对传统农业落后、科技含量低的观念。另外，智慧农业阶段，农业生产经营规模越来越大，生产效益越来越高，迫使小农生产被市场淘汰，必将催生以大规模农业协会为主体的农业组织体系。</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp; &nbsp; 智慧农业谷功能构建包括特色有机农业示范区、农科总部园区和高端休闲体验区，是促进农业的现代化精准管理、推进耕地资源的合理高效利用。</span></p>', 1614302220740, 0, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/1C/rBD-SWA4TAuAeaYsAAO_gZCRNW0703.jpg', NULL, 25, 3, NULL);
INSERT INTO `cms_article` VALUES (79, '现代农业', '<p><span style=\"font-size: 14px;\"> 我国是农业大国，在当今经济全球化的形势下，我们必须研究现代农业的发展趋势，扬长避短。在做好基础工作的前提下，创造条件，大力发展<a href=\"http://www.tpwlw.com/\" target=\"_blank\" rel=\"noopener\">智慧农业</a>、精细农业、生态农业、信息农业等不同模式的现代农业，促进农业经济、技术快速发展。</span></p>\n<p><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 一、现代农业的定义：</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 现代农业是一个动态的和历史的概念，它不是一个抽象的东西，而是一个具体的事物，它是农业发展史上的一个重要阶段。从发达国家的传统农业向现代农业转变的过程看，实现农业现代化的过程包括两方面的主要内容：一是农业生产的物质条件和技术的现代化，利用先进的科技和生产要素装备农业，实现农业生产机械化、电气化、信息化、生物化和化学化；二是农业组织管理的现代化，实现农业生产专业化、社会化、区域化和企业化。</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 现代农业发展的本质也就是科学技术化，未来农业将是以现代科技及其应用技术装备起来的崭新产业。农业的发展大体经历原始农业、传统农业和现代化农业三个阶段。原始农业主要靠大自然的恩赐，传统农业以经验为基础，现代农业则是依靠科学技术。</span></p>\n<p>&nbsp;</p>\n<p><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 二、现代农业发展阶段：</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 准备阶段<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 这是传统农业向现代农业发展的过渡阶段。在这个阶段开始有较少现代因素进入农业系统。如农业生产投入量已经较高，土地产出水平也已经较高。但农业机械化水平、农业商品率还很低，资金投入水平、农民文化程度、农业科技和农业管理水平尚处于传统农业阶段。<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 起步阶段<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 本阶段为农业现代化进入阶段。其特点表现为：①现代投入物快速增长。②生产目标从物品需求转变为商品需求；③现代因素（如技术等）对农业发展和农村进步已经有明显的推进作用。在这一阶段，农业现代化的特征已经开始显露出来。<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 初步实现阶段<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 本阶段是现代农业发展较快的时期，农业现代化实现程度进一步提高，已经初步具备农业现代化特征。具体表现为现代物质投入水平较高，农业产出水平，特别是农业劳动生产率水平得到快速发展。但这一时期的农业生产和农村经济发展与环境等非经济因素还存在不协调问题。<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 基本实现阶段<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 本阶段的现代农业特征十分明显：①现代物质投入已经处于较大规模，较高的程度；②资金对劳动和土地的替代率已达到较高水平；③现代农业发展已经逐步适应工业化、商品化和信息化的要求；④农业生产组织和农村整体水平与商品化程度，农村工业化和农村社会现代化已经处于较为协调的发展过程中。<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 发达阶段<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 它是现代农业和农业现代化实现程度较高的发展阶段，与同时期中等发达国家相比，其现代农业水平已基本一致，与已经实现农业现代化的国家相比虽仍有差距，但这种差距是由于非农业系统因素造成，就农业和农村本身而论，这种差距并不明显。这一时期，现代农业水平、农村工业、农村城镇化和农民知识化建设水平较高，农业生产、农村经济与社会和环境的关系进入了比较协调和可持续发展阶段，已经全面实现了农业现代化。</span>&nbsp;</p>\n<p>&nbsp;</p>\n<p><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 三、现代农业发展前景：</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1、现代农业发展面临的机遇：</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 从信息化发展趋势看，信息社会的到来，为农业农村信息化发展提供了前所未有的良好环境。人类社会经历了农业革命、工业革命，正在经历信息革命。当前，以信息技术为代表的新一轮科技革命方兴未艾，以数字化、网络化、智能化为特征的信息化浪潮蓬勃兴起，为农业农村信息化发展营造了强大势能。党中央、国务院高度重视信息化发展，对实施创新驱动发展战略、网络强国战略、国家大数据战略、&ldquo;互联网+&rdquo;行动等作出部署，并把农业农村摆在突出重要位置，为农业农村信息化发展提供了强有力的政策保障。网络经济空间不断拓展，农业农村信息化服务加快普及，网络基础设施建设深入推进，信息消费快速增长，信息经济潜力巨大，为农业农村信息化发展提供了广阔空间。信息技术创新日新月异并加速与农业农村渗透融合，农业信息技术创新应用不断加快，为农业农村信息化发展提供了坚实的基础支撑。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2、现代农业发展面临的挑战：</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 从农业现代化建设需求看，加快破解发展难题，为农业农村信息化发展提供了前所未有的内生动力。资源环境约束日益趋紧，农业发展方式亟待转变，迫切需要运用信息技术优化资源配置、提高资源利用效率，充分发挥信息资源新的生产要素的作用。居民消费结构加快升级，农业供给侧结构性改革任务艰巨，迫切需要运用信息技术精准对接产销、提升供给的质量效益和竞争力，充分发挥信息技术核心生产力的作用。农业小规模经营长期存在，规模效益亟待提高，迫切需要运用信息技术探索走出一条具有中国特色的农业规模化路子，充分发挥互联网平台集聚放大单个农户和新型经营主体规模效益的作用。农产品价格提升空间有限，转移就业增收空间收窄，农民持续增收难度加大，迫切需要运用信息技术促进农村大众创业万众创新、发展农业农村新经济，充分发挥&ldquo;互联网+&rdquo;开辟农民增收新途径的作用。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 同时，我国农业农村信息化正处在起步阶段，基础相当薄弱，发展相对滞后，总体水平不高。思想认识亟待提升。客观上，我国农业正处在由传统农业向现代农业转变的阶段，信息化对农业现代化的作用尚未充分显现。各级农业部门对发展农业农村信息化的重要性、紧迫性的认识有待深化，关心支持农业农村信息化发展的社会氛围有待进一步形成。基础条件建设亟待加强。农业数据采集、传输、存储、共享的手段和方式落后，农业物联网产品和设备还未实现规模量产，支撑电子商务发展的分等分级、包装仓储、冷链物流等基础设施十分薄弱。农业信息技术标准和信息服务体系尚不健全。重要信息系统安全面临严峻挑战。农村网络基础设施建设滞后，互联网普及率尤其是接入能力还较低。科技创新亟待突破。自主创新能力不足，农业物联网生命体感知、智能控制、动植物生长模型和农业大数据分析挖掘等核心技术尚未攻克,技术和系统集成度低、整体效能差。农业信息化学科群和科研团队规模偏小，领军人才和专业人才匮乏。农业信息技术成果转化和推广应用比例低。体制机制亟待创新。管理职能和机构队伍建设没有跟上农业农村信息化发展的需要。投融资机制尚不健全，政府与社会资本合作模式尚未破题，市场化可持续的商业模式亟需探索完善。市场服务和监管制度、软硬件产品检验检测体系不健全。</span></p>\n<p>&nbsp;</p>', 1614302278248, 0, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/1C/rBD-SWA4TEWAXbECAAS_l_Hs0rY259.jpg', NULL, 25, 3, NULL);
INSERT INTO `cms_article` VALUES (80, '生态养殖', '<p><span style=\"font-size: 14px;\"><strong> 什么是生态养殖？</strong></span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>生态养殖应是养殖业、种植业、能源开发利用、环境、经济、社会发展综合开发的系统工程。通俗地讲，生态养殖是在一定的生态环境中，或者运用生态技术措施，按照特定的养殖模式进行养殖。生态养殖目标是生产出绿色食品或有机食品。其实早在二十世纪初，《中华人民共和国农业部令》（第31号）就定义了生态养殖的概念，指根据不同养殖生物间的共生互补原理，利用自然界物质循环系统，在一定的养殖空间和区域内，通过相应的技术和管理措施，使不同生物在同一环境中共同生长，实现保持生态平衡、提高养殖效益的一种养殖方式。</span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; 生态养殖的几种模式：</strong></span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>第一种、放养模式：</span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>放养模式也可以理解为&ldquo;林下经济&rdquo;。</span><span style=\"font-size: 14px;\">通过山林、果园、作物等植物种植与畜禽养殖结合，保持山林、果园良好的生态平衡，形成优质绿色农产品的种养结合模式</span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>第二种、立体养殖模式：</span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>立体养殖模式被形象的比喻为&ldquo;水陆空模式&rdquo;。较早出现的一种养殖模式是&ldquo;桑-蚕-鱼塘&rdquo;，在这种养殖模式中，蚕的粪便、残渣等排泄物可以为鱼提供饵料，鱼塘最底部的塘泥为桑树供应有机肥料，桑树上生长的桑叶作为食料供给给家蚕，以此来实现资源的循环利用。此外，还有通过用饲料喂鸡，鸡粪喂猪，猪粪发酵后喂鱼，或畜禽粪便入池肥水，转化成浮游生物，为水产养殖提供天然饵料。另有塘泥用作农作物肥料，从而形成良性循环的生物链，如鸡-猪-鱼、鸭(鹅)-鱼-果-草、鱼-蛙-畜-禽等。</span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>第三种、生物发酵模式：</span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>此模式以沼气为纽带，通过微生物发酵技术将畜禽粪便中的有机物转化成可利用的再生资源。除了以畜禽粪便发酵生产沼气、沼渣和沼液还田外，近年来以&ldquo;发酵床&rdquo;为特征的养殖模式正在国内外兴起，其原理是利用锯末、秸秆、稻壳等配以微生物菌剂形成一定厚度的垫料(床)，家畜在垫料上生活，垫料里的特殊有益微生物能够迅速降解粪尿排泄物。</span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; 如何发展生态养殖？</strong></span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>一、选择合适自然生态环境：</span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>选择一个合适的自然生态环境对禽类的养殖是比较重要的，如果没有自然的生态环境，自然也就没有所谓的生态养殖了。所以要选择一个天然的无污染的比较大的地区，可以让禽类自由活动自然生长，也可以从这一方面提高禽类的质量，比较适合的自然生态地区就是平原和林地了。</span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>二、使用配合饲料：</span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>配合饲料可以增长禽类的生长速度，增加饲养者经济效益。如果不使用配合饲料的话就会满足不了消费者的需求，也会影响饲养者的积极性。但是生态养殖的配合饲料里面不可以添加促生长剂，因为这个会影响禽类的品质和口感，就会影响市场销售量，不能达到消费者的要求。</span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>三、做好防疫工作：</span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>因为现在生态养殖的禽类都是放养模式，几乎都是将场地健在户外，所以就更容易感染细菌和病毒，所以防疫工作这方面是需要重点关注的。一般当地防疫部门会按照疫情来制定专门的免疫程序，而除了要做这些药物防疫，然而药物多多少少都会产生残留影响禽类的品质，所以养殖户可以利用一些中草药来景象防疫，比如增强免疫力的马齿觅，五点草等等。中草药不会像西药那样有产生药物残留，这样可以提高禽类的质量和产量。</span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>四、注意收集畜禽粪便：</span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>禽类的粪便会造成环境污染，而粪便也比较容易产生疫病和传播。而生态养殖的禽类又大多是养在户外，如果禽类的粪便不及时处理的话会产生疫病影响禽类的健康和产量。所以在养殖过程中要按时清理禽类粪便，保证环境卫生，减少污染。</span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>五、生态养殖宣传工作：</span><br /><span style=\"font-size: 14px;\"><strong>&nbsp;&nbsp;&nbsp; </strong>一个商品的知名度很重要，能够打响知名度就可以扩大销售量增加经济收益。所以可以建立一个关于生态养殖的品牌，做好宣传工作，让消费者了解生态养殖好处和通过生态养殖养殖出来的禽类的质量。只有提高知名度，才能让消费者更容易接受，所以建立一个生态养殖品牌是一个比较重要的工作。</span></p>', 1614302303363, 0, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/1C/rBD-SWA4TF-AfeubAAJyKmklJN0942.jpg', NULL, 25, 3, NULL);
INSERT INTO `cms_article` VALUES (81, '节水灌溉设备', '<p><span style=\"font-size: 14px;\"> 节水灌溉设备方案是托普物联网为保证农业作物需水量的前提下，实现节约用水而提出的一整套节水灌溉解决方案。简单的说就是农业灌溉不需要人的控制，灌溉设备能自动感测到什么时候需要灌溉，灌溉多长时间；系统可以自动开启灌溉，也可以自动关闭灌溉；可以实现土壤太干时增大喷灌量，太湿时减少喷灌量。&nbsp;</span></p>\n<p><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>一、灌溉设备</strong><strong>的功能设计&nbsp;</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 灌溉设备要实现上述功能就要充分利用可编程控制器的控制作用。系统要实现自动感测土壤湿度的功能必须要有土壤湿度传感器。要实现灌溉水量的多与少的调节，必须要有变频器。在可编程控制器内预先设定50%&mdash;60%RH为标准湿度，传感器采集的湿度模拟信号经A/D模块转换成数字信号。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 针对灌溉水利用系数较低,文中提出一种基于嵌入式智能灌溉控制系统。依托无线传感器网络采集灌区作物需水信息,汇聚到网关节点发送给主控中心,中心主机根据信息确定灌溉状态并计算灌水量,控制灌溉设备工作实现智能灌溉;依托Internet管理员有权对系统远程管理,满足了规模化灌溉的需求。根据示范区观测,灌溉水利用系数由原来的0.6提高到0.9。系统结合了无线传感、计算和网络通信技术,解决了精确农业亟待解决的关键技术问题。&nbsp;</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 方案涉及到传感器技术、自动控制技术、计算机技术、无线通信技术等多种高新技术，这些新技术的应用使我国的农业由传统的劳动密集型向技术密集型转变奠定了重要的基础。&nbsp;</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 节水灌溉设备可以根据植物和土壤种类，光照数量来优化用水量，还可以在雨後监控土壤的湿度。有研究现实，和传统灌溉系统相比，智能农业灌溉系统的成本差不多，却可节水16%到30%。</span></p>\n<p><span style=\"font-size: 14px;\"><br /></span><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>二、</strong><strong>节水灌溉设备方案</strong><strong>的设计背景 </strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 灌溉造成水资源大量浪费 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 美国每年浪费掉的水资源高达8,520亿升，而若安装一种智能农业灌溉系统则可有效地控制水流量，达到节水目的。HydroPoint公司负责可持续领域业务的Chris Spain援引美国用水工程协会的报告称，美国住宅区和商业区的草坪、植物灌溉用水浪费了30%到300%。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 水资源被浪费的原因是技术不行，美国有4,500万个仅是安有简易计时器的灌溉系统，它们在时间控制上还可以，但精准度不高。Spain称，城市灌溉系统占城市用水的58%，这些被浪费的水资源每年生产54.4万吨温室气体。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 在中国农业用水量约占总用水量的80%左右，由于农业灌溉效率普遍低下，水的利用率仅为45%，而水资源利用率高的国家已达70%～80%，因而，解决农业灌溉用水的问题，对于缓解水资源的紧缺是非常重要的。我们的智能农业灌溉系统在这种背景下应运而生了。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 不仅美国，英国也开始关注节水问题。英国节能信托基金会和能源部警告，随着越来越多的家庭开始节约能源，使用热水可能会超过取暖成为制造二氧化碳的主要途径。&nbsp;</span></p>\n<p><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>三、</strong><strong>节水灌溉设备方案</strong><strong>工作原理&nbsp;</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 灌溉设备工作时，湿度传感器采集土壤里的干湿度信号，检测到的湿度信号通过A/D模块转换，将标准的电流模拟信号转换为湿度数字信号，输入到可编程控制器。可编程控制器内预先设定50%-60%RH为标准湿度值，实际测得的湿度信号与50%-60%RH比较，可以分为：在这个范围内，超出这个范围，小于这个范围三种情况。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 可编程控制器将控制信号传给变频器，变频器根据湿度值，相应的调节电动机的转速，电动机带动水泵从水源抽水，需要灌溉时，电磁阀就自动开启，通过主管道和支管道为喷头输水，喷头以各自的旋转角度自动旋转。灌溉结束时电磁阀自动关闭。为了避免离水源远的喷头不能被供给足够的压力，在电磁阀的一侧安装一块压力表，保证个喷头的水压满足设定的喷灌射程，避免发生因为水压不足，喷头射程减少的现象。整个系统协调工作，实现对草坪灌溉的智能控制。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 综上所述，要实现智能灌溉，系统需要有可编程控制器、传感器、A/D模块、变频器、电动机、水泵、电磁阀、管网和喷头等设备。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ①可编程控制器：负责发出和接收各种运行程序指令，是整个控制系统的中枢部分。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ②传感器：由于本次设计时间比较仓促，忽略了温度对灌溉的影响，因此没有使用温度传感器，只使用了土壤湿度传感器。通过传感器采集土壤里的湿度信号，判断是否需要灌溉。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ③A/D模块：因为可编程控制器不能接收模拟信号，所以需将传感器的电压或电流信号转换成数字信号。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ④变频器：通过改变电动机的转速调节喷灌流量，达到节水的目的。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ⑤电动机、水泵：由电动机带动水泵从水源抽水，为喷灌系统提供一定的压力。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ⑥电磁阀：控制喷头的喷灌与否。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ⑦喷头：实现均匀喷洒，便于充分吸收。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ⑧管网：系统输送水的管路。&nbsp;</span></p>\n<p><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>四、</strong><strong>节水灌溉设备方案</strong><strong>的设计特点 </strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1、系统模块化、层次化设计，以提高效率，增加可维护性，便于扩展； </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2、灵活的硬件配置，用户可以任意升级、更换被控硬件设备，而不需要更换软件； </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3、人机界面友好，实现灌溉过程的无人值守，减少人员的工作强度，提高灌溉效率； </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4、抗电磁干扰的能力强，保证系统在野外强电磁干扰的恶劣环境下能可靠地运行； </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5、故障自动检测功能，提高系统的健壮性，各种设备的布局要求美观。&nbsp;</span></p>\n<p><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>五、</strong><strong>节水灌溉设备方案</strong><strong>功能介绍</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 为了节约喷灌用水和实现智能控制，节水灌溉设备必须具备以下功能： </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1、数据采集功能：可接收土壤湿度传感器采集的模拟量。模拟量信号的处理是将模拟信号转变成数字信号（A/D转换）。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2、控制功能：具有定时控制、循环控制的功能，用户可根据需要灵活选用控制方式。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ①自动控制功能：可编程控制器通过程序将传感器检测的湿度信号与预先设定的标准湿度范围值相比较，如果检测的湿度值超出了设定湿度值，（低于设定值则调大电动机转速，高于设定值则调小电动机转速）则自动调节电动机转速，进行灌溉操作。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ②定时控制功能：系统可对电磁阀设定开、关时间，当灌溉的湿度值达到设定的湿度值时，电动机自动停止灌溉。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ③循环控制功能 ：用户在可编程控制器内预先编好控制程序，分别设定起始时间、结束时间、灌溉时间、停止时间，系统按设定好的时间自动循环灌溉。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 变速功能：当前所测的土壤湿度值与预先设定的草坪生长的湿度值50%&mdash;60%RH比较，分为大于、等于、小于三种结果，即可将湿度分为高湿度、中湿度、低湿度三种状态。在控制面板上表现为高湿度、中湿度、低湿度三个指示灯。变频器根据土壤湿度的三个状态自动调节电动机的转速，电动机设有高速，中速，低速3种旋转速度，分别对应高速，中速，低速三个指示灯。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4、自动转停功能：控制系统根据土壤的干湿度情况自动启动喷灌，控制电动机以所需的转速转动，喷头喷灌5分钟，停2分钟，再喷5分钟后自动停转。</span></p>\n<p><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5.、电动机过载保护功能：当电动机过载时，电动机立即停止转动，灌溉过程中止，并且故障指示灯闪烁报警，过载消除后自动恢复运转。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6、阴雨天自动停止：利用湿度传感器的开关量作为一个可编程控制器的输入信号，实现控制相关程序的功能。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 7、省电功能：定时控制器在断电时正常计时，故采用其作为可编程控制器的电源控制。在定时灌溉控制时间之内，由定时器接通可编程控制器的电源，可编程控制器按预先编制的程序依次打开各控制设备电源，并根据输入信号的变化随时调整程序的执行。在非系统工作时间里，定时器自动断开可编程控制器的电源，这样既减少了系统耗费的电能又延长了设备的使用寿命。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 8、急停功能：当出现紧急意外事故时，按下急停按钮，电动机立即停止运转，阀门关闭，喷头停止灌溉。 </span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 9、故障自动检测功能：当灌溉系统出现故障，如水管破裂（水压为零），传感器故障，电动机故障，变频器故障，电磁阀故障等，水泵立即停止运行，电磁阀关闭，故障报警灯闪烁并伴有警笛声响起。操作人员可以按下&ldquo;消音&rdquo;按钮以解除铃响，但故障指示灯仍在闪烁，直到故障消除，故障指示灯才自动停止闪烁。 </span></p>', 1614302340677, 0, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/1C/rBD-SWA4TISAONuoAAMxfSql58g670.jpg', NULL, 25, 3, NULL);
INSERT INTO `cms_article` VALUES (82, '乡村振兴战略20字方针', '<p><span style=\"font-size: 14px;\">&nbsp; 乡村振兴战略20字方针是指产业兴旺、生态宜居、乡风文明、治理有效、生活富裕，这乡村振兴战略二十字方针所体现的五大具体目标任务具有相互联系性，因此，既要准确把握方针的科学内涵，还要把握好方针中五大目标任务的相互关系。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>一、产业兴旺</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 实现乡村&ldquo;产业兴旺&rdquo;，是乡村振兴的核心，也是我国经济建设的核心。现阶段农村发展落后，资源分布极不均匀，收入低直接导致农民背井离乡，外出打工，村里只留下空巢老人和留守儿童，村子自然就演变成&ldquo;空心村&rdquo;。唯有乡村产业兴旺，才能从根本上解决农村的社会问题，走上可持续发展的道路。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>二、生态宜居</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 实现乡村&ldquo;生态宜居&rdquo;，是我国生态建设的重点，是农村农民的梦想，同时也是每个国人的梦想。生态环境是宜居的根本，只有生态环境得到改善，人们的生活质量才会得到保障。现在许多肺病都是由空气直接引起的，饮用被污染的水资源会影响整个人体内脏器官。因此，乡村拥有得天独厚的自然条件和地形地势，可打造城市没有的绿水青山，遍地多彩野花，设施服务的环境，实现人们安居乐业的理想。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>三、乡风文明</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 实现乡村&ldquo;乡风文明&rdquo;，例如上篇文章中的山东中郝峪村，硬件软件一手抓。但凡村里坚持戒烟的，村委会核实后，会每人每年奖励1000元的奖金；我们都知道这个村的每位村民都参加每年的利润分红，只要有老人向村委会投诉其子女不赡养他们，经村委会核实，会取消其子女的分红机会。因此，村里的每位村民都遵守道德原则，整个村子的乡风都带动起来，成为了乡村文明示范村！</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>四、治理有效</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 实现乡村&ldquo;治理有效&rdquo;，还是以中郝峪村为例，该村实行的是全民入股的政策，利润的80%归村民所有，这是许多乡村发展休闲农业没有完全调动村民积极性的一大法宝。因此，运营负责人在管理与治理上会得心应手，十分顺畅的开展工作。因为每位村民都是在为自己工作，跟自己的收入息息相关。运营人员只要将制度规则设计好，就能将整个村子运营得井井有条。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>五、生活富裕</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 实现乡村&ldquo;生活富裕&rdquo;，以村民为中心，美丽乡村的建设最终目的是让村民过上稳定美好的日子，判定人们生活是否富裕，村里是否有活力，关键看村民的神态，来到中郝峪村，你会发现，这里的每位村民都喜笑颜开，充满幸福感。此外，外出打工的年轻人基本上都回村里发展了，空心村一去不复返。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>六、乡村振兴战略的灵魂</strong></span><br /><span style=\"font-size: 14px;\">习近平总书记强调：&ldquo;文化是一个国家、一个民族的灵魂。文化兴国运兴，文化强民族强。没有高度的文化自信，没有文化的繁荣兴盛，就没有中华民族伟大复兴。&rdquo;</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 与2005年中央提出的社会主义新农村建设&ldquo;生产发展、生活宽裕、乡风文明、村容整洁、管理民主&rdquo;20字方针相比，乡村振兴战略提出的&ldquo;产业兴旺、生态宜居、乡风文明、治理有效、生活富裕&rdquo;20字方针，进一步丰富了内涵、提升了层次。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 乡风文明既是乡村振兴战略的重要内容，更是加强农村文化建设的重要举措。实施乡村振兴战略，实质上是在推进融生产、生活、生态、文化等多要素于一体的系统工程。文化是农村几千年发展历史的沉淀，是农村人与物两大载体的外在体现，也是乡村振兴战略的灵魂所在。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 因此，在实施乡村振兴战略的过程中应转变过去重经济轻生态、轻文化的发展理念，也就是&ldquo;既要护口袋，还要护脑袋&rdquo;。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 乡风文明涵盖丰富的内涵，一是对中华优秀传统文化的传承与创新，并以此提升文化软实力；</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 二是物质文化和非物质文化的保护，特别是关键区域农耕文明、游牧文明、海洋文明的保护，以及民族地区民俗、民风、民居等文化要素的保护；</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 三是优良传统的继承与发扬光大，特别是传承了几千年的道德伦理，这是&ldquo;不忘初心&rdquo;的体现；</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 四是新时代意识的培养，广大的农村居民是乡村振兴的主体，也是乡村振兴成效的受益主体和价值主体，为此，应提高农村居民对乡村振兴战略的认知水平，培养农村居民的责任意识、参与意识。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 在乡风文明建设过程中，还应注意避免两大误区：一是把过多的现代化元素引入农村；二是把过多的城市元素引入农村。应注重强化农村原生态文化的建设与传承，乡村&ldquo;灵魂&rdquo;不能走样也不容歪曲。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 全面落实乡村振兴战略20字总要求</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &ldquo;产业兴旺、生态宜居、乡风文明、治理有效、生活富裕&rdquo;，是党的十九大报告提出的实施乡村振兴战略的总要求。据国家发展改革委负责人介绍，规划围绕这一总要求，明确了阶段性重点任务。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 在促进乡村产业兴旺方面，部署了一系列重要举措，构建现代农业产业体系、生产体系、经营体系，完善农业支持保护制度。同时，通过发展壮大乡村产业，激发农村创新创业活力。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 在促进乡村生态宜居方面，提出强化资源保护与节约利用，推进农业清洁生产，集中治理农业环境突出问题，实现农业绿色发展，并确定推进美丽宜居乡村建设，持续改善农村人居环境。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 在促进乡村乡风文明方面，提出传承发展乡村优秀传统文化，培育文明乡风、良好家风、淳朴民风，建设邻里守望、诚信重礼、勤俭节约的文明乡村，推动乡村文化振兴。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 在促进乡村治理有效方面，建立健全党委领导、政府负责、社会协同、公众参与、法治保障的现代乡村社会治理体制，推动乡村组织振兴，打造充满活力、和谐有序的善治乡村。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 在促进乡村生活富裕方面，提出加快补齐农村民生短板，如在改善农村交通物流设施条件、加强农村水利基础设施网络建设、拓宽转移就业渠道，以及加强农村社会保障体系建设等方面都出台了一系列措施。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 中央农村工作领导小组办公室主任、农业农村部部长韩长赋说，把乡村振兴摆上优先位置，必须坚持规划先行，树立城乡融合、一体设计、多规合一理念，在产业发展、人口布局、公共服务、基础设施、土地利用、生态保护等方面，因地制宜编制乡村振兴地方规划和专项规划方案，做到乡村振兴事事有规可循、层层有人负责，一张蓝图绘到底，久久为功搞建设。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 系统解决乡村振兴&ldquo;人、地、钱&rdquo;难题</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 围绕乡村振兴&ldquo;人、地、钱&rdquo;等要素供给，规划部署了加快农业转移人口市民化、强化乡村振兴人才支撑、加强乡村振兴用地保障、健全多元投入保障机制、加大金融支农力度等方面的具体任务。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &ldquo;乡村振兴要真刀真枪地干，就离不开真金白银地投。补上乡村建设发展的多年欠账，光靠农村农民自身力量远远不够。&rdquo;韩长赋说，&ldquo;把乡村振兴摆上优先位置，必须下决心调整城乡要素配置结构，建立健全乡村振兴投入保障机制。&rdquo;</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 他强调，当前要抓紧研究制定、调整完善土地出让收入使用范围、提高用于&ldquo;三农&rdquo;比例的政策文件，推动将跨省域补充耕地指标交易和城乡建设用地增减挂钩节余指标省域调剂所得收益，全部用于巩固脱贫攻坚成果和实施乡村振兴战略，并广开投融资渠道，引导撬动各类社会资本投向农村。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 中国宏观经济研究院研究员马晓河说，解决&ldquo;人&rdquo;的问题，关键要推动两类人在城乡之间双向自由流动。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 一类是农业转移人口，围绕推进市民化&ldquo;降门槛&rdquo;，让有意愿、有能力的农业转移人口在城镇落户。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 另一类是乡村人才，既在&ldquo;育&rdquo;上下功夫，培育新型职业农民、培养一大批乡村本土人才，也要在&ldquo;引&rdquo;上做文章，鼓励社会人才投身乡村建设。</span></p>', 1614302395939, 0, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/1C/rBD-SWA4TLuAaZhuAANiZNZsZSg462.jpg', NULL, 25, 3, NULL);
INSERT INTO `cms_article` VALUES (83, '温室大棚环境远程监控系统', '<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;温室远程监控系统也叫</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">温室大棚环境远程监控系统、大棚环境远程监控系统、温室大棚环境监控系统等，该系统可以实时远程获取温室大棚内部的空气温湿度、土壤水分温度、二氧化碳浓度、光照强度及视频图像，&nbsp;通过模型分析，可以自动控制温室湿帘风机、喷淋滴灌、内外遮阳、顶窗侧窗、加温补光等设备。同时，系统还可以通过手机、PDA、计算机等信息终端向管理者推送实时监测信息、报警信息，实现温室大棚信息化、智能化远程管理，充分发挥物联网技术在设施农业生产中的作用，保证温室大棚内环境适宜作物生长实现精细化的管理,为作物的高产、优质、高效、生态、安全创造条件，帮助客户提高效率、降低成本、增加收益。</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\'; font-weight: bold;\">1.系统功能&nbsp;</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\'; font-weight: bold;\">1.1&nbsp;温室环境实时监控</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(1)&nbsp;通过电脑或者手机远程查看温室的实时环境数据，包括空气温度、空气湿度、土壤温度、土壤湿度、光照度、二氧化碳浓度、氧气浓度等。远程实时查看温室视频监控视频，并可以保存录像文件，防止农作物被盗等状况出现。&nbsp;</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(2)&nbsp;温室环境报警记录及时提醒，用户可直接处理报警，系统记录处理信息，可以远程控制温室设备。&nbsp;</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(3)&nbsp;远程、自动化控制温室内环境设备，提高工作效率，如自动灌溉系统、风机、侧窗、顶窗等。&nbsp;</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(4)&nbsp;用户可以直观查看温室环境数据的实时曲线图，及时掌握温室农作物生长环境。</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\'; font-weight: bold;\">1.2&nbsp;智能报警系统</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(1)&nbsp;系统可以灵活的设置各个温室不同环境参数的上下阀值。一旦超出阀值，系统可以根据配置，通过手机短信、系统消息等方式提醒相应管理者。&nbsp;</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(2)&nbsp;报警提醒内容可根据模板灵活设置，根据不同客户需求可以设置不同的提醒内容，满足客户个性化需求。&nbsp;</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(3)&nbsp;可以根据报警记录查看关联的温室设备，更加及时、快速远程控制温室设备，高效处理温室环境问题。&nbsp;</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(4)&nbsp;可及时发现不正常状态设备，通过短信或系统消息及时提醒管理者，保证系统稳定运行。&nbsp;</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\'; font-weight: bold;\">1.3&nbsp;远程自动控制</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(1)&nbsp;系统通过先进的远程工业自动化控制技术，让用户足不出户远程控制温室设备。&nbsp;</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(2)&nbsp;可以自定义规则，让整个温室设备随环境参数变化&nbsp;自动控制，比如当土壤湿度过低时，温室灌溉系统自动开始浇水。&nbsp;</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(3)&nbsp;提供手机客户端，客户可以通过手机在任意地点远程控制温室的所有设备。</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\'; font-weight: bold;\">1.4&nbsp;历史数据分析</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;(</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">1)&nbsp;系统可以通过不同条件组合查询和对比历史环境数据。&nbsp;</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(2)&nbsp;支持列表和图表两种不同方式查看，用户可以更直观看到历史数据曲线。&nbsp;</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(3)&nbsp;与农业生产数据建立统一的数据模型，系统通过数据挖掘等技术可以分析更适合农作物生长、能提高农作物产量的环境参数，辅助决策。&nbsp;</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\'; font-weight: bold;\">1.5&nbsp;视频监控</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(1)&nbsp;视频采集。</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(2)&nbsp;视频存储。</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(3)&nbsp;视频检索及播放。</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\'; font-weight: bold;\">2</span><span style=\"font-size: 10.5pt; font-family: \'宋体\'; font-weight: bold;\">.系统特点及优势&nbsp;</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(1)&nbsp;软件系统基于成熟的物联网通用平台,&nbsp;系统运行稳定可靠,&nbsp;性能优异。采用云计算SAAS模式,&nbsp;客户无需架设专门的服务器和网络系统,&nbsp;节省投资,&nbsp;软件系统部署和维护简便。</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(2)&nbsp;集成的视频监控功能,&nbsp;视频与环境监控无缝集成,&nbsp;实现真正意义的可视化监控管理.。</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(3)&nbsp;集成度高,&nbsp;前端监控设备选用自主研发的一体化温室智能监控终端,&nbsp;集成度高,&nbsp;无需专门安装,&nbsp;插电即用,&nbsp;后期维护简便。</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(4)&nbsp;硬件可靠性高,&nbsp;IP66以上的前端设备防护等级,&nbsp;防尘、防水,&nbsp;可以安装在户外,适应阴雨潮湿等恶劣环境。</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(5)&nbsp;软件系统功能完善,&nbsp;专门为农业客户定制的系统界面,&nbsp;界面友好,&nbsp;操作便捷。&nbsp;集成的视频监控功能,实现便捷的远程可视化管理。</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(6)&nbsp;兼容性好,&nbsp;通过配置,&nbsp;可以连接客户已有的不同厂商品牌的采集和控制设备。灵活的配置功能,&nbsp;可以满足不同客户的个性化需求。</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(7)&nbsp;支持计算机、手机、Pad等多种终端访问,&nbsp;提供智能手机客户端软件,&nbsp;可以随时随地监控温室环境。集成微信、手机短信等多种展现和交互方式,&nbsp;信息传递及时、便捷。</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(8)&nbsp;配套的农业知识库、农产品溯源系统等附加功能。</span></p>\n<p><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size: 10.5pt; font-family: \'宋体\';\">(9)&nbsp;系统扩展性好,&nbsp;支持二次开发,&nbsp;可以与客户现有其它信息系统深度集成</span></p>', 1614302450964, 0, '推荐', 0, 'http://121.199.29.84:8888/group1/M00/00/1C/rBD-SWA4TPKARnCBAAIJjYyyPlI964.jpg', NULL, 25, 3, NULL);
INSERT INTO `cms_article` VALUES (84, '智慧农机管理系统', '<p><span style=\"font-size: 14px;\"><strong>一、智慧农机管理系统概述：</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 智慧农机管理系统构筑以农机综合信息化服务网络和农机综合监管网络两大服务网络，实现内部办公与业务自动化，建立农机监理、农机管理、农机推广和农机化服务等农机业务管理信息系统，使农机业务管理和社会服务完成有效融合。</span></p>\n<p align=\"center\"><img src=\"http://www.tpwlw.com/Public/Admin/js/kindeditor-4.1/attached/image/20170627/20170627132235_28551.jpg\" alt=\"\" /></p>\n<p align=\"center\">&nbsp;</p>\n<p><span style=\"font-size: 14px;\"><strong>二、智慧农机管理系统组成：</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>1、农机信息管理系统（数字农机平台）</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 包括对农田、农机生产企业、农机专业合作社、维修服务站、农机大户、培训机构等的数据采集及存储、管理、查询分析，实现政府部门对农机组织的快速管理。</span></p>\n<p align=\"center\"><img src=\"http://www.tpwlw.com/Public/Admin/js/kindeditor-4.1/attached/image/20170627/20170627132303_78519.jpg\" alt=\"\" /></p>\n<p><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>2、农机办证系统</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 负责农机登记及驾驶证业务的管理工作，建立统一的农机监理业务计算机管理系统；依法确定注册登记农机的机型；负责考试员、检验员的考核、发证工作等。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&nbsp; </strong><strong>3、农机作业系统（农机业务管理、农机监理）</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 农机作业系统建立在农机卫星定位基础上，系统首先要在作业机械上安装卫星定位监测终端设备，通过信息平台显示出农机作业的各种作业信息，然后将这些信息进行相关的监测、统计和管理。</span></p>\n<p align=\"center\"><img src=\"http://www.tpwlw.com/Public/Admin/js/kindeditor-4.1/attached/image/20170627/20170627132954_30747.jpg\" alt=\"\" /></p>\n<p align=\"center\">&nbsp;</p>\n<p align=\"center\"><img src=\"http://www.tpwlw.com/Public/Admin/js/kindeditor-4.1/attached/image/20170627/20170627132402_99185.jpg\" alt=\"\" /></p>\n<p align=\"center\">&nbsp;</p>\n<p align=\"center\"><img src=\"http://www.tpwlw.com/Public/Admin/js/kindeditor-4.1/attached/image/20170627/20170627133011_13535.jpg\" alt=\"\" /></p>\n<p align=\"left\"><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>4、农机补贴管理系统</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 农机补贴管理系统利用先进的网络系统和软件体系架构对农机购置补贴工作进行科学管理，针对不同的管理要求，提供合适的信息，把管理系统的各部分信息融合成一个有机的信息整体，为农机购置补贴管理的各层次提供有力的决策信息,从而更好的提高省农机办的管理水平，为广大农机企业和农户服务。</span><br /><span style=\"font-size: 14px;\"><strong>二、智慧农机管理系统解决目标:</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1、实现对所辖区域农机状况的综合管理；</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2、规范农机安全监理人员管理；</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3、实现对农机行业经营场所开展农用拖拉机无牌无证、超载等排查工作；</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4、为农机购置补贴管理的各层次提供有力的决策信息,从而更好的提高省农机办的管理水平。</span><br /><span style=\"font-size: 14px;\"><strong>三、适用对象：</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 用户群面向于农民、农机服务组织和政府三大类</span></p>', 1614302564567, 0, '审核通过', 0, 'http://121.199.29.84:8888/group1/M00/00/1C/rBD-SWA4TWCAf437AAMlhcsHM4s002.jpg', NULL, 25, 4, NULL);
INSERT INTO `cms_article` VALUES (85, '智慧农机管理系统', '<p><span style=\"font-size: 14px;\"><strong>一、智慧农机管理系统概述：</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 智慧农机管理系统构筑以农机综合信息化服务网络和农机综合监管网络两大服务网络，实现内部办公与业务自动化，建立农机监理、农机管理、农机推广和农机化服务等农机业务管理信息系统，使农机业务管理和社会服务完成有效融合。</span></p>\n<p align=\"center\"><img src=\"http://www.tpwlw.com/Public/Admin/js/kindeditor-4.1/attached/image/20170627/20170627132235_28551.jpg\" alt=\"\" /></p>\n<p align=\"center\">&nbsp;</p>\n<p><span style=\"font-size: 14px;\"><strong>二、智慧农机管理系统组成：</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>1、农机信息管理系统（数字农机平台）</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 包括对农田、农机生产企业、农机专业合作社、维修服务站、农机大户、培训机构等的数据采集及存储、管理、查询分析，实现政府部门对农机组织的快速管理。</span></p>\n<p align=\"center\"><img src=\"http://www.tpwlw.com/Public/Admin/js/kindeditor-4.1/attached/image/20170627/20170627132303_78519.jpg\" alt=\"\" /></p>\n<p><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>2、农机办证系统</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 负责农机登记及驾驶证业务的管理工作，建立统一的农机监理业务计算机管理系统；依法确定注册登记农机的机型；负责考试员、检验员的考核、发证工作等。</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&nbsp; </strong><strong>3、农机作业系统（农机业务管理、农机监理）</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 农机作业系统建立在农机卫星定位基础上，系统首先要在作业机械上安装卫星定位监测终端设备，通过信息平台显示出农机作业的各种作业信息，然后将这些信息进行相关的监测、统计和管理。</span></p>\n<p align=\"center\"><img src=\"http://www.tpwlw.com/Public/Admin/js/kindeditor-4.1/attached/image/20170627/20170627132954_30747.jpg\" alt=\"\" /></p>\n<p align=\"center\">&nbsp;</p>\n<p align=\"center\"><img src=\"http://www.tpwlw.com/Public/Admin/js/kindeditor-4.1/attached/image/20170627/20170627132402_99185.jpg\" alt=\"\" /></p>\n<p align=\"center\">&nbsp;</p>\n<p align=\"center\"><img src=\"http://www.tpwlw.com/Public/Admin/js/kindeditor-4.1/attached/image/20170627/20170627133011_13535.jpg\" alt=\"\" /></p>\n<p align=\"left\"><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>4、农机补贴管理系统</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 农机补贴管理系统利用先进的网络系统和软件体系架构对农机购置补贴工作进行科学管理，针对不同的管理要求，提供合适的信息，把管理系统的各部分信息融合成一个有机的信息整体，为农机购置补贴管理的各层次提供有力的决策信息,从而更好的提高省农机办的管理水平，为广大农机企业和农户服务。</span><br /><span style=\"font-size: 14px;\"><strong>二、智慧农机管理系统解决目标:</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1、实现对所辖区域农机状况的综合管理；</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2、规范农机安全监理人员管理；</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3、实现对农机行业经营场所开展农用拖拉机无牌无证、超载等排查工作；</span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4、为农机购置补贴管理的各层次提供有力的决策信息,从而更好的提高省农机办的管理水平。</span><br /><span style=\"font-size: 14px;\"><strong>三、适用对象：</strong></span><br /><span style=\"font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 用户群面向于农民、农机服务组织和政府三大类</span></p>', 1614302564828, 0, '审核通过', 0, 'http://121.199.29.84:8888/group1/M00/00/1C/rBD-SWA4TWCAf437AAMlhcsHM4s002.jpg', NULL, 25, 4, NULL);
INSERT INTO `cms_article` VALUES (87, 'test', '<p>testdvxd</p>', 1622021870837, 0, '审核通过', 0, 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCuFuWABTrOAAx1nBeU9jo165.png', NULL, 91, 8, NULL);
INSERT INTO `cms_article` VALUES (92, '詹姆斯---', 'sss', 1623120693537, 0, '推荐', 0, '', NULL, NULL, 10, NULL);

-- ----------------------------
-- Table structure for cms_carousel
-- ----------------------------
DROP TABLE IF EXISTS `cms_carousel`;
CREATE TABLE `cms_carousel`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `introduce` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cms_carousel
-- ----------------------------
INSERT INTO `cms_carousel` VALUES (1, 'one', 'this is a one', 'http://121.199.29.84:8888/group1/M00/00/1F/rBD-SWCZ7nGAMbPCAAC5FpuPWHk805.jpg', '停用');
INSERT INTO `cms_carousel` VALUES (2, 'two', 'this is a two', 'http://121.199.29.84:8888/group1/M00/00/1F/rBD-SWCZ7qWAGqiiAACxnzXuYrE503.jpg', '正常');
INSERT INTO `cms_carousel` VALUES (3, 'three', 'this is a three', 'http://121.199.29.84:8888/group1/M00/00/1F/rBD-SWCZ7rOAFi90AABfUavzXjo614.jpg', '正常');
INSERT INTO `cms_carousel` VALUES (5, 'five', 'test', 'http://121.199.29.84:8888/group1/M00/00/1F/rBD-SWCbgbGAStplAAG_cvBJFec47.jpeg', '正常');
INSERT INTO `cms_carousel` VALUES (6, 'test', 'test', 'http://121.199.29.84:8888/group1/M00/00/1F/rBD-SWCbg5WADlujAAGIQYWuAWY41.jpeg', '停用');
INSERT INTO `cms_carousel` VALUES (7, '222', NULL, 'http://121.199.29.84:8888/group1/M00/00/9E/rBD-SWGq3mmAdVLUAAGz0tJ-Wz0961.jpg', '正常');

-- ----------------------------
-- Table structure for cms_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_category`;
CREATE TABLE `cms_category`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `no` int(11) NULL DEFAULT NULL,
  `parent_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_category_category`(`parent_id`) USING BTREE,
  CONSTRAINT `fk_category_category` FOREIGN KEY (`parent_id`) REFERENCES `cms_category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cms_category
-- ----------------------------
INSERT INTO `cms_category` VALUES (3, '家政百科', '...', 20, NULL);
INSERT INTO `cms_category` VALUES (4, '家政指南', '', 2, NULL);
INSERT INTO `cms_category` VALUES (5, '家政动态', '', 3, NULL);
INSERT INTO `cms_category` VALUES (6, '政策解读', '', 4, NULL);
INSERT INTO `cms_category` VALUES (7, '优秀职工', '', 5, NULL);
INSERT INTO `cms_category` VALUES (8, 'test3', '', NULL, NULL);
INSERT INTO `cms_category` VALUES (10, '体育', '体育健儿的新闻资讯', 4, NULL);
INSERT INTO `cms_category` VALUES (11, '足球', '足球健儿', 1, 10);

-- ----------------------------
-- Table structure for cms_comment
-- ----------------------------
DROP TABLE IF EXISTS `cms_comment`;
CREATE TABLE `cms_comment`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `comment_time` bigint(20) NULL DEFAULT NULL,
  `reply` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `reply_time` bigint(20) NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` bigint(20) NULL DEFAULT NULL,
  `article_id` bigint(20) NULL DEFAULT NULL,
  `parent_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_comment_user`(`user_id`) USING BTREE,
  INDEX `fk_comment_article`(`article_id`) USING BTREE,
  INDEX `fk_comment_comment`(`parent_id`) USING BTREE,
  CONSTRAINT `fk_comment_article` FOREIGN KEY (`article_id`) REFERENCES `cms_article` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `fk_comment_comment` FOREIGN KEY (`parent_id`) REFERENCES `cms_comment` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `fk_comment_user` FOREIGN KEY (`user_id`) REFERENCES `base_user` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cms_comment
-- ----------------------------
INSERT INTO `cms_comment` VALUES (1, '资讯写的真棒', 1590150600197, NULL, NULL, '通过', 26, 68, NULL);
INSERT INTO `cms_comment` VALUES (2, '资讯内容还需修改', 1590150600197, NULL, NULL, '未审核', 86, 74, NULL);
INSERT INTO `cms_comment` VALUES (3, NULL, 1625579229702, NULL, NULL, '未审核', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for jz_account_apply
-- ----------------------------
DROP TABLE IF EXISTS `jz_account_apply`;
CREATE TABLE `jz_account_apply`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `money` double NULL DEFAULT NULL COMMENT '申请金额',
  `apply_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '申请类型（提现、充值）',
  `apply_time` bigint(20) NULL DEFAULT NULL COMMENT '申请时间',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户编号',
  `reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '不通过理由',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `jz_account_apply_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `base_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jz_account_apply
-- ----------------------------
INSERT INTO `jz_account_apply` VALUES (1, 100, '提现', 1621393374398, '通过', 25, NULL);
INSERT INTO `jz_account_apply` VALUES (2, 10, '提现', 1621393374398, '不通过', 25, NULL);
INSERT INTO `jz_account_apply` VALUES (3, 1, '充值', 1621393374398, '不通过', 25, NULL);
INSERT INTO `jz_account_apply` VALUES (4, 0.1, '提现', 1621393374398, '通过', 25, NULL);
INSERT INTO `jz_account_apply` VALUES (5, 100, '提现', 1622096653183, '未审核', 25, NULL);
INSERT INTO `jz_account_apply` VALUES (6, 10, '提现', 1622096862926, '未审核', 25, NULL);
INSERT INTO `jz_account_apply` VALUES (7, 1, '提现', 1622096926837, '未审核', 25, NULL);
INSERT INTO `jz_account_apply` VALUES (8, 12, '提现', 1622098904450, '未审核', 25, NULL);

-- ----------------------------
-- Table structure for jz_account_customer
-- ----------------------------
DROP TABLE IF EXISTS `jz_account_customer`;
CREATE TABLE `jz_account_customer`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `transfer_money` double NULL DEFAULT NULL COMMENT '交易金额',
  `transfer_time` bigint(20) NULL DEFAULT NULL COMMENT '交易时间',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '交易类型[充值、消费]',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态[正常]',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户编号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_user_ac`(`user_id`) USING BTREE,
  CONSTRAINT `fk_user_ac` FOREIGN KEY (`user_id`) REFERENCES `base_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jz_account_customer
-- ----------------------------
INSERT INTO `jz_account_customer` VALUES (8, 1000, 1620944916221, NULL, '充值', '正常', 84);
INSERT INTO `jz_account_customer` VALUES (9, 362, 1620944949875, NULL, '消费', '正常', 84);
INSERT INTO `jz_account_customer` VALUES (10, 1000, 1620945276487, NULL, '充值', '正常', 114);
INSERT INTO `jz_account_customer` VALUES (11, 392, 1620945287503, NULL, '消费', '正常', 114);
INSERT INTO `jz_account_customer` VALUES (12, 243, 1621239656834, NULL, '消费', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (13, 1000, 1621239709875, NULL, '充值', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (14, 243, 1621239796138, NULL, '消费', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (15, 120, 1621240299872, NULL, '消费', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (16, 120, 1621240341502, NULL, '消费', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (17, 120, 1621240385408, NULL, '消费', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (18, 1000, 1621240464708, NULL, '充值', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (19, 1000, 1621240466476, NULL, '充值', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (20, 1000, 1621240467122, NULL, '充值', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (21, 1000, 1621240467721, NULL, '充值', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (22, 1000, 1621240468321, NULL, '充值', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (23, 243, 1621240769193, NULL, '消费', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (24, 0, 1621241580495, NULL, '消费', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (25, 120, 1621241586296, NULL, '消费', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (26, 0, 1621387206074, NULL, '消费', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (27, 10, 1621387222190, NULL, '充值', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (28, 1, 1621387292702, NULL, '充值', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (29, 100, 1621387430208, NULL, '充值', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (30, 111, 1621387446447, NULL, '充值', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (31, 100, 1621387486657, NULL, '充值', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (32, 100, 1621387739935, NULL, '充值', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (33, 100, 1621387752738, NULL, '充值', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (34, 100, 1621387785900, NULL, '充值', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (35, 120, 1622087856944, NULL, '消费', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (36, 240, 1622114840009, NULL, '消费', '正常', 25);
INSERT INTO `jz_account_customer` VALUES (37, 240, 1622169749072, NULL, '消费', '正常', 25);

-- ----------------------------
-- Table structure for jz_account_employee
-- ----------------------------
DROP TABLE IF EXISTS `jz_account_employee`;
CREATE TABLE `jz_account_employee`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `transfer_money` double NULL DEFAULT NULL COMMENT '交易金额',
  `transfer_time` bigint(20) NULL DEFAULT NULL COMMENT '交易账户',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '交易类型[收益、提现]',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态[正常、未审核]',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户编号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_ue_id`(`user_id`) USING BTREE,
  CONSTRAINT `fk_ue_id` FOREIGN KEY (`user_id`) REFERENCES `base_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jz_account_employee
-- ----------------------------
INSERT INTO `jz_account_employee` VALUES (1, 50, 1620896272392, '申请提现', '提现', '正常', 81);
INSERT INTO `jz_account_employee` VALUES (2, 100, 1620896406894, '收益', '收益', '正常', 82);
INSERT INTO `jz_account_employee` VALUES (3, 60, 1620896455596, '提现', '提现', '正常', 87);
INSERT INTO `jz_account_employee` VALUES (4, 100, 1620896683735, '收益', '收益', '正常', 81);
INSERT INTO `jz_account_employee` VALUES (5, 20, 1620896747954, '提现', '提现', '正常', 82);
INSERT INTO `jz_account_employee` VALUES (8, 289.6, 1621173533343, NULL, '收益', '正常', 25);
INSERT INTO `jz_account_employee` VALUES (9, 100, 1621393426623, NULL, '提现', '正常', 25);
INSERT INTO `jz_account_employee` VALUES (10, 0.1, 1622006872965, NULL, '提现', '正常', 25);

-- ----------------------------
-- Table structure for jz_account_system
-- ----------------------------
DROP TABLE IF EXISTS `jz_account_system`;
CREATE TABLE `jz_account_system`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `transfer_money` double NULL DEFAULT NULL COMMENT '交易金额',
  `transfer_time` bigint(20) NULL DEFAULT NULL COMMENT '交易时间',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型[充值、提现、收益]',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态[正常、未审核]',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jz_account_system
-- ----------------------------
INSERT INTO `jz_account_system` VALUES (1, 400, 1620897409398, '充值', '充值', '正常');
INSERT INTO `jz_account_system` VALUES (2, 30, 1620897450328, '收益', '收益', '正常');
INSERT INTO `jz_account_system` VALUES (3, 50, 1620897484336, '提现', '提现', '正常');
INSERT INTO `jz_account_system` VALUES (4, 80, 1620897513896, '提现', '提现', '正常');
INSERT INTO `jz_account_system` VALUES (5, 100, 1620897543497, '收益', '收益', '正常');
INSERT INTO `jz_account_system` VALUES (6, 72.4, 1621173534644, NULL, '收益', '正常');

-- ----------------------------
-- Table structure for jz_address
-- ----------------------------
DROP TABLE IF EXISTS `jz_address`;
CREATE TABLE `jz_address`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收件人',
  `telephone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `province` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省份',
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '城市',
  `area` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细地址',
  `is_default` int(2) NULL DEFAULT NULL COMMENT '是否默认，1为默认地址',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户编号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `jz_address_ibfk_1`(`user_id`) USING BTREE,
  CONSTRAINT `jz_address_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `base_user` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2301 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jz_address
-- ----------------------------
INSERT INTO `jz_address` VALUES (2295, '高大山', '17898766678', '山西省', '运城市', '闻喜县', '公园壹号3期2单元901', NULL, 84);
INSERT INTO `jz_address` VALUES (2296, '马宏宇', '18896827921', '山西省', '临汾市', '侯马市', '幸福里2栋201', NULL, 84);
INSERT INTO `jz_address` VALUES (2297, '张三', '11111111111', '江苏', '苏州', '昆山', '巴城镇学院路浦东软件园', 0, NULL);
INSERT INTO `jz_address` VALUES (2298, '张二二', '13000000000', '江苏省', '苏州市', '昆山市', '巴城镇浦东软件园', 0, NULL);
INSERT INTO `jz_address` VALUES (2299, '张尔尔', '19000000000', '内蒙古自治区', '赤峰市', '元宝山区', '购物广场', 0, NULL);
INSERT INTO `jz_address` VALUES (2300, '詹姆斯', '11111112222', '上海市', '上海市', '静安区', '静安区', 0, NULL);

-- ----------------------------
-- Table structure for jz_certification_apply
-- ----------------------------
DROP TABLE IF EXISTS `jz_certification_apply`;
CREATE TABLE `jz_certification_apply`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `realname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `id_card` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `idcard_photo_positive` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '身份证正面',
  `idcard_photo_negative` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '身份证反面',
  `bank_card` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '银行卡号',
  `bank_card_photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '银行卡正面',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态',
  `apply_time` bigint(20) NULL DEFAULT NULL COMMENT '申请时间',
  `reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '审核不通过理由',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户编号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `jz_certification_apply_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `base_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jz_certification_apply
-- ----------------------------
INSERT INTO `jz_certification_apply` VALUES (1, '托马斯', '63890137401387480134', 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCiMXmAOTXBAAEYdVeZwgs142.jpg', 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCiMX2Acg3UAALhPXMPnyw967.jpg', '13984019734031413', 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCiMYKAGQK9AAXEWQYzwG4499.jpg', '未审核', 1621242244627, NULL, 25);
INSERT INTO `jz_certification_apply` VALUES (2, '小点点', '298359723904762937', 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCiMfCALZ66AAEYdVeZwgs752.jpg', 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCiMfSARGh8AALhPXMPnyw636.jpg', '23874528374592372345', 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCiMfqAQDDsAAXEWQYzwG4834.jpg', '通过', 1621242362697, NULL, 25);
INSERT INTO `jz_certification_apply` VALUES (3, NULL, NULL, 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCiM8OAHYA_AAEYdVeZwgs344.jpg', NULL, NULL, 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCiM86AKjJRAAC_TgKpeIw508.jpg', '通过', 1621242835065, NULL, 25);
INSERT INTO `jz_certification_apply` VALUES (4, NULL, NULL, 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCiM_KAc0PDAAWlptmbBi0434.jpg', 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCiM_2AajLvAAkN2n_rFy0149.jpg', NULL, 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCiM86AKjJRAAC_TgKpeIw508.jpg', '未审核', 1621242881829, NULL, 25);
INSERT INTO `jz_certification_apply` VALUES (5, NULL, NULL, 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCiM_KAc0PDAAWlptmbBi0434.jpg', 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCiM_2AajLvAAkN2n_rFy0149.jpg', NULL, 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCiNAKAPtjFAAL5l8OGicA109.jpg', '不通过', 1621243140717, NULL, 25);
INSERT INTO `jz_certification_apply` VALUES (6, NULL, NULL, NULL, NULL, NULL, NULL, '不通过', 1621243145174, NULL, 25);
INSERT INTO `jz_certification_apply` VALUES (7, '赵大伟', '621224197808091243', 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCkfTiAO9rcAAB5bVXcH9k472.jpg', 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCkfT6AfO8AAABlzknMpo4712.jpg', '58901929293004040404', 'http://121.199.29.84:8888/group1/M00/00/21/rBD-SWCkfWSAV4OMAAB730LVt6w266.jpg', '通过', 1621392743729, NULL, 25);

-- ----------------------------
-- Table structure for jz_order
-- ----------------------------
DROP TABLE IF EXISTS `jz_order`;
CREATE TABLE `jz_order`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `order_time` bigint(20) NULL DEFAULT NULL COMMENT '订单时间',
  `service_time` bigint(20) NULL DEFAULT NULL COMMENT '服务时间',
  `total` double NULL DEFAULT NULL COMMENT '费用',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标注',
  `customer_id` bigint(20) NULL DEFAULT NULL COMMENT '顾客编号',
  `address_id` bigint(20) NULL DEFAULT NULL COMMENT '地址编号',
  `employee_id` bigint(20) NULL DEFAULT NULL COMMENT '员工编号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `customer_id`(`customer_id`) USING BTREE,
  INDEX `address_id`(`address_id`) USING BTREE,
  CONSTRAINT `jz_order_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `base_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `jz_order_ibfk_3` FOREIGN KEY (`address_id`) REFERENCES `jz_address` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1049 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jz_order
-- ----------------------------
INSERT INTO `jz_order` VALUES (1034, 1620944949654, NULL, 362, '已完成', NULL, 84, 2295, 25);
INSERT INTO `jz_order` VALUES (1035, 1620945287330, NULL, 392, '待接单', NULL, 114, 2296, 26);
INSERT INTO `jz_order` VALUES (1037, 1621239656688, NULL, 243, '待接单', NULL, 25, 2296, 81);
INSERT INTO `jz_order` VALUES (1038, 1621239796101, NULL, 243, '待派单', NULL, 25, 2296, NULL);
INSERT INTO `jz_order` VALUES (1039, 1621240299827, NULL, 120, '待派单', NULL, 25, 2296, NULL);
INSERT INTO `jz_order` VALUES (1040, 1621240341474, NULL, 120, '待接单', NULL, 25, 2296, 123);
INSERT INTO `jz_order` VALUES (1041, 1621240385406, NULL, 120, '待接单', NULL, 25, 2296, 87);
INSERT INTO `jz_order` VALUES (1042, 1621240769178, 1621240767158, 243, '待派单', NULL, 25, 2296, NULL);
INSERT INTO `jz_order` VALUES (1043, 1621241580492, 1621241569504, 0, '待服务', NULL, 25, 2296, 25);
INSERT INTO `jz_order` VALUES (1044, 1621241586295, 1621241584735, 120, '待接单', NULL, 25, 2296, 90);
INSERT INTO `jz_order` VALUES (1045, 1621387205875, 1621387199477, 0, '待派单', NULL, 25, 2296, NULL);
INSERT INTO `jz_order` VALUES (1046, 1622087856926, 1622087855422, 120, '待服务', NULL, 25, 2300, 25);
INSERT INTO `jz_order` VALUES (1047, 1622114840007, 1622114835331, 240, '待派单', NULL, 25, 2300, NULL);
INSERT INTO `jz_order` VALUES (1048, 1622169748889, 1622169747234, 240, '待接单', NULL, 25, 2300, 123);

-- ----------------------------
-- Table structure for jz_order_comment
-- ----------------------------
DROP TABLE IF EXISTS `jz_order_comment`;
CREATE TABLE `jz_order_comment`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评论内容',
  `comment_time` bigint(20) NULL DEFAULT NULL COMMENT '评论时间',
  `status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  `order_id` bigint(20) NULL DEFAULT NULL COMMENT '订单编号',
  `customer_id` bigint(20) NULL DEFAULT NULL COMMENT '顾客编号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `order_id`(`order_id`) USING BTREE,
  INDEX `ej_cus_com`(`customer_id`) USING BTREE,
  CONSTRAINT `ej_cus_com` FOREIGN KEY (`customer_id`) REFERENCES `base_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `jz_order_comment_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `jz_order` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 502377 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jz_order_comment
-- ----------------------------
INSERT INTO `jz_order_comment` VALUES (502375, '服务很好，清理的很干净', 1590150600197, '不通过', 1034, 84);
INSERT INTO `jz_order_comment` VALUES (502376, '服务不错', 1590150600198, '通过', 1034, 85);

-- ----------------------------
-- Table structure for jz_order_line
-- ----------------------------
DROP TABLE IF EXISTS `jz_order_line`;
CREATE TABLE `jz_order_line`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `number` int(11) NULL DEFAULT NULL COMMENT '数量',
  `price` double(10, 2) NULL DEFAULT NULL COMMENT '单价',
  `product_id` bigint(20) NULL DEFAULT NULL COMMENT '产品编号',
  `order_id` bigint(20) NULL DEFAULT NULL COMMENT '订单编号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `product_id`(`product_id`) USING BTREE,
  INDEX `order_id`(`order_id`) USING BTREE,
  CONSTRAINT `jz_order_line_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `jz_product` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT,
  CONSTRAINT `jz_order_line_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `jz_order` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10000000461 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jz_order_line
-- ----------------------------
INSERT INTO `jz_order_line` VALUES (10000000443, 1, 20.00, 9127, 1034);
INSERT INTO `jz_order_line` VALUES (10000000444, 1, 342.00, 9128, 1034);
INSERT INTO `jz_order_line` VALUES (10000000445, 1, 50.00, 9138, 1035);
INSERT INTO `jz_order_line` VALUES (10000000446, 1, 342.00, 9128, 1035);
INSERT INTO `jz_order_line` VALUES (10000000447, 1, 120.00, 9130, 1037);
INSERT INTO `jz_order_line` VALUES (10000000448, 1, 123.00, 9129, 1037);
INSERT INTO `jz_order_line` VALUES (10000000449, 1, 120.00, 9130, 1038);
INSERT INTO `jz_order_line` VALUES (10000000450, 1, 123.00, 9129, 1038);
INSERT INTO `jz_order_line` VALUES (10000000451, 1, 120.00, 9130, 1039);
INSERT INTO `jz_order_line` VALUES (10000000452, 0, 123.00, 9129, 1039);
INSERT INTO `jz_order_line` VALUES (10000000453, 1, 120.00, 9130, 1040);
INSERT INTO `jz_order_line` VALUES (10000000454, 1, 120.00, 9130, 1041);
INSERT INTO `jz_order_line` VALUES (10000000455, 1, 120.00, 9130, 1042);
INSERT INTO `jz_order_line` VALUES (10000000456, 1, 123.00, 9129, 1042);
INSERT INTO `jz_order_line` VALUES (10000000457, 1, 120.00, 9130, 1044);
INSERT INTO `jz_order_line` VALUES (10000000458, 1, 120.00, 9130, 1046);
INSERT INTO `jz_order_line` VALUES (10000000459, 2, 120.00, 9130, 1047);
INSERT INTO `jz_order_line` VALUES (10000000460, 2, 120.00, 9130, 1048);

-- ----------------------------
-- Table structure for jz_platform
-- ----------------------------
DROP TABLE IF EXISTS `jz_platform`;
CREATE TABLE `jz_platform`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `balance` double NULL DEFAULT NULL COMMENT '账户余额',
  `bank_card` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '银行卡号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jz_platform
-- ----------------------------
INSERT INTO `jz_platform` VALUES (1, -72.4, '62222020502014241488');

-- ----------------------------
-- Table structure for jz_product
-- ----------------------------
DROP TABLE IF EXISTS `jz_product`;
CREATE TABLE `jz_product`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品介绍',
  `price` double NULL DEFAULT NULL COMMENT '单价',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `product_category_id` bigint(20) NULL DEFAULT NULL COMMENT '分类编号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `category_id`(`product_category_id`) USING BTREE,
  CONSTRAINT `jz_product_ibfk_1` FOREIGN KEY (`product_category_id`) REFERENCES `jz_product_category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9142 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jz_product
-- ----------------------------
INSERT INTO `jz_product` VALUES (9127, '玻璃洗护', '清洁羽绒服', 20, '正常', 'http://121.199.29.84:8888/group1/M00/00/20/rBD-SWCbkEqAS2h6AABOvwgZvRA393.png', 9442);
INSERT INTO `jz_product` VALUES (9128, '楼梯清洗', '123', 342, '正常', 'http://121.199.29.84:8888/group1/M00/00/20/rBD-SWCbkDeABAwWAABBbiB_WEc228.png', 9442);
INSERT INTO `jz_product` VALUES (9129, '墙面护理', '测试数据', 123, '正常', 'http://121.199.29.84:8888/group1/M00/00/20/rBD-SWCbkCKABRQsAAHxf3WD3YQ552.png', 9442);
INSERT INTO `jz_product` VALUES (9130, '地板护理', '各类材质地板打光清洗', 120, '下架', 'http://121.199.29.84:8888/group1/M00/00/20/rBD-SWCbj6qAaXKYAABjV1cIs3Y798.png', 9442);
INSERT INTO `jz_product` VALUES (9131, '羽绒服', '测试数据', 80, '正常', 'http://121.199.29.84:8888/group1/M00/00/20/rBD-SWCbhauAfquSAAYAWGAa0Ms616.png', 9441);
INSERT INTO `jz_product` VALUES (9132, '月嫂', '测试数据', 1000, '正常', 'http://121.199.29.84:8888/group1/M00/00/20/rBD-SWCbhZiAYHXJAABpCc962kc596.png', 9440);
INSERT INTO `jz_product` VALUES (9133, '沙发养护', '测试数据', 90, '正常', 'http://121.199.29.84:8888/group1/M00/00/20/rBD-SWCbhYqAIkjDAACZFCUIlYk393.png', 9439);
INSERT INTO `jz_product` VALUES (9134, '油烟机', '测试数据', 200, '正常', 'http://121.199.29.84:8888/group1/M00/00/20/rBD-SWCbjMCAeFs2AACFbzsoYgQ471.png', 9438);
INSERT INTO `jz_product` VALUES (9135, '草坪修剪', '测试数据', 600, '正常', 'http://121.199.29.84:8888/group1/M00/00/20/rBD-SWCbhWuAdqPWAAC99TQhh68866.png', 9437);
INSERT INTO `jz_product` VALUES (9136, '长款羊毛大衣', '测试数据', 130, '正常', 'http://121.199.29.84:8888/group1/M00/00/20/rBD-SWCbi3CAbIiuAAA1Dw-y7cE393.png', 9441);
INSERT INTO `jz_product` VALUES (9137, '老人陪护', '测试数据', 8000, '正常', 'http://121.199.29.84:8888/group1/M00/00/20/rBD-SWCbixWAFnd1AADAqkX4NXs534.png', 9440);
INSERT INTO `jz_product` VALUES (9138, '床铺除螨', '测试数据', 50, '正常', 'http://121.199.29.84:8888/group1/M00/00/1F/rBD-SWCbhVWAcp7EAAA1effBLJc684.jpg', 9439);
INSERT INTO `jz_product` VALUES (9139, '冰箱清洗', '测试数据', 50, '正常', 'http://121.199.29.84:8888/group1/M00/00/20/rBD-SWCbhkiAM2-KAAA5iZO3iOU086.png', 9438);
INSERT INTO `jz_product` VALUES (9140, '盆景修剪', '测试描述', 401, '正常', 'http://121.199.29.84:8888/group1/M00/00/1F/rBD-SWCbhTmAdXO0AACeZkYxpNM412.png', 9437);
INSERT INTO `jz_product` VALUES (9141, '盆景摆放', '测试数据', 12, '正常', 'http://121.199.29.84:8888/group1/M00/00/20/rBD-SWCbjkmAaEBiAAG_cvBJFec12.jpeg', 9437);

-- ----------------------------
-- Table structure for jz_product_category
-- ----------------------------
DROP TABLE IF EXISTS `jz_product_category`;
CREATE TABLE `jz_product_category`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `num` int(11) NULL DEFAULT NULL COMMENT '序号',
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父编号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  CONSTRAINT `jz_product_category_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `jz_product_category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9443 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jz_product_category
-- ----------------------------
INSERT INTO `jz_product_category` VALUES (9437, '环境整治', 'http://121.199.29.84:8888/group1/M00/00/1F/rBD-SWCbMCaAQ0WhAAAA_v3Vd-g492.png', 100, NULL);
INSERT INTO `jz_product_category` VALUES (9438, '家电清洗', 'http://121.199.29.84:8888/group1/M00/00/1F/rBD-SWCbMDmADmlgAAADoPvefAU439.png', 100, NULL);
INSERT INTO `jz_product_category` VALUES (9439, '家居养护', 'http://121.199.29.84:8888/group1/M00/00/1F/rBD-SWCbMEqAD3zpAAAC5-OahT0538.png', 100, NULL);
INSERT INTO `jz_product_category` VALUES (9440, '母婴陪护', 'http://121.199.29.84:8888/group1/M00/00/1F/rBD-SWCbMFeABgDRAAAE8fNXQk0386.png', 100, NULL);
INSERT INTO `jz_product_category` VALUES (9441, '衣物护理', 'http://121.199.29.84:8888/group1/M00/00/1F/rBD-SWCbMGOAK2ATAAAD_g1h8DU792.png', 100, NULL);
INSERT INTO `jz_product_category` VALUES (9442, '家庭保洁', 'http://121.199.29.84:8888/group1/M00/00/1F/rBD-SWCbMG2ATb0nAAAEgPR0tR8029.png', 100, NULL);

SET FOREIGN_KEY_CHECKS = 1;
